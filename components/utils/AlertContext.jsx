/**
 * AGUA Copyright (C) 2023  Atix Labs
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/* eslint-disable react/no-multi-comp */
import React, { useContext, useEffect, useState } from 'react';
import PropTypes from 'prop-types';
import { useRouter } from 'next/router';
import { UserContext } from './UserContext';
import { ALERT_VARIANTS_ENUM } from 'model/alertVariants';

export const AlertContext = React.createContext();

export function AlertProvider({ children }) {
  const router = useRouter();
  const { preview, projectId } = router.query;

  const { user } = useContext(UserContext);
  const isAdmin = user?.isAdmin;

  const [alertVariant, setAlertVariant] = useState('');
  const [alertShowed, setAlertShowed] = useState(false);

  const closeAlert = () => {
    setAlertVariant('');
    setAlertShowed(false);
  };

  const setVariantAndShow = variant => {
    setAlertVariant(variant);
    setAlertShowed(true);
  };

  useEffect(() => {
    if (preview && JSON.parse(preview) && isAdmin) {
      setVariantAndShow(ALERT_VARIANTS_ENUM.PREVIEW);
    }
  }, [preview, isAdmin, projectId, router]);

  useEffect(() => {
    if (!preview && alertVariant === ALERT_VARIANTS_ENUM.PREVIEW) {
      closeAlert();
      setAlertVariant('');
    }
  }, [alertVariant, preview]);

  return (
    <AlertContext.Provider
      value={{
        alertShowed,
        alertVariant,
        setAlertVariant,
        setVariantAndShow,
        closeAlert
      }}
    >
      {children}
    </AlertContext.Provider>
  );
}

AlertProvider.propTypes = {
  children: PropTypes.node.isRequired
};
