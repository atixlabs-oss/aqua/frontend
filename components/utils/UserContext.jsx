/**
 * AGUA Copyright (C) 2023  Atix Labs
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/* eslint-disable react/no-multi-comp */
import React, { useState } from 'react';
import PropTypes from 'prop-types';

import { ACCESS_TOKEN_KEY, INACTIVE_USER_KEY, USER_KEY } from 'constants/constants';

export const UserContext = React.createContext();

export function UserProvider({ children }) {
  const [user, setUser] = useState(JSON.parse(sessionStorage.getItem(USER_KEY)));
  const isInactiveUser =
    sessionStorage.getItem(INACTIVE_USER_KEY) &&
    JSON.parse(sessionStorage.getItem(INACTIVE_USER_KEY));

  const changeUser = nuser => {
    const newUser = JSON.stringify({ ...nuser, seenModal: false });
    sessionStorage.setItem(USER_KEY, newUser);
    setUser(nuser);
  };

  const removeUser = () => {
    sessionStorage.removeItem(USER_KEY);
    sessionStorage.removeItem(ACCESS_TOKEN_KEY);
    setUser(null);
  };

  const removeUserInactive = () => {
    sessionStorage.setItem(INACTIVE_USER_KEY, true);
    removeUser();
  };

  const setSeenUserModal = () => {
    const internalUser = JSON.parse(sessionStorage.getItem(USER_KEY));
    sessionStorage.setItem(USER_KEY, JSON.stringify({ ...internalUser, seenModal: true }));
  };

  return (
    <UserContext.Provider
      value={{
        changeUser,
        removeUser,
        user,
        setSeenUserModal,
        removeUserInactive,
        isInactiveUser
      }}
    >
      {children}
    </UserContext.Provider>
  );
}

UserProvider.propTypes = {
  children: PropTypes.node.isRequired
};
