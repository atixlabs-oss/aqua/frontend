import React, { useContext, useRef } from 'react';
import { QueryClient, QueryClientProvider } from '@tanstack/react-query';
import { ReactQueryDevtools } from '@tanstack/react-query-devtools';
import PropTypes from 'prop-types';

import { UserContext } from './UserContext';

export const ReactQueryProvider = ({ children }) => {
  const { removeUserInactive, isInactiveUser } = useContext(UserContext);
  const queryClient = useRef(
    new QueryClient({
      defaultOptions: {
        queries: {
          refetchOnWindowFocus: false,
          onError: error => {
            if (error?.status === 401 && !isInactiveUser) {
              removeUserInactive();
            }
          },
          retry: 0
        }
      }
    })
  );

  return (
    <QueryClientProvider client={queryClient.current}>
      {children}
      <ReactQueryDevtools initialIsOpen={false} />
    </QueryClientProvider>
  );
};

ReactQueryProvider.propTypes = {
  children: PropTypes.element
};
