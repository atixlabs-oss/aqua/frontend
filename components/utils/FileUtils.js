/**
 * AGUA Copyright (C) 2023  Atix Labs
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import blobToBuffer from 'blob-to-buffer';

export const toBase64 = file =>
  new Promise((resolve, reject) => {
    const reader = new FileReader();
    reader.readAsDataURL(file);
    reader.onload = () => resolve(reader.result);
    reader.onerror = error => reject(error);
  });

export const toBuffer = file =>
  new Promise((resolve, reject) => {
    blobToBuffer(file, (err, buffer) => {
      if (err) {
        reject(err);
      }
      resolve(buffer);
    });
  });

export const downloadFileFromPath = (filePath, filename) => {
  const link = document.createElement('a');
  link.href = filePath;
  link.rel = 'noopener noreferrer';
  link.target = '_blank';
  if (filename) {
    link.setAttribute('download', filename);
  }
  document.body.appendChild(link);
  link.click();
  link.remove();
};
