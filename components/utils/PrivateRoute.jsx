/**
 * AGUA Copyright (C) 2023  Atix Labs
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import { useRouter } from 'next/router';
import { useContext } from 'react';
import PropTypes from 'prop-types';

import { UserContext } from './UserContext';
import Loading from 'components/molecules/Loading/Loading';
import { CookiesModal } from 'components/organisms/CookiesModal/CookiesModal';

function PrivateRoute({ children }) {
  const router = useRouter();
  const { user, isInactiveUser } = useContext(UserContext);
  const { pathname } = router;
  const isAuthenticated = !!user;

  let Component = <>{children}</>;

  if (pathname === '/' && isAuthenticated && !user?.isAdmin) {
    router.push('/my-projects');
    Component = <Loading>{Component}</Loading>;
  }

  if (pathname === '/' && isAuthenticated && user?.isAdmin) {
    router.push('/back-office/projects');
    Component = <Loading>{Component}</Loading>;
  }

  if (
    ((pathname.includes('/back-office') || pathname === '/my-projects') && !isAuthenticated) ||
    isInactiveUser
  ) {
    router.push('/login');
    Component = <Loading />;
  }

  if (!pathname.includes('/public')) {
    Component = (
      <>
        {Component}
        <CookiesModal />
      </>
    );
  }

  return Component;
}

PrivateRoute.propTypes = {
  children: PropTypes.elementType
};

export default PrivateRoute;
