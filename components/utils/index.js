/**
 * AGUA Copyright (C) 2023  Atix Labs
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/* eslint-disable no-restricted-syntax */
export const addElipsesToText = (text, number) => `${text.slice(0, number)}...`;

export async function scrollToLandingElement(id, isHeaderBig) {
  const element = document.getElementById(id);

  const windowWidth = window.innerWidth;
  const elementPosition = element.offsetTop;

  if (windowWidth < 1440) {
    window.scrollTo({
      top: elementPosition - 100,
      behavior: 'smooth'
    });
  }
  if (windowWidth >= 1440) {
    const headerHeight = isHeaderBig ? 720 + 60 : 250 + 60;
    window.scrollTo({
      top: elementPosition - headerHeight,
      behavior: 'smooth'
    });
  }
}

export const sortArrayByDate = (arr, field) =>
  arr.sort((a, b) => new Date(b[field]).getTime() - new Date(a[field]).getTime());

function padTo2Digits(num) {
  return num.toString().padStart(2, '0');
}

export const formatDate = date => {
  const newDate = new Date(date);
  return [
    padTo2Digits(newDate.getDate()),
    padTo2Digits(newDate.getMonth() + 1),
    newDate.getFullYear()
  ].join('-');
};
