/**
 * AGUA Copyright (C) 2023  Atix Labs
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import React from 'react';
import PropTypes from 'prop-types';
import { Divider } from 'antd';
import { ACTIVITY_TYPES_ENUM } from 'model/activityTypes';
import { addMiddleEllipsis, formatCurrency } from '../../../helpers/formatter';
import { CoinIcon } from 'components/atoms/CustomIcons/CoinIcon/CoinIcon';
import { BankIcon } from 'components/atoms/CustomIcons/BankIcon';
import { DocumentIcon } from 'components/atoms/CustomIcons/DocumentIcon/DocumentIcon';
import { CoaLink } from 'components/atoms/CoaLink/CoaLink';
import { CopyClipboardButton } from 'components/atoms/CopyClipboardButton/CopyClipboardButton';
import { useTranslation } from 'react-i18next';

export default function EvidenceDetailType(props) {
  const { t } = useTranslation();
  const {
    evidenceType,
    currency,
    activityType,
    amount,
    destinationAccount,
    isCryptoProject
  } = props;
  const isTransfer = evidenceType?.toLowerCase() === 'transfer';
  const type = evidenceType ? evidenceType.charAt(0).toUpperCase() + evidenceType.slice(1) : '';

  return (
    <div className="">
      <Divider />
      <div className="evidenceDetailType__container">
        <div className="evidenceDetailType__block">
          <h3 className="evidenceDetailType__block__label">
            <DocumentIcon fontSize="1.3125rem" className="evidenceDetailType__block__label__icon" />
            {t('evidenceDetail.evidenceType')}
          </h3>
          <h3 className="evidenceDetailType__block__labelValue">
            {t(`general.${[(type || '').toLowerCase()]}`) || type}
          </h3>
        </div>
        {isTransfer && (
          <>
            <div className="evidenceDetailType__block">
              <h3 className="evidenceDetailType__block__label">
                <CoinIcon fontSize="1.3125rem" className="evidenceDetailType__block__label__icon" />
                {activityType === ACTIVITY_TYPES_ENUM.FUNDING
                  ? t('general.income')
                  : t('general.outcome')}
              </h3>
              <h3 className="evidenceDetailType__block__labelValue">
                {formatCurrency(currency, amount, true)}
              </h3>
            </div>

            <div className="evidenceDetailType__block">
              <h3 className="evidenceDetailType__block__label">
                <BankIcon fontSize="1.3125rem" className="evidenceDetailType__block__label__icon" />
                Destination Account
              </h3>
              <h3 className="evidenceDetailType__block__labelValue">
                {isCryptoProject ? (
                  <CoaLink
                    href={{ pathname: `https://etherscan.io/address/${destinationAccount}` }}
                    target="_blank"
                    rel="noopener noreferrer"
                  >
                    {addMiddleEllipsis(destinationAccount)}
                  </CoaLink>
                ) : (
                  destinationAccount
                )}
                {isCryptoProject && <CopyClipboardButton textToCopy={destinationAccount} />}
              </h3>
            </div>
          </>
        )}
      </div>
    </div>
  );
}

EvidenceDetailType.defaultProps = {
  currency: '',
  evidenceType: ''
};

EvidenceDetailType.propTypes = {
  currency: PropTypes.string,
  evidenceType: PropTypes.string
};
