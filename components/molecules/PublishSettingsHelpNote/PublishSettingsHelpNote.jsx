import React from 'react';

import Styles from './publish-settings-help-note.module.scss';
import { HelpNote } from '../HelpNote/HelpNote';
import { useTranslation } from 'react-i18next';

export const PublishSettingsHelpNote = () => {
  const { t } = useTranslation();
  return (
    <HelpNote
      tooltipPlacement="bottom"
      Note={t('projectsSection.Project Publish Settings')}
      TooltipNote={
        <ul className={Styles.list}>
          <li>
            <span className={Styles.title}>{t('general.private')}: </span>{' '}
            {t('projectsSection.It wont be listed in the')}{' '}
            <span className={Styles.marked}>{t('projectsSection.Projects Page')}</span>.{' '}
            {t('projectsSection.It will be only visible for the Administrator')}.
          </li>
          <li>
            <span className={Styles.title}>{t('general.public')}:</span>{' '}
            {t('projectsSection.It will be listed in the')}{' '}
            <span className={Styles.marked}>{t('projectsSection.Projects Page')}.</span>
          </li>
          <li>
            <span className={Styles.title}>{t('general.highlighted')}:</span>{' '}
            {t('projectsSection.It will be listed in the')}{' '}
            <span className={Styles.marked}>{t('projectsSection.Projects Page')}</span>{' '}
            {t('projectsSection.and also shown in the')}
            <span className={Styles.marked}>{t('projectsSection.Organization Landing Page')}.</span>
          </li>
        </ul>
      }
    />
  );
};
