import { useState } from 'react';
import PropTypes from 'prop-types';

import { CoaButton } from 'components/atoms/CoaButton/CoaButton';
import { useTranslation } from 'react-i18next';

export const ApproveRejectButtons = ({
  onReject,
  onApprove,
  approveButtonClassName,
  rejectButtonClassName
}) => {
  const { t } = useTranslation();
  const [isApproveLoading, setIsApproveLoading] = useState(false);
  const [isRejectLoading, setIsRejectLoading] = useState(false);
  return (
    <>
      <CoaButton
        disabled={isApproveLoading}
        icon="close"
        type="danger"
        className={rejectButtonClassName}
        onClick={async () => {
          setIsRejectLoading(true);
          await onReject();
          setIsRejectLoading(false);
        }}
      >
        {t('general.actions.Reject')}
      </CoaButton>
      <CoaButton
        disabled={isRejectLoading}
        icon="check"
        type="success"
        className={approveButtonClassName}
        onClick={async () => {
          setIsApproveLoading(true);
          await onApprove();
          setIsApproveLoading(false);
        }}
      >
        {t('general.actions.Approve')}
      </CoaButton>
    </>
  );
};

ApproveRejectButtons.propTypes = {
  onReject: PropTypes.func,
  onApprove: PropTypes.func,
  approveButtonClassName: PropTypes.string,
  rejectButtonClassName: PropTypes.string
};
