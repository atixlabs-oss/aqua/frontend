/**
 * AGUA Copyright (C) 2023  Atix Labs
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import React, { useContext, useEffect, useRef } from 'react';
import { Alert, Col, Row, Icon as AntIcon } from 'antd';
import classNames from 'classnames';
import PropTypes from 'prop-types';
import { ALERT_VARIANTS_ENUM } from 'model/alertVariants';
import { CoaButton } from 'components/atoms/CoaButton/CoaButton';
import { useTranslation } from 'react-i18next';
import { useRouter } from 'next/router';
import { AlertContext } from 'components/utils/AlertContext';

export const CoaAlert = ({
  highlightedText,
  message,
  Icon,
  className,
  customColor,
  show,
  closeContent,
  closable,
  variant,
  contentCentered,
  fixed,
  ...rest
}) => {
  const { t } = useTranslation();
  const router = useRouter();
  const { projectId } = router.query;
  const timeoutRef = useRef(null);

  const { closeAlert } = useContext(AlertContext);

  const COA_ALERT_VARIANTS = {
    [ALERT_VARIANTS_ENUM.PREVIEW]: {
      message: t('header.preview'),
      props: {
        closable: false,
        show: true,
        closeText: (
          <CoaButton
            onClick={() => router.push(`/back-office/projects/edit/${projectId}`)}
            type="ghost"
            primaryColor="white"
          >
            <AntIcon type="arrow-left" /> {t('general.backToEdit')}
          </CoaButton>
        )
      }
    },
    [ALERT_VARIANTS_ENUM.EVIDENCE_DELETED]: {
      message: t('evidences.feedbackDelete.message'),
      customColor: 'green',
      withCloseTimer: true,
      props: {
        closable: true,
        show: true,
        Icon: (
          <AntIcon
            type="check-circle"
            theme="filled"
            style={{ fontSize: '1.5rem', color: '#ffffff' }}
          />
        )
      }
    },
    [ALERT_VARIANTS_ENUM.DRAFT_PROJECT]: {
      message: t('alert.theProjectIsInDraftVersion'),
      props: {
        closable: false,
        show: true,
        Icon: (
          <AntIcon
            type="exclamation-circle"
            theme="filled"
            style={{ fontSize: '1.5rem', color: '#ffffff' }}
          />
        )
      }
    },
    [ALERT_VARIANTS_ENUM.CANCEL_REQUEST]: {
      message: t('createProject.alert.cancelProject'),
      customColor: 'orange',
      contentCentered: true,
      props: {
        closable: false
      }
    },
    [ALERT_VARIANTS_ENUM.PROJECT_BEING_EDITED]: {
      message: t('alert.theProjectIsBeingEdited'),
      customColor: 'orange',
      contentCentered: true,
      props: {
        closable: false
      }
    },
    [ALERT_VARIANTS_ENUM.PASSWORD_CHANGED]: {
      message: 'Your password has been successfully changed!',
      customColor: 'green',
      withCloseTimer: true,
      props: {
        closable: true,
        show: true,
        Icon: (
          <AntIcon
            type="check-circle"
            theme="filled"
            style={{ fontSize: '1.5rem', color: '#ffffff' }}
          />
        )
      }
    },
    [ALERT_VARIANTS_ENUM.SIGNATURE_APPROVED]: {
      message: t('signature.newSignatureApprovedSuccessfully'),
      customColor: 'green',
      withCloseTimer: true,
      props: {
        closable: true,
        show: true,
        Icon: (
          <AntIcon
            type="check-circle"
            theme="filled"
            style={{ fontSize: '1.5rem', color: '#ffffff' }}
          />
        )
      }
    },
    [ALERT_VARIANTS_ENUM.SIGNATURE_REJECTED]: {
      message: t('signature.newSignatureRejectedSuccessfully'),
      customColor: 'green',
      withCloseTimer: true,
      props: {
        closable: true,
        show: true,
        Icon: (
          <AntIcon
            type="check-circle"
            theme="filled"
            style={{ fontSize: '1.5rem', color: '#ffffff' }}
          />
        )
      }
    },
    [ALERT_VARIANTS_ENUM.ACTIVE_STATUS_CHANGED]: {
      message: t('alert.usersAdministration.The active status was changed successfully'),
      customColor: 'green',
      withCloseTimer: true,
      props: {
        closable: true,
        show: true,
        Icon: (
          <AntIcon
            type="check-circle"
            theme="filled"
            style={{ fontSize: '1.5rem', color: '#ffffff' }}
          />
        )
      }
    }
  };

  const CURRENT_VARIANT = COA_ALERT_VARIANTS[variant];

  const currentHighlightedText = CURRENT_VARIANT?.highlightedText || highlightedText;
  const currentMessage = CURRENT_VARIANT?.message || message;
  const currentClosable = CURRENT_VARIANT?.props.closable || closable;
  const currentCloseContent = CURRENT_VARIANT?.props.closeText || closeContent;
  const currentCustomColor = CURRENT_VARIANT?.customColor || customColor;
  const currentShow = CURRENT_VARIANT?.props.show || show;
  const currentIcon = CURRENT_VARIANT?.props.Icon || Icon;
  const currentContentCentered = CURRENT_VARIANT?.contentCentered || contentCentered;

  useEffect(() => {
    if (currentShow && CURRENT_VARIANT?.withCloseTimer)
      timeoutRef.current = setTimeout(() => closeAlert(), 3000);
    return () => clearTimeout(timeoutRef.current);
  }, [currentShow, closeAlert, CURRENT_VARIANT?.withCloseTimer]);

  return (
    currentShow && (
      <Alert
        className={classNames(
          'coaAlert',
          {
            [`--${currentCustomColor}`]: currentCustomColor,
            ['--contentCentered']: currentContentCentered,
            '--fixed': fixed
          },
          className
        )}
        message={
          <Row type="flex" align="middle" className="coaAlert__content" gutter={15}>
            <Col>{currentIcon}</Col>
            <p>
              <span className="coaAlert__content__highlightedText">{currentHighlightedText}</span>
              <span className="coaAlert__content__message">{currentMessage}</span>
            </p>
          </Row>
        }
        closable={currentClosable}
        closeText={currentCloseContent}
        {...rest}
      />
    )
  );
};

CoaAlert.defaultProps = {
  highlightedText: '',
  message: '',
  Icon: undefined,
  className: '',
  customType: 'warning',
  show: false,
  fixed: false
};

CoaAlert.propTypes = {
  highlightedText: PropTypes.string,
  message: PropTypes.string,
  Icon: PropTypes.oneOfType([PropTypes.arrayOf(PropTypes.node), PropTypes.node]),
  className: PropTypes.string,
  customColor: PropTypes.string,
  show: PropTypes.bool,
  closeContent: PropTypes.oneOfType([PropTypes.arrayOf(PropTypes.node), PropTypes.node]),
  closable: PropTypes.bool,
  variant: PropTypes.string,
  contentCentered: PropTypes.bool,
  fixed: PropTypes.bool
};
