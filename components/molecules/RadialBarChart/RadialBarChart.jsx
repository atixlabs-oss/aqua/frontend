/**
 * AGUA Copyright (C) 2023  Atix Labs
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import React from 'react';
import PropTypes from 'prop-types';

import { removeDecimals } from 'helpers/formatter';

export const RadialBarChart = ({
  className,
  externalDonutColor,
  internalDonutColor,
  externalDonutSymbol,
  internalDonutSymbol,
  externalDonutPercent,
  internalDonutPercent
}) => {
  const externalDonutPercentFormatted = removeDecimals(externalDonutPercent);
  const internalDonutPercentFormatted = removeDecimals(internalDonutPercent);

  return (
    <div className={`m-radialBarChart__container ${className}`}>
      <div className="m-radialBarChart__container__graph">
        <div
          className="m-radialBarChart__container__graph__percent --bigger"
          style={{
            '--clr': externalDonutColor,
            '--num': externalDonutPercentFormatted
          }}
        >
          <div className="m-radialBarChart__container__graph__dot --end">
            <div className="m-radialBarChart__container__graph__dot__content --end">
              {externalDonutPercentFormatted}
              {externalDonutSymbol}
            </div>
          </div>
          <div className="m-radialBarChart__container__graph__dot --init">
            <div className="m-radialBarChart__container__graph__dot__content --init">
              {externalDonutSymbol}
            </div>
          </div>
          <svg>
            <circle cx="70" cy="70" r="70"></circle>
            <circle cx="70" cy="70" r="70"></circle>
          </svg>
        </div>
        <div
          className="m-radialBarChart__container__graph__percent --smaller"
          style={{
            '--clr': internalDonutColor,
            '--num': internalDonutPercentFormatted
          }}
        >
          <div className="m-radialBarChart__container__graph__dot --end">
            <div className="m-radialBarChart__container__graph__dot__content --end">
              {internalDonutPercentFormatted}
              {internalDonutSymbol}
            </div>
          </div>
          <div className="m-radialBarChart__container__graph__dot --init">
            <div className="m-radialBarChart__container__graph__dot__content --init">
              {internalDonutSymbol}
            </div>
          </div>
          <svg>
            <circle cx="50" cy="50" r="50"></circle>
            <circle cx="50" cy="50" r="50"></circle>
          </svg>
        </div>
      </div>
    </div>
  );
};

RadialBarChart.propTypes = {
  externalDonutColor: PropTypes.string,
  internalDonutColor: PropTypes.string,
  externalDonutSymbol: PropTypes.string,
  internalDonutSymbol: PropTypes.string,
  className: PropTypes.string
};

RadialBarChart.defaultProps = {
  externalDonutColor: '#08ceaa',
  internalDonutColor: '26385b',
  externalDonutSymbol: '%',
  internalDonutSymbol: '$',
  className: ''
};
