/**
 * AGUA Copyright (C) 2023  Atix Labs
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/* eslint-disable func-names */

import React, { useContext, useEffect, useState, useCallback } from 'react';
import { useTranslation } from 'react-i18next';
import { Form, Upload, Icon, Input, message, Select } from 'antd';
import PropTypes from 'prop-types';
import classNames from 'classnames';
import { useRouter } from 'next/router';

import GoBackButton from 'components/atoms/GoBackButton/GoBackButton';
import Breadcrumb from 'components/atoms/BreadCrumb/BreadCrumb';
import { checkIsBeneficiaryByProject, checkIsInvestorByProject } from 'helpers/roles';
import { ACTIVITY_TYPES_ENUM } from 'model/activityTypes';
import { formatCurrency } from 'helpers/formatter';
import Loading from '../Loading/Loading';
import { UserContext } from '../../utils/UserContext';
import { EvidenceContext } from '../../utils/EvidenceContext';
import { CoaButton } from 'components/atoms/CoaButton/CoaButton';
import {
  useGetProjectQuery,
  useGetProjectTransactionsQuery
} from 'api/react-query/project/projectQueries';
import { useCreateActivityEvidenceMutation } from 'api/react-query/activities/activitiesMutations';

const CURRENCY_TYPE_ENUM = { FIAT: 'fiat', CRYPTO: 'crypto' };
const EVIDENCE_TYPE_ENUM = { IMPACT: 'impact', TRANSFER: 'transfer' };
const EVIDENCE_TRANSACTION_TYPE_ENUM = { OUTCOME: 'outcome', INCOME: 'income' };
const transactionQueryParam = {
  income: 'received',
  outcome: 'sent'
};

const { Option } = Select;

const EvidenceFormContent = props => {
  const { form, breadCrumbPath } = props;
  const { getFieldDecorator } = form;

  const { t } = useTranslation();
  const router = useRouter();
  const { projectId, activityId } = router.query;

  const [currentType, setCurrentType] = useState();
  const { data: project, loading } = useGetProjectQuery({ projectId });
  const createActivityEvidence = useCreateActivityEvidenceMutation();
  const { refetch: fetchProjectTransactions } = useGetProjectTransactionsQuery({
    enabled: false,
    projectId,
    type: currentType
  });

  const { user } = useContext(UserContext);
  const { setMessage } = useContext(EvidenceContext);

  const [state, setState] = useState({
    type: EVIDENCE_TYPE_ENUM.TRANSFER,
    title: '',
    description: '',
    files: [],
    transferTxHash: '',
    amount: 0
  });
  const { type } = state;

  const [currencyType, setCurrencyType] = useState(CURRENCY_TYPE_ENUM.FIAT);
  const [currency, setCurrency] = useState();
  const [buttonLoading, setButtonLoading] = useState(false);
  const [transactions, setTransactions] = useState([]);

  const activities = project?.milestones?.reduce((acc, curr) => [...acc, ...curr?.activities], []);
  const activityType = activities?.find(
    activity => activity?.id?.toString() === activityId?.toString()
  )?.type;

  const transactionType =
    activityType === ACTIVITY_TYPES_ENUM.FUNDING
      ? EVIDENCE_TRANSACTION_TYPE_ENUM.INCOME
      : EVIDENCE_TRANSACTION_TYPE_ENUM.OUTCOME;
  const [loadingTransactions, setLoadingTransactions] = useState(false);

  const getTransactions = useCallback(
    async _transactionType => {
      form.resetFields(['transferTxHash']);
      setLoadingTransactions(true);
      const _transactionQueryParam = transactionQueryParam[_transactionType];
      if (!_transactionQueryParam) {
        setLoadingTransactions(false);
        return message.error(t('createEvidence.errorFetchingTransactions'));
      }
      setCurrentType(_transactionQueryParam);
    },
    [t, form]
  );

  useEffect(() => {
    if (currentType) {
      (async () => {
        try {
          const { data } = await fetchProjectTransactions();

          const transactionHashes = data.transactions.map(hash => ({
            txHash: hash.txHash,
            value: hash.value,
            label: {
              date: (hash.timestamp || '').split(' ')[0],
              hour: `${(hash.timestamp || '').split(' ')[1]} ${
                (hash.timestamp || '').split(' ')[2]
              }`,
              txValue: formatCurrency(hash.tokenSymbol, hash.value, true)
            }
          }));

          setTransactions(transactionHashes);
        } catch {
        } finally {
          setLoadingTransactions(false);
        }
      })();
    }
  }, [currentType, fetchProjectTransactions]);

  useEffect(() => {
    if (!loading) {
      setCurrencyType(project?.details?.currencyType.toLowerCase());
      setCurrency(project?.details?.currency);

      const isBeneficiary = checkIsBeneficiaryByProject({ user, project });
      const isInvestor = checkIsInvestorByProject({ user, project });

      if (!isBeneficiary && !isInvestor) {
        message.error(t('createEvidence.errorUser'));
        router.push(`/${project.id}`);
      }
    }
  }, [loading]);

  useEffect(() => {
    if (currencyType === CURRENCY_TYPE_ENUM.CRYPTO) {
      getTransactions(transactionType);
    }
  }, [currencyType, transactionType]);

  const onChangeAmount = e => setState({ ...state, amount: e.currentTarget.value });

  const handleFileChange = async value => {
    if (value?.file?.status !== 'removed') {
      const { files } = state;
      files.push(value.file);
      setState({ ...state, files });
    }
  };

  const handleFileRemove = field => {
    const { files } = state;

    const newFiles = files.filter(file => file.uid !== field.uid);

    setState({ ...state, files: newFiles });
  };

  const onCancel = () => router.push(`/${project.id}`);

  const uploadProps = {
    name: 'files',
    accept: '.jpg,.png,.pdf',
    multiple: true,
    listType: 'text',
    beforeUpload: () => false,
    onChange: handleFileChange,
    onRemove: handleFileRemove
  };

  const handleSubmit = async e => {
    e.preventDefault();

    try {
      setButtonLoading(true);

      form.validateFields();
      const fieldErrors = form.getFieldsError();
      const isThereAnyError = Object.values(fieldErrors).some(err => !!err);

      if (isThereAnyError) {
        setButtonLoading(false);
        return;
      }

      await createActivityEvidence.mutateAsync({ activityId, data: state });

      setMessage(t('createEvidence.success'));
      router.push(`/${project.id}/activity/${activityId}/evidences`);
    } catch {
    } finally {
      setButtonLoading(false);
    }
  };

  if (loading) return <Loading></Loading>;

  const isFiatProject = currencyType === CURRENCY_TYPE_ENUM.FIAT;

  return (
    <div className="evidenceForm">
      <GoBackButton
        goBackTo={() => router.push(`/${projectId}/activity/${activityId}/evidences`)}
      />
      <Breadcrumb route={breadCrumbPath} />
      <div className="evidenceForm__body">
        <p className="evidenceForm__body__title">
          <span>{t('general.btnAdd')}</span>
          <span> {t('createEvidence.newEvidence')}</span>
        </p>
        <div className="evidenceForm__body__text">
          <p className="evidenceForm__body__text_top">{t('createEvidence.evidenceTypeLabel')}</p>
        </div>

        <Form className="evidenceForm__body__form" onSubmit={handleSubmit}>
          <div className="evidenceForm__body__form__group">
            <div className="formDivInfo">
              <p className="formDivTitle">{t('createEvidence.evidenceType')}</p>
              <p className="formDIvInfo">
                <span>{t('general.transfer')}:</span> {t('createEvidence.transferRelatedTo')}
              </p>
              <p className="formDIvInfo">
                <span>{t('general.impact')}:</span> {t('createEvidence.impactRelatedTo')}
              </p>
            </div>
            <div className="evidenceType">
              <div className="formDivBorder">
                <div className="customRadioDiv">
                  <input
                    type="radio"
                    name="evidenceType"
                    id=""
                    checked={state.type === EVIDENCE_TYPE_ENUM.TRANSFER}
                    value={EVIDENCE_TYPE_ENUM.TRANSFER}
                    onChange={e => setState({ ...state, type: e.target.value })}
                  />
                  <div className="customRadio">
                    <div></div>
                  </div>
                </div>
                <span>{t('general.transfer')}</span>
              </div>
              <div className="formDivBorder">
                <div className="customRadioDiv">
                  <input
                    type="radio"
                    name="evidenceType"
                    id=""
                    checked={state.type === EVIDENCE_TYPE_ENUM.IMPACT}
                    value={EVIDENCE_TYPE_ENUM.IMPACT}
                    onChange={e => setState({ ...state, type: e.target.value })}
                    disabled={
                      activityType === ACTIVITY_TYPES_ENUM.FUNDING ||
                      activityType === ACTIVITY_TYPES_ENUM.PAYBACK
                    }
                  />
                  <div className="customRadio">
                    <div></div>
                  </div>
                </div>
                <span>{t('general.impact')}</span>
              </div>
            </div>
          </div>
          <div className="evidenceForm__body__form__group">
            <p className="formDivTitle formDivInfo">
              {t('createEvidence.evidenceTitle') || 'Evidence Title'}
            </p>
            <div className="formDivInput itemInput">
              <Form.Item>
                {getFieldDecorator('title', {
                  rules: [
                    {
                      required: true,
                      message: t('createEvidence.pleaseEnterTitle')
                    },
                    {
                      max: 50,
                      message: t('createEvidence.titleLowerThan')
                    }
                  ]
                })(
                  <Input
                    name="title"
                    placeholder={t('createEvidence.enterTitle')}
                    onChange={e => {
                      setState({
                        ...state,
                        title: e.target.value
                      });
                    }}
                  />
                )}
              </Form.Item>
            </div>
          </div>
          <div className="evidenceForm__body__form__group">
            <p className="formDivTitle formDivInfo">{t('createEvidence.description')}</p>
            <div className="formDivInput">
              <Form.Item>
                {getFieldDecorator('description', {
                  rules: [
                    {
                      required: true,
                      message: t('createEvidence.pleaseEnterDescription')
                    }
                  ]
                })(
                  <Input.TextArea
                    name="description"
                    rows={5}
                    maxLength={500}
                    placeholder={t('createEvidence.enterDescription')}
                    onChange={e => {
                      setState({
                        ...state,
                        description: e.target.value
                      });
                    }}
                  />
                )}
              </Form.Item>
            </div>
          </div>
          {currencyType === CURRENCY_TYPE_ENUM.CRYPTO && type === EVIDENCE_TYPE_ENUM.TRANSFER && (
            <div className="evidenceForm__body__form__group">
              <p className="formDivTitle formDivInfo">{t('createEvidence.selectRightTx')}</p>
              <div className="formDivInput itemInput">
                <Form.Item>
                  {getFieldDecorator('transferTxHash', {
                    rules: [
                      {
                        required: currencyType === CURRENCY_TYPE_ENUM.CRYPTO,
                        message: t('createEvidence.pleaseChooseTxHash')
                      }
                    ]
                  })(
                    <Select
                      className="evidenceForm__select"
                      placeholder={t('createEvidence.selectTx')}
                      disabled={loadingTransactions}
                      loading={loadingTransactions}
                      onChange={txHash => {
                        const _amount =
                          transactions.find(item => item.txHash === txHash)?.value || 0;
                        setState({
                          ...state,
                          transferTxHash: txHash,
                          amount: _amount
                        });
                      }}
                    >
                      {transactions.map(({ txHash, label }) => (
                        <Option value={txHash} key={txHash}>
                          <div className="evidenceForm__select__option">
                            <div>
                              <span className="evidenceForm__select__option__date">
                                {label.date}
                              </span>
                              <span className="evidenceForm__select__option__hour">
                                {label.hour}
                              </span>
                            </div>
                            <div>
                              <span className="evidenceForm__select__option__txValue">
                                {label.txValue}
                              </span>
                            </div>
                          </div>
                        </Option>
                      ))}
                    </Select>
                  )}
                </Form.Item>
              </div>
            </div>
          )}
          {type === EVIDENCE_TYPE_ENUM.TRANSFER && (
            <div className="evidenceForm__body__form__group">
              <p className="formDivTitle formDivInfo">{t('general.amount')}</p>
              <div className="formDivInput formDivAmount">
                <input
                  type="number"
                  name="amount"
                  value={state.amount}
                  placeholder={t('createEvidence.amountSpent')}
                  onChange={onChangeAmount}
                  disabled={currencyType === CURRENCY_TYPE_ENUM.CRYPTO}
                  className={classNames({
                    formCurrencyValue: currencyType === CURRENCY_TYPE_ENUM.CRYPTO
                  })}
                />
                <div className="formCurrency">{currency}</div>
              </div>
            </div>
          )}
          {isFiatProject && type === EVIDENCE_TYPE_ENUM.TRANSFER && (
            <div className="evidenceForm__body__form__group">
              <div className="formDivInfo">
                <p className="formDivTitle">{t('createEvidence.destinationAccount')}</p>
                <p className="formDIvInfo">
                  {t('createEvidence.accountDetails')}:
                  <ul>
                    <li>{t('createEvidence.bankInformation')}</li>
                    <li>{t('createEvidence.accountNumber')}</li>
                    <li>{t('createEvidence.routingNumber')}</li>
                    <li>{t('createEvidence.aba')}</li>
                    <li>{t('createEvidence.swift')}</li>
                  </ul>
                </p>
              </div>
              <div className="formDivInput">
                <Form.Item>
                  {getFieldDecorator('destinationAccount', {
                    rules: [
                      {
                        required: true,
                        message: 'Please enter destination account'
                      }
                    ]
                  })(
                    <Input.TextArea
                      name="destinationAccount"
                      rows={5}
                      maxLength={500}
                      placeholder={t('createEvidence.enterDestinationAccount')}
                      onChange={e => {
                        setState({
                          ...state,
                          destinationAccount: e.target.value
                        });
                      }}
                    />
                  )}
                </Form.Item>
              </div>
            </div>
          )}
          {(isFiatProject || type === EVIDENCE_TYPE_ENUM.IMPACT) && (
            <div className="evidenceForm__body__form__group">
              <div className="formDivInfo">
                <p className="formDivTitle">{t('createEvidence.upload')}</p>
                <p className="formDIvInfo">{t('createEvidence.checkLegible')}</p>
                <p className="formDIvInfo">
                  <span>{t('general.formats')}:</span> PNG, JPG, PDF
                </p>
              </div>
              <div className="formDivInput">
                <div className="uploadBtnDiv">
                  <Form.Item>
                    {getFieldDecorator('files', {
                      rules: [
                        {
                          required: type === EVIDENCE_TYPE_ENUM.IMPACT || isFiatProject,
                          message: t('createEvidence.pleaseSelectFiles')
                        }
                      ]
                    })(
                      <Upload {...uploadProps} className="evidenceForm__upload">
                        <CoaButton type="primary">
                          <Icon type="upload" /> {t('general.uploadClick')}
                        </CoaButton>
                      </Upload>
                    )}
                  </Form.Item>
                </div>
              </div>
            </div>
          )}
          <div className="evidenceForm__body__form__group evidenceForm__body__form__btns">
            <CoaButton type="ghost" onClick={() => onCancel()}>
              {t('general.btnCancel') || 'Cancel'}
            </CoaButton>
            <CoaButton
              disabled={loading || loadingTransactions}
              loading={buttonLoading}
              htmlType="submit"
              type="primary"
            >
              {t('general.btnAdd')}&nbsp;{t('general.evidence')}
            </CoaButton>
          </div>
        </Form>
      </div>
    </div>
  );
};

EvidenceFormContent.defaultProps = {
  form: () => undefined,
  breadCrumbPath: ''
};

EvidenceFormContent.propTypes = {
  form: PropTypes.objectOf(PropTypes.any),
  breadCrumbPath: PropTypes.string
};

export const EvidenceForm = Form.create({
  name: 'EvidenceForm'
})(EvidenceFormContent);
