/**
 * AGUA Copyright (C) 2023  Atix Labs
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import { Checkbox } from 'antd';

import React from 'react';
import PropTypes from 'prop-types';
import { CoaFormItem } from '../CoaFormItem/CoaFormItem';

export const CoaFormItemCheckbox = ({
  form,
  formItemProps,
  name,
  errorsToShow,
  fieldDecoratorOptions,
  withErrorFeedback,
  Note,
  checkboxProps,
  checkboxGroupProps,
  options,
  option
}) => {
  const { getFieldDecorator } = form;

  return (
    <>
      {options && (
        <CoaFormItem {...{ formItemProps, withErrorFeedback, errorsToShow, name, form }}>
          {Note}
          {getFieldDecorator(name, {
            ...fieldDecoratorOptions
          })(
            <Checkbox.Group {...checkboxGroupProps}>
              {options.map((option, index) => (
                <Checkbox
                  key={index}
                  value={option?.value}
                  disabled={option?.disabled}
                  {...checkboxProps}
                >
                  {option?.label}
                </Checkbox>
              ))}
            </Checkbox.Group>
          )}
        </CoaFormItem>
      )}
      {option && (
        <CoaFormItem {...{ formItemProps, withErrorFeedback, errorsToShow, name, form }}>
          {Note}
          {getFieldDecorator(name, {
            ...fieldDecoratorOptions
          })(
            <Checkbox value={option?.value} disabled={option?.disabled} {...checkboxProps}>
              {option?.label}
            </Checkbox>
          )}
        </CoaFormItem>
      )}
    </>
  );
};

CoaFormItemCheckbox.defaultProps = {
  form: undefined,
  formItemProps: undefined,
  inputProps: undefined,
  errorsToShow: undefined,
  fieldDecoratorOptions: undefined,
  withErrorFeedback: false,
  Note: undefined
};

CoaFormItemCheckbox.propTypes = {
  form: PropTypes.objectOf(PropTypes.any),
  formItemProps: PropTypes.objectOf(PropTypes.any),
  inputProps: PropTypes.objectOf(PropTypes.any),
  name: PropTypes.string.isRequired,
  errorsToShow: PropTypes.arrayOf(PropTypes.string),
  fieldDecoratorOptions: PropTypes.objectOf(PropTypes.any),
  withErrorFeedback: PropTypes.bool,
  Note: PropTypes.oneOfType([PropTypes.arrayOf(PropTypes.node), PropTypes.node]),
  checkboxProps: PropTypes.objectOf(PropTypes.any),
  checkboxGroupProps: PropTypes.objectOf(PropTypes.any),
  options: PropTypes.arrayOf(
    PropTypes.shape({
      value: PropTypes.string.isRequired,
      disabled: PropTypes.bool,
      label: PropTypes.string.isRequired
    })
  ),
  option: PropTypes.shape({
    value: PropTypes.string.isRequired,
    disabled: PropTypes.bool,
    label: PropTypes.string.isRequired
  })
};
