/**
 * AGUA Copyright (C) 2023  Atix Labs
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import { Select } from 'antd';
import React from 'react';
import PropTypes from 'prop-types';
import { CoaFormItem } from '../CoaFormItem/CoaFormItem';

const { Option } = Select;

export const CoaFormItemSelect = ({
  form,
  selectProps,
  fieldDecoratorOptions,
  name,
  errorsToShow,
  formItemProps,
  options,
  withErrorFeedback
}) => {
  const { getFieldDecorator } = form;
  return (
    <CoaFormItem {...{ formItemProps, withErrorFeedback, errorsToShow, name, form }}>
      {getFieldDecorator(name, {
        ...fieldDecoratorOptions
      })(
        <Select placeholder="Type" {...selectProps}>
          {options?.map(({ value, label }) => (
            <Option value={value} key={value}>
              {label}
            </Option>
          ))}
        </Select>
      )}
    </CoaFormItem>
  );
};

CoaFormItemSelect.defaultProps = {
  form: undefined,
  formItemProps: undefined,
  selectProps: undefined,
  errorsToShow: undefined,
  fieldDecoratorOptions: undefined,
  withErrorFeedback: false,
  options: undefined
};

CoaFormItemSelect.propTypes = {
  form: PropTypes.objectOf(PropTypes.any),
  formItemProps: PropTypes.objectOf(PropTypes.any),
  selectProps: PropTypes.objectOf(PropTypes.any),
  name: PropTypes.string.isRequired,
  errorsToShow: PropTypes.arrayOf(PropTypes.string),
  fieldDecoratorOptions: PropTypes.objectOf(PropTypes.any),
  withErrorFeedback: PropTypes.bool,
  options: PropTypes.arrayOf(PropTypes.objectOf(PropTypes.any))
};
