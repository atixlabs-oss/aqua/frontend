/**
 * AGUA Copyright (C) 2023  Atix Labs
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import { Input } from 'antd';

import React, { useEffect, useState } from 'react';
import PropTypes from 'prop-types';
import { CoaFormItem } from '../CoaFormItem/CoaFormItem';

export const CoaFormItemInput = ({
  form,
  formItemProps,
  inputProps,
  name,
  errorsToShow,
  fieldDecoratorOptions,
  withErrorFeedback,
  Note
}) => {
  const { getFieldDecorator } = form;
  const initialValueLength = fieldDecoratorOptions?.initialValue?.length || 0;
  const [charsLength, setCharsLength] = useState(0);
  useEffect(() => {
    setCharsLength(initialValueLength);
  }, [initialValueLength]);
  return (
    <CoaFormItem {...{ formItemProps, withErrorFeedback, errorsToShow, name, form }}>
      {Note}
      {getFieldDecorator(name, {
        ...fieldDecoratorOptions
      })(
        <Input
          {...inputProps}
          onChange={e => {
            setCharsLength(e.target.value?.length);
            inputProps?.onChange?.(e);
          }}
        />
      )}
      {inputProps?.showCount && (
        <span className="m-coaFormItemInput__counter">{`${charsLength}/${inputProps?.maxLength}`}</span>
      )}
    </CoaFormItem>
  );
};

CoaFormItemInput.defaultProps = {
  form: undefined,
  formItemProps: undefined,
  inputProps: undefined,
  errorsToShow: undefined,
  fieldDecoratorOptions: undefined,
  withErrorFeedback: false,
  Note: undefined
};

CoaFormItemInput.propTypes = {
  form: PropTypes.objectOf(PropTypes.any),
  formItemProps: PropTypes.objectOf(PropTypes.any),
  inputProps: PropTypes.objectOf(PropTypes.any),
  name: PropTypes.string.isRequired,
  errorsToShow: PropTypes.arrayOf(PropTypes.string),
  fieldDecoratorOptions: PropTypes.objectOf(PropTypes.any),
  withErrorFeedback: PropTypes.bool,
  Note: PropTypes.oneOfType([PropTypes.arrayOf(PropTypes.node), PropTypes.node])
};
