/**
 * AGUA Copyright (C) 2023  Atix Labs
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import React, { useState, useEffect } from 'react';
import { Input } from 'antd';
import PropTypes from 'prop-types';
import classNames from 'classnames';
import { CoaFormItem } from '../CoaFormItem/CoaFormItem';

export const CoaFormItemTextArea = ({
  form,
  formItemProps,
  inputTextAreaProps,
  name,
  errorsToShow,
  fieldDecoratorOptions,
  withErrorFeedback,
  Note
}) => {
  const { getFieldDecorator } = form;
  const initialValueLength = fieldDecoratorOptions?.initialValue?.length || 0;
  const [charsLength, setCharsLength] = useState(0);
  useEffect(() => {
    setCharsLength(initialValueLength);
  }, [initialValueLength]);
  return (
    <CoaFormItem {...{ formItemProps, withErrorFeedback, errorsToShow, name, form }}>
      {Note}
      {getFieldDecorator(name, {
        ...fieldDecoratorOptions
      })(
        <Input.TextArea
          {...inputTextAreaProps}
          className={classNames(inputTextAreaProps?.className, 'm-coaFormItemTextArea__textArea')}
          onChange={e => setCharsLength(e.target.value?.length)}
        />
      )}
      {inputTextAreaProps?.showCount && (
        <span className="m-coaFormItemTextArea__counter">{`${charsLength}/${inputTextAreaProps?.maxLength}`}</span>
      )}
    </CoaFormItem>
  );
};

CoaFormItemTextArea.defaultProps = {
  form: undefined,
  formItemProps: undefined,
  inputTextAreaProps: undefined,
  errorsToShow: undefined,
  fieldDecoratorOptions: undefined,
  withErrorFeedback: false,
  Note: undefined
};

CoaFormItemTextArea.propTypes = {
  form: PropTypes.objectOf(PropTypes.any),
  formItemProps: PropTypes.objectOf(PropTypes.any),
  inputTextAreaProps: PropTypes.objectOf(PropTypes.any),
  name: PropTypes.string.isRequired,
  errorsToShow: PropTypes.arrayOf(PropTypes.string),
  fieldDecoratorOptions: PropTypes.objectOf(PropTypes.any),
  withErrorFeedback: PropTypes.bool,
  Note: PropTypes.oneOfType([PropTypes.arrayOf(PropTypes.node), PropTypes.node])
};
