/**
 * AGUA Copyright (C) 2023  Atix Labs
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import React from 'react';
import { Form } from 'antd';
import classNames from 'classnames';
import PropTypes from 'prop-types';

import { getErrorMessagesField } from 'helpers/utils';
import Styles from './coa-form-item.module.scss';

export const CoaFormItem = ({
  children,
  withErrorFeedback,
  form,
  name,
  formItemProps,
  errorsToShow,
  ErrorsComponent
}) => {
  const { getFieldError } = form;
  const inputError = getFieldError?.(name) || [];
  const hasErrors = inputError.length > 0;
  return (
    <Form.Item
      {...formItemProps}
      hasFeedback={withErrorFeedback && hasErrors}
      className={classNames(
        Styles.coaFormItem,
        {
          [Styles['--withError']]: hasErrors
        },
        formItemProps?.className
      )}
      help={
        (errorsToShow && (
          <>{getErrorMessagesField(inputError, errorsToShow).map(errorMessage => errorMessage)}</>
        )) ||
        (formItemProps?.ErrorsComponent && <formItemProps.ErrorsComponent errors={inputError} />) ||
        (inputError?.length > 1 && (
          <ul className={Styles.coaFormItem__errorsList}>
            {inputError.map(errorMessage => (
              <li key={errorMessage}>- {errorMessage}</li>
            ))}
          </ul>
        )) ||
        inputError.map(errorMessage => <span key={errorMessage}>{errorMessage}</span>)
      }
    >
      {children}
    </Form.Item>
  );
};

CoaFormItem.defaultProps = {
  children: undefined,
  withErrorFeedback: false,
  form: {},
  formItemProps: undefined,
  errorsToShow: undefined
};

CoaFormItem.propTypes = {
  children: PropTypes.oneOfType([PropTypes.arrayOf(PropTypes.node), PropTypes.node]),
  withErrorFeedback: PropTypes.bool,
  form: PropTypes.objectOf(PropTypes.any),
  formItemProps: PropTypes.objectOf(PropTypes.any),
  errorsToShow: PropTypes.arrayOf(PropTypes.string),
  name: PropTypes.string.isRequired
};
