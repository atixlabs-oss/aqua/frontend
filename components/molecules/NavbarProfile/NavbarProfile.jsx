/**
 * AGUA Copyright (C) 2023  Atix Labs
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/* eslint-disable
 jsx-a11y/anchor-is-valid,
 jsx-a11y/click-events-have-key-events,
 jsx-a11y/no-static-element-interactions,
 jsx-a11y/interactive-supports-focus,
*/
import React, { useState } from 'react';
import PropTypes from 'prop-types';
import { LogoutOutlined } from '@ant-design/icons';
import { CoaUserAvatar } from '../../atoms/CoaUserAvatar/CoaUserAvatar';
import { useRouter } from 'next/router';
import { useTranslation } from 'react-i18next';

export default function NavbarProfile({ user, removeUser, projectId, role }) {
  const [isAvatarOpen, setIsAvatarOpen] = useState(false);
  const { push, pathname } = useRouter();
  const { t } = useTranslation();

  function toggleAvatarDropdown() {
    setIsAvatarOpen(open => !open);
  }

  const logout = () => {
    removeUser();
    push(`/${pathname.split('/')[1]}`);
  };

  const { firstName, lastName } = user;

  return (
    <div
      className="navbar__right__profile"
      onClick={toggleAvatarDropdown}
      onKeyPress={toggleAvatarDropdown}
      role="button"
    >
      <div className="navbar__user__avatar">
        <CoaUserAvatar firstName={firstName} lastName={lastName} />
      </div>
      <div className="navbar__user">
        <div className="user__details">
          <h2>
            {user.firstName} {user.lastName}
          </h2>
          <span>{role}</span>
        </div>
        <div className="dropdown">
          <img src="/static/images/arrow-down.svg" alt="arrow-down" />
        </div>
        {isAvatarOpen && <div className="navbar__dropdown__mask"></div>}
        <div
          className={`navbar__dropdown ${isAvatarOpen ? '--visible' : '--hidden'}`}
          onClick={logout}
          role="button"
        >
          <LogoutOutlined className="navbar__dropdown__icon" />
          <p className="navbar__dropdown__logout">{t('loginSection.logout')}</p>
        </div>
      </div>
    </div>
  );
}

NavbarProfile.defaultProps = {
  user: null,
  projectId: -1,
  removeUser: () => undefined
};

NavbarProfile.propTypes = {
  projectId: PropTypes.number,
  user: PropTypes.objectOf(PropTypes.any),
  removeUser: PropTypes.func
};
