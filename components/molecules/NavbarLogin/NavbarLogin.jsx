/**
 * AGUA Copyright (C) 2023  Atix Labs
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import React from 'react';
import PropTypes from 'prop-types';
import { CoaTextButton } from 'components/atoms/CoaTextButton/CoaTextButton';
import Styles from './navbar-login.module.scss';
import { useTranslation } from 'react-i18next';

export default function NavbarLogin({ loginFn, ...rest }) {
  const { t } = useTranslation();
  return (
    <div className={Styles.navbarLogin__container} {...rest}>
      <CoaTextButton variant="primary" onClick={loginFn}>
        <span className={Styles.navbarLogin__button__content}>{t('loginSection.login')}</span>
      </CoaTextButton>
    </div>
  );
}

NavbarLogin.defaultProps = {
  loginFn: () => undefined
};

NavbarLogin.propTypes = {
  loginFn: PropTypes.func
};
