/**
 * AGUA Copyright (C) 2023  Atix Labs
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import React from 'react';
import PropTypes from 'prop-types';
import { Col, Progress } from 'antd';

const ProjectMilestonesProgress = ({ projectProgress }) => (
  <Col className="ProjectProgress" span={24}>
    <div className="block">
      <h1 className="title">Milestones Progress</h1>
    </div>
    <Col span={12}>
      <h4>
        Project <strong>Started</strong>
      </h4>
    </Col>
    <Col className="txtright" xs={{ span: 10, offset: 2 }} lg={{ span: 6, offset: 6 }}>
      <h4>
        Project <strong>Finished!</strong>
      </h4>
    </Col>
    <Col xs={2} lg={1}>
      <h4>
        <strong>0%</strong>
      </h4>
    </Col>
    <Col xs={19} lg={21}>
      <Progress strokeColor="#6FCF97" percent={projectProgress} showInfo={false} />
    </Col>
    <Col className="txtright" xs={3} lg={2}>
      <h4>
        <strong>100%</strong>
      </h4>
    </Col>
  </Col>
);

ProjectMilestonesProgress.propTypes = {
  projectProgress: PropTypes.number.isRequired
};

export default ProjectMilestonesProgress;
