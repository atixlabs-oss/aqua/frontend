/**
 * AGUA Copyright (C) 2023  Atix Labs
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import Styles from './statement-pill.module.scss';
import { formatCurrencyAtTheBeginning } from 'helpers/formatter';
import PropTypes from 'prop-types';

export const CoaStatementPill = ({ initialLabel, endLabel, percentage, barColor, currency }) => (
  <>
    <div className={Styles.statementPill__label}>{initialLabel}</div>
    <div
      className={Styles.statementPill__bar}
      style={{ '--barColor': barColor, '--barPercentage': `${percentage}%` }}
    ></div>
    <div className={Styles.statementPill__amount} style={{ '--barColor': barColor }}>
      {formatCurrencyAtTheBeginning(currency, endLabel)}
    </div>
  </>
);

CoaStatementPill.propTypes = {
  initialLabel: PropTypes.string,
  endLabel: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
  percentage: PropTypes.number,
  barColor: PropTypes.string,
  currency: PropTypes.string
};
CoaStatementPill.defaultProps = {
  initialLabel: '',
  endLabel: '',
  percentage: 0,
  className: '',
  barColor: '',
  currency: 'USD'
};
