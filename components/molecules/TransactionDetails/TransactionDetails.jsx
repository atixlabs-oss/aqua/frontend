/**
 * AGUA Copyright (C) 2023  Atix Labs
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import React, { useState } from 'react';
import { CoaButton } from 'components/atoms/CoaButton/CoaButton';
import { BigNumber } from 'ethers';
import PropTypes from 'prop-types';
import { message } from 'antd';
import { getTransactionDetails } from '../../../helpers/getTransactionDetail';
import { CoaLink } from 'components/atoms/CoaLink/CoaLink';
import { useTranslation } from 'react-i18next';

const Loader = () => (
  <div className="sk-chase">
    <div className="sk-chase-dot"></div>
    <div className="sk-chase-dot"></div>
    <div className="sk-chase-dot"></div>
    <div className="sk-chase-dot"></div>
    <div className="sk-chase-dot"></div>
    <div className="sk-chase-dot"></div>
  </div>
);

const Detail = ({ _key, value }) => {
  const { t } = useTranslation();

  const transactionItems = {
    _projectId: t('changelog.transaction_details.projectId'),
    _claimHash: t('changelog.transaction_details.claimHash'),
    _proposalProofHash: t('changelog.transaction_details.proposalProofHash'),
    _proposerAddress: t('changelog.transaction_details.proposerAddress'),
    _authorizationSignature: t('changelog.transaction_details.authorizationSignature'),
    '_audit,auditorEmail,auditorAddress,ipfsHash,approved': t(
      'changelog.transaction_details.auditorData'
    ),
    _proofHash: t('changelog.transaction_details.proofHash'),
    _activityId: t('changelog.transaction_details.activityId'),
    _proposerEmail: t('changelog.transaction_details.proposerEmail'),
    _initialProposalIpfsHash: t('changelog.transaction_details.initialProposalIpfsHash')
  };

  const _value = BigNumber.isBigNumber(value) ? value.toNumber() : value;

  return (
    <div className="transactionDetail__infoContainer__keyValue">
      <div className="transactionDetail__infoContainer__keyValue__item transactionDetail__keyText">
        {transactionItems[_key] ? transactionItems[_key] : _key}
      </div>
      <div className="transactionDetail__infoContainer__keyValue__item transactionDetail__valueText">
        {_key === '_proofHash' ||
        _key.toLowerCase().includes('ipfs') ||
        _key === '_proposalProofHash' ? (
          <CoaLink
            href={{ pathname: `${process.env.NEXT_PUBLIC_IPFS_PROVIDER}/${value}` }}
            className="transactionLink__link"
            target="_blank"
            rel="noopener noreferrer"
          >
            {_value}
          </CoaLink>
        ) : (
          _value
        )}
      </div>
    </div>
  );
};

export const TransactionDetail = ({ action, transactionHash }) => {
  const [txDetail, setTxDetail] = useState(undefined);
  const [isLoading, setIsLoading] = useState(false);
  const [isDetailsButtonAvailable, setIsDetailsButtonAvailable] = useState(true);
  const { t } = useTranslation();

  const handleClickSeeDetails = async () => {
    if (txDetail) return setIsDetailsButtonAvailable(false);

    setIsLoading(true);
    try {
      const _txDetail = await getTransactionDetails({ action, transactionHash });

      setTxDetail(_txDetail);
      setIsDetailsButtonAvailable(false);
    } catch (error) {
      message.error('An error occurred while fetching the transaction detail');
    } finally {
      setIsLoading(false);
    }
  };

  const handleClickHideDetails = () => {
    setIsDetailsButtonAvailable(true);
  };

  return (
    <div className="transactionDetail">
      <CoaButton
        type={isDetailsButtonAvailable ? 'primary' : 'ghost'}
        className="transactionDetail__button"
        disabled={isLoading}
        onClick={isDetailsButtonAvailable ? handleClickSeeDetails : handleClickHideDetails}
      >
        {isDetailsButtonAvailable
          ? t('changelog.transaction_details.see_transaction_details')
          : t('changelog.transaction_details.hide_details')}
      </CoaButton>
      {isLoading && (
        <div className="transactionDetail__loader">
          <Loader />
        </div>
      )}
      {!isDetailsButtonAvailable && txDetail && (
        <div className="transactionDetail__infoContainer">
          {Object.entries(txDetail).map(([_key, value]) => (
            <Detail key={_key} _key={_key} value={value} />
          ))}
        </div>
      )}
    </div>
  );
};

TransactionDetail.defaultProps = {
  action: '',
  transactionHash: ''
};

TransactionDetail.propTypes = {
  action: PropTypes.string,
  transactionHash: PropTypes.string
};

Detail.defaultProps = {
  _key: '',
  value: ''
};

Detail.propTypes = {
  _key: PropTypes.string,
  value: PropTypes.string
};
