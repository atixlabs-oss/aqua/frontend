/**
 * AGUA Copyright (C) 2023  Atix Labs
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/* eslint-disable func-names */

import React, { useState } from 'react';
import { useTranslation } from 'react-i18next';
import PropTypes from 'prop-types';
import _ from 'lodash';
import { Form, Divider, Icon } from 'antd';

import { ERROR_TYPES, KB_FACTOR_CONVERTER, TIMEFRAME_UNITS } from 'constants/constants';
import TitlePage from 'components/atoms/TitlePage/TitlePage';
import { toBase64 } from 'components/utils/FileUtils';
import { formatCurrency, formatTimeframeValue } from 'helpers/formatter';
import { decimalCount, getErrorMessagesFields, getExtensionFromUrl } from 'helpers/utils';
import { CoaTag } from 'components/atoms/CoaTag/CoaTag';
import { CLONE_STATUS_ENUM, CLONE_STATUS_MAP } from 'model/cloneStatus';
import { PROJECT_STATUS_MAP } from 'model/projectStatus';
import { getUsersByRole } from 'helpers/modules/projectUsers';
import { ROLES_IDS } from 'components/organisms/AssignProjectUsers/constants';
import { CoaFormItemInput } from '../CoaFormItems/CoaFormItemInput/CoaFormItemInput';
import { CoaFormItemUpload } from '../CoaFormItems/CoaFormItemUpload/CoaFormItemUpload';
import { CoaFormItemSelect } from '../CoaFormItems/CoaFormItemSelect/CoaFormItemSelect';
import { CoaFormItemInputNumber } from '../CoaFormItems/CoaFormItemInputNumber/CoaFormItemInputNumber';
import { CustomHead } from 'components/atoms/CustomHead/CustomHead';
import { useGetCountriesQuery } from 'api/react-query/countries/countriesQueries';
import { useUpdateBasicInformationMutation } from 'api/react-query/project/projectMutations';

const FormProjectBasicInformationContent = ({ form, project, Footer }) => {
  const { t } = useTranslation();
  const { getFieldsError } = form;

  const updateBasicInformation = useUpdateBasicInformationMutation();
  const { data: countriesAvailable = [], isLoading: areCountriesLoading } = useGetCountriesQuery();

  const [currentBasicInformation, setCurrentBasicInformation] = useState({
    ...project?.basicInformation,
    thumbnailPhoto:
      project?.basicInformation?.thumbnailPhoto &&
      `${process.env.NEXT_PUBLIC_URL_HOST}${project?.basicInformation?.thumbnailPhoto}`
  });

  const projectName = currentBasicInformation?.projectName || t('createProject.projectName');
  const timeframe = currentBasicInformation?.timeframe;
  const timeframeUnit = currentBasicInformation?.timeframeUnit || t('general.months');
  const location = currentBasicInformation?.location;
  const thumbnailPhoto = currentBasicInformation?.thumbnailPhoto;

  const projectStatus = project?.status;
  const beneficiary = getUsersByRole(ROLES_IDS.beneficiary, project?.users)?.[0];
  const beneficiaryFirstName = beneficiary?.firstName;
  const beneficiaryLastName = beneficiary?.lastName;
  const beneficiaryCompleteName =
    beneficiaryFirstName || beneficiaryLastName
      ? `${beneficiaryFirstName} ${beneficiaryLastName}`
      : 'No name';
  const currency = project?.details?.currency || 'USD';
  const budget = project?.budget || 0;
  const cloneStatus = project?.cloneStatus;

  const onSubmit = () =>
    new Promise((resolve, reject) => {
      form.validateFields(async (err, values) => {
        try {
          if (!err) {
            const { thumbnailPhoto: _thumbnailPhoto, ...restValues } = values;
            const valuesProcessed = { ...restValues };

            if (_thumbnailPhoto) valuesProcessed.thumbnailPhoto = _thumbnailPhoto?.file;

            const formData = new FormData();
            Object.keys(valuesProcessed).forEach(key => {
              formData.append(key, valuesProcessed[key]);
            });

            await updateBasicInformation.mutateAsync({ projectId: project?.id, data: formData });

            return resolve();
          }
          return reject({ formError: err });
        } catch (error) {
          reject(error?.errors);
        }
      });
    });

  const uploadProps = {
    name: 'file',
    beforeUpload: () => false,
    accept: '.jpg,.png,.jpeg',
    multiple: false
  };

  const handleThumbnailChange = async value => {
    if (value?.file?.status !== 'removed') {
      const b64Photo = await toBase64(value?.file);
      setCurrentBasicInformation({ ...currentBasicInformation, thumbnailPhoto: b64Photo });
    }
  };

  const handleThumbnailRemove = () => {
    const {
      thumbnailPhoto: _thumbnailPhoto,
      ...restCurrentBasicInformation
    } = currentBasicInformation;
    setCurrentBasicInformation({ ...restCurrentBasicInformation });
  };

  const validateImageSize = async (_rule, value, callback) => {
    if (value?.status === 'removed') return callback();
    if (value) {
      const fileSize = value.size;
      if (fileSize / KB_FACTOR_CONVERTER > 500) return callback(ERROR_TYPES.IMAGE_INVALID);
      return callback();
    }
    return callback();
  };

  const thumbnailRules = initialValue =>
    initialValue
      ? [
          {
            validator: validateImageSize
          }
        ]
      : [
          {
            required: true,
            message: ERROR_TYPES.EMPTY
          },
          {
            validator: validateImageSize
          }
        ];

  const validateCurrencyValue = (_rule, value, callback) => {
    if (value === 0) return callback(ERROR_TYPES.NO_ZERO);
    if (decimalCount(value) > 1) return callback(ERROR_TYPES.MORE_THAN_1_DECIMAL);
    if (value < 0) return callback(ERROR_TYPES.LESS_THAN_ZERO);
    return callback();
  };

  const _cloneStatus = cloneStatus?.toLowerCase();
  const _projectStatus = projectStatus?.toLowerCase();
  const status =
    cloneStatus && cloneStatus !== CLONE_STATUS_ENUM.OPEN_REVIEW
      ? CLONE_STATUS_MAP[_cloneStatus]
      : PROJECT_STATUS_MAP[_projectStatus];
  const statusName = status?.name;
  const statusColor = status?.color;

  return (
    <>
      <CustomHead
        titleText={`${t('general.BackOffice')} - ${t('createProject.Basic information')}`}
      />
      <div className="formProjectBasicInformation__content">
        <div className="formProjectBasicInformation__content__left">
          <TitlePage textTitle={t('createProject.completeBasicInfo')} />
          <div className="formProjectBasicInformation__content__left__preview">
            <h3 className="formProjectBasicInformation__content__left__preview__title">
              {t('createProject.resume')}
            </h3>
            <img
              src={thumbnailPhoto || '/static/images/thumbnail-default.svg'}
              alt="thumbnail"
              className="formProjectBasicInformation__content__left__preview__thumbnailPhoto"
            />
            <div className="formProjectBasicInformation__content__left__preview__info">
              <div className="formProjectBasicInformation__content__left__preview__info__main">
                <h4 className="formProjectBasicInformation__content__left__preview__info__main__title">
                  {projectName || t('createProject.projectName')}
                </h4>

                <CoaTag
                  predefinedColor={statusColor}
                  className="formProjectBasicInformation__left__preview__tag"
                >
                  {statusName}
                </CoaTag>
              </div>
              <div className="formProjectBasicInformation__content__left__preview__info__description">
                <div>
                  <p className="formProjectBasicInformation__content__left__preview__info__description__value">
                    {location || t('header.impact')}
                  </p>
                  <h5 className="formProjectBasicInformation__content__left__preview__info__description__title">
                    {t('general.country')}
                  </h5>
                </div>
                <Divider
                  type="vertical"
                  className="formProjectBasicInformation__content__left__preview__info__divider"
                />
                <div>
                  <p className="formProjectBasicInformation__content__left__preview__info__description__value">
                    {formatTimeframeValue({ timeframe, timeframeUnit, t })}
                  </p>
                  <h5 className="formProjectBasicInformation__content__left__preview__info__description__title">
                    {t('general.time')}
                  </h5>
                </div>
                <Divider
                  type="vertical"
                  className="formProjectBasicInformation__content__left__preview__info__divider"
                />
                <div>
                  <p className="formProjectBasicInformation__content__left__preview__info__description__value">
                    {formatCurrency(currency, budget)}
                  </p>
                  <h5 className="formProjectBasicInformation__content__left__preview__info__description__title">
                    {t('header.budget')}
                  </h5>
                </div>
                <Divider
                  type="vertical"
                  className="formProjectBasicInformation__content__left__preview__info__divider"
                />
                <div>
                  <p className="formProjectBasicInformation__content__left__preview__info__description__value">
                    {beneficiaryCompleteName}
                  </p>
                  <h5 className="formProjectBasicInformation__content__left__preview__info__description__title">
                    {t('header.beneficiary')}
                  </h5>
                </div>
              </div>
            </div>
          </div>
        </div>
        <Divider type="vertical" className="formProjectBasicInformation__content__divider" />
        <Form className="formProjectBasicInformation__content__right">
          <CoaFormItemInput
            withErrorFeedback
            name="projectName"
            form={form}
            formItemProps={{
              label: t('createProject.projectName'),
              className: 'formProjectBasicInformation__content__right__item'
            }}
            fieldDecoratorOptions={{
              rules: [
                {
                  required: true,
                  message: ERROR_TYPES.EMPTY,
                  whitespace: true
                }
              ],
              initialValue: projectName,
              validateTrigger: 'onSubmit'
            }}
            errorsToShow={[]}
            inputProps={{
              maxLength: 50,
              placeholder: 'Input Text Example',
              onChange: ({ currentTarget: { value } }) => {
                setCurrentBasicInformation({
                  ...currentBasicInformation,
                  projectName: value
                });
              }
            }}
          />
          <CoaFormItemSelect
            withErrorFeedback
            form={form}
            name="location"
            formItemProps={{
              label: t('header.impact'),
              className: 'formProjectBasicInformation__content__right__item'
            }}
            errorsToShow={[]}
            fieldDecoratorOptions={{
              rules: [
                {
                  required: true,
                  message: ERROR_TYPES.EMPTY,
                  whitespace: true
                }
              ],
              initialValue: location,
              validateTrigger: 'onSubmit'
            }}
            selectProps={{
              placeholder: t('createProject.selectCountryRegion'),
              loading: areCountriesLoading,
              onChange: value =>
                setCurrentBasicInformation({
                  ...currentBasicInformation,
                  location: value
                }),
              showSearch: true
            }}
            options={countriesAvailable?.map(country => ({
              label: country?.name,
              value: country?.name
            }))}
          />
          <div className="formProjectBasicInformation__content__right__timeframeContainer">
            <CoaFormItemInputNumber
              form={form}
              formItemProps={{
                className: 'formProjectBasicInformation__content__right__item',
                label: t('header.timeframe')
              }}
              errorsToShow={[
                ERROR_TYPES.MORE_THAN_1_DECIMAL,
                ERROR_TYPES.NO_ZERO,
                ERROR_TYPES.LESS_THAN_ZERO
              ]}
              name="timeframe"
              fieldDecoratorOptions={{
                rules: [
                  {
                    required: true,
                    message: ERROR_TYPES.EMPTY
                  },
                  {
                    validator: validateCurrencyValue
                  }
                ],
                validateTrigger: 'onSubmit',
                initialValue: timeframe && parseFloat?.(timeframe)
              }}
              inputNumberProps={{
                step: '0.1',
                placeholder: 0,
                onChange: value =>
                  setCurrentBasicInformation({
                    ...currentBasicInformation,
                    timeframe: value
                  })
              }}
            />

            <CoaFormItemSelect
              name="timeframeUnit"
              form={form}
              formItemProps={{
                className: 'formProjectBasicInformation__content__right__item',
                label: ''
              }}
              fieldDecoratorOptions={{
                initialValue: timeframeUnit,
                rules: [
                  {
                    required: true,
                    message: ERROR_TYPES.EMPTY,
                    whitespace: true
                  }
                ],
                validateTrigger: 'onSubmit'
              }}
              selectProps={{
                placeholder: t('general.type'),
                onChange: value =>
                  setCurrentBasicInformation({
                    ...currentBasicInformation,
                    timeframeUnit: value
                  })
              }}
              options={TIMEFRAME_UNITS.map(item => ({
                label: t(`general.${item.value}`) || item.label,
                value: item.value
              }))}
            />
          </div>

          <CoaFormItemUpload
            buttonContent={
              <div className="formProjectBasicInformation__content__right__uploadItemContainer__buttonContent">
                <Icon type="upload" />
                {t('general.uploadClick')}
              </div>
            }
            contentContainerClassName="formProjectBasicInformation__content__right__uploadItemContainer"
            form={form}
            formItemProps={{
              label: t('createProject.thumbnailImage'),
              className: 'formProjectBasicInformation__content__right__item'
            }}
            fieldDecoratorOptions={{
              rules: thumbnailRules(thumbnailPhoto),
              validateTrigger: 'onSubmit'
            }}
            name="thumbnailPhoto"
            errorsToShow={[ERROR_TYPES.IMAGE_INVALID]}
            initialValue={
              thumbnailPhoto
                ? [
                    {
                      uid: _.uniqueId(),
                      url: thumbnailPhoto,
                      name: `thumbnail-image.${getExtensionFromUrl(thumbnailPhoto)}`
                    }
                  ]
                : []
            }
            buttonType="ghost"
            onChange={handleThumbnailChange}
            onRemove={handleThumbnailRemove}
            uploadProps={uploadProps}
            Note={
              <ul className="formProjectBasicInformation__content__right__uploadItemContainer__note">
                <li>{t('createProject.recommendedDimension')}: 1400x720px. </li>
                <li>
                  {t('general.formats')}: PNG {t('general.or')} JPG.
                </li>
                <li>{t('general.maxSize')}: 500kb.</li>
              </ul>
            }
          />
        </Form>
      </div>
      {Footer({
        onSubmit,
        errors: getErrorMessagesFields(getFieldsError(), [ERROR_TYPES.EMPTY])
      })}
    </>
  );
};

export const FormProjectBasicInformation = Form.create({
  name: 'FormProjectBasicInformation'
})(FormProjectBasicInformationContent);

FormProjectBasicInformationContent.defaultProps = {
  form: () => undefined,
  Footer: undefined
};

FormProjectBasicInformationContent.propTypes = {
  form: PropTypes.objectOf(PropTypes.any),
  project: PropTypes.shape({
    details: PropTypes.shape({
      problemAddressed: PropTypes.string,
      mission: PropTypes.string,
      currencyType: PropTypes.string,
      currency: PropTypes.string,
      additionalCurrencyInformation: PropTypes.string
    })
  }).isRequired,
  Footer: PropTypes.oneOfType([PropTypes.arrayOf(PropTypes.node), PropTypes.node])
};
