/**
 * AGUA Copyright (C) 2023  Atix Labs
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import React from 'react';
import PropTypes from 'prop-types';
import { CRYPTO_CURRENCY_PATH_SCANNER } from '../../../constants/constants';
import { links } from '../../../constants/links';
import { CoaLink } from 'components/atoms/CoaLink/CoaLink';
import { useTranslation } from 'react-i18next';

const TransactionLink = props => {
  const { t } = useTranslation();
  const { txHash, currency, showTitle, isChangelogActive } = props;

  const txPathInChangelog = links.transaction(txHash);

  const txPath = isChangelogActive
    ? txPathInChangelog
    : CRYPTO_CURRENCY_PATH_SCANNER[currency](txHash);

  return (
    <>
      {showTitle && <h5 className="transactionLink__title">{t('general.transaction')}</h5>}
      <div className="transactionLink__linkContainer">
        <span className="transactionLink__linkContainer__title">Hash:&nbsp;</span>
        <CoaLink
          href={{ pathname: txPath }}
          target="_blank"
          className="transactionLink__link"
          rel="noopener noreferrer"
        >
          {txHash}
        </CoaLink>
      </div>
    </>
  );
};

TransactionLink.defaultProps = {
  txHash: '',
  currency: '',
  showTitle: false
};

TransactionLink.propTypes = {
  txHash: PropTypes.string,
  currency: PropTypes.string,
  showTitle: PropTypes.bool
};

export default TransactionLink;
