/**
 * AGUA Copyright (C) 2023  Atix Labs
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import React from 'react';
import PropTypes from 'prop-types';

import { CoaTag } from 'components/atoms/CoaTag/CoaTag';
import { PROJECT_STATUS_MAP } from 'model/projectStatus';
import { CLONE_STATUS_MAP } from 'model/cloneStatus';
import BlockIcon from '../../atoms/BlockIcon/BlockIcon';

const ProjectStatus = ({ blockchainHistoryUrl, cloneStatus, projectStatus }) => {
  const _cloneStatus = cloneStatus?.toLowerCase();
  const _projectStatus = projectStatus?.toLowerCase();
  const status = cloneStatus ? CLONE_STATUS_MAP[_cloneStatus] : PROJECT_STATUS_MAP[_projectStatus];
  const statusName = status?.name;
  const statusColor = status?.color;

  return (
    <div className="status">
      <CoaTag predefinedColor={statusColor}>{statusName}</CoaTag>
      {blockchainHistoryUrl && <BlockIcon url={blockchainHistoryUrl} />}
    </div>
  );
};

export default ProjectStatus;

ProjectStatus.defaultProps = {
  blockchainHistoryUrl: undefined
};

ProjectStatus.propTypes = {
  blockchainHistoryUrl: PropTypes.string
};
