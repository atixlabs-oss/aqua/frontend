/**
 * AGUA Copyright (C) 2023  Atix Labs
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import React, { useEffect, useState } from 'react';
import { useTranslation } from 'react-i18next';
import PropTypes from 'prop-types';
import { Form, Icon, Input } from 'antd';
import WAValidator from 'multicoin-address-validator';

import { ERROR_TYPES, MB_FACTOR_CONVERTER } from 'constants/constants';
import TitlePage from 'components/atoms/TitlePage/TitlePage';
import { toBase64 } from 'components/utils/FileUtils';
import _ from 'lodash';
import { getErrorMessagesFields, getExtensionFromUrl } from 'helpers/utils';
import { formatCurrency } from 'helpers/formatter';
import { CoaFormItemTextArea } from '../CoaFormItems/CoaFormItemTextArea/CoaFormItemTextArea';
import { CoaFormItemSelect } from '../CoaFormItems/CoaFormItemSelect/CoaFormItemSelect';
import { CoaFormItemUpload } from '../CoaFormItems/CoaFormItemUpload/CoaFormItemUpload';
import { CoaFormItemInput } from '../CoaFormItems/CoaFormItemInput/CoaFormItemInput';
import { CustomHead } from 'components/atoms/CustomHead/CustomHead';
import { useGetCurrenciesQuery } from 'api/react-query/currencies/currenciesQueries';
import { useUpdateProjectDetailsMutation } from 'api/react-query/project/projectMutations';

const FormProjectDetailContent = ({ form, project, Footer, isACloneBeingEdited }) => {
  const { t } = useTranslation();
  const updateProjectDetailsMutation = useUpdateProjectDetailsMutation();
  const { setFieldsValue, getFieldsError, getFieldValue } = form;
  const [currentCurrencyType, setCurrentCurrencyType] = useState();
  const [currentFiles, setCurrentFiles] = useState();
  const { data: currencies = {}, isFetching: areCurrenciesFetching } = useGetCurrenciesQuery();

  const { budget, type } = project;

  const {
    problemAddressed,
    mission,
    currencyType,
    currency,
    additionalCurrencyInformation,
    legalAgreementFile,
    projectProposalFile
  } = project?.details || {};

  const legalAgreementFileCompletePath =
    legalAgreementFile && `${process.env.NEXT_PUBLIC_URL_HOST}${legalAgreementFile}`;
  const projectProposalFileCompletePath =
    projectProposalFile && `${process.env.NEXT_PUBLIC_URL_HOST}${projectProposalFile}`;

  const currentCurrency = getFieldValue('currency');

  useEffect(() => {
    if (currencyType) {
      setCurrentCurrencyType(currencyType.toLowerCase());
    }
  }, [currencyType]);

  const onSubmit = () =>
    new Promise((resolve, reject) => {
      form.validateFields(async (err, values) => {
        try {
          if (!err) {
            const {
              projectProposalFile: _projectProposalFile,
              legalAgreementFile: _legalAgreementFile,
              ...restValues
            } = values;
            const valuesProcessed = {
              ...restValues
            };

            if (_projectProposalFile)
              valuesProcessed.projectProposalFile = _projectProposalFile.file;
            if (_legalAgreementFile) valuesProcessed.legalAgreementFile = _legalAgreementFile.file;

            const formData = new FormData();

            Object.keys(valuesProcessed).forEach(key => {
              formData.append(key, valuesProcessed[key]);
            });

            await updateProjectDetailsMutation.mutateAsync({
              projectId: project?.id,
              data: formData
            });

            resolve();
          }
          return reject({ formError: err });
        } catch (error) {
          reject(error?.errors);
        }
      });
    });

  const validateFileSize = (_rule, value, callback) => {
    if (value?.file.size === 'removed') return callback();
    if (value?.file.size / MB_FACTOR_CONVERTER > 20) return callback(ERROR_TYPES.INVALID_FILE);
    return callback();
  };

  const validateWalletAddress = (_rule, value, callback) => {
    const _currentCurrency = currentCurrency?.toLowerCase();
    if (['rbtc', 'doc', 'rdoc'].includes(_currentCurrency) && !WAValidator.validate(value, 'btc'))
      return callback(ERROR_TYPES.INVALID_WALLET_ADDRESS);
    if (_currentCurrency === 'eth' && !WAValidator.validate(value, 'eth'))
      return callback(ERROR_TYPES.INVALID_WALLET_ADDRESS);
    if (_currentCurrency === 'usdt' && !WAValidator.validate(value, 'usdt'))
      return callback(ERROR_TYPES.INVALID_WALLET_ADDRESS);
    if (_currentCurrency === 'etc' && !WAValidator.validate(value, 'etc'))
      return callback(ERROR_TYPES.INVALID_WALLET_ADDRESS);
    return callback();
  };

  const uploadProps = {
    name: 'file',
    beforeUpload: () => false,
    accept: '.pdf',
    multiple: false
  };

  const filesRules = initialValue =>
    initialValue
      ? [
          {
            validator: validateFileSize
          }
        ]
      : [
          {
            required: true,
            message: ERROR_TYPES.EMPTY_FILE
          },
          {
            validator: validateFileSize
          }
        ];

  const handleFileChange = async (value, field) => {
    if (value?.file?.status !== 'removed') {
      const b64Photo = await toBase64(value?.file);
      setCurrentFiles({ ...currentFiles, [field]: b64Photo });
    }
  };

  const handleFileRemove = field => {
    const { [field]: dynamicField, ...restCurrentFiles } = currentFiles;
    setCurrentFiles({ ...restCurrentFiles });
  };

  return (
    <>
      <CustomHead
        titleText={`${t('general.BackOffice')} - ${t('createProject.Project details')}`}
      />
      <div className="formProjectDetail__content">
        <TitlePage textTitle={t('createProject.completeProjectDetails')} />
        <Form className="formProjectDetail__content__form">
          <div className="formProjectDetail__content__form__row">
            <CoaFormItemTextArea
              form={form}
              errorsToShow={[]}
              name="problemAddressed"
              formItemProps={{
                label: t('createProject.aboutTheProject')
              }}
              Note={
                <p className="formProjectDetail__content__form__row__note">
                  {t('createProject.shareInfo') ||
                    'Share your information about the entrepreneurs and the project'}
                </p>
              }
              fieldDecoratorOptions={{
                rules: [
                  {
                    required: true,
                    message: ERROR_TYPES.EMPTY,
                    whitespace: true
                  }
                ],
                initialValue: problemAddressed
              }}
              inputTextAreaProps={{
                placeholder: '',
                maxLength: 500,
                showCount: true,
                autosize: {
                  minRows: 8
                }
              }}
            />
            <CoaFormItemTextArea
              form={form}
              errorsToShow={[]}
              name="mission"
              formItemProps={{
                label: t('createProject.missionVision')
              }}
              Note={
                <p className="formProjectDetail__content__form__row__note">
                  {t('createProject.shareProjectMission')}
                </p>
              }
              fieldDecoratorOptions={{
                rules: [
                  {
                    required: true,
                    message: ERROR_TYPES.EMPTY,
                    whitespace: true
                  }
                ],
                initialValue: mission
              }}
              inputTextAreaProps={{
                placeholder: '',
                maxLength: 500,
                showCount: true,
                autosize: {
                  minRows: 8
                }
              }}
            />
          </div>
          <div className="formProjectDetail__content__form__row">
            <CoaFormItemSelect
              form={form}
              errorsToShow={[]}
              name="type"
              formItemProps={{ label: t('general.projectType') }}
              fieldDecoratorOptions={{
                rules: [
                  {
                    required: true,
                    message: ERROR_TYPES.EMPTY,
                    whitespace: true
                  }
                ],
                initialValue: type
              }}
              selectProps={{
                placeholder: t('createProject.selectCurrencyType') || 'Select project type',
                disabled: isACloneBeingEdited
              }}
              options={[
                { label: t('general.grant'), value: 'grant' },
                { label: t('general.loan'), value: 'loan' }
              ]}
            />
          </div>
          <div className="formProjectDetail__content__form__row">
            <CoaFormItemSelect
              form={form}
              errorsToShow={[]}
              name="currencyType"
              formItemProps={{ label: t('createProject.currencyType') }}
              fieldDecoratorOptions={{
                rules: [
                  {
                    required: true,
                    message: ERROR_TYPES.EMPTY,
                    whitespace: true
                  }
                ],
                initialValue: currencyType
              }}
              selectProps={{
                placeholder: t('createProject.selectCurrencyType') || 'Select currency type',
                onChange: value => {
                  setCurrentCurrencyType(value?.toLowerCase());
                  if (value !== currentCurrencyType)
                    setFieldsValue({
                      currency: null,
                      additionalCurrencyInformation: ''
                    });
                },
                disabled: isACloneBeingEdited || areCurrenciesFetching,
                loading: areCurrenciesFetching
              }}
              options={[{ label: 'FIAT', value: 'Fiat' }, { label: 'CRYPTO', value: 'Crypto' }]}
            />
            <CoaFormItemSelect
              form={form}
              errorsToShow={[]}
              name="currency"
              formItemProps={{ label: t('general.currency') }}
              fieldDecoratorOptions={{
                rules: [
                  {
                    required: true,
                    message: ERROR_TYPES.EMPTY,
                    whitespace: true
                  }
                ],
                initialValue: currency
              }}
              selectProps={{
                placeholder: t('createProject.selectCurrency'),
                disabled: isACloneBeingEdited || areCurrenciesFetching,
                loading: areCurrenciesFetching
              }}
              options={currencies[currentCurrencyType]}
            />
          </div>

          <div className="formProjectDetail__content__form__row">
            <div>
              {!currentCurrencyType && (
                <>
                  <p className="formProjectDetail__content__form__row__label">
                    {t('createProject.accountInfo')}
                  </p>
                  <p className="formProjectDetail__content__form__row__note">
                    {t('createProject.firstSelect')}
                  </p>
                </>
              )}
              {currentCurrencyType === 'fiat' && (
                <CoaFormItemTextArea
                  form={form}
                  errorsToShow={[]}
                  name="additionalCurrencyInformation"
                  formItemProps={{
                    label: t('createProject.accountInfo')
                  }}
                  Note={
                    <p className="formProjectDetail__content__form__row__note">
                      {t('createProject.fillAccountInfo')}
                    </p>
                  }
                  fieldDecoratorOptions={{
                    rules: [
                      {
                        required: true,
                        message: ERROR_TYPES.EMPTY,
                        whitespace: true
                      }
                    ],
                    initialValue: additionalCurrencyInformation
                  }}
                  inputTextAreaProps={{
                    placeholder: '',
                    disabled: isACloneBeingEdited
                  }}
                />
              )}
              {currentCurrencyType === 'crypto' && (
                <CoaFormItemInput
                  form={form}
                  errorsToShow={[ERROR_TYPES.INVALID_WALLET_ADDRESS]}
                  name="additionalCurrencyInformation"
                  formItemProps={{
                    label: t('general.address')
                  }}
                  Note={
                    <p className="formProjectDetail__content__form__row__note">
                      {t('createProject.enterWallet')}
                    </p>
                  }
                  fieldDecoratorOptions={{
                    rules: [
                      {
                        required: true,
                        message: ERROR_TYPES.EMPTY,
                        whitespace: true
                      },
                      {
                        validator: validateWalletAddress
                      }
                    ],
                    initialValue: additionalCurrencyInformation,
                    validateTrigger: 'onSubmit'
                  }}
                  inputTextAreaProps={{
                    placeholder: t('general.address'),
                    disabled: isACloneBeingEdited
                  }}
                />
              )}
            </div>
            <Form.Item label={t('header.budget') || 'Budget'}>
              <p className="formProjectDetail__content__form__row__note">
                {t('createProject.sumRecorded')}
                ed
              </p>
              <Input placeholder="0.00" disabled value={formatCurrency(currency, budget)} />
            </Form.Item>
          </div>
          <div className="formProjectDetail__content__form__row">
            <CoaFormItemUpload
              buttonContent={
                <div className="formProjectDetail__content__form__row__uploadContainer__buttonContent">
                  <Icon type="upload" />
                  {t('createProject.uploadLegalAgreement')}
                </div>
              }
              contentContainerClassName="formProjectDetail__content__form__row__uploadContainer"
              form={form}
              name="legalAgreementFile"
              formItemProps={{
                label: ''
              }}
              errorsToShow={[ERROR_TYPES.EMPTY_FILE, ERROR_TYPES.INVALID_FILE]}
              fieldDecoratorOptions={{
                rules: filesRules(legalAgreementFile),
                validateTrigger: 'onSubmit'
              }}
              initialValue={
                legalAgreementFileCompletePath
                  ? [
                      {
                        uid: _.uniqueId(),
                        url: legalAgreementFileCompletePath,
                        name: `legal-agreement.${getExtensionFromUrl(
                          legalAgreementFileCompletePath
                        )}`
                      }
                    ]
                  : []
              }
              buttonType="primary"
              onChange={value => handleFileChange(value, 'legalAgreementFile')}
              onRemove={() => handleFileRemove('legalAgreementFile')}
              uploadProps={uploadProps}
              Note={
                <span>
                  {t('createProject.recommendedFiles')}. {t('general.format')}: PDF,{' '}
                  {t('general.upTo')} 20 MB.
                </span>
              }
            />

            <CoaFormItemUpload
              buttonContent={
                <div className="formProjectDetail__content__form__row__uploadContainer__buttonContent">
                  <Icon type="upload" />
                  {t('createProject.uploadProposal')}
                </div>
              }
              contentContainerClassName="formProjectDetail__content__form__row__uploadContainer"
              form={form}
              name="projectProposalFile"
              formItemProps={{
                label: ''
              }}
              errorsToShow={[ERROR_TYPES.EMPTY_FILE, ERROR_TYPES.INVALID_FILE]}
              fieldDecoratorOptions={{
                rules: filesRules(projectProposalFile),
                validateTrigger: 'onSubmit'
              }}
              initialValue={
                projectProposalFileCompletePath
                  ? [
                      {
                        uid: _.uniqueId(),
                        url: projectProposalFileCompletePath,
                        name: `project-proposal.${getExtensionFromUrl(
                          projectProposalFileCompletePath
                        )}`
                      }
                    ]
                  : []
              }
              buttonType="primary"
              onChange={value => handleFileChange(value, 'projectProposalFile')}
              onRemove={() => handleFileRemove('projectProposalFile')}
              uploadProps={uploadProps}
              Note={
                <span>
                  {t('createProject.recommendedFiles')}. {t('general.format')}: PDF,{' '}
                  {t('general.upTo')} 20 MB.
                </span>
              }
            />
          </div>
        </Form>
      </div>
      {Footer({ errors: getErrorMessagesFields(getFieldsError(), [ERROR_TYPES.EMPTY]), onSubmit })}
    </>
  );
};

export const FormProjectDetail = Form.create({ name: 'FormProjectDetail' })(
  FormProjectDetailContent
);

FormProjectDetailContent.defaultProps = {
  form: () => undefined,
  Footer: undefined
};

FormProjectDetailContent.propTypes = {
  project: PropTypes.shape({
    details: PropTypes.shape({
      problemAddressed: PropTypes.string,
      mission: PropTypes.string,
      currencyType: PropTypes.string,
      currency: PropTypes.string,
      additionalCurrencyInformation: PropTypes.string
    })
  }).isRequired,
  form: PropTypes.objectOf(PropTypes.any),
  Footer: PropTypes.oneOfType([PropTypes.arrayOf(PropTypes.node), PropTypes.node])
};
