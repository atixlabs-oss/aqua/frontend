import { Collapse, Divider, Icon } from 'antd';
import PropTypes from 'prop-types';

import Styles from './custom-simple-collapse.module.scss';
import classNames from 'classnames';

const { Panel } = Collapse;

export const CustomSimpleCollapse = ({ className, title, content }) => (
  <Collapse
    bordered={false}
    expandIcon={({ isActive }) => <Icon type={isActive ? 'caret-up' : 'caret-down'} />}
    className={classNames(Styles.collapse, className)}
  >
    <Panel
      header={<div className={Styles.collapse__header}>{title}</div>}
      key="1"
      extra={<Divider className={Styles.divider} />}
    >
      {content}
    </Panel>
  </Collapse>
);

CustomSimpleCollapse.propTypes = {
  className: PropTypes.string,
  title: PropTypes.oneOfType([PropTypes.elementType, PropTypes.string]),
  content: PropTypes.oneOfType([PropTypes.elementType, PropTypes.string])
};
