/**
 * AGUA Copyright (C) 2023  Atix Labs
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import React from 'react';
import { Divider } from 'antd';

import classNames from 'classnames';
import PropTypes from 'prop-types';
import { formatCurrency } from 'helpers/formatter';
import { useTranslation } from 'react-i18next';

export const CoaActivityIndicators = ({
  withEvidences,
  transferQuantity,
  impactQuantity,
  predefinedColor,
  budget,
  currency,
  current
}) => {
  const { t } = useTranslation('common');
  return (
    <div className="m-coaMilestoneIndicators">
      <div
        className={classNames(
          'm-coaMilestoneIndicators__indicatorContainer',
          `--${predefinedColor}`
        )}
      >
        <div className="m-coaMilestoneIndicators__indicators">
          <div>
            <h4 className="m-coaMilestoneIndicators__indicators__title">
              {t('coaIndicator.committed')}
            </h4>
            <p className="m-coaMilestoneIndicators__indicators__value">
              {formatCurrency(currency, budget)}
            </p>
          </div>
          <div>
            <h4 className="m-coaMilestoneIndicators__indicators__title">
              {t('coaIndicator.current')}
            </h4>
            <p className="m-coaMilestoneIndicators__indicators__value">
              {formatCurrency(currency, current)}
            </p>
          </div>
        </div>
      </div>
      {withEvidences && (
        <div
          className={classNames(
            'm-coaMilestoneIndicators__indicatorContainer',
            `--${predefinedColor}`
          )}
        >
          <h4 className="m-coaMilestoneIndicators__indicators__title">
            {t('coaIndicator.evidences')}
          </h4>
          <p className="m-coaMilestoneIndicators__indicators__value">
            {transferQuantity} {t('coaIndicator.transfer')} <Divider type="vertical" />
            {impactQuantity} {t('coaIndicator.impact')}
          </p>
        </div>
      )}
    </div>
  );
};

CoaActivityIndicators.defaultProps = {
  predefinedColor: 'green',
  withEvidences: false
};

CoaActivityIndicators.propTypes = {
  predefinedColor: PropTypes.string,
  withEvidences: PropTypes.bool
};
