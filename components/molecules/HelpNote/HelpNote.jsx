/**
 * AGUA Copyright (C) 2023  Atix Labs
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import { Icon } from 'antd';
import React from 'react';
import PropTypes from 'prop-types';

import { CustomTooltip } from 'components/atoms/CustomTooltip/CustomTooltip';
import Styles from './help-note.module.scss';

export const HelpNote = ({ Note, TooltipNote, tooltipPlacement }) => (
  <div className={Styles.container}>
    <span className={Styles.container__text}>{Note}</span>
    <CustomTooltip title={TooltipNote} variant="black" placement={tooltipPlacement}>
      <Icon type="question-circle" theme="filled" />
    </CustomTooltip>
  </div>
);

HelpNote.propTypes = {
  Note: PropTypes.elementType,
  TooltipNote: PropTypes.elementType,
  tooltipPlacement: PropTypes.string
};
