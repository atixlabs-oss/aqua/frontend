/**
 * AGUA Copyright (C) 2023  Atix Labs
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import { Icon } from 'antd';
import classNames from 'classnames';
import { formatCurrency } from 'helpers/formatter';
import { PROJECT_TYPE_ENUM } from 'model/projectType';
import React from 'react';
import { useTranslation } from 'react-i18next';

export const CoaMilestoneIndicators = ({
  funding,
  spending,
  payback,
  currency,
  transferQuantity,
  impactQuantity,
  withEvidences,
  projectType
}) => {
  const { t } = useTranslation('common');
  const { budget: fundingBudget, current: fundingCurrent } = funding || {};
  const { budget: spendingBudget, current: spendingCurrent } = spending || {};
  const { budget: paybackBudget, current: paybackCurrent } = payback || {};
  return (
    <div className="m-coaMilestoneIndicators">
      <div className={classNames('m-coaMilestoneIndicators__indicatorContainer', '--green')}>
        <h3 className={classNames('m-coaMilestoneIndicators__title', '--green')}>
          {t('coaIndicator.funding')}
        </h3>
        <div className="m-coaMilestoneIndicators__indicators">
          <div>
            <h4 className="m-coaMilestoneIndicators__indicators__title">
              {t('coaIndicator.budget')}
            </h4>
            <p className="m-coaMilestoneIndicators__indicators__value">
              {formatCurrency(currency, fundingBudget || 0)}
            </p>
          </div>
          <div>
            <h4 className="m-coaMilestoneIndicators__indicators__title">
              {t('coaIndicator.current')}
            </h4>
            <p className="m-coaMilestoneIndicators__indicators__value">
              {formatCurrency(currency, fundingCurrent || 0)}
            </p>
          </div>
        </div>
      </div>
      <div className={classNames('m-coaMilestoneIndicators__indicatorContainer', '--orange')}>
        <h3 className={classNames('m-coaMilestoneIndicators__title', '--orange')}>
          {t('coaIndicator.spending')}
        </h3>
        <div className="m-coaMilestoneIndicators__indicators">
          <div>
            <h4 className="m-coaMilestoneIndicators__indicators__title">
              {t('coaIndicator.budget')}
            </h4>
            <p className="m-coaMilestoneIndicators__indicators__value">
              {formatCurrency(currency, spendingBudget || 0)}
            </p>
          </div>
          <div>
            <h4 className="m-coaMilestoneIndicators__indicators__title">
              {t('coaIndicator.current')}
            </h4>
            <p className="m-coaMilestoneIndicators__indicators__value">
              {formatCurrency(currency, spendingCurrent || 0)}
            </p>
          </div>
        </div>
      </div>
      {projectType === PROJECT_TYPE_ENUM.LOAN && (
        <div className={classNames('m-coaMilestoneIndicators__indicatorContainer', '--violet')}>
          <h3 className={classNames('m-coaMilestoneIndicators__title', '--violet')}>
            {t('coaIndicator.payback')}
          </h3>
          <div className="m-coaMilestoneIndicators__indicators">
            <div>
              <h4 className="m-coaMilestoneIndicators__indicators__title">
                {t('coaIndicator.budget')}
              </h4>
              <p className="m-coaMilestoneIndicators__indicators__value">
                {formatCurrency(currency, paybackBudget || 0)}
              </p>
            </div>
            <div>
              <h4 className="m-coaMilestoneIndicators__indicators__title">
                {t('coaIndicator.current')}
              </h4>
              <p className="m-coaMilestoneIndicators__indicators__value">
                {formatCurrency(currency, paybackCurrent || 0)}
              </p>
            </div>
          </div>
        </div>
      )}
      {withEvidences && (
        <div className="m-coaMilestoneIndicators__evidenceContainer">
          <h3 className="m-coaMilestoneIndicators__evidenceContainer__title">
            {t('coaIndicator.evidences')}
          </h3>
          <p className="m-coaMilestoneIndicators__evidenceContainer__item">
            <Icon type="right" style={{ color: '#4C7FF7' }} />{' '}
            <span className="m-coaMilestoneIndicators__evidenceContainer__item__value">
              {transferQuantity}
            </span>{' '}
            {t('coaIndicator.transfer')}
          </p>
          <p className="m-coaMilestoneIndicators__evidenceContainer__item">
            <Icon type="right" style={{ color: '#4C7FF7' }} />{' '}
            <span className="m-coaMilestoneIndicators__evidenceContainer__item__value">
              {impactQuantity}
            </span>{' '}
            {t('coaIndicator.impact')}
          </p>
        </div>
      )}
    </div>
  );
};
