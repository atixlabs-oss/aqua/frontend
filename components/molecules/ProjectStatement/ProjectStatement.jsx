/**
 * AGUA Copyright (C) 2023  Atix Labs
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import React from 'react';
import PropTypes from 'prop-types';
import { CoaCard } from 'components/atoms/CoaCard/CoaCard';
import { CoaStatementPill } from 'components/molecules/CoaStatementPill/CoaStatementPill';
import Styles from './project-statement.module.scss';
import { useTranslation } from 'react-i18next';

const budgetColor = '#5CAEFA';
const fundingColor = '#58C984';
const spendingColor = '#FFAA29';
const paybackColor = '#BC66FF';

export const ProjectStatement = ({ budget, funding, spending, payback, currency, showPayback }) => {
  const { t } = useTranslation();
  const maxNumber = Math.max(budget, funding, spending, payback);
  const getPercentage = data => (data / maxNumber) * 100;
  return (
    <CoaCard>
      <h2 className={Styles.projectStatement__title}>{t('general.projectStatement')}</h2>
      <div className={Styles.projectStatement__pillsContainer}>
        <CoaStatementPill
          initialLabel={t('header.budget')}
          endLabel={budget}
          percentage={getPercentage(budget)}
          barColor={budgetColor}
          currency={currency}
        />
        <CoaStatementPill
          initialLabel={t('general.funding')}
          endLabel={funding}
          percentage={getPercentage(funding)}
          barColor={fundingColor}
          currency={currency}
        />
        <CoaStatementPill
          initialLabel={t('general.spending')}
          endLabel={spending}
          percentage={getPercentage(spending)}
          barColor={spendingColor}
          currency={currency}
        />
        {showPayback && (
          <CoaStatementPill
            initialLabel={t('general.payback')}
            endLabel={payback}
            percentage={getPercentage(payback)}
            barColor={paybackColor}
            currency={currency}
          />
        )}
      </div>
    </CoaCard>
  );
};

ProjectStatement.propTypes = {
  budget: PropTypes.number,
  funding: PropTypes.number,
  spending: PropTypes.number,
  payback: PropTypes.number,
  currency: PropTypes.string,
  showPayback: PropTypes.bool
};

ProjectStatement.defaultProps = {
  budget: 0,
  funding: 0,
  spending: 0,
  payback: 0,
  currency: 'USD',
  showPayback: false
};
