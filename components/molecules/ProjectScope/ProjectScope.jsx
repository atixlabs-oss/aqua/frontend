/**
 * AGUA Copyright (C) 2023  Atix Labs
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import React from 'react';
import { ProgressBar } from 'components/atoms/ProgressBar/ProgressBar';
import { CoaCard } from 'components/atoms/CoaCard/CoaCard';
import PropTypes from 'prop-types';
import { useTranslation } from 'react-i18next';

export const ProjectScope = ({ milestonesProgressPercentage, activityProgressPercentage }) => {
  const { t } = useTranslation();
  return (
    <CoaCard className="projectScope">
      <h2 className="projectScope__title">{t('general.projectScope')}</h2>
      <div>
        <h3 className="projectScope__subtitle">
          {t('landingProjectProgress.milestone') || 'Milestones Progress'}
        </h3>
        <ProgressBar
          currentPercentage={milestonesProgressPercentage}
          endLabel={`${milestonesProgressPercentage.toFixed(2)}%`}
        />
      </div>
      <div>
        <h3 className="projectScope__subtitle">
          {t('general.activitiesProgress') || 'Activities Progress'}
        </h3>
        <ProgressBar
          currentPercentage={activityProgressPercentage}
          endLabel={`${activityProgressPercentage.toFixed(2)}%`}
        />
      </div>
    </CoaCard>
  );
};

ProjectScope.propTypes = {
  milestonesProgressPercentage: PropTypes.number,
  activityProgressPercentage: PropTypes.number
};

ProjectScope.defaultProps = { milestonesProgressPercentage: 0, activityProgressPercentage: 0 };
