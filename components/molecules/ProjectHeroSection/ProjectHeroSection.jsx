/**
 * AGUA Copyright (C) 2023  Atix Labs
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import React from 'react';
import PropTypes from 'prop-types';
import ProjectStatus from '../ProjectStatus/ProjectStatus';
import ProjectHeroDetails from '../ProjectHeroDetails/ProjectHeroDetails';
import ProjectHeroDownload from '../ProjectHeroDownload/ProjectHeroDownload';

const ProjectHeroSection = ({
  status,
  subtitle,
  title,
  country,
  timeframe,
  budget,
  beneficiary,
  projectProposalUrl,
  legalAgreementUrl,
  blockchainHistoryUrl,
  revision,
  cloneStatus
}) => (
  <div className="hero">
    <div className="content">
      <ProjectStatus
        cloneStatus={cloneStatus}
        projectStatus={status}
        blockchainHistoryUrl={blockchainHistoryUrl}
      />
      <div className="text">
        <h3>{subtitle}</h3>
        <h1>{title}</h1>
      </div>
    </div>

    <div className="bottom">
      <div className="backoffice">
        <ProjectHeroDetails
          country={country}
          timeframe={timeframe}
          budget={budget}
          beneficiary={beneficiary}
          revision={revision}
        />
        {projectProposalUrl && legalAgreementUrl && (
          <ProjectHeroDownload
            projectProposalUrl={projectProposalUrl}
            legalAgreementUrl={legalAgreementUrl}
          />
        )}
      </div>
    </div>
  </div>
);

export default ProjectHeroSection;

ProjectHeroSection.defaultProps = {
  subtitle: undefined,
  country: 'Thailand',
  timeframe: '2 Months',
  budget: '$ 48,000',
  beneficiary: 'Joe Demin',
  projectProposalUrl: undefined,
  legalAgreementUrl: undefined,
  blockchainHistoryUrl: undefined
};

ProjectHeroSection.propTypes = {
  status: PropTypes.string.isRequired,
  subtitle: PropTypes.string,
  title: PropTypes.string.isRequired,
  country: PropTypes.string,
  timeframe: PropTypes.string,
  budget: PropTypes.string,
  beneficiary: PropTypes.string,
  projectProposalUrl: PropTypes.string,
  legalAgreementUrl: PropTypes.string,
  blockchainHistoryUrl: PropTypes.string
};
