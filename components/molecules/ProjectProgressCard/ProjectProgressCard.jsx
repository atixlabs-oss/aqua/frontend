/**
 * AGUA Copyright (C) 2023  Atix Labs
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import React from 'react';
import PropTypes from 'prop-types';
import { CoaButton } from 'components/atoms/CoaButton/CoaButton';

import TitlePage from 'components/atoms/TitlePage/TitlePage';
import { RadialBarChart } from '../RadialBarChart/RadialBarChart';
import { useTranslation } from 'react-i18next';

export const ProjectProgressCard = ({
  externalProgressCurrentValue,
  externalProgressTotalValue,
  internalProgressCurrentValue,
  internalProgressTotalValue,
  onClickSeeMilestones
}) => {
  const { t } = useTranslation();
  const externalDonutPercent = (externalProgressCurrentValue / externalProgressTotalValue) * 100;
  const internalDonutPercent = (internalProgressCurrentValue / internalProgressTotalValue) * 100;

  return (
    <div className="m-projectProgressCard">
      <TitlePage
        textTitle={t('general.projectProgress')}
        underlinePosition="center"
        className="m-projectProgressCard__title"
      />
      <RadialBarChart
        className="m-projectProgressCard__radialBarChart"
        externalDonutColor="#08ceaa"
        externalDonutPercent={externalDonutPercent}
        internalDonutPercent={internalDonutPercent}
        internalDonutColor="#26385b"
        externalDonutLabel="Milestones progress"
        internalDonutLabel="Activities progress"
        externalDonutSymbol="%"
        internalDonutSymbol="%"
      />
      <div className="m-projectProgressCard__infoContainer">
        <div className="m-projectProgressCard__info">
          <div className="m-projectProgressCard__icon --progress">%</div>
          <div>
            <p className="m-projectProgressCard__title --progress">
              {t('general.milestones_progress')}
            </p>
            <p className="m-projectProgressCard__value --progress">
              %{externalDonutPercent.toFixed(2)}
            </p>
          </div>
        </div>
        <div className="m-projectProgressCard__info">
          <div className="m-projectProgressCard__icon --expenses">%</div>
          <div>
            <p className="m-projectProgressCard__title --expenses">
              {t('general.activities_progress')}
            </p>
            <p className="m-projectProgressCard__value --expenses">
              %{internalDonutPercent.toFixed(2)}
            </p>
          </div>
        </div>
      </div>
      <CoaButton
        type="primary"
        className="m-projectProgressCard__milestoneButton"
        onClick={onClickSeeMilestones}
      >
        {t('landingInfoSection.progressCardBtnMilestone')}
      </CoaButton>
    </div>
  );
};

ProjectProgressCard.propTypes = {
  onClickSeeMilestones: PropTypes.func
};

ProjectProgressCard.defaultProps = {
  onClickSeeMilestones: undefined
};
