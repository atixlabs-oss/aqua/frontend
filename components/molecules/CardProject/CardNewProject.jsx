/**
 * AGUA Copyright (C) 2023  Atix Labs
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import React, { useState } from 'react';
import PropTypes from 'prop-types';
import { useTranslation } from 'react-i18next';

import NewProjectIcon from 'components/atoms/CustomIcons/NewProjectIcon/NewProjectIcon';
import Styles from './card-new-project.module.scss';
import Loading from '../Loading/Loading';
import classNames from 'classnames';

const CardNewProject = ({ onClick }) => {
  const { t } = useTranslation();
  const [isLoading, setIsLoading] = useState(false);

  const handleClick = async () => {
    try {
      setIsLoading(true);
      await onClick?.();
      setIsLoading(false);
    } catch {
      setIsLoading(false);
    }
  };

  return (
    <div
      className={classNames(Styles.container, {
        [Styles['--loading']]: isLoading
      })}
      onClick={handleClick}
      role="button"
      onKeyPress={handleClick}
      tabIndex="0"
    >
      <Loading spinning={isLoading}>
        <div className={Styles.container__contentContainer}>
          <NewProjectIcon />
          <h2 className={Styles.container__text}>{t('projectsSection.new_project')}</h2>
        </div>
      </Loading>
    </div>
  );
};

export default CardNewProject;

CardNewProject.propTypes = {
  onClick: PropTypes.func.isRequired
};
