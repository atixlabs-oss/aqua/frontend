/**
 * AGUA Copyright (C) 2023  Atix Labs
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/* eslint-disable jsx-a11y/alt-text */

import React, { useState } from 'react';
import { useTranslation } from 'react-i18next';
import { Icon, Select } from 'antd';
import PropTypes from 'prop-types';

import Styles from './card-project.module.scss';
import { CoaTag } from 'components/atoms/CoaTag/CoaTag';
import { PROJECT_STATUS_ENUM, PROJECT_STATUS_MAP } from 'model/projectStatus';
import { PROJECT_TYPE_MAP } from 'model/projectType';
import { CLONE_STATUS_ENUM, CLONE_STATUS_MAP } from 'model/cloneStatus';
import InfoItem from '../../atoms/InfoItem/InfoItem';
import { projectCardPropType } from '../../../helpers/proptypes';
import { formatCurrency, formatTimeframeValue } from '../../../helpers/formatter';
import { CoaButton } from 'components/atoms/CoaButton/CoaButton';
import { VISIBILITY_TYPES_ENUM } from 'model/visibilityTypes';

const { Option } = Select;

const CardProject = ({
  onClick,
  project,
  countries,
  withDescription,
  onClickEdit,
  canExecuteActions,
  onVisibilityChange
}) => {
  const {
    cardPhotoPath,
    goalAmount,
    location,
    projectName,
    timeframe,
    status: projectStatus,
    beneficiary,
    currency,
    timeframeUnit,
    revision,
    type,
    cloneStatus,
    visibility
  } = project;

  const { t } = useTranslation();
  const [isSelectorLoading, setIsSelectorLoading] = useState(false);
  const [selectorValue, setSelectorValue] = useState(visibility);

  const locationsNames = () => {
    if (!location) return 'Not set';
    const countriesIds = countries.filter(
      // eslint-disable-next-line radix
      country => location.split(',').includes(String(country.value))
    );
    if (!countriesIds.length) {
      return location;
    }
    return countriesIds.map(country => country.name).join(', ');
  };

  const beneficiaryFirstName = beneficiary?.firstName || '';
  const beneficiaryLastName = beneficiary?.lastName || '';
  const beneficiaryCompleteName = beneficiary
    ? `${beneficiaryFirstName} ${beneficiaryLastName}`
    : 'Not set';

  const _cloneStatus = cloneStatus?.toLowerCase();
  const _projectStatus = projectStatus?.toLowerCase();
  const status = _cloneStatus ?? _projectStatus;
  const statusMapped = cloneStatus
    ? CLONE_STATUS_MAP[_cloneStatus]
    : PROJECT_STATUS_MAP[_projectStatus];
  const statusName = statusMapped?.name;
  const statusColor = statusMapped?.color;

  const handleVisibilitySelectChange = async visibility => {
    try {
      setIsSelectorLoading(true);
      await onVisibilityChange?.(project, visibility);
      setSelectorValue(visibility);
    } catch {
    } finally {
      setIsSelectorLoading(false);
    }
  };

  return (
    <div
      role="button"
      className={Styles.container}
      onKeyPress={onClick}
      tabIndex="0"
      onClick={onClick}
    >
      <div
        className={Styles.container__cover}
        style={{
          backgroundImage: `linear-gradient(0.65deg, rgba(0, 0, 0, 0.7) 0.02%, rgba(0, 0, 0, 0) 50.2%), url(${
            cardPhotoPath
              ? `${process.env.NEXT_PUBLIC_URL_HOST}${cardPhotoPath}`
              : '/static/images/empty-img.svg'
          })`
        }}
      >
        {canExecuteActions && (
          <div className={Styles.container__cover__actions} onClick={e => e.stopPropagation()}>
            {status !== PROJECT_STATUS_ENUM.DRAFT && (
              <Select
                loading={isSelectorLoading}
                defaultValue="private"
                className={Styles.container__cover__actions__select}
                onChange={handleVisibilitySelectChange}
                value={selectorValue}
                disabled={
                  ![
                    PROJECT_STATUS_ENUM.IN_PROGRESS,
                    PROJECT_STATUS_ENUM.PUBLISHED,
                    PROJECT_STATUS_ENUM.COMPLETED,
                    PROJECT_STATUS_ENUM.CANCELLED
                  ].includes(status) || isSelectorLoading
                }
              >
                <Option value={VISIBILITY_TYPES_ENUM.PRIVATE}>{t('general.private')}</Option>
                <Option value={VISIBILITY_TYPES_ENUM.PUBLISHED}>{t('general.public')}</Option>
                <Option value={VISIBILITY_TYPES_ENUM.HIGHLIGHTED}>
                  {t('general.highlighted')}
                </Option>
              </Select>
            )}

            <CoaButton
              shape="circle"
              onClick={onClickEdit}
              disabled={
                ![
                  PROJECT_STATUS_ENUM.DRAFT,
                  PROJECT_STATUS_ENUM.IN_REVIEW,
                  CLONE_STATUS_ENUM.IN_REVIEW
                ].includes(status)
              }
            >
              <Icon type="edit" className={Styles.container__cover__editButton__icon} />
            </CoaButton>
          </div>
        )}
        {/* {canEdit && (
        )} */}
        <div className={Styles.container__cover__titleContainer}>
          {statusName && (
            <div className={Styles.container__cover__tagsContainer}>
              <CoaTag predefinedColor={statusColor}>{statusName}</CoaTag>
            </div>
          )}
          <h2 className={Styles.container__cover__titleContainer__title}>{projectName} </h2>
        </div>
      </div>
      {withDescription && (
        <div className={Styles.container__body}>
          <InfoItem
            subtitle={t('projectCard.country')}
            title={locationsNames()}
            className={Styles.container__body__country}
          />
          <InfoItem
            subtitle={t('projectCard.timeframe')}
            title={formatTimeframeValue({ timeframe, timeframeUnit, t })}
            className={Styles.container__body__timeframe}
          />
          <InfoItem
            subtitle={t('projectCard.project_type')}
            title={PROJECT_TYPE_MAP[type]?.name}
            className={Styles.container__body__projectType}
          />
          <InfoItem
            subtitle={t('projectCard.beneficiary')}
            title={beneficiaryCompleteName}
            className={Styles.container__body__beneficiary}
          />
          <InfoItem
            subtitle={t('projectCard.budget')}
            title={formatCurrency(currency, goalAmount)}
            iconInfoItem="dollar"
            className={Styles.container__body__budget}
          />
          <InfoItem
            subtitle={t('projectCard.project_version')}
            title={revision}
            className={Styles.container__body__projectVersion}
          />
        </div>
      )}
    </div>
  );
};

CardProject.defaultProps = {
  onClick: () => null,
  countries: [],
  withDescription: false
};

CardProject.propTypes = {
  project: PropTypes.shape(projectCardPropType).isRequired,
  onClick: PropTypes.func,
  countries: PropTypes.arrayOf({
    name: PropTypes.string,
    value: PropTypes.number
  }),
  withDescription: PropTypes.bool,
  onClickEdit: PropTypes.func,
  canExecuteActions: PropTypes.bool,
  onVisibilityChange: PropTypes.func
};

export default CardProject;
