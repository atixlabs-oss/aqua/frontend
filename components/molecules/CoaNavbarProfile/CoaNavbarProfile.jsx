/**
 * AGUA Copyright (C) 2023  Atix Labs
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import React from 'react';

import { Dropdown, Icon, Menu } from 'antd';
import { CoaUserAvatar } from 'components/atoms/CoaUserAvatar/CoaUserAvatar';
import { LogoutOutlined } from '@ant-design/icons';
import { useRouter } from 'next/router';
import { useTranslation } from 'react-i18next';

const CustomMenu = ({ handleLogout }) => {
  const { t } = useTranslation();

  return (
    <Menu>
      <Menu.Item key="0" onClick={handleLogout} className="customMenu__item">
        <LogoutOutlined className="customMenu__item__icon" />
        <span className="customMenu__item__text">{t('loginSection.logout')}</span>
      </Menu.Item>
    </Menu>
  );
};

export const CoaNavbarProfile = ({ user, role, removeUser, isProtectedRoute }) => {
  const { firstName, lastName } = user;
  const { push } = useRouter();
  const { t } = useTranslation();

  const logout = () => {
    removeUser();
    if (isProtectedRoute) push('/');
  };

  return (
    <div className="coaNavbarProfile">
      <CoaUserAvatar firstName={firstName} lastName={lastName} />
      <div className="coaNavbarProfile__user">
        <div className="coaNavbarProfile__user__details">
          <h2 className="coaNavbarProfile__userName">{user.firstName}</h2>
          <span className="coaNavbarProfile__userRole">
            {role && t(`roles.${role?.toLowerCase()}`)}
          </span>
        </div>

        <Dropdown overlay={<CustomMenu handleLogout={logout} />} trigger={['click']}>
          <Icon type="down" className="coaNavbarProfile__dropdownIcon" />
        </Dropdown>
      </div>
    </div>
  );
};
