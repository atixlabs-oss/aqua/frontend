/**
 * AGUA Copyright (C) 2023  Atix Labs
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import React from 'react';
import PropTypes from 'prop-types';
import { Divider } from 'antd';
import { useTranslation } from 'react-i18next';

import { CoaTag } from 'components/atoms/CoaTag/CoaTag';
import { EVIDENCE_STATUS_MAP } from 'model/evidence';
import { addMiddleEllipsis } from 'helpers/formatter';
import { getDateAndTime } from '../../../helpers/utils';
import { CopyClipboardButton } from 'components/atoms/CopyClipboardButton/CopyClipboardButton';

export default function EvidenceDetailBox(props) {
  const { t } = useTranslation();

  const { title, description, createdAt, user, status, activity } = props;
  const date = createdAt && new Date(createdAt);

  const { firstName: userFirstName, lastName: userLastName, address: userAddress } = user || {};
  const createdByName =
    userFirstName || userLastName ? `${userFirstName} ${userLastName}` : 'No name';

  const { firstName: fnAuditor, lastName: lnAuditor, address: auditorAddress } =
    activity?.auditor || {};
  const auditorName = fnAuditor || lnAuditor ? `${fnAuditor} ${lnAuditor}` : 'No name';

  return (
    <div className="evidence-container">
      {/* Evidence header */}
      <div className="evidence-container__header">
        <h3 className="evidence-container__date">{getDateAndTime(date)}</h3>
        {status && (
          <CoaTag predefinedColor={EVIDENCE_STATUS_MAP[status].color}>
            {EVIDENCE_STATUS_MAP[status].name}
          </CoaTag>
        )}
      </div>
      <Divider style={{ marginTop: '0' }}></Divider>
      {/* Evidence indicator */}
      <h3 className="evidence-container__indicator">{t('evidenceDetail.title')}</h3>
      {/* Evidence title */}
      <h2 className="evidence-container__title">{title}</h2>
      <p className="evidence-container__description">{description}</p>
      <div className="evidenceDetailBox__member">
        {/* Evidence creator */}
        <h3 className="evidenceDetailBox__member__Label">
          {t('evidenceDetail.createdBy')}{' '}
          <b className="evidenceDetailBox__member__LabelValue">{createdByName}</b>
        </h3>
        <h3 className="evidenceDetailBox__member__Label">
          Public Key: {!userAddress && <span>Not set</span>}
          {userAddress && (
            <>
              <span className="evidenceDetailBox__member__LabelValue">
                {addMiddleEllipsis(userAddress)}
              </span>

              <CopyClipboardButton textToCopy={userAddress} />
            </>
          )}
        </h3>
      </div>
      <div className="evidenceDetailBox__member">
        {/* Auditor */}

        <h3 className="evidenceDetailBox__member__Label">
          {t('evidenceDetail.auditor')}:{' '}
          <b className="evidenceDetailBox__member__LabelValue">{auditorName}</b>
        </h3>
        <h3 className="evidenceDetailBox__member__Label">
          Public Key: {!auditorAddress && <span>Not set</span>}
          {auditorAddress && (
            <>
              <span className="evidenceDetailBox__member__LabelValue">
                {addMiddleEllipsis(auditorAddress)}
              </span>

              <CopyClipboardButton textToCopy={auditorAddress} />
            </>
          )}
        </h3>
      </div>
    </div>
  );
}

EvidenceDetailBox.defaultProps = {
  title: '',
  description: '',
  createdAt: '',
  user: {},
  status: '',
  auditor: {}
};

EvidenceDetailBox.propTypes = {
  title: PropTypes.string,
  description: PropTypes.string,
  createdAt: PropTypes.string,
  status: PropTypes.string,
  user: PropTypes.shape({
    firstName: PropTypes.string,
    lastName: PropTypes.string
  }),
  auditor: PropTypes.shape({
    firstName: PropTypes.string,
    lastName: PropTypes.string
  })
};
