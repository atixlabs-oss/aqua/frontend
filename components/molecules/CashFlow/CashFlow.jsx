/**
 * AGUA Copyright (C) 2023  Atix Labs
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import React from 'react';
import { useTranslation } from 'react-i18next';
import { Table } from 'antd';
import classNames from 'classnames';
import PropTypes from 'prop-types';

import { useGetCashflow } from 'api/react-query/project/projectQueries';

const CashFlow = ({ projectId, currency }) => {
  const { t } = useTranslation('common');
  const { data } = useGetCashflow({ projectId, currency });
  const columns = [
    {
      title: t('general.date'),
      dataIndex: 'date',
      key: 'date'
    },
    {
      title: t('general.type'),
      dataIndex: 'type',
      key: 'type',
      render: text => (
        <span
          className={classNames('cashFlow__table__type', {
            '--green': text === 'incoming',
            '--orange': text === 'outgoing'
          })}
        >
          {text}
        </span>
      )
    },
    {
      title: 'Origin',
      dataIndex: 'type',
      render: (type, item) => (
        <span>{type == 'incoming' ? item.otherAccount : item.accountId}</span>
      ),
      key: 'origin'
    },
    {
      title: 'Destination',
      dataIndex: 'type',
      render: (type, item) => (
        <span>{type == 'incoming' ? item.accountId : item.otherAccount}</span>
      ),
      key: 'destination'
    },
    {
      title: t('general.activityType'),
      dataIndex: 'activityType',
      key: 'activityType',
      render: text => (
        <span
          className={classNames('cashFlow__table__activityType', {
            '--green': text === 'incoming',
            '--orange': text === 'outgoing'
          })}
        >
          {text}
        </span>
      )
    },
    {
      title: 'TXID',
      dataIndex: 'displayId',
      key: 'txid'
    },
    {
      title: t('general.currentAmount'),
      dataIndex: 'currentAmount',
      key: 'currentAmount',
      render: text => <span className="cashFlow__table__currentAmount">{text}</span>
    },
    {
      title: 'Audited',
      dataIndex: 'audited',
      key: 'audited'
    }
  ];

  return (
    <>
      <Table columns={columns} dataSource={data} className="cashFlow__table" pagination={false} />
      <div className="cashFlow__tablesMobileContainer">
        {data?.map(item => (
          <table className="cashFlow__tablesMobileContainer__tableMobile" key={`${item.key}`}>
            <tbody>
              <tr>
                <th>{t('general.date')}</th>
                <td>{item.date}</td>
              </tr>
              <tr>
                <th>{t('general.type')}</th>
                <td
                  className={classNames('cashFlow__table__type', {
                    '--green': item.type === 'incoming',
                    '--orange': item.type === 'outgoing'
                  })}
                >
                  {item.type}
                </td>
              </tr>
              <tr>
                <th>{'Origin'}</th>
                <td>{item.type}</td>
              </tr>
              <tr>
                <th>{'Destination'}</th>
                <td>{item.type}</td>
              </tr>
              <tr>
                <th>{t('general.activityType')}</th>
                <td
                  className={classNames('cashFlow__table__activityType', {
                    '--green': item.activityType === 'incoming',
                    '--orange': item.activityType === 'outgoing'
                  })}
                >
                  {item.activityType}
                </td>
              </tr>
              <tr>
                <th>{'TXID'}</th>
                <td>{item.displayId}</td>
              </tr>
              <tr>
                <th>{t('general.currentAmount')}</th>
                <td className="cashFlow__table__currentAmount">{item.currentAmount}</td>
              </tr>
              <tr>
                <th>{'Audited'}</th>
                <td>{item.audited}</td>
              </tr>
            </tbody>
          </table>
        ))}
      </div>
    </>
  );
};

CashFlow.propTypes = {
  projectId: PropTypes.string,
  currency: PropTypes.string
};

CashFlow.defaultProps = {
  projectId: '',
  currency: ''
};

export default CashFlow;
