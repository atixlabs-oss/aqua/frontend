/**
 * AGUA Copyright (C) 2023  Atix Labs
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import React from 'react';
import PropTypes from 'prop-types';

import { Divider, Icon } from 'antd';
import ProjectHeroDetail from '../../atoms/ProjectHeroDetail/ProjectHeroDetail';
import { useTranslation } from 'react-i18next';

const ProjectHeroDetails = ({ country, timeframe, budget, beneficiary, revision }) => {
  const { t } = useTranslation();

  return (
    <div className="m-projectHeroDetails">
      <ProjectHeroDetail
        text={t('header.impact')}
        icon="/static/images/globe.svg"
        title={country}
        data-testid="projectHeroDetails-country"
      />
      <Divider type="vertical" className="m-projectHeroDetails__divider" />
      <ProjectHeroDetail
        text={t('header.timeframe')}
        icon="/static/images/calendar.svg"
        title={timeframe}
        data-testid="projectHeroDetails-timeframe"
      />
      <Divider type="vertical" className="m-projectHeroDetails__divider" />
      <ProjectHeroDetail
        text={t('header.budget')}
        icon="/static/images/coin.svg"
        title={budget}
        data-testid="projectHeroDetails-budget"
      />
      <Divider type="vertical" className="m-projectHeroDetails__divider" />
      <ProjectHeroDetail
        text={t('header.beneficiary')}
        icon="/static/images/arrow.svg"
        title={beneficiary}
        data-testid="projectHeroDetails-beneficiary"
      />
      <Divider type="vertical" className="m-projectHeroDetails__divider" />
      <ProjectHeroDetail
        text={t('header.projectVersion')}
        customIcon={<Icon type="info-circle" />}
        title={revision}
        data-testid="projectHeroDetails-projectVersion"
      />
    </div>
  );
};

export default ProjectHeroDetails;

ProjectHeroDetails.propTypes = {
  country: PropTypes.string.isRequired,
  timeframe: PropTypes.string.isRequired,
  budget: PropTypes.string.isRequired,
  beneficiary: PropTypes.string.isRequired
};
