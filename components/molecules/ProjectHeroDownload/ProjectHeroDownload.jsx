/**
 * AGUA Copyright (C) 2023  Atix Labs
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import React from 'react';
import PropTypes from 'prop-types';
import { Dropdown, Menu } from 'antd';
import { CoaTextButton } from 'components/atoms/CoaTextButton/CoaTextButton';
import DownloadIcon from '../../atoms/DownloadIcon/DownloadIcon';
import { useTranslation } from 'react-i18next';

const ProjectHeroMenu = ({ projectProposalUrl, legalAgreementUrl }) => {
  const { t } = useTranslation();
  return (
    <Menu>
      <Menu.Item>
        <a target="_blank" rel="noopener noreferrer" href={projectProposalUrl}>
          {t('header.btnProposal')}
        </a>
      </Menu.Item>
      <Menu.Item>
        <a target="_blank" rel="noopener noreferrer" href={legalAgreementUrl}>
          {t('header.btnAgreement')}
        </a>
      </Menu.Item>
    </Menu>
  );
};

ProjectHeroMenu.defaultProps = {
  projectProposalUrl: undefined,
  legalAgreementUrl: undefined
};

ProjectHeroMenu.propTypes = {
  projectProposalUrl: PropTypes.string,
  legalAgreementUrl: PropTypes.string
};

const ProjectHeroDownload = ({ projectProposalUrl, legalAgreementUrl }) => {
  const { t } = useTranslation();

  return (
    <div className="download">
      <div className="download-btn--mobile">
        <Dropdown
          overlay={<ProjectHeroMenu {...{ projectProposalUrl, legalAgreementUrl }} />}
          placement="topLeft"
        >
          <CoaTextButton>
            <DownloadIcon />
          </CoaTextButton>
        </Dropdown>
      </div>
      <a
        href={projectProposalUrl}
        className="proposal--btn"
        target="_blank"
        rel="noopener noreferrer"
      >
        <DownloadIcon /> <span>{t('header.btnProposal')}</span>
      </a>
      <a
        href={legalAgreementUrl}
        className="agreement--btn"
        target="_blank"
        rel="noopener noreferrer"
      >
        <DownloadIcon /> <span>{t('header.btnAgreement')}</span>
      </a>
    </div>
  );
};

ProjectHeroDownload.defaultProps = {
  projectProposalUrl: undefined,
  legalAgreementUrl: undefined
};

ProjectHeroDownload.propTypes = {
  projectProposalUrl: PropTypes.string,
  legalAgreementUrl: PropTypes.string
};

export default ProjectHeroDownload;
