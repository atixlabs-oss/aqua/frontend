/**
 * AGUA Copyright (C) 2023  Atix Labs
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import React from 'react';
import { Collapse } from 'antd';
import { CoaUserAvatar } from '../../atoms/CoaUserAvatar/CoaUserAvatar';
import { useTranslation } from 'react-i18next';
import { UserAddressesHistory } from 'components/organisms/UserAddressesHistory/UserAddressesHistory';

const { Panel } = Collapse;

const CustomCollapseHeader = ({ firstName, lastName, rol }) => {
  const fullName = `${firstName} ${lastName}`;
  return (
    <div className="m-coaProjectMembersCard__header" data-testid="project-member-card">
      <CoaUserAvatar firstName={firstName} lastName={lastName} />
      <div style={{ flex: 1, minWidth: 0 }}>
        <h5 className="m-coaProjectMembersCard__title">{fullName}</h5>
        <h6 className="m-coaProjectMembersCard__subtitle">{rol}</h6>
      </div>
    </div>
  );
};

export const CoaProjectMembersCard = ({
  id,
  firstName,
  lastName,
  email,
  rol,
  address,
  walletHistory
}) => {
  const { t } = useTranslation();
  return (
    <div>
      <Collapse bordered={false} expandIconPosition="right" className="m-coaProjectMembersCard">
        <Panel
          header={<CustomCollapseHeader firstName={firstName} lastName={lastName} rol={rol} />}
          key={id}
          style={{ border: 0 }}
        >
          <div className="m-coaProjectMembersCard__extraInfo">
            <div>
              <span className="m-coaProjectMembersCard__boldText">
                {t('landingProjectMembers.email')}&nbsp;
              </span>
              {email}
            </div>
            {address && (
              <div className="m-coaProjectMembersCard__addressContainer">
                <span className="m-coaProjectMembersCard__boldText">
                  {t('landingProjectMembers.public_key')}:&nbsp;
                </span>
                <span className="m-coaProjectMembersCard__addressContainer__value">{address}</span>
              </div>
            )}
            {walletHistory && <UserAddressesHistory addressHistory={walletHistory} twoRows />}
          </div>
        </Panel>
      </Collapse>
    </div>
  );
};

CoaProjectMembersCard.defaultProps = {
  id: undefined,
  firstName: undefined,
  lastName: undefined,
  email: undefined,
  rol: undefined,
  address: undefined
};
