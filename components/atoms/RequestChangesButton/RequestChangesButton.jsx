import { useContext, useState } from 'react';
import { useRouter } from 'next/router';
import { useTranslation } from 'react-i18next';
import { message } from 'antd';

import { CoaButton } from '../CoaButton/CoaButton';
import ModalConfirmWithSK from 'components/organisms/ModalConfirmWithSK/ModalConfirmWithSK';
import {
  useCloneProjectMutation,
  useSendToReviewProjectMutation,
  useSignProjectMutation,
  useUpdateProjectCanceledStateMutation
} from 'api/react-query/project/projectMutations';
import ModalPublishLoading from 'components/organisms/ModalPublishLoading/ModalPublishLoading';
import ModalPublishSuccess from 'components/organisms/ModalPublishSuccess/ModalPublishSuccess';
import { signMessage } from 'helpers/blockchain/wallet';
import { useQueryClient } from '@tanstack/react-query';
import { ConfirmRequestChangeModal } from 'components/organisms/CoaModals/ConfirmModals/confirmModals';
import { CustomTooltip } from '../CustomTooltip/CustomTooltip';
import { PROJECT_STATUS_ENUM } from 'model/projectStatus';
import { CLONE_STATUS_ENUM } from 'model/cloneStatus';
import { checkHasActivityPending } from 'helpers/utils';
import { UserContext } from 'components/utils/UserContext';
import { openRequestChangeUserEditingAlreadyModal } from 'components/organisms/CoaModals/ErrorModals/errorModals';

export const RequestChangesButton = ({
  project,
  cloneStatus,
  milestones,
  isPinStatusApproved,
  editProposer,
  editing,
  projectStatus
}) => {
  const router = useRouter();
  const { user } = useContext(UserContext);
  const { t } = useTranslation();
  const queryClient = useQueryClient();
  const cloneProjectMutation = useCloneProjectMutation();
  const [loadingModal, setLoadingModal] = useState({ isVisible: false });
  const [modalSuccess, setModalSuccess] = useState({ isVisible: false });

  const updateProjectCanceledStateMutation = useUpdateProjectCanceledStateMutation();
  const signProjectMutation = useSignProjectMutation();
  const sendToReviewProjectMutation = useSendToReviewProjectMutation();

  const [modalConfirmWithSk, setModalConfirmWithSk] = useState({ isVisible: false });

  const checkAndGetClone = async () => {
    if (project?.cloneId) return project?.cloneId;

    const response = await cloneProjectMutation.mutateAsync(project?.parent || project?.id);

    return response?.data?.projectId;
  };

  const handleRequestChanges = async () => {
    try {
      const _cloneId = await checkAndGetClone();
      return router.push(`/back-office/projects/edit/${_cloneId}`);
    } catch {
      openRequestChangeUserEditingAlreadyModal();
    } finally {
      queryClient.invalidateQueries(['project', project?.parent]);
    }
  };

  const handleCancelRequest = async (_pin, _password, wallet, key) => {
    try {
      setLoadingModal({ isVisible: true });

      const _cloneId = await checkAndGetClone();
      await updateProjectCanceledStateMutation.mutateAsync(_cloneId);

      const result = await sendToReviewProjectMutation.mutateAsync(_cloneId);
      const messageToSign = result?.data?.toSign;
      const authorizationSignature = await signMessage(wallet, messageToSign, key);
      await signProjectMutation.mutateAsync({ authorizationSignature, projectId: _cloneId });

      setModalSuccess({
        isVisible: true,
        onSave: () => setModalSuccess({ ...modalSuccess, isVisible: false }),
        title: t('createProject.ttSent'),
        description: t('createProject.dSent')
      });
    } catch (error) {
      message.error(t('createProject.anErrorOcurredWhileSendingToReviewTheProject'));
      openRequestChangeUserEditingAlreadyModal();
    } finally {
      setLoadingModal({ isVisible: false });
      queryClient.invalidateQueries(['project', project?.parent]);
    }
  };

  const handleOpenCancelRequestSecretKeyModal = () => {
    setModalConfirmWithSk({
      isVisible: true,
      onCancel: () => setModalConfirmWithSk({ isVisible: false }),
      onSuccess: handleCancelRequest,
      title: t('modals.dialog.secretKey.title'),
      description: t('modals.dialog.secretKey.description'),
      okText: t('general.confirm'),
      cancelText: t('general.btnCancel')
    });
  };

  const isAnotherUserEditing = editing && editProposer !== user?.id;

  const getTooltipText = () => {
    if (isPinStatusApproved) return t('pin.actionDisabledPinNotSet');
    if (isAnotherUserEditing)
      return t('landingSubheader.This project is currently being edited by other user');
  };

  return (
    <>
      <CustomTooltip title={getTooltipText()} variant="black">
        <CoaButton
          type="primary"
          onClick={() =>
            ConfirmRequestChangeModal({
              onOk: handleRequestChanges,
              onCancel: handleOpenCancelRequestSecretKeyModal
            })
          }
          className="o-previewProject__buttons__requestChanges"
          disabled={
            ![PROJECT_STATUS_ENUM.IN_PROGRESS, PROJECT_STATUS_ENUM.PUBLISHED].includes(
              projectStatus
            ) ||
            (cloneStatus && cloneStatus !== CLONE_STATUS_ENUM.OPEN_REVIEW) ||
            checkHasActivityPending({ milestones }) ||
            isPinStatusApproved ||
            isAnotherUserEditing
          }
        >
          {t('landingSubheader.btnRequestChanges')}
        </CoaButton>
      </CustomTooltip>
      <ModalConfirmWithSK
        visible={modalConfirmWithSk?.isVisible}
        onCancel={() => setModalConfirmWithSk({ ...modalConfirmWithSk, isVisible: false })}
        onSuccess={modalConfirmWithSk?.onSuccess}
        title={modalConfirmWithSk?.title}
        description={modalConfirmWithSk?.description}
        okText={modalConfirmWithSk?.okText}
        cancelText={modalConfirmWithSk?.cancelText}
        leaveAComment={modalConfirmWithSk?.leaveAComment}
      />
      <ModalPublishLoading visible={loadingModal?.isVisible} textTitle={loadingModal?.title} />
      <ModalPublishSuccess
        visible={modalSuccess?.isVisible}
        onCancel={modalSuccess?.onCancel}
        onSave={modalSuccess?.onSave}
        textTitle={modalSuccess?.title}
        description={modalSuccess?.description}
      >
        {modalSuccess?.children}
      </ModalPublishSuccess>
    </>
  );
};
