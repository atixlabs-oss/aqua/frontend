/**
 * AGUA Copyright (C) 2023  Atix Labs
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import React from 'react';
import PropTypes from 'prop-types';

const ProjectHeroDetail = ({ icon, text, title, customIcon, ...rest }) => (
  <div className="a-projectHeroDetail" {...rest}>
    <div className="a-projectHeroDetail__icon">
      {customIcon ?? <img width={22} height={22} src={icon} alt="icon" />}
    </div>
    <div className="a-projectHeroDetail__text">
      <p>{text}</p>
      <h3>{title}</h3>
    </div>
  </div>
);

export default ProjectHeroDetail;

ProjectHeroDetail.propTypes = {
  customIcon: PropTypes.oneOfType([PropTypes.arrayOf(PropTypes.node), PropTypes.node]),
  icon: PropTypes.string,
  text: PropTypes.string.isRequired,
  title: PropTypes.oneOfType([PropTypes.string, PropTypes.number])
};
