/**
 * AGUA Copyright (C) 2023  Atix Labs
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import React from 'react';
import PropTypes from 'prop-types';
import { CoaButton } from '../CoaButton/CoaButton';

const EvidenceButton = ({ onClick, text, width, variant, ...rest }) => (
  <CoaButton type={variant} style={{ width: width ? '140px' : '100%' }} onClick={onClick} {...rest}>
    {text}
  </CoaButton>
);

export default EvidenceButton;

EvidenceButton.defaultProps = {
  width: false
};

EvidenceButton.propTypes = {
  onClick: PropTypes.func.isRequired,
  text: PropTypes.string.isRequired,
  variant: PropTypes.string.isRequired,
  width: PropTypes.bool
};
