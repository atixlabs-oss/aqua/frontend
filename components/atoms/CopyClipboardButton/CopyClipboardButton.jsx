/**
 * AGUA Copyright (C) 2023  Atix Labs
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import { Icon, message } from 'antd';
import React from 'react';
import { CoaTextButton } from '../CoaTextButton/CoaTextButton';
import Styles from './copy-clipboard-button.module.scss';

export const CopyClipboardButton = ({ textToCopy }) => (
  <CoaTextButton
    onClick={() => {
      navigator.clipboard.writeText(textToCopy);
      message.info('Text Copied!');
    }}
    className={Styles.copyClipboardButton}
  >
    {' '}
    <Icon type="copy" />
  </CoaTextButton>
);
