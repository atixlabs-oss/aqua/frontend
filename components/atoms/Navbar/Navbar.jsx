/**
 * AGUA Copyright (C) 2023  Atix Labs
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/* eslint-disable
 jsx-a11y/anchor-is-valid,
 jsx-a11y/click-events-have-key-events,
 jsx-a11y/no-static-element-interactions
*/

import React, { useContext, useState } from 'react';
import { UserContext } from 'components/utils/UserContext';
import NavbarLogin from 'components/molecules/NavbarLogin/NavbarLogin';
import { Icon } from 'antd';
import classNames from 'classnames';
import { CoaNavbarProfile } from 'components/molecules/CoaNavbarProfile/CoaNavbarProfile';
import { checkRoleByProject } from 'helpers/roles';
import { useRouter } from 'next/router';
import Styles from './navbar.module.scss';
import Image from 'next/image';
import { CoaTextButton } from '../CoaTextButton/CoaTextButton';
import { useTranslation } from 'react-i18next';
import Link from 'next/link';

const Navbar = ({ project, isProtectedRoute, setModalOpen, ...rest }) => {
  const [isNavOpen, setIsNavOpen] = useState(false);
  const { user, removeUser } = useContext(UserContext);
  const { push, query } = useRouter();
  const { projectId } = query;
  const { t } = useTranslation();

  const role = checkRoleByProject({ user, project });

  return (
    <>
      <nav className={Styles.navbar} data-testid="navbar" role="navigation" {...rest}>
        <Icon
          type="menu"
          className={classNames(Styles.navbar__collapsibleMenuIcon, Styles['--open'])}
          onClick={() => setIsNavOpen(true)}
        />
        <div className={Styles.navbar__largeLogo}>
          <Link href="/">
            <Image
              className={Styles.navbar__largeLogo}
              src={process.env.NEXT_PUBLIC_LARGE_LOGO_SVG_PATH}
              alt={`${process.env.NEXT_PUBLIC_ORGANIZATION_NAME} logo`}
              width={289}
              height={39}
            />
          </Link>
        </div>
        <div className={Styles.navbar__smallLogo}>
          <Image
            src={process.env.NEXT_PUBLIC_SMALL_LOGO_PATH}
            alt={`${process.env.NEXT_PUBLIC_ORGANIZATION_NAME} logo`}
            width={36}
            height={36}
          />
        </div>
        <div className={Styles.navbar__loginButtonContainer}>
          {!!user && (
            <CoaNavbarProfile
              user={user}
              removeUser={removeUser}
              projectId={projectId}
              role={role?.name}
              isProtectedRoute={isProtectedRoute}
            />
          )}
          {!user && (
            <NavbarLogin
              data-testid="navbar-login-button"
              id="navbar-login-button"
              loginFn={() => {
                if (setModalOpen) return setModalOpen(true);
                projectId ? push(`/${projectId}/login`) : push('/login');
              }}
            />
          )}
        </div>
      </nav>
      <div
        className={classNames(Styles.navbar__menuBox, {
          [Styles['--visible']]: isNavOpen
        })}
        data-testid="navbar-menuBox"
      >
        <div className={Styles.navbar__menuBox__header}>
          <Icon
            onClick={() => setIsNavOpen(false)}
            type="menu-fold"
            className={Styles.navbar__collapsibleMenuIcon}
          />
          <Image
            width={36}
            height={36}
            src={process.env.NEXT_PUBLIC_SMALL_LOGO_PATH}
            alt={`${process.env.NEXT_PUBLIC_ORGANIZATION_NAME} logo`}
          />
          <Icon
            type="close"
            onClick={() => setIsNavOpen(false)}
            className={Styles.navbar__collapsibleMenuIcon}
          />
        </div>
        <div className={Styles.navbar__menuBox__body}>
          <ul>
            <li>
              {!!user && (
                <CoaTextButton variant="primary" onClick={removeUser}>
                  <span className={Styles.navbar__menuBox__logoutButton}>
                    {t('loginSection.logout')}
                  </span>
                </CoaTextButton>
              )}
              {!user && (
                <NavbarLogin
                  loginFn={() => {
                    if (setModalOpen) {
                      setIsNavOpen(false);
                      return setModalOpen(true);
                    }
                    push(`/${projectId}/login`);
                  }}
                />
              )}
            </li>
          </ul>
        </div>
      </div>
      <div
        className={classNames(Styles.navbar__menuBox__overlay, {
          [Styles['--visible']]: isNavOpen
        })}
      ></div>
    </>
  );
};

export default Navbar;
