/**
 * AGUA Copyright (C) 2023  Atix Labs
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import React, { useEffect, useState, useRef } from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';
import { CustomTooltip } from '../CustomTooltip/CustomTooltip';

const InfoItem = ({ subtitle, title, className }) => {
  const [overflowTooltip, setOverflowTooltip] = useState(false);

  const titleRef = useRef();

  const isTextOverflow = element => element && element.clientWidth < element.scrollWidth;

  const checkOverflow = () => {
    const element = titleRef.current;

    const overflow = isTextOverflow(element);
    if (overflow !== overflowTooltip) {
      setOverflowTooltip(overflow);
    }
  };

  useEffect(() => {
    checkOverflow();
  });

  const valueText = () => <p ref={titleRef}>{title}</p>;

  return (
    <div className={classNames('InfoItem', className)}>
      <h3>{subtitle}</h3>
      {overflowTooltip ? (
        <CustomTooltip placement="bottom" title={title} noPadding noArrow variant="white">
          {valueText()}
        </CustomTooltip>
      ) : (
        valueText()
      )}
    </div>
  );
};

export default InfoItem;

InfoItem.defaultProps = {
  subtitle: '',
  title: '',
  className: ''
};

InfoItem.propTypes = {
  subtitle: PropTypes.string,
  title: PropTypes.string,
  className: PropTypes.string
};
