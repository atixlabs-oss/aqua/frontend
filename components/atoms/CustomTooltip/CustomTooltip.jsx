import { Tooltip } from 'antd';
import classNames from 'classnames';
import PropTypes from 'prop-types';

import styles from './custom-tooltip.module.scss';

export const CustomTooltip = ({
  variant,
  className,
  noPadding,
  noArrow,
  placement,
  children,
  ...rest
}) => (
  <Tooltip
    overlayClassName={classNames(styles.customTooltip, className, {
      [styles[`--${variant}`]]: variant,
      [styles['--noPadding']]: noPadding,
      [styles['--noArrow']]: noArrow,
      [styles[`--${placement}`]]: placement
    })}
    placement={placement}
    {...rest}
  >
    <span>{children}</span>
  </Tooltip>
);

CustomTooltip.propTypes = {
  variant: PropTypes.string,
  className: PropTypes.string,
  noPadding: PropTypes.bool,
  noArrow: PropTypes.bool,
  placement: PropTypes.string,
  children: PropTypes.element
};
