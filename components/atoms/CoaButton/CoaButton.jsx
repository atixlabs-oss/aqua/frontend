/**
 * AGUA Copyright (C) 2023  Atix Labs
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import { Button } from 'antd';
import React, { useState } from 'react';
import PropTypes from 'prop-types';

import classNames from 'classnames';

export const CoaButton = ({
  children,
  disabled,
  className,
  type,
  fullWidth,
  shape,
  onClick,
  loading,
  ...rest
}) => {
  const [isLoading, setIsLoading] = useState(false);

  const handleClick = async event => {
    try {
      setIsLoading(true);
      await onClick?.(event);
    } catch {
    } finally {
      setIsLoading(false);
    }
  };

  return (
    <Button
      loading={loading || isLoading}
      disabled={disabled}
      type={type}
      className={classNames('coaButton', {
        [className]: Boolean(className),
        '--disabled': disabled,
        [type]: Boolean(type),
        '--fullWidth': fullWidth,
        '--withDefaultHeight': shape !== 'circle'
      })}
      shape={shape}
      onClick={handleClick}
      {...rest}
    >
      {children}
    </Button>
  );
};

CoaButton.defaultProps = {
  children: '',
  disabled: false,
  className: '',
  type: undefined
};

CoaButton.propTypes = {
  children: PropTypes.oneOfType([PropTypes.arrayOf(PropTypes.node), PropTypes.node]),
  disabled: PropTypes.bool,
  className: PropTypes.string,
  type: PropTypes.string,
  fullWidth: PropTypes.bool,
  shape: PropTypes.string,
  onClick: PropTypes.func,
  loading: PropTypes.bool
};
