import React from 'react';
import Head from 'next/head';
import PropTypes from 'prop-types';

export const CustomHead = ({ titleText }) => (
  <Head>
    <title>
      {process.env.NEXT_PUBLIC_ORGANIZATION_NAME} {titleText && `- ${titleText}`}
    </title>
  </Head>
);

CustomHead.propTypes = {
  titleText: PropTypes.string
};
