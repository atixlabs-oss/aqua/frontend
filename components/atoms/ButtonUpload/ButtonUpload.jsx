/**
 * AGUA Copyright (C) 2023  Atix Labs
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import React from 'react';
import PropTypes from 'prop-types';
import { Upload, Button, Icon } from 'antd';

const ButtonUpload = ({
  change,
  name,
  typeAccepts,
  buttonText,
  showUploadList,
  defaultFileList,
  hideButton,
  onRemove,
  beforeUpload,
  fileList,
  listType
}) => {
  const dummyRequest = ({ onSuccess }) => {
    setTimeout(() => {
      onSuccess('ok');
    }, 0);
  };

  const props = {
    name,
    customRequest: dummyRequest,
    onChange: change,
    accept: typeAccepts,
    showUploadList,
    defaultFileList,
    onRemove,
    beforeUpload,
    listType
  };

  if (fileList && fileList.length > 0) {
    props.fileList = fileList;
  } else {
    props.fileList = defaultFileList;
  }

  return (
    <Upload {...props}>
      {!hideButton && (
        <Button>
          {buttonText} <Icon type="upload" />
        </Button>
      )}
    </Upload>
  );
};

export default ButtonUpload;

ButtonUpload.defaultProps = {
  typeAccepts: '',
  showUploadList: false,
  hideButton: false,
  beforeUpload: () => {},
  fileList: [],
  listType: []
};

ButtonUpload.propTypes = {
  change: PropTypes.func.isRequired,
  name: PropTypes.string.isRequired,
  typeAccepts: PropTypes.string,
  buttonText: PropTypes.string.isRequired,
  showUploadList: PropTypes.bool,
  defaultFileList: PropTypes.node.isRequired,
  hideButton: PropTypes.bool,
  onRemove: PropTypes.func.isRequired,
  beforeUpload: PropTypes.func,
  fileList: PropTypes.node,
  listType: PropTypes.node
};
