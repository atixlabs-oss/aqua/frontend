/**
 * AGUA Copyright (C) 2023  Atix Labs
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import React from 'react';
import PropTypes from 'prop-types';
import { Form, Input } from 'antd';

// TODO : allow to pass another kind of elements, no just use the Form.Item harcoded.
export default function FieldInput(props) {
  const {
    name,
    label,
    placeholder,
    value,
    valid,
    errorMessage,
    handleChange,
    type,
    maxLength,
    disabled,
    style,
    hasFeedback
  } = props;

  const validateStatus = () => {
    if (valid === undefined) {
      return '';
    }
    return valid ? 'success' : 'error';
  };

  return (
    <Form.Item
      label={label}
      validateStatus={validateStatus()}
      help={errorMessage}
      hasFeedback={hasFeedback}
    >
      <Input
        name={name}
        type={type}
        placeholder={placeholder}
        value={value}
        size="large"
        onChange={handleChange}
        disabled={disabled}
        maxLength={maxLength}
        style={style}
      />
    </Form.Item>
  );
}
FieldInput.defaultProps = {
  name: undefined,
  label: undefined,
  valid: undefined,
  errorMessage: undefined,
  placeholder: undefined,
  type: undefined,
  maxLength: 80,
  disabled: false,
  value: undefined,
  style: {},
  hasFeedback: false
};

FieldInput.propTypes = {
  name: PropTypes.string,
  label: PropTypes.oneOfType([PropTypes.arrayOf(PropTypes.node), PropTypes.node]),
  value: PropTypes.element,
  valid: PropTypes.bool,
  errorMessage: PropTypes.string,
  handleChange: PropTypes.func,
  placeholder: PropTypes.string,
  type: PropTypes.string,
  maxLength: PropTypes.number,
  disabled: PropTypes.bool,
  style: PropTypes.objectOf(PropTypes.any),
  hasFeedback: PropTypes.bool
};
