/**
 * AGUA Copyright (C) 2023  Atix Labs
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import React from 'react';
import Link from 'next/link';
import classNames from 'classnames';
import Styles from './coa-link.module.scss';
import PropTypes from 'prop-types';

export const CoaLink = ({ children, className, variant, target, rel, underlined, ...rest }) => (
  <Link {...rest}>
    <a
      className={classNames(
        Styles.coaLink,
        Styles[`--${variant}`],
        {
          [Styles['--underlined']]: underlined
        },
        className
      )}
      target={target}
      rel={rel}
    >
      {children}
    </a>
  </Link>
);

CoaLink.propTypes = {
  variant: PropTypes.string,
  className: PropTypes.string,
  children: PropTypes.oneOfType([PropTypes.arrayOf(PropTypes.node), PropTypes.node]),
  underlined: PropTypes.bool
};

CoaLink.defaultProps = {
  variant: 'primary',
  className: undefined,
  children: undefined,
  underlined: true
};
