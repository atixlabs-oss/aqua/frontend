/**
 * AGUA Copyright (C) 2023  Atix Labs
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import { Icon } from 'antd';
import {
  FEEDBACK_MESSAGE_ICONS,
  ICON_CLASSES_BY_FEEDBACK_TYPE
} from 'components/organisms/AssignProjectUsers/constants';
import React, { useState, useEffect } from 'react';

import PropTypes from 'prop-types';

export const FeedbackMessage = ({ message, type, seconds, show, className }) => {
  const [showMessage, setShowMessage] = useState(false);

  useEffect(() => {
    setShowMessage(show);
  }, [show]);

  useEffect(() => {
    let timeout;
    if (showMessage && seconds > 0) {
      timeout = setTimeout(() => {
        setShowMessage(false);
      }, [seconds * 1000]);
    }
    return () => {
      clearTimeout(timeout);
    };
  }, [showMessage, seconds]);

  return (
    <div className={`feedbackMessage__container ${showMessage ? '--show' : '--hide'} ${className}`}>
      <Icon
        type={FEEDBACK_MESSAGE_ICONS[type] || 'close-circle'}
        theme="filled"
        className={`feedbackMessage__container__icon --${ICON_CLASSES_BY_FEEDBACK_TYPE[type]}`}
      />
      <p>{message}</p>
    </div>
  );
};

FeedbackMessage.defaultProps = {
  message: undefined,
  type: '',
  seconds: undefined,
  show: false,
  className: ''
};

FeedbackMessage.propTypes = {
  message: PropTypes.string,
  type: PropTypes.string,
  seconds: PropTypes.number,
  show: PropTypes.bool,
  className: PropTypes.string
};
