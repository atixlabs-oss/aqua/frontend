/**
 * AGUA Copyright (C) 2023  Atix Labs
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import { Button } from 'antd';
import React from 'react';
import PropTypes from 'prop-types';

import classNames from 'classnames';

export const CoaTextButton = ({ children, disabled, className, variant, ...rest }) => (
  <Button
    type="link"
    disabled={disabled}
    className={classNames('customTextButton', {
      '--disabled': disabled,
      [`--${variant}`]: variant,
      [className]: Boolean(className)
    })}
    {...rest}
  >
    {children}
  </Button>
);

CoaTextButton.defaultProps = {
  children: '',
  disabled: false,
  className: undefined,
  variant: undefined
};

CoaTextButton.propTypes = {
  children: PropTypes.oneOfType([PropTypes.arrayOf(PropTypes.node), PropTypes.node]),
  disabled: PropTypes.bool,
  className: PropTypes.string,
  variant: PropTypes.oneOf(['danger, muted', 'black', 'primary'])
};
