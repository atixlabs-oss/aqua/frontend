import React, { useState } from 'react';
import PropTypes from 'prop-types';
import { Switch } from 'antd';

import Styles from './custom-toggle.module.scss';

export const CustomToggle = ({ onToggle, defaultChecked }) => {
  const [isLoading, setIsLoading] = useState(false);
  return (
    <Switch
      className={Styles.toggle}
      defaultChecked={defaultChecked}
      onChange={async () => {
        try {
          setIsLoading(true);
          await onToggle();
        } catch {
        } finally {
          setIsLoading(false);
        }
      }}
      isLoading={isLoading}
    />
  );
};

CustomToggle.propTypes = {
  onToggle: PropTypes.func,
  defaultChecked: PropTypes.bool
};
