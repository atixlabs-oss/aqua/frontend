/**
 * AGUA Copyright (C) 2023  Atix Labs
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import React from 'react';

export const BankIcon = ({ ...rest }) => (
  <svg
    xmlns="http://www.w3.org/2000/svg"
    width="18"
    height="18"
    viewBox="0 0 18 18"
    fill="none"
    {...rest}
  >
    <path
      d="M8.0265 0.354078C8.60125 -0.0859216 9.40012 -0.0859216 9.97487 0.354078L16.7976 5.56945C17.6666 6.23358 17.2005 7.61958 16.1088 7.62508H1.89125C0.800875 7.61958 0.333375 6.23358 1.20375 5.56945L8.0265 0.354078ZM9 5.21883C9.2735 5.21883 9.53581 5.11018 9.7292 4.91678C9.9226 4.72339 10.0312 4.46108 10.0312 4.18758C10.0312 3.91407 9.9226 3.65177 9.7292 3.45837C9.53581 3.26498 9.2735 3.15633 9 3.15633C8.7265 3.15633 8.46419 3.26498 8.2708 3.45837C8.0774 3.65177 7.96875 3.91407 7.96875 4.18758C7.96875 4.46108 8.0774 4.72339 8.2708 4.91678C8.46419 5.11018 8.7265 5.21883 9 5.21883ZM2.8125 9.00008V13.1251H4.875V9.00008H2.8125ZM6.25 9.00008V13.1251H8.3125V9.00008H6.25ZM9.6875 9.00008V13.1251H11.75V9.00008H9.6875ZM13.125 9.00008V13.1251H15.1875V9.00008H13.125ZM0.75 16.2188C0.75 15.2701 1.52 14.5001 2.46875 14.5001H15.5312C16.48 14.5001 17.25 15.2701 17.25 16.2188V16.5626C17.25 16.7449 17.1776 16.9198 17.0486 17.0487C16.9197 17.1776 16.7448 17.2501 16.5625 17.2501H1.4375C1.25516 17.2501 1.0803 17.1776 0.951364 17.0487C0.822433 16.9198 0.75 16.7449 0.75 16.5626V16.2188Z"
      fill="currentColor"
    />
  </svg>
);
