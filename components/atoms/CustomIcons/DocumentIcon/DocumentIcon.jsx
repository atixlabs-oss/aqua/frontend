/**
 * AGUA Copyright (C) 2023  Atix Labs
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import React from 'react';
import Styles from './document-icon.module.scss';
import PropTypes from 'prop-types';

export const DocumentIcon = ({ fontSize, ...rest }) => (
  <svg
    xmlns="http://www.w3.org/2000/svg"
    width="14"
    height="18"
    viewBox="0 0 14 18"
    fill="none"
    className={Styles.documentIcon}
    style={{ '--fontSize': fontSize }}
    {...rest}
  >
    <path
      d="M13.5664 4.3887C13.6836 4.50589 13.75 4.66409 13.75 4.83011V16.875C13.75 17.2207 13.4707 17.5 13.125 17.5H0.625C0.279297 17.5 0 17.2207 0 16.875V0.625031C0 0.279327 0.279297 3.05176e-05 0.625 3.05176e-05H8.91992C9.08594 3.05176e-05 9.24609 0.0664368 9.36328 0.183624L13.5664 4.3887ZM12.3086 5.11722L8.63281 1.44144V5.11722H12.3086ZM3.125 8.16409C3.08356 8.16409 3.04382 8.18056 3.01451 8.20986C2.98521 8.23916 2.96875 8.2789 2.96875 8.32034V9.25784C2.96875 9.29928 2.98521 9.33903 3.01451 9.36833C3.04382 9.39763 3.08356 9.41409 3.125 9.41409H10.625C10.6664 9.41409 10.7062 9.39763 10.7355 9.36833C10.7648 9.33903 10.7812 9.29928 10.7812 9.25784V8.32034C10.7812 8.2789 10.7648 8.23916 10.7355 8.20986C10.7062 8.18056 10.6664 8.16409 10.625 8.16409H3.125ZM3.125 10.8203C3.08356 10.8203 3.04382 10.8368 3.01451 10.8661C2.98521 10.8954 2.96875 10.9352 2.96875 10.9766V11.9141C2.96875 11.9555 2.98521 11.9953 3.01451 12.0246C3.04382 12.0539 3.08356 12.0703 3.125 12.0703H6.71875C6.76019 12.0703 6.79993 12.0539 6.82924 12.0246C6.85854 11.9953 6.875 11.9555 6.875 11.9141V10.9766C6.875 10.9352 6.85854 10.8954 6.82924 10.8661C6.79993 10.8368 6.76019 10.8203 6.71875 10.8203H3.125Z"
      fill="currentColor"
    />
  </svg>
);

DocumentIcon.defaultProps = {
  fontSize: '1rem'
};

DocumentIcon.propTypes = {
  fontSize: PropTypes.string
};
