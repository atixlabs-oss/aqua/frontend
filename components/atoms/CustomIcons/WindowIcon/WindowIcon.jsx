/**
 * AGUA Copyright (C) 2023  Atix Labs
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import React from 'react';
import classNames from 'classnames';
import Styles from './window-icon.module.scss';

const WindowIcon = props => (
  <svg
    height="161"
    width="178"
    fill="none"
    viewBox="0 0 178 161"
    xmlns="http://www.w3.org/2000/svg"
  >
    <ellipse cx="89.1344" cy="153.434" fill="#9CA6B8" opacity="0.2" rx="50.4074" ry="7.06645" />
    <path
      d="M55.6816 40.3427C49.8016 54.1522 28.9457 44.8223 19.7864 55.8758C-7.77146 74.611 -6.32178 121.964 29.8818 128.624C58.015 139.304 84.7991 122.09 112.07 120.54C132.354 125.249 160.722 137.446 174.53 114.536C189.429 86.4653 137.912 87.6688 156.336 58.4033C181.181 12.3104 70.3932 -35.6613 55.6722 40.3488L55.6816 40.3427Z"
      fill="#DBE5FD"
      className={classNames(Styles.windowIcon, Styles['--backgroundFill'])}
    />
    <rect
      height="73"
      width="83"
      rx="3.5"
      stroke="#4C7FF7"
      strokeWidth="7"
      x="47.2271"
      y="37"
      className={classNames(Styles.windowIcon, Styles['--iconStroke'])}
    />
    <rect
      height="18"
      width="79"
      fill="#4C7FF7"
      x="49.7271"
      y="39.5"
      className={classNames(Styles.windowIcon, Styles['--iconFill'])}
    />
    <rect
      height="5.74074"
      width="12"
      fill="#4C7FF7"
      rx="2.87037"
      x="58.7271"
      y="66.5"
      className={classNames(Styles.windowIcon, Styles['--iconFill'])}
    />
    <rect
      height="5.74074"
      width="42"
      fill="#4C7FF7"
      rx="2.87037"
      x="77.7271"
      y="66.5"
      className={classNames(Styles.windowIcon, Styles['--iconFill'])}
    />
    <rect
      height="5.74074"
      width="12"
      fill="#4C7FF7"
      rx="2.87037"
      x="58.7271"
      y="91.7598"
      className={classNames(Styles.windowIcon, Styles['--iconFill'])}
    />
    <rect
      height="5.74074"
      width="42"
      fill="#4C7FF7"
      rx="2.87037"
      x="77.7271"
      y="91.7598"
      className={classNames(Styles.windowIcon, Styles['--iconFill'])}
    />
    <rect
      height="5.74074"
      width="12"
      fill="#4C7FF7"
      rx="2.87037"
      x="58.7271"
      y="79.1289"
      className={classNames(Styles.windowIcon, Styles['--iconFill'])}
    />
    <rect
      height="5.74074"
      width="32"
      fill="#4C7FF7"
      rx="2.87037"
      x="77.7271"
      y="79.1289"
      className={classNames(Styles.windowIcon, Styles['--iconFill'])}
    />
  </svg>
);

export default WindowIcon;
