import React from 'react';

export const UserIcon = () => (
  <svg xmlns="http://www.w3.org/2000/svg" width="25" height="25" viewBox="0 0 25 25" fill="none">
    <mask id="mask0_5709_12935" maskUnits="userSpaceOnUse" x="0" y="0" width="25" height="25">
      <circle cx="12.125" cy="12.1738" r="12" fill="currentColor" />
    </mask>
    <g mask="url(#mask0_5709_12935)">
      <rect
        x="0.125"
        y="0.173828"
        width="24"
        height="24"
        rx="2"
        fill="currentcolor"
        opacity="0.3"
      />
      <path
        d="M12.125 0.173828H22.125C23.2296 0.173828 24.125 1.06926 24.125 2.17383V22.1738C24.125 23.2784 23.2296 24.1738 22.125 24.1738H12.125V0.173828Z"
        fill="currentColor"
        opacity="0.8"
      />
    </g>
    <path
      d="M12 5C12.9283 5 13.8185 5.36875 14.4749 6.02513C15.1313 6.6815 15.5 7.57174 15.5 8.5C15.5 9.42826 15.1313 10.3185 14.4749 10.9749C13.8185 11.6313 12.9283 12 12 12C11.0717 12 10.1815 11.6313 9.52513 10.9749C8.86875 10.3185 8.5 9.42826 8.5 8.5C8.5 7.57174 8.86875 6.6815 9.52513 6.02513C10.1815 5.36875 11.0717 5 12 5ZM12 13.75C15.8675 13.75 19 15.3162 19 17.25V19H5V17.25C5 15.3162 8.1325 13.75 12 13.75Z"
      fill="currentColor"
    />
  </svg>
);
