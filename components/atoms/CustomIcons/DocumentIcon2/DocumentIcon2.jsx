/**
 * AGUA Copyright (C) 2023  Atix Labs
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import classNames from 'classnames';
import React from 'react';
import Styles from './document-icon-2.module.scss';

export const DocumentIcon2 = props => (
  <svg
    height="160"
    width="178"
    fill="none"
    viewBox="0 0 178 160"
    xmlns="http://www.w3.org/2000/svg"
  >
    <ellipse cx="89.1344" cy="152.934" fill="#9CA6B8" opacity="0.2" rx="50.4074" ry="7.06645" />
    <path
      d="M55.6816 39.8427C49.8016 53.6522 28.9457 44.3223 19.7864 55.3758C-7.77146 74.111 -6.32178 121.464 29.8818 128.124C58.015 138.804 84.7991 121.59 112.07 120.04C132.354 124.749 160.722 136.946 174.53 114.036C189.429 85.9653 137.912 87.1688 156.336 57.9033C181.181 11.8104 70.3932 -36.1613 55.6722 39.8488L55.6816 39.8427Z"
      fill="#DBE5FD"
      className={classNames(Styles.documentIcon2, Styles['--backgroundFill'])}
    />
    <path
      d="M110.727 43V48C110.727 51.866 113.861 55 117.727 55H123.727"
      stroke="#4C7FF7"
      strokeWidth="7"
      className={classNames(Styles.documentIcon2, Styles['--iconStroke'])}
    />
    <path
      d="M123.727 110.5H53.727C51.7941 110.5 50.2271 108.933 50.2271 107V43C50.2271 41.067 51.7941 39.5 53.7271 39.5H111.828C112.756 39.5 113.646 39.8687 114.302 40.5251L126.202 52.4246C126.858 53.081 127.227 53.9712 127.227 54.8995V107C127.227 108.933 125.66 110.5 123.727 110.5Z"
      stroke="#4C7FF7"
      strokeWidth="7"
      className={classNames(Styles.documentIcon2, Styles['--iconStroke'])}
    />
    <rect
      height="5.74074"
      width="10.2295"
      fill="#4C7FF7"
      rx="2.87037"
      transform="rotate(180 114.727 70.7402)"
      x="114.727"
      y="70.7402"
      className={classNames(Styles.documentIcon2, Styles['--iconFill'])}
    />
    <rect
      height="5.74074"
      width="35.8033"
      fill="#4C7FF7"
      rx="2.87037"
      transform="rotate(180 98.5304 70.7402)"
      x="98.5304"
      y="70.7402"
      className={classNames(Styles.documentIcon2, Styles['--iconFill'])}
    />
    <rect
      height="6"
      width="50"
      fill="#4C7FF7"
      rx="3"
      transform="rotate(180 112.727 84)"
      x="112.727"
      y="84"
      className={classNames(Styles.documentIcon2, Styles['--iconFill'])}
    />
    <rect
      height="6"
      width="36"
      fill="#4C7FF7"
      rx="3"
      transform="rotate(180 98.7271 97)"
      x="98.7271"
      y="97"
      className={classNames(Styles.documentIcon2, Styles['--iconFill'])}
    />
  </svg>
);
