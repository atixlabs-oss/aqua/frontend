/**
 * AGUA Copyright (C) 2023  Atix Labs
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import React from 'react';

export const MembersIcon = () => (
  <svg xmlns="http://www.w3.org/2000/svg" width="32" height="24" viewBox="0 0 32 24" fill="none">
    <path
      d="M21.9167 13.695C23.9717 15.09 25.4117 16.98 25.4117 19.5V24H31.4117V19.5C31.4117 16.23 26.0567 14.295 21.9167 13.695Z"
      fill="currentColor"
    />
    <path
      d="M19.4117 12C22.7267 12 25.4117 9.315 25.4117 6C25.4117 2.685 22.7267 0 19.4117 0C18.7067 0 18.0467 0.15 17.4167 0.36C18.6617 1.905 19.4117 3.87 19.4117 6C19.4117 8.13 18.6617 10.095 17.4167 11.64C18.0467 11.85 18.7067 12 19.4117 12Z"
      fill="currentColor"
    />
    <path
      d="M12 12C15.315 12 18 9.315 18 6C18 2.685 15.315 0 12 0C8.685 0 6 2.685 6 6C6 9.315 8.685 12 12 12Z"
      fill="currentColor"
    />
    <path
      d="M12 13.5C7.995 13.5 0 15.51 0 19.5V24H24V19.5C24 15.51 16.005 13.5 12 13.5Z"
      fill="currentColor"
    />
  </svg>
);
