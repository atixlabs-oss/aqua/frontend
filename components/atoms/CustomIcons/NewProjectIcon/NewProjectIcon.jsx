/**
 * AGUA Copyright (C) 2023  Atix Labs
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import classNames from 'classnames';
import React from 'react';
import Styles from './new-project-icon.module.scss';

const NewProjectIcon = () => (
  <svg
    height="113"
    width="118"
    fill="none"
    viewBox="0 0 118 113"
    xmlns="http://www.w3.org/2000/svg"
  >
    <mask
      height="113"
      id="mask0_841_3628"
      style={{ maskType: 'alpha' }}
      width="114"
      x="4"
      y="0"
      maskUnits="userSpaceOnUse"
    >
      <path
        d="M118 56.5C118 87.7041 89.3402 113 61.2691 113C33.8818 113 4.53833 87.7041 4.53833 56.5C4.53833 25.2959 38.4464 0 61.2691 0C84.0918 0 118 22.0805 118 56.5Z"
        fill="#C4C4C4"
      />
    </mask>
    <g mask="url(#mask0_841_3628)">
      <path
        d="M118 56.5C118 87.7041 89.3402 113 61.2691 113C33.8818 113 4.53833 87.7041 4.53833 56.5C4.53833 25.2959 38.4464 0 61.2691 0C84.0918 0 118 22.0805 118 56.5Z"
        fill="url(#paint0_linear_841_3628)"
      />
      <g opacity="0.9">
        <g>
          <path
            d="M96.0637 81.3601L78.6663 74.5801L96.0637 67.8L110.435 74.5801L96.0637 81.3601Z"
            fill="#A9C0FD"
            className={classNames(Styles.newProjectIcon, Styles['--fillPrimaryExtremeLight'])}
          />
        </g>
        <g>
          <path
            d="M29.4997 81.3601L12.1023 74.5801L29.4997 67.8L45.3843 74.5801L29.4997 81.3601Z"
            fill="#A9C0FD"
            className={classNames(Styles.newProjectIcon, Styles['--fillPrimaryExtremeLight'])}
          />
        </g>
        <path
          d="M28.7434 121.287V81.3601H96.0639V121.287H28.7434Z"
          fill="url(#paint1_linear_841_3628)"
        />
        <path
          d="M45.3844 74.5801L28.7434 81.3601H96.0639L78.6665 74.5801H45.3844Z"
          fill="url(#paint2_linear_841_3628)"
        />
        <path
          d="M75.955 58.941C75.4718 59.3589 74.9552 59.7369 74.4102 60.0711C70.6292 62.4886 64.7562 64.0112 59.9329 64.7974C56.7358 61.0828 53.1932 56.1258 51.8679 51.8273C51.6834 51.2463 51.5418 50.6528 51.4443 50.0513C50.2719 43.7874 52.1537 38.6411 56.1551 37.1682C58.5998 36.4111 61.2545 36.7517 63.4268 38.1011C65.5992 39.4506 67.0738 41.6752 67.4664 44.1949C69.3941 42.5169 71.9597 41.7579 74.4945 42.1158C77.0293 42.4736 79.2821 43.9127 80.6658 46.058C82.7878 49.7517 80.906 54.8981 75.955 58.941Z"
          fill="url(#paint3_linear_841_3628)"
        />
        <path
          d="M85.0319 57.4344C84.5487 57.8523 84.0321 58.2303 83.4871 58.5645C79.7061 60.982 73.8332 62.5046 69.0098 63.2908C65.8127 59.5762 62.2701 54.6192 60.9448 50.3207C60.7603 49.7397 60.6188 49.1462 60.5212 48.5447C59.3488 42.2809 61.2306 37.1345 65.232 35.6616C67.6767 34.9045 70.3314 35.2451 72.5038 36.5946C74.6761 37.944 76.1507 40.1686 76.5433 42.6883C78.471 41.0103 81.0366 40.2513 83.5714 40.6092C86.1062 40.967 88.359 42.4061 89.7427 44.5514C91.8647 48.2451 89.9828 53.3915 85.0319 57.4344Z"
          fill="url(#paint4_linear_841_3628)"
          opacity="0.1"
        />
      </g>
    </g>
    <path
      d="M9.58974 47.4601C9.58974 49.8164 7.67071 51.7334 5.29487 51.7334C2.91903 51.7334 1 49.8164 1 47.4601C1 45.1038 2.91903 43.1868 5.29487 43.1868C7.67071 43.1868 9.58974 45.1038 9.58974 47.4601Z"
      stroke="#D1DAF6"
      strokeWidth="2"
      className={classNames(Styles.newProjectIcon, Styles['--strokePrimaryExtremeLight'])}
    />
    <path
      d="M102.872 15.0667V24.1067"
      stroke="#729FFA"
      strokeLinecap="round"
      strokeWidth="2.5"
      className={classNames(Styles.newProjectIcon, Styles['--strokePrimaryLighter'])}
    />
    <path
      d="M107.41 19.5867L98.3332 19.5867"
      stroke="#729FFA"
      strokeLinecap="round"
      strokeWidth="2.5"
      className={classNames(Styles.newProjectIcon, Styles['--strokePrimaryLighter'])}
    />
    <path
      d="M43.8718 52.7334V79.8534H74.1282V52.7334H43.8718Z"
      fill="url(#paint5_linear_841_3628)"
      opacity="0.2"
    />
    <path
      d="M54.4614 52.7334V79.8534H84.7178V52.7334H54.4614Z"
      fill="url(#paint6_linear_841_3628)"
      opacity="0.2"
    />
    <defs>
      <linearGradient
        id="paint0_linear_841_3628"
        gradientUnits="userSpaceOnUse"
        x1="61.2691"
        x2="61.2691"
        y1="0"
        y2="113"
      >
        <stop
          offset="0.59375"
          stopColor="#F1F6FF"
          className={classNames(Styles.newProjectIcon, Styles['--stopPrimaryExtremeLightest'])}
        />
        <stop
          offset="0.838542"
          stopColor="#B9CFFC"
          className={classNames(Styles.newProjectIcon, Styles['--stopPrimaryExtremeLightest'])}
        />
      </linearGradient>
      <linearGradient
        id="paint1_linear_841_3628"
        gradientUnits="userSpaceOnUse"
        x1="62.4037"
        x2="62.4037"
        y1="81.3601"
        y2="121.287"
      >
        <stop
          stopColor="#AAC1FD"
          className={classNames(Styles.newProjectIcon, Styles['--stopPrimaryExtremeLighter'])}
        />
        <stop
          offset="1"
          stopColor="#92B4FC"
          className={classNames(Styles.newProjectIcon, Styles['--stopPrimaryUltraLighter'])}
        />
      </linearGradient>
      <linearGradient
        id="paint2_linear_841_3628"
        gradientUnits="userSpaceOnUse"
        x1="62.4037"
        x2="62.4037"
        y1="74.5801"
        y2="81.3601"
      >
        <stop
          stopColor="#7A9EF5"
          className={classNames(Styles.newProjectIcon, Styles['--stopPrimaryLightest'])}
        />
        <stop
          offset="1"
          stopColor="#5590FC"
          className={classNames(Styles.newProjectIcon, Styles['--stopPrimary'])}
        />
      </linearGradient>
      <linearGradient
        id="paint3_linear_841_3628"
        gradientUnits="userSpaceOnUse"
        x1="69.0292"
        x2="60.0014"
        y1="39.931"
        y2="64.8223"
      >
        <stop
          stopColor="#A6BEFD"
          className={classNames(Styles.newProjectIcon, Styles['--stopPrimaryUltraLightest'])}
        />
        <stop
          offset="1"
          stopColor="#6393EA"
          className={classNames(Styles.newProjectIcon, Styles['--stopPrimaryLight'])}
        />
      </linearGradient>
      <linearGradient
        id="paint4_linear_841_3628"
        gradientUnits="userSpaceOnUse"
        x1="78.1061"
        x2="69.0783"
        y1="38.4244"
        y2="63.3157"
      >
        <stop
          stopColor="#A6BEFD"
          className={classNames(Styles.newProjectIcon, Styles['--stopPrimaryUltraLightest'])}
        />
        <stop
          offset="1"
          stopColor="#6393EA"
          className={classNames(Styles.newProjectIcon, Styles['--stopPrimaryLight'])}
        />
      </linearGradient>
      <linearGradient
        id="paint5_linear_841_3628"
        gradientUnits="userSpaceOnUse"
        x1="41.6026"
        x2="74.1282"
        y1="70.1677"
        y2="70.1677"
      >
        <stop stopColor="white" stopOpacity="0" />
        <stop
          offset="0.505208"
          stopColor="#7AA2F9"
          className={classNames(Styles.newProjectIcon, Styles['--stopPrimaryUltraLight'])}
        />
        <stop
          offset="1"
          stopColor="#E9F0FE"
          stopOpacity="0.164948"
          className={classNames(Styles.newProjectIcon, Styles['--stopPrimaryExtremeLightest'])}
        />
      </linearGradient>
      <linearGradient
        id="paint6_linear_841_3628"
        gradientUnits="userSpaceOnUse"
        x1="52.1922"
        x2="84.7178"
        y1="70.1677"
        y2="70.1677"
      >
        <stop stopColor="white" stopOpacity="0" />
        <stop
          offset="0.505208"
          stopColor="#7AA2F9"
          className={classNames(Styles.newProjectIcon, Styles['--stopPrimaryUltraLight'])}
        />
        <stop
          offset="1"
          stopColor="#E9F0FE"
          stopOpacity="0.164948"
          className={classNames(Styles.newProjectIcon, Styles['--stopPrimaryExtremeLightest'])}
        />
      </linearGradient>
    </defs>
  </svg>
);

export default NewProjectIcon;
