/**
 * AGUA Copyright (C) 2023  Atix Labs
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import classNames from 'classnames';
import React from 'react';
import Styles from './check-icon.module.scss';

export const CheckIcon = ({ checked }) => (
  <svg height="35" width="35" fill="none" viewBox="0 0 35 35" xmlns="http://www.w3.org/2000/svg">
    <path
      d="M17.5 35C27.165 35 35 27.165 35 17.5C35 7.83502 27.165 0 17.5 0C7.83502 0 0 7.83502 0 17.5C0 27.165 7.83502 35 17.5 35Z"
      className={classNames(Styles.check, { [Styles['--checked']]: checked })}
      fill="currentColor"
      fillOpacity="0.3"
    />
    <path
      d="M23.1153 13.2995C22.8827 13.0668 22.5055 13.0668 22.2728 13.2995L15.1355 20.4369L12.392 17.6934C12.1594 17.4608 11.7822 17.4608 11.5495 17.6934C11.3168 17.9261 11.3168 18.3032 11.5495 18.5359L14.7142 21.7006C14.9468 21.9332 15.3242 21.9331 15.5567 21.7006L23.1153 14.142C23.348 13.9094 23.3479 13.5322 23.1153 13.2995Z"
      fill="currentColor"
      className={classNames(Styles.background, { [Styles['--checked']]: checked })}
      stroke="currentColor"
    />
  </svg>
);
