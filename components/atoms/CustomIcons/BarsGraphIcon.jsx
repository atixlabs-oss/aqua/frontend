import React from 'react';

export const BarsGraphIcon = () => (
  <svg width="24" height="25" viewBox="0 0 24 25" fill="none" xmlns="http://www.w3.org/2000/svg">
    <rect y="0.202148" width="6.66667" height="24" rx="1" fill="currentColor" />
    <rect
      x="8.6665"
      y="6.20215"
      width="6.66667"
      height="18"
      rx="1"
      fill="currentColor"
      fillOpacity="0.6"
    />
    <rect
      x="17.3335"
      y="12.2021"
      width="6.66667"
      height="12"
      rx="1"
      fill="currentColor"
      fillOpacity="0.3"
    />
  </svg>
);
