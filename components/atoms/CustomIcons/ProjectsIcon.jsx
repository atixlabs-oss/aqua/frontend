export const ProjectsIcon = () => (
  <svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
    <rect
      x="13.4351"
      width="10.5646"
      height="10.5646"
      rx="2"
      fill="currentColor"
      fillOpacity="0.3"
    />
    <rect x="0.22998" width="10.5646" height="10.5646" rx="2" fill="currentColor" />
    <rect
      x="13.4351"
      y="13.4355"
      width="10.5646"
      height="10.5646"
      rx="2"
      fill="currentColor"
      fillOpacity="0.3"
    />
    <rect
      y="13.4355"
      width="10.5646"
      height="10.5646"
      rx="2"
      fill="currentColor"
      fillOpacity="0.3"
    />
  </svg>
);
