/**
 * AGUA Copyright (C) 2023  Atix Labs
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import classNames from 'classnames';
import React from 'react';
import Styles from './milestone-icon-2.module.scss';

export const MilestoneIcon2 = () => (
  <svg
    height="160"
    width="177"
    fill="none"
    viewBox="0 0 177 160"
    xmlns="http://www.w3.org/2000/svg"
  >
    <ellipse cx="92.9909" cy="152.933" fill="#9CA6B8" opacity="0.2" rx="50.4074" ry="7.06645" />
    <path
      d="M55.1816 39.8427C49.3016 53.6522 28.4457 44.3223 19.2864 55.3758C-8.27146 74.111 -6.82178 121.464 29.3818 128.124C57.515 138.804 84.2991 121.59 111.57 120.04C131.854 124.749 160.222 136.946 174.03 114.036C188.929 85.9653 137.412 87.1688 155.836 57.9033C180.681 11.8104 69.8932 -36.1613 55.1722 39.8488L55.1816 39.8427Z"
      fill="#DBE5FD"
      className={classNames(Styles.milestoneIcon2, Styles['--secondary'])}
    />
    <path
      d="M75.8749 48.9764C76.5111 47.8744 78.1018 47.8744 78.738 48.9764L111.092 105.016C111.728 106.118 110.933 107.495 109.661 107.495H44.9522C43.6797 107.495 42.8844 106.118 43.5207 105.016L75.8749 48.9764Z"
      stroke="#4C7FF7"
      strokeLinejoin="round"
      strokeWidth="7"
      className={classNames(Styles.milestoneIcon2, Styles['--primary'])}
    />
    <path
      d="M115.364 64.9674L140.138 107.5H111.381L101.67 88.6008L115.364 64.9674Z"
      stroke="#4C7FF7"
      strokeLinejoin="round"
      strokeWidth="7"
      className={classNames(Styles.milestoneIcon2, Styles['--primary'])}
    />
    <path
      d="M77.2271 52.6235V38.9488M77.2271 38.9488V25.9208C77.2271 25.4122 77.6393 25 78.1478 25H94.2731C95.1027 25 95.5092 26.0111 94.9104 26.5853L89.9841 31.3098C89.6062 31.6723 89.6062 32.2765 89.9841 32.639L94.9104 37.3635C95.5092 37.9377 95.1027 38.9488 94.2731 38.9488H77.2271Z"
      stroke="#4C7FF7"
      strokeWidth="6"
      className={classNames(Styles.milestoneIcon2, Styles['--primary'])}
    />
    <path
      d="M65.7271 66.5002L69.9459 75.3598C70.3045 76.1128 71.3305 76.2244 71.8425 75.5661L76.8424 69.1377C77.2911 68.5608 78.163 68.5608 78.6117 69.1377L83.6116 75.5661C84.1236 76.2244 85.1496 76.1128 85.5082 75.3598L89.7271 66.5002"
      stroke="#4C7FF7"
      strokeWidth="7"
      className={classNames(Styles.milestoneIcon2, Styles['--primary'])}
    />
  </svg>
);
