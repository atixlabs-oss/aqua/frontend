/**
 * AGUA Copyright (C) 2023  Atix Labs
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import React from 'react';
import { Icon } from 'antd';
import classNames from 'classnames';
import { CoaTextButton } from '../CoaTextButton/CoaTextButton';
import { useTranslation } from 'react-i18next';

export const AddEvidenceButton = ({ onClickAddEvidence, disabled, responsiveLayout }) => {
  const { t } = useTranslation('common');
  return (
    <CoaTextButton
      variant="primary"
      onClick={onClickAddEvidence}
      className={classNames({ addEvidenceButton: responsiveLayout })}
      disabled={disabled}
    >
      <Icon type="plus" /> {t('general.btnAdd') || 'Add'}&nbsp;
      <span className={classNames({ addEvidenceButton__text: responsiveLayout })}>
        {t('coaIndicator.evidences') || 'Evidences'}
      </span>
    </CoaTextButton>
  );
};

AddEvidenceButton.defaultProps = {
  disabled: false,
  onClickAddEvidence: () => {},
  responsiveLayout: true
};
