/**
 * AGUA Copyright (C) 2023  Atix Labs
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import React from 'react';
import { ArrowLeftOutlined } from '@ant-design/icons';
import { useRouter } from 'next/router';
import { useTranslation } from 'react-i18next';

const GoBackButton = ({ goBackTo }) => {
  const router = useRouter();
  const { t } = useTranslation();

  return (
    <div className="goBack">
      <button
        className="goBack__button"
        type="button"
        onClick={() => {
          if (typeof goBackTo === 'function') return goBackTo();
          return router.push(goBackTo);
        }}
      >
        <ArrowLeftOutlined />
        <h3 className="goBack__button__text">{t('general.btnGoBack')}</h3>
      </button>
    </div>
  );
};

export default GoBackButton;
