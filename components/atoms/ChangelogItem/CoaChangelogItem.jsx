/**
 * AGUA Copyright (C) 2023  Atix Labs
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import React from 'react';
import { Divider } from 'antd';
import PropTypes from 'prop-types';
import { formatLeadWithZero } from 'helpers/formatter';
import TransactionLink from 'components/molecules/TransactionLink/TransactionLink';
import { getDateAndTime } from 'helpers/utils';
import changelogActions from '../../../constants/ChangelogActions';
import { TransactionDetail } from '../../molecules/TransactionDetails/TransactionDetails';
import { useTranslation } from 'react-i18next';

const availableSeeTransactionDetails = [
  'send_project_to_review',
  'approve_review',
  'cancel_review',
  'activity_to_review',
  'approve_activity',
  'reject_activity',
  'publish_project'
];

const CoaChangelogItem = ({ changelog, currency }) => {
  const { transaction, datetime, action, revision } = changelog;
  const allowSeeDetails = transaction && availableSeeTransactionDetails.includes(action);
  const { t } = useTranslation();

  return (
    <div className="coaChangelogItem">
      <div className="coaChangelogItem__header">
        <p>
          <span className="coaChangelogItem__date">{getDateAndTime(datetime, 'minimal')}</span> -{' '}
          <span className="coaChangelogItem__title">
            {changelogActions(changelog, t)?.[action]?.title?.()}
          </span>
        </p>
        <span className="coaChangelogItem__rev">REV-{formatLeadWithZero(revision)}</span>
      </div>
      <Divider type="horizontal" style={{ marginBlock: '0.6875rem' }} />
      <div className="coaChangelogItem__body">
        {changelogActions(changelog, t)?.[action]?.description?.()}
      </div>
      {transaction && (
        <div className="coaChangelogItem__footer">
          <TransactionLink isChangelogActive txHash={transaction} currency={currency} />
        </div>
      )}
      {allowSeeDetails && <TransactionDetail action={action} transactionHash={transaction} />}
    </div>
  );
};

export default CoaChangelogItem;

CoaChangelogItem.defaultProps = {
  changelog: {
    user: {
      firstName: 'Juan Pablo',
      lastName: 'Yoroi'
    }
  },
  currency: undefined
};

CoaChangelogItem.propTypes = {
  changelog: PropTypes.shape({
    id: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
    project: PropTypes.objectOf(PropTypes.any),
    revision: PropTypes.number,
    milestone: PropTypes.shape({
      id: PropTypes.number,
      title: PropTypes.string
    }),
    activity: PropTypes.shape({
      id: PropTypes.number,
      title: PropTypes.string
    }),
    evidence: PropTypes.shape({
      id: PropTypes.number,
      title: PropTypes.string
    }),
    user: PropTypes.shape({
      id: PropTypes.string,
      firstName: PropTypes.string,
      lastName: PropTypes.string,
      roles: PropTypes.arrayOf(
        PropTypes.shape({
          id: PropTypes.string,
          description: PropTypes.string
        })
      )
    }),
    transaction: PropTypes.string,
    description: PropTypes.string,
    action: PropTypes.string.isRequired,
    datetime: PropTypes.string.isRequired
  }),
  currency: PropTypes.string
};
