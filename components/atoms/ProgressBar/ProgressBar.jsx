/**
 * AGUA Copyright (C) 2023  Atix Labs
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import React from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';

export const ProgressBar = ({
  initialLabel,
  endLabel,
  progressBarColor,
  currentPercentage,
  className
}) => (
  <div className={classNames('progressBar', className)}>
    {initialLabel ? (
      <div className="progressBar__text progressBar__initialLabel">{initialLabel}</div>
    ) : null}
    <div className="progressBar__bar">
      <span
        className="progressBar__bar__fill"
        style={{
          '--currentPercentage': `${currentPercentage}%`,
          '--progressBarColor': progressBarColor
        }}
      ></span>
    </div>
    {endLabel ? <div className="progressBar__text progressBar__endLabel">{endLabel}</div> : null}
  </div>
);

ProgressBar.propTypes = {
  initialLabel: PropTypes.string,
  endLabel: PropTypes.string,
  progressBarColor: PropTypes.string,
  className: PropTypes.string,
  currentPercentage: PropTypes.number
};

ProgressBar.defaultProps = {
  initialLabel: '',
  endLabel: '',
  progressBarColor: '',
  className: '',
  currentPercentage: 0
};
