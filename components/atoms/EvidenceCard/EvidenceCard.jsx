/**
 * AGUA Copyright (C) 2023  Atix Labs
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import React from 'react';
import PropTypes from 'prop-types';
import { EVIDENCE_STATUS_MAP, EVIDENCE_TYPES_ENUM, EVIDENCE_STATUS_ENUM } from 'model/evidence';
import { formatCurrency } from 'helpers/formatter';
import { ACTIVITY_TYPES_ENUM } from 'model/activityTypes';
import { ACTIVITY_STATUS_ENUM } from 'model/activityStatus';
import { getDateAndTime } from '../../../helpers/utils';
import { CoaTag } from '../CoaTag/CoaTag';
import { useRouter } from 'next/router';
import { DocumentIcon } from '../CustomIcons/DocumentIcon/DocumentIcon';
import { CoinIcon } from '../CustomIcons/CoinIcon/CoinIcon';
import { useTranslation } from 'react-i18next';
import { CoaButton } from '../CoaButton/CoaButton';
import { CoaDeleteEvidenceButton } from '../CoaDeleteEvidenceButton/CoaDeleteEvidenceButton';

const EvidenceCard = ({
  evidence,
  currency,
  evidenceNumber,
  isActivityAuditor,
  preview,
  activityType,
  activityStatus,
  withDeleteButton
}) => {
  const { title, status, createdAt, description, id, type } = evidence;

  const isAuditEvidenceAvailable = isActivityAuditor && status === EVIDENCE_STATUS_ENUM.NEW;

  const { push, query, pathname } = useRouter();
  const { projectId, activityId } = query;
  const { t } = useTranslation();

  const handleGoToEvidenceDetails = () => {
    push({
      pathname: preview
        ? `${pathname}/[detailEvidenceId]?preview=true`
        : `${pathname}/[detailEvidenceId]`,
      query: {
        projectId,
        activityId,
        detailEvidenceId: id
      }
    });
  };

  return (
    <div className="evidenceCard">
      <div className="evidenceCard__header">
        <p className="evidenceCard__date">{getDateAndTime(createdAt)}</p>
        <CoaTag predefinedColor={EVIDENCE_STATUS_MAP[status]?.color}>
          {EVIDENCE_STATUS_MAP[status]?.name}
        </CoaTag>
      </div>
      <div className="evidenceCard__body">
        <p className="evidenceCard__orderNumber">
          {t('general.evidence')} N°{evidenceNumber}
        </p>
        <p className="evidenceCard__title">{title}</p>
        <div className="evidenceCard__description">{description}</div>
        <div className="evidenceCard__indicatorContainer">
          <p className="evidenceCard__indicatorContainer__indicator">
            <DocumentIcon />
            <span className="evidenceCard__indicatorContainer__indicator__text">
              {t('evidenceDetail.evidenceType')}
            </span>
          </p>
          <p className="evidenceCard__indicatorContainer__value">{type}</p>
        </div>
        {type === EVIDENCE_TYPES_ENUM.TRANSFER && (
          <div className="evidenceCard__indicatorContainer">
            <p className="evidenceCard__indicatorContainer__indicator">
              <CoinIcon />
              <span className="evidenceCard__indicatorContainer__indicator__text">
                {activityType === ACTIVITY_TYPES_ENUM.FUNDING
                  ? t('general.income')
                  : t('general.outcome')}
              </span>
            </p>
            <p className="evidenceCard__indicatorContainer__value">
              {formatCurrency(currency, evidence?.amount, true)}
            </p>
          </div>
        )}
      </div>
      <div className="evidenceCard__footer">
        {withDeleteButton && (
          <CoaDeleteEvidenceButton
            projectId={projectId}
            activityStatus={activityStatus}
            evidenceStatus={status}
            activityId={activityId}
            evidenceId={id}
            className="evidenceCard__footer__deleteButton"
          />
        )}
        <CoaButton
          type={isAuditEvidenceAvailable ? 'primary' : 'ghost'}
          onClick={handleGoToEvidenceDetails}
          className="evidenceCard__footer__viewButton"
          disabled={
            isAuditEvidenceAvailable && activityStatus === ACTIVITY_STATUS_ENUM.PENDING_TO_REVIEW
          }
        >
          {isAuditEvidenceAvailable ? t('evidenceCard.audit') : t('evidenceCard.viewMore')}
        </CoaButton>
      </div>
    </div>
  );
};
export default EvidenceCard;

EvidenceCard.defaultProps = {
  evidence: undefined,
  currency: undefined,
  evidenceNumber: undefined,
  isActivityAuditor: false
};

EvidenceCard.propTypes = {
  evidence: PropTypes.objectOf(PropTypes.any),
  currency: PropTypes.string,
  evidenceNumber: PropTypes.oneOf([PropTypes.string, PropTypes.number]),
  isActivityAuditor: PropTypes.bool,
  preview: PropTypes.bool,
  activityType: PropTypes.string,
  activityStatus: PropTypes.string,
  withDeleteButton: PropTypes.bool
};
