/**
 * AGUA Copyright (C) 2023  Atix Labs
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import React from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';

import Styles from './title-page.module.scss';

const TitlePage = ({
  textTitle,
  underlinePosition,
  className,
  orderNumber,
  centeredText,
  variant,
  weight,
  HtmlTag,
  fontSize,
  ...props
}) => (
  <HtmlTag
    style={{ '--orderNumber': orderNumber }}
    className={classNames(Styles.text, className, {
      [Styles['--withOrderNumber']]: Boolean(orderNumber),
      [Styles[`--${variant}`]]: variant,
      [Styles[`--${weight}`]]: weight,
      [Styles[`--${underlinePosition}`]]: underlinePosition !== 'none',
      [Styles['--centered']]: centeredText,
      [Styles[`--${fontSize}`]]: fontSize
    })}
    {...props}
  >
    {textTitle}
  </HtmlTag>
);
export default TitlePage;

TitlePage.defaultProps = {
  underlinePosition: 'left',
  className: '',
  orderNumber: undefined,
  centeredText: false,
  variant: 'primary',
  weight: 'bold',
  HtmlTag: 'h1',
  fontSize: 'bigger'
};

TitlePage.propTypes = {
  textTitle: PropTypes.oneOfType([PropTypes.string, PropTypes.object]).isRequired,
  underlinePosition: PropTypes.oneOf(['left', 'center', 'right', 'none']),
  className: PropTypes.string,
  orderNumber: PropTypes.string,
  centeredText: PropTypes.bool,
  variant: PropTypes.oneOf(['dark', 'primary', 'grey']),
  weight: PropTypes.oneOf(['bold', 'bolder', 'boldest']),
  HtmlTag: PropTypes.string,
  fontSize: PropTypes.oneOf([
    'medium',
    'medium-bigger',
    'medium-biggest',
    'big',
    'bigger',
    'biggest'
  ])
};
