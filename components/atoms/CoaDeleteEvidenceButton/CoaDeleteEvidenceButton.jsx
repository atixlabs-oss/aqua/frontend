import PropTypes from 'prop-types';

import { EVIDENCE_STATUS_ENUM } from 'model/evidence';
import { CoaButton } from '../CoaButton/CoaButton';
import { ACTIVITY_STATUS_ENUM } from 'model/activityStatus';
import { useTranslation } from 'react-i18next';
import { Icon } from 'antd';
import { useDeleteActivityEvidenceMutation } from 'api/react-query/activities/activitiesMutations';
import { CoaConfirmDeleteModal } from 'components/organisms/CoaModals/ConfirmModals/confirmModals';

export const CoaDeleteEvidenceButton = ({
  evidenceStatus,
  activityStatus,
  largeButtonText,
  urlToRedirectWhenSuccess,
  activityId,
  evidenceId,
  projectId,
  ...rest
}) => {
  const { t } = useTranslation();
  const deleteEvidenceMutation = useDeleteActivityEvidenceMutation({
    projectId,
    activityId,
    evidenceId,
    urlToRedirectWhenSuccess
  });

  const canEvidenceBeDeleted =
    evidenceStatus === EVIDENCE_STATUS_ENUM.NEW &&
    ACTIVITY_STATUS_ENUM.IN_PROGRESS === activityStatus;
  const handleDeleteEvidence = () =>
    CoaConfirmDeleteModal({
      onOk: deleteEvidenceMutation.mutateAsync,
      title: t('evidences.confirmDelete.title'),
      subtitle: t('evidences.confirmDelete.subtitle')
    });
  return (
    <CoaButton
      type="dark"
      disabled={!canEvidenceBeDeleted}
      onClick={handleDeleteEvidence}
      {...rest}
    >
      <Icon type="delete" />
      {largeButtonText ? t('evidences.confirmDelete.deleteEvidence') : t('general.btnDelete')}
    </CoaButton>
  );
};

CoaDeleteEvidenceButton.propTypes = {
  evidenceStatus: PropTypes.string,
  activityStatus: PropTypes.string,
  largeButtonText: PropTypes.bool,
  urlToRedirectWhenSuccess: PropTypes.string,
  activityId: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
  evidenceId: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
  projectId: PropTypes.oneOfType([PropTypes.string, PropTypes.number])
};
