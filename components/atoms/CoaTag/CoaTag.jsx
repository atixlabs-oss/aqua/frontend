/**
 * AGUA Copyright (C) 2023  Atix Labs
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import React from 'react';
import { Tag } from 'antd';
import PropTypes from 'prop-types';
import classNames from 'classnames';

import Styles from './coa-tag.module.scss';
import { useTranslation } from 'react-i18next';
import { CustomTooltip } from '../CustomTooltip/CustomTooltip';

export const CoaTag = ({ children, predefinedColor, predefinedSize, tooltipTitle }) => {
  const { t } = useTranslation('common');

  const customStatusMap = {
    'not started': t('status.noStarted'),
    new: t('status.new'),
    rejected: t('status.rejected'),
    approved: t('status.approved'),
    draft: t('status.draft'),
    'in progress': t('status.inProgress'),
    'in review': t('status.inReview'),
    canceled: t('status.canceled'),
    completed: t('status.completed'),
    published: t('status.published')
  };

  return (
    <CustomTooltip title={tooltipTitle} variant="black">
      <Tag
        className={classNames(Styles.container, {
          [Styles[`--${predefinedColor}`]]: Boolean(predefinedColor),
          [Styles[`--${predefinedSize}`]]: Boolean(predefinedSize)
        })}
      >
        {typeof children === 'string' && customStatusMap[children.toLowerCase()]
          ? customStatusMap[children.toLowerCase()]
          : children}
      </Tag>
    </CustomTooltip>
  );
};

CoaTag.defaultProps = {
  children: '',
  predefinedColor: '',
  predefinedSize: 'small'
};

CoaTag.propTypes = {
  children: PropTypes.oneOfType([PropTypes.arrayOf(PropTypes.node), PropTypes.node]),
  predefinedColor: PropTypes.oneOf(['green', 'orange', 'yellow', 'gray', 'blue', 'red', '']),
  predefinedSize: PropTypes.oneOf(['small', 'big']),
  tooltipTitle: PropTypes.elementType
};
