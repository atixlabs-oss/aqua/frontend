/**
 * AGUA Copyright (C) 2023  Atix Labs
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import React from 'react';
import PropTypes from 'prop-types';
import { Avatar } from 'antd';
import { getInitials } from '../../../helpers/formatter';
import { userAvatarPropTypes } from '../../../helpers/proptypes';
import { CustomTooltip } from '../CustomTooltip/CustomTooltip';

const AvatarUser = ({ user }) => {
  const fullName = `${user.firstName} ${user.lastName}`;
  return (
    <CustomTooltip title={fullName} noPadding noArrow variant="white">
      {user.avatarImage ? (
        <Avatar src={user.avatarImage} alt={getInitials(fullName)} />
      ) : (
        <Avatar style={{ color: '#0055FF', backgroundColor: '#e5e6e8' }}>
          {getInitials(fullName)}
        </Avatar>
      )}
    </CustomTooltip>
  );
};

AvatarUser.propTypes = {
  user: PropTypes.shape(userAvatarPropTypes).isRequired
};

export default AvatarUser;
