/**
 * AGUA Copyright (C) 2023  Atix Labs
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import React, { useContext } from 'react';
import { Menu, Dropdown, Icon, Button } from 'antd';
import { UserContext } from '../../utils/UserContext';
import { useRouter } from 'next/router';
import { useTranslation } from 'react-i18next';

const SettingsMenu = () => {
  const { removeUser } = useContext(UserContext);
  const router = useRouter();
  const { t } = useTranslation();

  const logout = () => {
    removeUser();
    router.push('/');
  };

  const changePassword = () => {
    router.push('/password-change');
  };

  const menu = (
    <Menu>
      <Menu.Item key="1" onClick={changePassword}>
        Change Password
      </Menu.Item>
      <Menu.Item key="2" onClick={logout}>
        {t('loginSection.logout')}
      </Menu.Item>
    </Menu>
  );

  return (
    <div className="SettingsMenu">
      <Dropdown overlay={menu} trigger={['click']}>
        <Button type="link" size="small">
          <Icon type="down" />
        </Button>
      </Dropdown>
    </div>
  );
};

export default SettingsMenu;
