/**
 * AGUA Copyright (C) 2023  Atix Labs
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import React from 'react';
import { Form, Input, Select } from 'antd';
import { CoaButton } from 'components/atoms/CoaButton/CoaButton';
import { ERROR_MESSAGES } from 'constants/constants';
import { onlyAlphanumerics } from 'constants/Regex';
import { checkIsAuditorOfAnActivity } from 'helpers/utils';
import { FeedbackMessage } from 'components/atoms/FeedbackMessage/FeedbackMessage';
import PropTypes from 'prop-types';
import { FEEDBACK_MESSAGE_TYPES, USER_STATES } from '../constants';
import { useTranslation } from 'react-i18next';
import { useGetCountriesQuery } from 'api/react-query/countries/countriesQueries';

const { Option } = Select;

export const FormUserContent = ({
  form,
  userState,
  handleCreateAndAssignUser,
  handleAssignUser,
  handleUnassignUser,
  initialData,
  isFormSubmitted,
  prevUserState,
  milestones
}) => {
  const { t } = useTranslation();
  const { getFieldDecorator, getFieldValue } = form;
  const country = getFieldValue('country');
  const firstName = getFieldValue('firstName');
  const lastName = getFieldValue('lastName');
  const email = getFieldValue('email');
  const userId = getFieldValue('id');
  const { data: countries = [], isLoading: areCountriesLoading } = useGetCountriesQuery({
    enabled: false
  });

  const initialCountryName = countries?.find?.(_country => _country?.id === initialData?.country)
    ?.name;

  const genericDisabledButtonLogic =
    ((!country || !firstName || !lastName || !email) && !initialData) ||
    userState === USER_STATES.LOADING;

  const unassignDisabledButtonLogic = checkIsAuditorOfAnActivity({ milestones, auditorId: userId });

  const SUBMIT_BUTTON_DICT = ({ prevFunc, prevText } = { prevFunc: () => {}, prevText: '' }) => ({
    [USER_STATES.NO_EXIST]: {
      func: handleCreateAndAssignUser,
      text: t('users.Invite and Assign user'),
      disabled: genericDisabledButtonLogic
    },
    [USER_STATES.PENDING_WITH_TEXT]: {
      func: handleAssignUser,
      text: t('users.Assign user'),
      disabled: genericDisabledButtonLogic
    },
    [USER_STATES.EXIST_WITH_TEXT]: {
      func: handleAssignUser,
      text: t('users.Assign user'),
      disabled: genericDisabledButtonLogic
    },
    [USER_STATES.PENDING]: {
      func: handleUnassignUser,
      text: t('users.Unassign user'),
      disabled: genericDisabledButtonLogic || unassignDisabledButtonLogic
    },
    [USER_STATES.EXIST]: {
      func: handleUnassignUser,
      text: t('users.Unassign user'),
      disabled: genericDisabledButtonLogic || unassignDisabledButtonLogic
    },
    [USER_STATES.UNKNOWN]: {
      func: () => {},
      text: t('users.Assign user'),
      disabled: genericDisabledButtonLogic
    },
    [USER_STATES.LOADING]: {
      func: () => {},
      text: t('users.Assign user'),
      disabled: genericDisabledButtonLogic
    },
    [USER_STATES.WITH_ERROR]: {
      text: prevText,
      func: prevFunc,
      disabled: genericDisabledButtonLogic
    },
    [USER_STATES.EXIST_INACTIVE_WITH_TEXT]: {
      func: handleAssignUser,
      text: t('users.Assign user'),
      disabled: genericDisabledButtonLogic
    },
    [USER_STATES.EXIST_INACTIVE]: {
      func: handleUnassignUser,
      text: t('users.Unassign user'),
      disabled: genericDisabledButtonLogic || unassignDisabledButtonLogic
    }
  });

  return (
    <div
      onClick={e => e.stopPropagation()}
      aria-hidden="true"
      className="formUserContent__container"
    >
      <Form.Item className="formUserContent__container__formItem" label={t('users.First name')}>
        {getFieldDecorator('firstName', {
          rules: [
            {
              required: true,
              message: ERROR_MESSAGES.EMPTY,
              whitespace: true
            },
            {
              pattern: onlyAlphanumerics,
              message: ERROR_MESSAGES.ALPHANUMERIC
            }
          ],
          initialValue: initialData?.firstName
        })(
          <Input
            maxLength={50}
            placeholder={t('users.Enter first name')}
            disabled={
              userState === USER_STATES.EXIST ||
              userState === USER_STATES.LOADING ||
              userState === USER_STATES.PENDING ||
              userState === USER_STATES.PENDING_WITH_TEXT ||
              userState === USER_STATES.EXIST_WITH_TEXT ||
              userState === USER_STATES.EXIST_INACTIVE ||
              userState === USER_STATES.EXIST_INACTIVE_WITH_TEXT
            }
          />
        )}
      </Form.Item>
      <Form.Item className="formUserContent__container__formItem" label={t('users.Last name')}>
        {getFieldDecorator('lastName', {
          rules: [
            {
              required: true,
              message: ERROR_MESSAGES.EMPTY,
              whitespace: true
            },
            {
              pattern: onlyAlphanumerics,
              message: ERROR_MESSAGES.ALPHANUMERIC
            }
          ],
          initialValue: initialData?.lastName
        })(
          <Input
            maxLength={50}
            placeholder={t('users.Enter last name')}
            disabled={
              userState === USER_STATES.EXIST ||
              userState === USER_STATES.LOADING ||
              userState === USER_STATES.PENDING ||
              userState === USER_STATES.PENDING_WITH_TEXT ||
              userState === USER_STATES.EXIST_WITH_TEXT ||
              userState === USER_STATES.EXIST_INACTIVE ||
              userState === USER_STATES.EXIST_INACTIVE_WITH_TEXT
            }
          />
        )}
      </Form.Item>
      <Form.Item
        className="formUserContent__container__formItem"
        label={t('createProject.countryRegion')}
      >
        {getFieldDecorator('country', {
          initialValue: initialCountryName,
          rules: [
            {
              required: true,
              message: ERROR_MESSAGES.EMPTY
            }
          ]
        })(
          <Select
            loading={areCountriesLoading}
            placeholder={t('users.Select country or region')}
            disabled={
              userState === USER_STATES.EXIST ||
              userState === USER_STATES.LOADING ||
              userState === USER_STATES.PENDING ||
              userState === USER_STATES.PENDING_WITH_TEXT ||
              userState === USER_STATES.EXIST_WITH_TEXT ||
              userState === USER_STATES.EXIST_INACTIVE ||
              userState === USER_STATES.EXIST_INACTIVE_WITH_TEXT
            }
            showSearch
          >
            {countries?.map?.(_country => (
              <Option value={_country?.name} key={_country?.id}>
                {_country?.name}
              </Option>
            ))}
          </Select>
        )}
      </Form.Item>
      <div className="formUserContent__container__buttonsContainer">
        <CoaButton
          type="primary"
          onClick={
            SUBMIT_BUTTON_DICT({
              prevFunc: SUBMIT_BUTTON_DICT()[prevUserState]?.func
            })[userState]?.func
          }
          htmlType="submit"
          disabled={SUBMIT_BUTTON_DICT()[userState]?.disabled}
        >
          {
            SUBMIT_BUTTON_DICT({
              prevText: SUBMIT_BUTTON_DICT()[prevUserState]?.text
            })[userState]?.text
          }
        </CoaButton>

        <FeedbackMessage
          message={
            userState === USER_STATES.PENDING || userState === USER_STATES.PENDING_WITH_TEXT
              ? t('users.Invitation and instructions have been sent')
              : t('users.User assigned successfully')
          }
          type={FEEDBACK_MESSAGE_TYPES.SUCCESS}
          show={isFormSubmitted && userState !== USER_STATES.WITH_ERROR}
          seconds={2}
        />
        <FeedbackMessage
          type={FEEDBACK_MESSAGE_TYPES.ERROR}
          message={t('users.There was an error')}
          show={userState === USER_STATES.WITH_ERROR}
          seconds={2}
        />
      </div>
    </div>
  );
};

FormUserContent.defaultProps = {
  form: undefined,
  countries: undefined,
  userState: undefined,
  handleCreateAndAssignUser: undefined,
  handleAssignUser: undefined,
  initialData: undefined,
  isFormSubmitted: undefined,
  handleUnassignUser: undefined
};

FormUserContent.propTypes = {
  form: PropTypes.shape({
    getFieldDecorator: PropTypes.func,
    setFieldsValue: PropTypes.func,
    getFieldValue: PropTypes.func
  }),
  countries: PropTypes.shape({
    isLoading: PropTypes.bool,
    content: PropTypes.arrayOf(
      PropTypes.shape({
        id: PropTypes.number,
        name: PropTypes.string
      })
    )
  }),
  userState: PropTypes.string,
  handleCreateAndAssignUser: PropTypes.func,
  handleAssignUser: PropTypes.func,
  initialData: PropTypes.objectOf(PropTypes.any),
  isFormSubmitted: PropTypes.bool,
  handleUnassignUser: PropTypes.func
};
