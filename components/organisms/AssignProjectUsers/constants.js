/**
 * AGUA Copyright (C) 2023  Atix Labs
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import i18n from 'i18n';

const { t } = i18n;

export const USER_STATES = {
  PENDING: 'PENDING',
  PENDING_WITH_TEXT: 'PENDING_WITH_TEXT',
  EXIST: 'EXIST',
  EXIST_WITH_TEXT: 'EXIST_WITH_TEXT',
  UNKNOWN: 'UNKNOWN',
  LOADING: 'LOADING',
  NO_EXIST: 'NO_EXIST',
  WITH_ERROR: 'WITH_ERROR',
  EXIST_INACTIVE: 'EXIST_INACTIVE',
  EXIST_INACTIVE_WITH_TEXT: 'EXIST_INACTIVE_WITH_TEXT'
};

export const ROLES_IDS = {
  beneficiary: 1,
  investor: 2,
  auditor: 3
};

export const ROLES_IDS_NAMES = {
  1: 'Beneficiary',
  2: 'Investor',
  3: 'Auditor'
};

export const FEEDBACK_MESSAGE_TYPES = {
  SUCCESS: 'SUCCESS',
  INFO: 'INFO',
  PENDING: 'PENDING',
  ERROR: 'ERROR',
  WARNING: 'WARNING'
};

export const FEEDBACK_MESSAGE_TYPES_BY_USER_STATE = {
  [USER_STATES.EXIST]: FEEDBACK_MESSAGE_TYPES.SUCCESS,
  [USER_STATES.EXIST_WITH_TEXT]: FEEDBACK_MESSAGE_TYPES.SUCCESS,
  [USER_STATES.NO_EXIST]: FEEDBACK_MESSAGE_TYPES.INFO,
  [USER_STATES.PENDING]: FEEDBACK_MESSAGE_TYPES.PENDING,
  [USER_STATES.PENDING_WITH_TEXT]: FEEDBACK_MESSAGE_TYPES.PENDING,
  [USER_STATES.WITH_ERROR]: FEEDBACK_MESSAGE_TYPES.ERROR,
  [USER_STATES.EXIST_INACTIVE]: FEEDBACK_MESSAGE_TYPES.WARNING,
  [USER_STATES.EXIST_INACTIVE_WITH_TEXT]: FEEDBACK_MESSAGE_TYPES.WARNING
};

export const FEEDBACK_MESSAGE_ICONS = {
  [FEEDBACK_MESSAGE_TYPES.PENDING]: 'clock-circle',
  [FEEDBACK_MESSAGE_TYPES.SUCCESS]: 'check-circle',
  [FEEDBACK_MESSAGE_TYPES.INFO]: 'info-circle',
  [FEEDBACK_MESSAGE_TYPES.ERROR]: 'close-circle',
  [FEEDBACK_MESSAGE_TYPES.WARNING]: 'warning'
};

export const ICON_CLASSES_BY_FEEDBACK_TYPE = {
  [FEEDBACK_MESSAGE_TYPES.SUCCESS]: 'success',
  [FEEDBACK_MESSAGE_TYPES.PENDING]: 'pending',
  [FEEDBACK_MESSAGE_TYPES.INFO]: 'info',
  [FEEDBACK_MESSAGE_TYPES.ERROR]: 'error',
  [FEEDBACK_MESSAGE_TYPES.WARNING]: 'warning'
};

export const FEEDBACK_MESSAGE_BY_USER_STATE = entity => ({
  [USER_STATES.EXIST]: '',
  [USER_STATES.EXIST_WITH_TEXT]: t('users.The entity is already registered', { entity }),
  [USER_STATES.NO_EXIST]: t('users.The entity is not registered Fill in the information below', {
    entity
  }),
  [USER_STATES.PENDING]: '',
  [USER_STATES.PENDING_WITH_TEXT]: t(
    'users.An email invitation has been sent for the user to enter the platform'
  ),
  [USER_STATES.EXIST_INACTIVE]: '',
  [USER_STATES.EXIST_INACTIVE_WITH_TEXT]: t('users.The entity is already registered but inactive', {
    entity
  })
});

export const TOOLTIP_TITLES_BY_USER_STATE = {
  [USER_STATES.EXIST]: t('users.Registered User'),
  [USER_STATES.EXIST_WITH_TEXT]: t('users.Registered User'),
  [USER_STATES.PENDING]: t('users.Pending User'),
  [USER_STATES.PENDING_WITH_TEXT]: t('users.Pending User'),
  [USER_STATES.EXIST_INACTIVE]: t('users.Registered but inactive User'),
  [USER_STATES.EXIST_INACTIVE_WITH_TEXT]: t('users.Registered but inactive User')
};
