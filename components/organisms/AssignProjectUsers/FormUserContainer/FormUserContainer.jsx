/**
 * AGUA Copyright (C) 2023  Atix Labs
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import React, { useState } from 'react';
import { useTranslation } from 'react-i18next';
import { Collapse, Form, message } from 'antd';
import PropTypes from 'prop-types';

import { ROLES_IDS, USER_STATES } from '../constants';
import { CustomCollapseHeader } from '../CustomCollapseHeader/CustomCollapseHeader';
import { useGetCountriesQuery } from 'api/react-query/countries/countriesQueries';
import {
  useCreateUserMutation,
  useSendWelcomeEmailMutation
} from 'api/react-query/users/usersMutations';
import {
  useAssignUserToProjectMutation,
  useRemoveUserFromProjectMutation
} from 'api/react-query/userProjects/userProjectsMutations';

const { Panel } = Collapse;

const CustomCollapse = ({
  children,
  entity,
  form,
  initialData,
  projectId,
  onError,
  setCanAddAdditionalAuditor,
  item,
  totalKeys,
  onRemove,
  ...rest
}) => {
  const { t } = useTranslation();
  const [activeKey, setActiveKey] = useState(0);
  const [userState, setUserState] = useState(USER_STATES.UNKNOWN);
  const [prevUserState, setPrevUserState] = useState(USER_STATES.UNKNOWN);
  const { validateFields, setFieldsValue, getFieldValue } = form;
  const [isFormSubmitted, setIsFormSubmitted] = useState(false);
  const userId = getFieldValue('id');
  const { data: countries = [] } = useGetCountriesQuery();
  const createUserMutation = useCreateUserMutation();
  const sendWelcomeEmailMutation = useSendWelcomeEmailMutation();
  const assignUserToProjectMutation = useAssignUserToProjectMutation();
  const removeUserFromProjectMutation = useRemoveUserFromProjectMutation();

  const handleError = () => {
    if (userState !== USER_STATES.WITH_ERROR) setPrevUserState(userState);
    setUserState(USER_STATES.WITH_ERROR);
    onError();
  };

  const handleCreateAndAssignUser = () => {
    validateFields(async (err, values) => {
      if (!err) {
        try {
          setIsFormSubmitted(true);
          const dataToSend = { ...values };
          const countryId = countries?.find(country => values?.country === country?.name)?.id;

          const response = await createUserMutation.mutateAsync({
            ...dataToSend,
            isAdmin: false,
            country: countryId
          });

          await assignUserToProjectMutation.mutateAsync({
            projectId,
            roleId: ROLES_IDS[entity],
            userId: response?.data?.id
          });

          await sendWelcomeEmailMutation.mutateAsync({
            userId: response?.data?.id,
            projectId
          });

          setUserState(USER_STATES.PENDING);
          setFieldsValue({ ...dataToSend, id: response?.data?.id });
          if (setCanAddAdditionalAuditor) setCanAddAdditionalAuditor(true);
        } catch {
          return handleError();
        }
      }
    });
  };

  const handleUnassignUser = async () => {
    try {
      setIsFormSubmitted(false);
      await removeUserFromProjectMutation.mutateAsync({
        projectId,
        roleId: ROLES_IDS[entity],
        userId
      });
      message.success(t('users.Successful unassign'));
      if (item !== undefined && totalKeys !== 1) return onRemove();
      setFieldsValue({
        id: undefined,
        firstName: undefined,
        lastName: undefined,
        country: undefined,
        email: undefined
      });
      setUserState(USER_STATES.UNKNOWN);
      setActiveKey(0);
    } catch {}
  };

  const handleAssignUser = async () => {
    try {
      setIsFormSubmitted(true);
      await assignUserToProjectMutation.mutateAsync({
        projectId,
        roleId: ROLES_IDS[entity],
        userId
      });
      if (setCanAddAdditionalAuditor) setCanAddAdditionalAuditor(true);
      if (userState === USER_STATES.PENDING_WITH_TEXT) return setUserState(USER_STATES.PENDING);
      if (userState === USER_STATES.EXIST_INACTIVE_WITH_TEXT)
        return setUserState(USER_STATES.EXIST_INACTIVE);
      return setUserState(USER_STATES.EXIST);
    } catch {
      return handleError();
    }
  };

  const handleResendEmail = async e => {
    e.stopPropagation();
    try {
      await sendWelcomeEmailMutation.mutateAsync({ userId, projectId });
      return message.success(t('users.Email was sent'));
    } catch {}
  };

  return (
    <Form
      aria-hidden="true"
      onClick={() => {
        if (activeKey === 0) setActiveKey(1);
        if (activeKey === 1) setActiveKey(0);
      }}
    >
      <Collapse
        {...rest}
        className="formUserContainer__collapse"
        bordered={false}
        activeKey={activeKey}
      >
        <Panel
          header={
            <CustomCollapseHeader
              setActiveKey={setActiveKey}
              entity={entity}
              setUserState={setUserState}
              userState={userState}
              form={form}
              initialData={initialData}
              setIsFormSubmitted={setIsFormSubmitted}
              handleResendEmail={handleResendEmail}
              countries={countries}
            />
          }
          key={1}
        >
          {children({
            setActiveKey,
            setUserState,
            userState,
            form,
            handleCreateAndAssignUser,
            handleAssignUser,
            isFormSubmitted,
            handleUnassignUser,
            prevUserState
          })}
        </Panel>
      </Collapse>
    </Form>
  );
};

export const FormUserContainer = Form.create({
  name: 'FormUserContainer'
})(CustomCollapse);

CustomCollapse.defaultProps = {
  children: undefined,
  entity: '',
  form: undefined,
  initialData: undefined,
  projectId: undefined,
  onError: () => {},
  setCanAddAdditionalAuditor: undefined,
  totalKeys: undefined,
  item: undefined,
  onRemove: undefined
};

CustomCollapse.propTypes = {
  children: PropTypes.oneOfType([
    PropTypes.arrayOf(PropTypes.node),
    PropTypes.node,
    PropTypes.func
  ]),
  entity: PropTypes.string,
  form: PropTypes.objectOf(PropTypes.any),
  initialData: PropTypes.objectOf(PropTypes.any),
  projectId: PropTypes.oneOfType([PropTypes.number, PropTypes.string]),
  onError: PropTypes.func,
  setCanAddAdditionalAuditor: PropTypes.func,
  totalKeys: PropTypes.number,
  item: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
  onRemove: PropTypes.func
};
