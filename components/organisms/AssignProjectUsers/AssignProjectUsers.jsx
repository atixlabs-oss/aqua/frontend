/**
 * AGUA Copyright (C) 2023  Atix Labs
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import React, { useState, useEffect } from 'react';
import PropTypes from 'prop-types';
import { Divider, Icon } from 'antd';
import TitlePage from 'components/atoms/TitlePage/TitlePage';

import _ from 'lodash';
import { CoaTextButton } from 'components/atoms/CoaTextButton/CoaTextButton';
import { CoaAlert } from 'components/molecules/CoaAlert/CoaAlert';
import { SentIcon } from 'components/atoms/CustomIcons/SentIcon';
import {
  checkProjectHasAnyUserWithoutFirstLogin,
  getUsersByRole
} from 'helpers/modules/projectUsers';
import { ROLES_IDS } from './constants';
import { FormUserContainer } from './FormUserContainer/FormUserContainer';
import { FormUserContent } from './FormUserContent/FormUserContent';
import { useTranslation } from 'react-i18next';
import { CustomHead } from 'components/atoms/CustomHead/CustomHead';
import { useGetCountriesQuery } from 'api/react-query/countries/countriesQueries';

const getUserLabel = ({ key, t }) => {
  if (ROLES_IDS[key] === ROLES_IDS.beneficiary) {
    return t('roles.beneficiary');
  }
  if (ROLES_IDS[key] === ROLES_IDS.investor) {
    return t('roles.investor');
  }
  if (ROLES_IDS[key] === ROLES_IDS.auditor) {
    return t('roles.auditor');
  }
  return key;
};

export const AssignProjectUsers = ({ project, onError, Footer }) => {
  const { t } = useTranslation();
  const { data: countries = [] } = useGetCountriesQuery();
  const [hasPendingUsers, setHasPendingUsers] = useState(false);

  const initialBeneficiariesUserData = getUsersByRole(ROLES_IDS.beneficiary, project?.users);
  const initialInvestorsUserData = getUsersByRole(ROLES_IDS.investor, project?.users);
  const initialAuditorsUserData = getUsersByRole(ROLES_IDS.auditor, project?.users);

  const [currentAuditorsElements, setCurrentAuditorsElements] = useState([_.uniqueId()]);

  const [canAddAdditionalAuditor, setCanAddAdditionalAuditor] = useState(
    initialAuditorsUserData?.length > 0
  );

  useEffect(() => {
    const projectHasAnyUserWithoutFirstLogin = checkProjectHasAnyUserWithoutFirstLogin({
      auditors: initialAuditorsUserData,
      beneficiaries: initialBeneficiariesUserData,
      investors: initialInvestorsUserData
    });
    setHasPendingUsers(projectHasAnyUserWithoutFirstLogin);
  }, [initialBeneficiariesUserData, initialAuditorsUserData, initialInvestorsUserData]);

  const onRemove = k => {
    if (currentAuditorsElements.length === 1) {
      return;
    }

    const updatedArray = currentAuditorsElements.filter(key => key !== k);

    setCurrentAuditorsElements([...updatedArray]);
  };

  const addAuditor = () => {
    setCurrentAuditorsElements([...currentAuditorsElements, _.uniqueId()]);
    setCanAddAdditionalAuditor(false);
  };

  useEffect(() => {
    if (initialAuditorsUserData) {
      const _keys = initialAuditorsUserData?.map(
        initialAuditorUserData => initialAuditorUserData?.id
      );

      setCurrentAuditorsElements([..._keys]);
    }
  }, [initialAuditorsUserData]);

  const initialData = [
    initialBeneficiariesUserData?.[0],
    initialInvestorsUserData?.[0],
    initialAuditorsUserData
  ];

  return (
    <>
      <CustomHead titleText={`${t('general.BackOffice')} - ${t('createProject.Project users')}`} />
      <CoaAlert
        className="assignProjectUsers__warningAlert"
        Icon={<Icon className="assignProjectUsers__warningAlert__icon" component={SentIcon} />}
        highlightedText={t('createProject.pendingUsers')}
        message={t('createProject.usersNotEnterInThePlatform')}
        customColor="yellow"
        closable
        show={hasPendingUsers}
      />
      <div className="assignProjectUsers__content">
        <TitlePage textTitle={t('createProject.assignUsers')} />
        {Object.keys(ROLES_IDS).map((key, index) => (
          <div key={key} className="assignProjectUsers__content__itemsContainer">
            <div className="assignProjectUsers__content__itemsContainer__titleContainer">
              <h3 className="assignProjectUsers__content__itemsContainer__titleContainer__title">
                {getUserLabel({ key, t })}
              </h3>
              {ROLES_IDS[key] === ROLES_IDS.auditor && (
                <CoaTextButton
                  variant="primary"
                  onClick={addAuditor}
                  disabled={!canAddAdditionalAuditor}
                >
                  <Icon type="plus" /> {t('createProject.addAuditor')}
                </CoaTextButton>
              )}
            </div>
            {ROLES_IDS[key] !== ROLES_IDS.auditor && (
              <FormUserContainer
                countries={countries}
                expandIconPosition="right"
                entity={key}
                initialData={initialData[index]}
                projectId={project?.id}
                onError={onError}
              >
                {({
                  setActiveKey,
                  setUserState,
                  userState,
                  form,
                  handleCreateAndAssignUser,
                  handleAssignUser,
                  handleUnassignUser,
                  isFormSubmitted,
                  removeCurrentUserFromProject,
                  prevUserState
                }) => (
                  <FormUserContent
                    setActiveKey={setActiveKey}
                    countries={countries}
                    setUserState={setUserState}
                    userState={userState}
                    form={form}
                    handleAssignUser={handleAssignUser}
                    handleCreateAndAssignUser={handleCreateAndAssignUser}
                    handleUnassignUser={handleUnassignUser}
                    initialData={initialData[index]}
                    isFormSubmitted={isFormSubmitted}
                    removeCurrentUserFromProject={removeCurrentUserFromProject}
                    onError={onError}
                    prevUserState={prevUserState}
                    milestones={project?.milestones}
                  />
                )}
              </FormUserContainer>
            )}
            {ROLES_IDS[key] === ROLES_IDS.auditor &&
              currentAuditorsElements.map(item => {
                const _initialData = initialData[index]?.find(
                  _initialAuditorsUserData => _initialAuditorsUserData?.id === item
                );
                return (
                  <FormUserContainer
                    countries={countries}
                    expandIconPosition="right"
                    entity={key}
                    initialData={_initialData}
                    projectId={project?.id}
                    onError={onError}
                    setCanAddAdditionalAuditor={setCanAddAdditionalAuditor}
                    key={item}
                    totalKeys={currentAuditorsElements?.length}
                    item={item}
                    onRemove={() => onRemove(item)}
                  >
                    {({
                      setActiveKey,
                      setUserState,
                      userState,
                      form,
                      handleCreateAndAssignUser,
                      handleAssignUser,
                      handleUnassignUser,
                      isFormSubmitted,
                      removeCurrentUserFromProject,
                      prevUserState
                    }) => (
                      <FormUserContent
                        removeCurrentUserFromProject={removeCurrentUserFromProject}
                        setActiveKey={setActiveKey}
                        countries={countries}
                        setUserState={setUserState}
                        userState={userState}
                        form={form}
                        handleAssignUser={handleAssignUser}
                        handleCreateAndAssignUser={handleCreateAndAssignUser}
                        handleUnassignUser={handleUnassignUser}
                        initialData={_initialData}
                        isFormSubmitted={isFormSubmitted}
                        prevUserState={prevUserState}
                        milestones={project?.milestones}
                      />
                    )}
                  </FormUserContainer>
                );
              })}
            {index + 1 < Object.keys(ROLES_IDS).length && (
              <Divider type="horizontal" className="assignProjectUsers__content__divider" />
            )}
          </div>
        ))}
      </div>
      {Footer()}
    </>
  );
};

AssignProjectUsers.defaultProps = {
  Footer: undefined
};

AssignProjectUsers.propTypes = {
  project: PropTypes.shape({
    details: PropTypes.shape({
      problemAddressed: PropTypes.string,
      mission: PropTypes.string,
      currencyType: PropTypes.string,
      currency: PropTypes.string,
      additionalCurrencyInformation: PropTypes.string
    })
  }).isRequired,
  onError: PropTypes.func.isRequired,
  Footer: PropTypes.func
};
