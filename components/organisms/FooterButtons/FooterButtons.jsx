/**
 * AGUA Copyright (C) 2023  Atix Labs
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import React from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';

const FooterButtons = ({ finishButton, nextStepButton, prevStepButton, errors, className }) => (
  <footer className={classNames('footerButtons__container', { [className]: Boolean(className) })}>
    <div>{prevStepButton}</div>
    <div className="footerButtons__container__right">
      <div className="footerButtons__container__right__nextButtonContainer">
        <div>
          {errors.map((error, index) => (
            <div
              key={index}
              className="footerButtons__container__right__nextButtonContainer__error"
            >
              {error}
            </div>
          ))}
        </div>
        {nextStepButton}
      </div>
      {!!finishButton && <div>{finishButton}</div>}
    </div>
  </footer>
);

FooterButtons.defaultProps = {
  nextStepButton: undefined,
  finishButton: undefined,
  prevStepButton: undefined,
  errors: [],
  className: undefined
};

FooterButtons.propTypes = {
  finishButton: PropTypes.oneOfType([PropTypes.func, PropTypes.objectOf(PropTypes.any)]),
  prevStepButton: PropTypes.oneOfType([PropTypes.func, PropTypes.objectOf(PropTypes.any)]),
  nextStepButton: PropTypes.oneOfType([PropTypes.func, PropTypes.objectOf(PropTypes.any)]),
  errors: PropTypes.arrayOf(PropTypes.string),
  className: PropTypes.string
};

export default FooterButtons;
