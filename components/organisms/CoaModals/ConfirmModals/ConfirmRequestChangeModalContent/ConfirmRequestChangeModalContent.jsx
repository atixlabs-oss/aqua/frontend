import React, { useState } from 'react';
import { Alert, Divider, Icon } from 'antd';
import classNames from 'classnames';
import { useTranslation } from 'react-i18next';
import PropTypes from 'prop-types';

import { InfoIcon1 } from 'components/atoms/CustomIcons/InfoIcon1';
import { InfoIcon2 } from 'components/atoms/CustomIcons/InfoIcon2';
import Styles from './confirm-request-change-modal-content.module.scss';
import { CoaButton } from 'components/atoms/CoaButton/CoaButton';

export const ConfirmRequestChangeModalContent = ({ modal, onOk, onCancel }) => {
  const { t } = useTranslation();
  const [isEditButtonLoading, setIsEditButtonLoading] = useState(false);
  const [isCancelButtonLoading, setIsCancelButtonLoading] = useState(false);

  return (
    <div className={Styles.contentContainer}>
      <Icon className={Styles.icon} component={InfoIcon1} />
      <h2 className={Styles.title}>{t('modals.confirm.requestProjectChange.title')}</h2>

      <div>
        <h3 className={Styles.subtitle}>{t('modals.confirm.requestProjectChange.subtitle1')}</h3>
        <p className={Styles.subtitle}>{t('modals.confirm.requestProjectChange.subtitle2')}:</p>
      </div>

      <div className={Styles.list}>
        <div className={Styles.list__edit}>
          <span className={classNames(Styles.listItem, Styles['--bold'])}>
            {t('modals.confirm.requestProjectChange.editProject')}
          </span>
          <p className={Styles.listItem}>
            {t('modals.confirm.requestProjectChange.editProjectNote')}
          </p>
        </div>
        <CoaButton
          type="primary"
          className={Styles.editButton}
          onClick={async () => {
            setIsEditButtonLoading(true);
            await onOk?.();
            setIsEditButtonLoading(false);
            modal.destroy();
          }}
          loading={isEditButtonLoading}
        >
          <Icon type="edit" />
          {t('modals.confirm.requestProjectChange.editProject')}
        </CoaButton>

        <Divider type="vertical" className={Styles.list__divider} />

        <div className={Styles.list__cancel}>
          <span className={classNames(Styles.listItem, Styles['--bold'])}>
            {t('modals.confirm.requestProjectChange.cancelProject')}
          </span>
          <p className={Styles.listItem}>
            {t('modals.confirm.requestProjectChange.cancelProjectNote')}
          </p>
        </div>
        <CoaButton
          type="dark"
          className={Styles.cancelButton}
          onClick={async () => {
            setIsCancelButtonLoading(true);
            await onCancel?.();
            setIsCancelButtonLoading(false);
            modal.destroy();
          }}
          loading={isCancelButtonLoading}
        >
          <Icon type="minus-circle" />
          {t('modals.confirm.requestProjectChange.cancelProject')}{' '}
        </CoaButton>
      </div>

      <Alert
        message={t('modals.confirm.requestProjectChange.alertText')}
        icon={<Icon component={InfoIcon2} />}
        showIcon
        show
        className={Styles.alert}
      />
      <Divider className={Styles.divider} />
    </div>
  );
};

ConfirmRequestChangeModalContent.propTypes = {
  modal: PropTypes.object,
  onOk: PropTypes.func,
  onCancel: PropTypes.func
};
