/**
 * AGUA Copyright (C) 2023  Atix Labs
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import { Divider, Icon, Modal } from 'antd';

import Styles from './confirm-modals.module.scss';
import classNames from 'classnames';
import { InfoIcon1 } from 'components/atoms/CustomIcons/InfoIcon1';
import { ApproveRejectButtons } from 'components/molecules/ApproveRejectButtons/ApproveRejectButtons';
import i18n from 'i18n';
import { ConfirmRequestChangeModalContent } from './ConfirmRequestChangeModalContent/ConfirmRequestChangeModalContent';

export const CoaConfirmDeleteModal = ({ title, subtitle, onOk, iconName = 'delete' }) => {
  const { t } = i18n;
  return Modal.confirm({
    className: Styles.confirmDeleteModal,
    onOk,
    title: (
      <div className={Styles.confirmDeleteModal__titleContainer}>
        <Icon className={Styles.confirmDeleteModal__titleContainer__icon} type={iconName} />
        <h2 className={Styles.confirmDeleteModal__titleContainer__title}>{title}</h2>
      </div>
    ),
    content: (
      <>
        <p className={Styles.confirmDeleteModal__description}>{subtitle}</p>
        <Divider />
      </>
    ),
    icon: null,
    cancelButtonProps: {
      ghost: true,
      type: 'primary',
      className: classNames(Styles.confirmDeleteModal__button, Styles['--no'])
    },
    okButtonProps: {
      className: classNames(Styles.confirmDeleteModal__button, Styles['--ok'])
    },
    cancelText: t('general.No'),
    okText: t('general.Yes'),
    centered: true
  });
};

export const ConfirmRequestChangeModal = ({ onOk, onCancel }) => {
  const { t } = i18n;
  const modal = Modal.confirm();
  modal.update({
    className: Styles.confirmRequestChangeModal,
    content: <ConfirmRequestChangeModalContent modal={modal} onOk={onOk} onCancel={onCancel} />,
    icon: null,
    cancelText: t('general.btnCancel'),
    cancelButtonProps: {
      type: 'ghost',
      className: Styles.cancelButton
    },
    okButtonProps: {
      className: Styles.okButton
    },
    centered: true,
    maskClosable: true,
    width: 480
  });
};

export const openManageSignatureRequestModal = ({ onApprove, onReject }) => {
  const { t } = i18n;
  const modal = Modal.confirm();

  modal.update({
    className: Styles.manageSignatureRequestModal,
    title: (
      <>
        <Icon className={Styles.manageSignatureRequestModal__icon} component={InfoIcon1} />
        <h2 className={Styles.manageSignatureRequestModal__title}>
          {t('modals.confirm.manageSignatureRequest.title')}
        </h2>
      </>
    ),
    content: (
      <>
        <p className={Styles.manageSignatureRequestModal__description}>
          <strong
            className={classNames(
              Styles.manageSignatureRequestModal__description,
              Styles['--bold']
            )}
          >
            {t('modals.confirm.manageSignatureRequest.description1')}{' '}
          </strong>
          {t('modals.confirm.manageSignatureRequest.description2')}
        </p>
        <div className={Styles.manageSignatureRequestModal__actionButtons}>
          <ApproveRejectButtons
            onApprove={async () => {
              try {
                await onApprove();
                modal.destroy();
              } catch (error) {}
            }}
            onReject={async () => {
              try {
                await onReject();
                modal.destroy();
              } catch (error) {}
            }}
            approveButtonClassName={Styles.manageSignatureRequestModal__actionButtons__button}
            rejectButtonClassName={Styles.manageSignatureRequestModal__actionButtons__button}
          />
        </div>
        <Divider />
      </>
    ),
    icon: null,
    cancelButtonProps: {
      ghost: true,
      type: 'primary',
      className: classNames(Styles.manageSignatureRequestModal__button, Styles['--cancel'])
    },
    okButtonProps: {
      className: classNames(Styles.manageSignatureRequestModal__button, Styles['--ok'])
    },
    cancelText: t('general.btnCancel'),
    centered: true,
    width: 494
  });
};
