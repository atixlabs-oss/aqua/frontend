/**
 * AGUA Copyright (C) 2023  Atix Labs
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import { Divider, Icon, Modal } from 'antd';
import i18n from 'i18n';
import classNames from 'classnames';

import { InfoIcon1 } from 'components/atoms/CustomIcons/InfoIcon1';
import Styles from './error-modals.module.scss';

const openBaseErrorModal = ({ title, descriptionBold, description, okText }) => {
  const modal = Modal.error();
  modal.update({
    className: Styles.baseErrorModal,
    title: (
      <>
        <Icon className={Styles.baseErrorModal__icon} component={InfoIcon1} />
        <h2 className={Styles.baseErrorModal__title}>{title}</h2>
      </>
    ),
    content: (
      <>
        <p className={classNames(Styles.baseErrorModal__description, Styles['--bold'])}>
          {descriptionBold}
        </p>
        <p className={Styles.baseErrorModal__description}>{description}</p>
        <Divider />
      </>
    ),
    centered: true,
    icon: null,
    width: 459,
    okButtonProps: {
      className: classNames(Styles.baseErrorModal__button, Styles['--ok'])
    },
    okText: okText
  });
};

export const openRequestChangeUserEditingAlreadyModal = () => {
  const { t } = i18n;

  openBaseErrorModal({
    title: t('modals.error.You can not request a change right now'),
    description: t('modals.error.The project is currently being edited by other user'),
    descriptionBold: t(
      'modals.error.Please return later if you want to edit or cancel this project'
    ),
    okText: t('general.Ok got it')
  });
};

export const openGenericErrorModal = ({ description } = { description: null }) => {
  const { t } = i18n;

  openBaseErrorModal({
    title: t('modals.error.An error ocurred'),
    description: description || t('modals.error.Try again later'),
    okText: t('general.Close')
  });
};
