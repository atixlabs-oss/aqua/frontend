import { Modal, Icon as AntIcon, Typography, Divider } from 'antd';
import Image from 'next/image';
import i18n from 'i18n';

import Styles from './dialog-modals.module.scss';
import TitlePage from 'components/atoms/TitlePage/TitlePage';
import { CoaButton } from 'components/atoms/CoaButton/CoaButton';

import jsPDF from 'jspdf';
import { RubikBold } from 'components/utils/Rubik-Bold';
import { RubikRegular } from 'components/utils/Rubik-Regular';
import { PinForm } from 'components/organisms/PinForm/PinForm';

export const openSetUpSecretKeyModal = (onSuccess, user, setPinMutation, setWalletMutation) => {
  const src = process.env.NEXT_PUBLIC_SMALL_LOGO_PATH;
  const { t } = i18n;

  const pin =
    process.env.NEXT_PUBLIC_NODE_ENV === 'testing'
      ? 111111
      : Math.floor(100000 + Math.random() * 900000);

  const saveSecretKey = () => {
    doc.save(`${process.env.NEXT_PUBLIC_ORGANIZATION_NAME} - secret key.pdf`);
  };

  const generatePDF = content => {
    const doc = jsPDF();
    const imageUrl = process.env.NEXT_PUBLIC_LARGE_LOGO_289x38_PNG_PATH;

    const imgWidth = 80;
    const imgHeight = 10;
    const pageWidth = doc.internal.pageSize.width;

    const x = (pageWidth - imgWidth) / 2 + 5;

    doc.addImage(imageUrl, 'png', x, 10, imgWidth, imgHeight);

    doc.addFileToVFS('Rubik-Bold.ttf', RubikBold);
    doc.addFont('Rubik-Bold.ttf', 'Rubik', 'bold');
    doc.addFileToVFS('Rubik-Regular.ttf', RubikRegular);
    doc.addFont('Rubik-Regular.ttf', 'Rubik', 'normal');
    doc.setFont('Rubik', 'bold').setFontSize(18);
    doc.text(70, 40, t('secretKey.thisIsYourSecretKey')).setTextColor('#26385B');

    doc
      .setFont('Rubik', 'normal')
      .setFontSize(12)
      .setTextColor('#728099');
    doc.text(t('secretKey.theKeyIsUniqueAndYouWillNeedItToPerform'), 60, 50);
    doc.text(t('secretKey.differentActionsWithinThePlatform'), 70, 55);

    doc.setFont('Rubik', 'bold');
    doc.text(t('secretKey.alwaysRememberToKeepItInASafePlace'), 63, 60);

    doc.setFont('Rubik', 'normal');
    doc.setFontSize(36);
    doc.setTextColor(process.env.NEXT_PUBLIC_PRIMARY_COLOR || '#4C7FF7');
    doc.text(87, 80, content?.toString());

    return doc;
  };

  const doc = generatePDF(pin);

  const modal = Modal.confirm();

  modal.update({
    icon: null,
    centered: true,
    className: Styles.modal,
    title: (
      <div className={Styles.titleContainer}>
        <AntIcon
          className={Styles.titleContainer__icon}
          component={() => (
            <Image
              src={src}
              alt={`${process.env.NEXT_PUBLIC_ORGANIZATION_NAME} logo`}
              width={56}
              height={56}
            />
          )}
        />
        <TitlePage
          HtmlTag="h2"
          textTitle={t('secretKey.setUpYourSecretKey')}
          underlinePosition="none"
          variant="primary"
          centeredText
          fontSize="bigger"
        />
      </div>
    ),
    content: (
      <div className={Styles.contentContainer}>
        <Typography.Paragraph className={Styles.contentContainer__description}>
          {t('secretKey.description1')} <strong>{t('secretKey.description2')}</strong>
          {t('secretKey.description3')} <strong>{t('secretKey.description4')}</strong>{' '}
          {t('secretKey.description5')} <strong>{t('secretKey.description6')}</strong>
        </Typography.Paragraph>
        <CoaButton
          icon="download"
          type="ghost"
          className={Styles.contentContainer__downloadButton}
          onClick={saveSecretKey}
        >
          {t('general.downloadFile')}
        </CoaButton>
        <div className={Styles.dividerContainer}>
          <Divider className={Styles.dividerContainer__divider} />
        </div>

        <PinForm
          setWalletMutation={setWalletMutation}
          setPinMutation={setPinMutation}
          pin={pin}
          onSuccess={() => {
            modal.destroy();
            onSuccess();
          }}
          user={user}
        />
      </div>
    ),
    width: '30rem'
  });
};
