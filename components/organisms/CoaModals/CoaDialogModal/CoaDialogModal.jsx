/**
 * AGUA Copyright (C) 2023  Atix Labs
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import React, { useState } from 'react';
import PropTypes from 'prop-types';

import classNames from 'classnames';
import { CoaButton } from 'components/atoms/CoaButton/CoaButton';
import { CoaBaseModal } from '../CoaBaseModal/CoaBaseModal';

export const CoaDialogModal = ({
  children,
  title,
  onSave,
  onCancel,
  form,
  withLogo,
  buttonsPosition,
  withoutCancelButton,
  okText,
  cancelText,
  okButtonProps,
  ...rest
}) => {
  const [isSubmitLoading, setIsSubmitLoading] = useState(false);
  const handleSave = async () => {
    setIsSubmitLoading(true);

    try {
      if (!form) {
        await onSave?.();
        return setIsSubmitLoading(false);
      }

      form.validateFields(async (err, values) => {
        if (!err) {
          await onSave(values);
          setIsSubmitLoading(false);
          form.resetFields();
          // NOTE: This behavior is for modals used in signing Messages
          if (cancelText === 'Go Back') return;
          return onCancel();
        }
        setIsSubmitLoading(false);
      });
    } catch {
      setIsSubmitLoading(false);
    }
  };

  const handleCancel = () => {
    if (!form) return onCancel?.();

    form.resetFields();
    onCancel();
  };

  return (
    <CoaBaseModal
      className="o-coaDialogModal"
      footer={
        <div
          className={classNames('o-coaDialogModal__footerContainer', {
            [`--${buttonsPosition}`]: buttonsPosition
          })}
        >
          {!withoutCancelButton && (
            <CoaButton type="ghost" onClick={handleCancel} className="o-coaDialogModal__button">
              <span className="o-coaDialogModal__button__text">{cancelText}</span>
            </CoaButton>
          )}
          <CoaButton
            {...okButtonProps}
            type="primary"
            onClick={handleSave}
            className="o-coaDialogModal__button"
            loading={isSubmitLoading || okButtonProps?.loading}
          >
            <span className="o-coaDialogModal__button__text">{okText}</span>
          </CoaButton>
        </div>
      }
      onCancel={onCancel}
      withLogo={withLogo}
      title={title}
      {...rest}
    >
      {children}
    </CoaBaseModal>
  );
};

CoaDialogModal.defaultProps = {
  children: undefined,
  title: undefined,
  onSave: undefined,
  onCancel: undefined,
  form: undefined,
  buttonsPosition: 'right',
  withLogo: undefined,
  okText: 'Save',
  cancelText: 'Cancel',
  withoutCancelButton: undefined
};

CoaDialogModal.propTypes = {
  children: PropTypes.oneOfType([PropTypes.arrayOf(PropTypes.node), PropTypes.node]),
  title: PropTypes.oneOfType([PropTypes.arrayOf(PropTypes.node), PropTypes.node]),
  onSave: PropTypes.func,
  onCancel: PropTypes.func,
  form: PropTypes.objectOf(PropTypes.any),
  buttonsPosition: PropTypes.string,
  withLogo: PropTypes.bool,
  okText: PropTypes.string,
  cancelText: PropTypes.string,
  withoutCancelButton: PropTypes.bool,
  okButtonProps: PropTypes.object
};
