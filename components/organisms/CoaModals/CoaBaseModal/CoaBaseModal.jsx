/**
 * AGUA Copyright (C) 2023  Atix Labs
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import React from 'react';
import PropTypes from 'prop-types';
import { Modal } from 'antd';

import classNames from 'classnames';
import Image from 'next/image';

const FOOTER_BUTTONS_POSITION = {
  left: '--footerButtonsLeft',
  center: '--footerButtonsCenter',
  right: '--footerButtonsRight'
};

export const CoaBaseModal = ({
  children,
  className,
  footerButtonPosition,
  withLogo,
  logoImage,
  title,
  description,
  ...rest
}) => {
  const src = logoImage || process.env.NEXT_PUBLIC_SMALL_LOGO_PATH;

  return (
    <Modal
      className={classNames('o-coaBaseModal', className, {
        [FOOTER_BUTTONS_POSITION[footerButtonPosition]]: footerButtonPosition
      })}
      closable
      centered
      {...rest}
    >
      <div className="o-coaBaseModal__header">
        <div className="o-coaBaseModal__header__logo">
          {withLogo && (
            <Image
              src={src}
              alt={`${process.env.NEXT_PUBLIC_ORGANIZATION_NAME} logo`}
              width={56}
              height={56}
            />
          )}
        </div>
        {title}
      </div>
      <div className="o-coaBaseModal__body">
        <p className="o-coaBaseModal__body__description">{description}</p>
        {children}
      </div>
    </Modal>
  );
};

CoaBaseModal.defaultProps = {
  children: undefined,
  className: undefined,
  footerButtonPosition: 'right',
  withLogo: undefined,
  logoImage: undefined,
  title: undefined,
  description: undefined
};

CoaBaseModal.propTypes = {
  children: PropTypes.oneOfType([PropTypes.arrayOf(PropTypes.node), PropTypes.node]),
  className: PropTypes.string,
  footerButtonPosition: PropTypes.string,
  withLogo: PropTypes.bool,
  logoImage: PropTypes.string,
  title: PropTypes.oneOfType([PropTypes.arrayOf(PropTypes.node), PropTypes.node]),
  description: PropTypes.string
};
