/**
 * AGUA Copyright (C) 2023  Atix Labs
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import { Icon as AntIcon, Divider, Modal } from 'antd';

import Styles from './success-modals.module.scss';
import { SuccessIcon } from 'components/atoms/CustomIcons/SuccessIcon';
import TitlePage from 'components/atoms/TitlePage/TitlePage';
import i18n from 'i18n';
import Router from 'next/router';
import Image from 'next/image';
import { CoaButton } from 'components/atoms/CoaButton/CoaButton';

export const openBaseSuccessModal = ({
  Icon,
  title,
  description,
  okButtonText,
  okButtonWidth,
  okButtonHeight,
  withCloseButton,
  onOk,
  ...rest
}) => {
  const modal = Modal.success();
  modal.update({
    className: Styles.modalContainer,
    title: (
      <div className={Styles.titleContainer}>
        {withCloseButton && (
          <CoaButton
            icon="close"
            type="link"
            className={Styles.titleContainer__closeIcon}
            onClick={modal.destroy}
          />
        )}

        <AntIcon className={Styles.titleContainer__icon} component={Icon} />
        <TitlePage
          HtmlTag="h2"
          textTitle={title}
          underlinePosition="none"
          variant="primary"
          centeredText
          fontSize="bigger"
        />
      </div>
    ),
    content: (
      <>
        <p className={Styles.description}>{description}</p>
        <Divider className={Styles.divider} />
      </>
    ),
    icon: null,
    okText: okButtonText,
    okButtonProps: {
      className: Styles.button,
      style: {
        '--width': okButtonWidth,
        '--height': okButtonHeight
      }
    },
    centered: true,
    onOk: async () => {
      await onOk?.();
      modal.destroy();
    },
    ...rest
  });
};

export const openPinRequestSuccessModal = () => {
  const { t } = i18n;
  openBaseSuccessModal({
    Icon: SuccessIcon,
    title: t('modals.success.pinRequest.title'),
    description: t('modals.success.pinRequest.description'),
    okButtonText: t('modals.common.continue'),
    okButtonWidth: '9.625rem',
    okButtonHeight: '2.5rem'
  });
};

export const openPinRequestApprovedModal = () => {
  const { t } = i18n;
  openBaseSuccessModal({
    Icon: SuccessIcon,
    title: t('modals.success.pinRequestApproved.title'),
    description: t('modals.success.pinRequestApproved.description'),
    okButtonText: t('modals.success.pinRequestApproved.okButtonText'),
    okButtonWidth: '12rem',
    okButtonHeight: '2.5rem',
    width: 459,
    onOk: () => Router.push('/secret-key'),
    withCloseButton: true
  });
};

export const openSetUpSecretKeySuccessModal = onSuccess => {
  const { t } = i18n;
  const src = process.env.NEXT_PUBLIC_SMALL_LOGO_PATH;

  openBaseSuccessModal({
    Icon: () => (
      <Image
        src={src}
        alt={`${process.env.NEXT_PUBLIC_ORGANIZATION_NAME} logo`}
        width={56}
        height={56}
      />
    ),
    title: t('modals.success.pinChange.title'),
    description: t('modals.success.pinChange.description'),
    okButtonText: t('modals.common.continue'),
    okButtonWidth: '8.8125rem',
    okButtonHeight: '2.375rem',
    width: 520,
    onOk: onSuccess
  });
};
