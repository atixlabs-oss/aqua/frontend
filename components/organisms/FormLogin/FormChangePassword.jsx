/**
 * AGUA Copyright (C) 2023  Atix Labs
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import PropTypes from 'prop-types';
import React from 'react';
import { Form, Input } from 'antd';
import { CoaDialogModal } from '../CoaModals/CoaDialogModal/CoaDialogModal';
import TitlePage from 'components/atoms/TitlePage/TitlePage';
import { useTranslation } from 'react-i18next';
import Styles from './form-change-password.module.scss';

const FormPassword = ({ form, onSubmit, visible, setVisible }) => {
  const { getFieldDecorator, getFieldProps } = form;
  const { t } = useTranslation();

  const submit = () => {
    form.validateFields(err => {
      if (!err) {
        return onSubmit(getFieldProps('password').value, getFieldProps('confirm').value);
      }
    });
  };

  const validPasswords = (_rule, value, callback) => {
    if (value && value !== form.getFieldValue('password')) {
      callback(t('resetPassword.passwordsDoNotMatch'));
    } else {
      callback();
    }
  };

  const validateNewPassword = (_rule, value, callback) => {
    const regexPassword = new RegExp('(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.{8,})');
    if (value && !regexPassword.test(value)) {
      callback(t('resetPassword.yourPasswordMustHaveTheFollowing'));
    } else {
      callback();
    }
  };

  return (
    <CoaDialogModal
      visible={visible}
      mask={false}
      onCancel={() => setVisible(false)}
      okText={t('resetPassword.changePassword')}
      onSave={submit}
      withLogo
      title={
        <TitlePage
          centeredText
          textTitle={t('resetPassword.changePassword')}
          underlinePosition="none"
        />
      }
    >
      <Form className={Styles.form} onSubmit={submit}>
        <Form.Item label={t('resetPassword.password')}>
          {getFieldDecorator('password', {
            rules: [
              { required: true, message: t('resetPassword.pleaseInputYourNewPassword') },
              {
                validator: validateNewPassword
              }
            ]
          })(<Input.Password placeholder={t('resetPassword.newPassword')} />)}
        </Form.Item>
        <Form.Item label={t('resetPassword.confirmPassword')}>
          {getFieldDecorator('confirm', {
            rules: [
              { required: true, message: t('resetPassword.pleaseConfirmYourNewPassword') },
              {
                validator: validPasswords
              }
            ]
          })(<Input.Password placeholder={t('resetPassword.repeatYourNewPassword')} />)}
        </Form.Item>
      </Form>
    </CoaDialogModal>
  );
};

const DynamicFormChangePassword = Form.create({ name: 'FormChangePassword' })(FormPassword);

export default DynamicFormChangePassword;

FormPassword.defaultProps = {
  visible: false,
  setVisible: () => undefined
};

FormPassword.propTypes = {
  form: PropTypes.element.isRequired,
  onSubmit: PropTypes.func.isRequired,
  visible: PropTypes.bool,
  setVisible: PropTypes.func
};
