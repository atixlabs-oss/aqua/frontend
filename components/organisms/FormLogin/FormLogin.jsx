/**
 * AGUA Copyright (C) 2023  Atix Labs
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import React, { useState } from 'react';
import PropTypes from 'prop-types';
import { Form, message, Typography } from 'antd';
import Field from '../../atoms/Field/Field';
import Captcha from '../../atoms/Captcha/Captcha';
import { CoaButton } from 'components/atoms/CoaButton/CoaButton';
import Styles from './form-login.module.scss';
import { useTranslation } from 'react-i18next';

const { Text } = Typography;

const DynamicLabel = ({ valid, label }) => (
  <p className={`${valid || valid === undefined ? 'valid' : 'invalid'}Label`}>{label}</p>
);

DynamicLabel.defaultProps = {
  valid: undefined,
  label: ''
};

DynamicLabel.propTypes = {
  valid: PropTypes.bool,
  label: PropTypes.string
};

export const formLoginInputs = {
  email: {
    name: 'email',
    placeholder: 'example@domain.com',
    rules: [
      {
        regex: /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/,
        message: 'The input is not valid E-mail!'
      },
      {
        required: true,
        message: 'Please input your E-mail!'
      }
    ]
  },
  password: {
    name: 'password',
    placeholder: 'Password',
    type: 'password',
    rules: [
      {
        required: true,
        message: 'Please input your password!'
      }
    ]
  }
};

const getInvalidRule = (field, fields) => {
  // TODO : input.value wont work for Checkbox (and maybe Select).
  if (field.rules === undefined || field.rules.length === 0) return undefined;

  // find the first not satisfied rule
  return field.rules.find(rule => {
    // allow custom validators.
    const validator = rule.validator ? rule.validator : validate;

    return !validator(rule, getFieldValue(field), fields) ? rule.message : undefined;
  });
};

const validateField = (field, fields) => {
  const rule = getInvalidRule(field, fields);
  const valid = rule === undefined;
  const errorMessage = valid ? undefined : rule.message;

  return {
    ...field,
    valid,
    errorMessage
  };
};

const getFieldValue = field => field.value || field.selected || field.checked;

const validate = (rule, value) => {
  let isValid = true;

  if (value === undefined) {
    // eslint-disable-next-line no-param-reassign
    value = '';
  }

  const v = rule.whitespace ? value.trim() : value;

  if (rule.required) {
    isValid = isValid && v.length > 0;
  }
  if (rule.regex) {
    isValid = isValid && v.match(rule.regex);
  }
  return isValid;
};

const FormLogin = ({ form, onSubmit }) => {
  const [fields, setFields] = useState(formLoginInputs);
  const [isCaptchaVerified, setIsCaptchaVerified] = useState(false);
  const [feedback, setFeedback] = useState('');
  const { t } = useTranslation();

  const clearFields = () => {
    resetFormLoginInputs();
    return setFields(formLoginInputs);
  };

  const handleChange = event => {
    const newValue = event.target.value;
    const inputName = event.target.name;
    const field = fields[inputName];

    setFeedback('');

    field.value = newValue;
    setFields({
      ...fields,
      [inputName]: validateField(field, fields)
    });
  };

  const submit = async e => {
    e.preventDefault();
    if (!isCaptchaVerified && process.env.NEXT_PUBLIC_NODE_ENV !== 'testing') {
      message.error('Please verify that you are a human with the Captcha');
      return;
    }
    form.validateFields();
    const { error } = await onSubmit(fields.email.value, fields.password.value, clearFields);

    if (error) {
      setFields(oldFields => ({
        email: {
          ...oldFields.email,
          valid: false
        },
        password: {
          ...oldFields.password,
          valid: false
        }
      }));
      // TODO: make api error response match UX specification improve source of truth
      setFeedback(error);
    }
  };

  const resetFormLoginInputs = () => {
    Object.keys(formLoginInputs).forEach(fieldName => {
      formLoginInputs[fieldName].value = '';
    });
  };

  return (
    <Form className="login-form" onSubmit={submit} data-testid="login-form">
      <Field
        {...fields.email}
        label={<DynamicLabel label={t('loginSection.email')} {...fields.email} />}
        handleChange={handleChange}
        hasFeedback
      />

      <Field
        {...fields.password}
        label={<DynamicLabel label={t('loginSection.password')} {...fields.password} />}
        handleChange={handleChange}
        hasFeedback
      />
      {feedback && (
        <Text type="danger" className="loginFeedback">
          {feedback}
        </Text>
      )}
      <Form.Item>
        <Captcha onChange={value => setIsCaptchaVerified(value)}>Captcha</Captcha>
        <CoaButton
          type="primary"
          onClick={submit}
          htmlType="submit"
          className={Styles.formLogin__button}
        >
          {t('loginSection.login')}
        </CoaButton>
      </Form.Item>
    </Form>
  );
};

const DynamicForm = Form.create({ name: 'FormLogin' })(FormLogin);

export default DynamicForm;

FormLogin.propTypes = {
  form: PropTypes.objectOf(PropTypes.any),
  onSubmit: PropTypes.func.isRequired
};
