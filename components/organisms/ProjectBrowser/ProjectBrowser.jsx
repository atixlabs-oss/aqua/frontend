/**
 * AGUA Copyright (C) 2023  Atix Labs
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import React from 'react';
import PropTypes from 'prop-types';
import { useTranslation } from 'react-i18next';
import { Row, Col } from 'antd';
import classNames from 'classnames';

import CardProject from '../../molecules/CardProject/CardProject';
import CardNewProject from '../../molecules/CardProject/CardNewProject';
import TitlePage from '../../atoms/TitlePage/TitlePage';
import { projectCardPropType } from '../../../helpers/proptypes';
import Styles from './project-browser.module.scss';
import { ProjectsEmptyState } from '../ProjectsEmptyState/ProjectsEmptyState';
import Loading from 'components/molecules/Loading/Loading';
import { useGetCountriesQuery } from 'api/react-query/countries/countriesQueries';

const ProjectBrowser = ({
  title,
  projects,
  onTagClick,
  onCardClick,
  onNewProject,
  withDescription,
  isAdmin,
  onClickEdit,
  canExecuteActions,
  isLoading,
  onVisibilityChange
}) => {
  const { t } = useTranslation();
  const { data: countries = [] } = useGetCountriesQuery();

  const canAddNewProjects = isAdmin && onNewProject;

  return (
    <div className={classNames(Styles.projectBrowser)}>
      {title && (
        <Row>
          <Col span={14}>
            <TitlePage textTitle={title} weight="boldest" />
          </Col>
        </Row>
      )}
      {isLoading && <Loading />}
      {projects?.length === 0 && !isLoading && !canAddNewProjects && (
        <ProjectsEmptyState title={t('projects.There are no projects to show yet')} />
      )}
      {!isLoading && (
        <div className={Styles.projectBrowser__cardsContainer}>
          {canAddNewProjects && <CardNewProject onClick={onNewProject} />}
          {projects?.map?.(project => (
            <CardProject
              onClickEdit={event => onClickEdit(event, project)}
              project={project}
              tagClick={() => onTagClick(project.id)}
              key={project.id}
              onClick={() => onCardClick(project)}
              countries={countries}
              withDescription={withDescription}
              canExecuteActions={canExecuteActions}
              onVisibilityChange={onVisibilityChange}
            />
          ))}
        </div>
      )}
    </div>
  );
};

ProjectBrowser.defaultProps = {
  onNewProject: undefined,
  withDescription: false,
  isAdmin: false,
  isLoading: true
};

ProjectBrowser.propTypes = {
  title: PropTypes.string.isRequired,
  projects: PropTypes.arrayOf(PropTypes.shape(projectCardPropType)).isRequired,
  onCardClick: PropTypes.func.isRequired,
  onTagClick: PropTypes.func.isRequired,
  onNewProject: PropTypes.func,
  withDescription: PropTypes.bool,
  isAdmin: PropTypes.bool,
  onClickEdit: PropTypes.func,
  canExecuteActions: PropTypes.bool,
  isLoading: PropTypes.bool,
  onVisibilityChange: PropTypes.func
};

export default ProjectBrowser;
