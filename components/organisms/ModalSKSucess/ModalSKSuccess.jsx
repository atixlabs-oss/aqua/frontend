/**
 * AGUA Copyright (C) 2023  Atix Labs
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import React from 'react';
import PropTypes from 'prop-types';
import { Typography } from 'antd';
import { CoaDialogModal } from '../CoaModals/CoaDialogModal/CoaDialogModal';
import TitlePage from 'components/atoms/TitlePage/TitlePage';

export default function ModalSKSuccess({ visible, onSuccess, ...rest }) {
  return (
    <CoaDialogModal
      {...rest}
      visible={visible}
      mask
      className="SecretKey"
      okText="Continue"
      onSave={onSuccess}
      buttonsPosition="center"
      withoutCancelButton
      withLogo
      title={
        <TitlePage
          textTitle="Your account was set successfully!"
          centeredText
          underlinePosition="none"
        />
      }
    >
      <Typography className="txtcenter">Now you can proceed</Typography>
    </CoaDialogModal>
  );
}

ModalSKSuccess.defaultProps = {
  visible: false,
  onSuccess: () => undefined
};

ModalSKSuccess.propTypes = {
  visible: PropTypes.bool,
  onSuccess: PropTypes.func
};
