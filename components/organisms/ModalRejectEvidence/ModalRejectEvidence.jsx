/**
 * AGUA Copyright (C) 2023  Atix Labs
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import React from 'react';
import { Form } from 'antd';
import TitlePage from 'components/atoms/TitlePage/TitlePage';
import { CoaFormItemTextArea } from 'components/molecules/CoaFormItems/CoaFormItemTextArea/CoaFormItemTextArea';
import { CoaDialogModal } from '../CoaModals/CoaDialogModal/CoaDialogModal';
import { useTranslation } from 'react-i18next';

export const FormModalRejectEvidence = ({ form, visible, setVisible, onSuccess }) => {
  const { t } = useTranslation();
  return (
    <CoaDialogModal
      form={form}
      visible={visible}
      onCancel={() => setVisible(false)}
      cancelText={t('general.btnCancel')}
      okText={t('general.btnReject')}
      onSave={({ comment }) => onSuccess(comment)}
      buttonsPosition="center"
      withLogo
      title={
        <TitlePage
          centeredText
          textTitle={t('modalRejectEvidence.title')}
          underlinePosition="none"
        />
      }
      description={t('modalRejectEvidence.leaveComment')}
    >
      <Form>
        <CoaFormItemTextArea
          form={form}
          formItemProps={{
            label: t('modalRejectEvidence.textPlaceholder')
          }}
          name="comment"
          fieldDecoratorOptions={{
            rules: [
              {
                required: true,
                message: t('modalRejectEvidence.ruleComment')
              }
            ]
          }}
          inputTextAreaProps={{
            rows: 5
          }}
        />
      </Form>
    </CoaDialogModal>
  );
};

const ModalRejectEvidence = Form.create({ name: 'RejectEvidence' })(FormModalRejectEvidence);
export default ModalRejectEvidence;
