/**
 * AGUA Copyright (C) 2023  Atix Labs
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import React, { useEffect, useState, useContext } from 'react';
import PropTypes from 'prop-types';
import { message } from 'antd';
import { ACCESS_TOKEN_KEY } from 'constants/constants';
import { UserContext } from '../../utils/UserContext';
import DynamicForm from '../FormLogin/FormLogin';
import ModalRecovery from '../ModalRecovery/ModalRecovery';
import { useRouter } from 'next/router';
import { CoaDialogModal } from '../CoaModals/CoaDialogModal/CoaDialogModal';
import TitlePage from 'components/atoms/TitlePage/TitlePage';
import { useTranslation } from 'react-i18next';
import { ADDRESS_STATUS_ENUM } from 'model/addressStatus';
import { useLoginUserMutation } from 'api/react-query/users/usersMutations';

const ModalLogin = ({ setVisibility, visibility }) => {
  const context = useContext(UserContext);
  const loginUserMutation = useLoginUserMutation();
  const { changeUser } = context || { changeUser: () => {} };
  const [onLoginRoute, setOnLoginRoute] = useState(false);
  const [closable, setClosable] = useState(false);
  const router = useRouter();
  const { t } = useTranslation();
  const { projectId } = router.query;

  useEffect(() => {
    setOnLoginRoute(router.pathname.includes('/login'));
  }, [router.pathname]);

  useEffect(() => {
    if (onLoginRoute) {
      setVisibility(true);
      setClosable(false);
    }
  }, [onLoginRoute, setVisibility]);

  const onLoginSubmit = async (email, pwd, clearFields) => {
    // Return an object to provide feedback after submitting
    const result = {};
    if (!email || !pwd || email === '' || pwd === '') {
      result.error = 'Must add fields';
      return result;
    }
    try {
      const { data: user, headers } = await loginUserMutation.mutateAsync({
        email,
        pwd
      });

      if (!user) return { error: t('apiErrorMessages.Invalid user or password') };

      changeUser(user);
      const authorization = headers?.authorization;
      if (authorization) sessionStorage.setItem(ACCESS_TOKEN_KEY, authorization);
      result.user = user;

      const { isAdmin, pinStatus } = user;

      let nextRoute = projectId ? `/${projectId}` : '/my-projects';

      if ([ADDRESS_STATUS_ENUM.APPROVED, ADDRESS_STATUS_ENUM.INACTIVE].includes(pinStatus)) {
        nextRoute = projectId ? `/${projectId}/secret-key` : '/secret-key';
      } else if (isAdmin) {
        nextRoute = '/back-office/projects';
      }

      clearFields();
      setVisibility(false);
      router.push(nextRoute);
    } catch (error) {
      if (error?.status === 401) {
        message.error(t('general.user is not active'));
        result.error = t('general.user is not active');
      } else if (error?.status === 400) {
        message.error(t('apiErrorMessages.Invalid user or password'));
        result.error = t('apiErrorMessages.Invalid user or password');
      } else if (error?.status !== 401) {
        message.error(t('apiErrorMessages.Internal error please contact support'));
        result.error = t('apiErrorMessages.Internal error please contact support');
      }
    }
    return result;
  };

  return (
    <CoaDialogModal
      visible={visibility}
      onOk={() => setVisibility(false)}
      onCancel={() => setVisibility(false)}
      className="ModalLogin"
      mask={!onLoginRoute}
      footer={null}
      withLogo
      title={
        <TitlePage textTitle={t('loginSection.login')} centeredText underlinePosition="none" />
      }
    >
      <DynamicForm onSubmit={onLoginSubmit} />
      <div className="flex link">
        <ModalRecovery />
      </div>
    </CoaDialogModal>
  );
};

export default ModalLogin;

ModalLogin.defaultProps = {
  visibility: false
};

ModalLogin.propTypes = {
  setVisibility: PropTypes.func.isRequired,
  visibility: PropTypes.bool
};
