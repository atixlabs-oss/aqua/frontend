/**
 * AGUA Copyright (C) 2023  Atix Labs
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/* eslint-disable max-len */
import GoBackButton from 'components/atoms/GoBackButton/GoBackButton';
import React from 'react';
import { useRouter } from 'next/router';
import Styles from './privacy-policies-container.module.scss';
import { useTranslation } from 'react-i18next';

const PrivacyPoliciesContainer = () => {
  const router = useRouter();
  const { t } = useTranslation('privacyPolicies');
  return (
    <div className={Styles.privacyPoliciesContainer}>
      <GoBackButton goBackTo={() => router.back()} />
      <div className={Styles.privacyPoliciesContainer__content}>
        <h1 className={Styles.privacyPoliciesContainer__title}>{t('title')}</h1>
        <p>{t('firstParagraph')}</p>
        <p>{t('secondParagraph')}</p>
        <p>{t('thirdParagraph')}</p>
        <h2>I. {t('collectedData.title')}</h2>
        <p>{t('collectedData.firstParagraph')}</p>
        <ul>
          <li>{t('collectedData.firstItem')}</li>
          <li>{t('collectedData.secondItem')}</li>
          <li>{t('collectedData.thirdItem')}</li>
          <li>{t('collectedData.fourthItem')}</li>
          <li>{t('collectedData.fifthItem')}</li>
        </ul>
        <p>{t('collectedData.secondParagraph')}</p>
        <p>{t('collectedData.thirdParagraph')}</p>
        <h2>II. {t('purposesForProcessing.title')}</h2>
        <p>{t('purposesForProcessing.firstParagraph')}</p>
        <ul>
          <li>{t('purposesForProcessing.firstItem')}</li>
          <li>{t('purposesForProcessing.secondItem')}</li>
          <li>{t('purposesForProcessing.thirdItem')}</li>
        </ul>
        <h2>III. {t('informationWeShare.title')}</h2>
        <p>{t('informationWeShare.firstParagraph')}</p>
        <ul>
          <li>{t('informationWeShare.firstItem')}</li>
          <li>{t('informationWeShare.secondItem')}</li>
          <li>{t('informationWeShare.thirdItem')}</li>
          <li>{t('informationWeShare.fourthItem')}</li>
          <li>{t('informationWeShare.fifthItem')}</li>
        </ul>
        <p>{t('informationWeShare.secondParagraph')}</p>
        <h2>IV. {t('informationStorage.title')}</h2>
        <p>{t('informationStorage.firstParagraph')}</p>
        <p>{t('informationStorage.secondParagraph')}</p>
        <p>{t('informationStorage.thirdParagraph')}</p>
        <h2>V. {t('yourRights.title')}</h2>
        <p>{t('yourRights.firstParagraph')}</p>
        <h2>VI. {t('linksToOtherWebsites.title')}</h2>
        <p>{t('linksToOtherWebsites.firstParagraph')}</p>
        <h2>VII. {t('updates.title')}</h2>
        <p>{t('updates.firstParagraph')}</p>
        <p>{t('updates.endNote', { date: '22/03/2023' })} </p>
      </div>
    </div>
  );
};

export default PrivacyPoliciesContainer;
