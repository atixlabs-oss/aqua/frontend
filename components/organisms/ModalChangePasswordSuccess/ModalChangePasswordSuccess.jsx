/**
 * AGUA Copyright (C) 2023  Atix Labs
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import React from 'react';
import { Typography } from 'antd';
import PropTypes from 'prop-types';
import { useRouter } from 'next/router';
import { CoaDialogModal } from '../CoaModals/CoaDialogModal/CoaDialogModal';
import { useTranslation } from 'react-i18next';
import TitlePage from 'components/atoms/TitlePage/TitlePage';

function ModalChangePasswordSuccess({ visible, first, ...rest }) {
  const router = useRouter();
  const { projectId } = router.query;
  const { t } = useTranslation();

  const goToLogin = () => {
    router.push(projectId ? `/${projectId}/login` : '/login');
  };

  return (
    <CoaDialogModal
      visible={visible}
      mask={false}
      className="ChangePasswordSuccess"
      onSave={goToLogin}
      okText={t('general.continue')}
      withLogo
      title={
        <TitlePage
          textTitle={
            first
              ? t('resetPassword.thanksForYourRegistration')
              : t('resetPassword.passwordResetSuccessfully')
          }
          underlinePosition="none"
          centeredText
        />
      }
      withoutCancelButton
      buttonsPosition="center"
      {...rest}
    >
      <Typography.Paragraph className="ChangePasswordParagraph">
        {t('resetPassword.useYourNewPasswordToLogin')}
      </Typography.Paragraph>
    </CoaDialogModal>
  );
}

export default ModalChangePasswordSuccess;

ModalChangePasswordSuccess.defaultProps = {
  visible: false
};

ModalChangePasswordSuccess.propTypes = {
  visible: PropTypes.bool
};
