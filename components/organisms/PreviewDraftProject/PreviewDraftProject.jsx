/**
 * AGUA Copyright (C) 2023  Atix Labs
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import React from 'react';
import { LandingDraftLayout } from 'components/Layouts/LandingDraftLayout/LandingDraftLayout';
import ProjectHeroSection from 'components/molecules/ProjectHeroSection/ProjectHeroSection';
import { formatCurrencyAtTheBeginning, formatTimeframeValue } from 'helpers/formatter';
import { useTranslation } from 'react-i18next';

export const PreviewDraftProject = ({ project, isAdmin, id, preview }) => {
  const { t } = useTranslation();
  const { basicInformation, status, details, budget, inReview, revision, cloneStatus } =
    project || {};
  const { projectName, location, beneficiary, timeframe, timeframeUnit, thumbnailPhoto } =
    basicInformation || {};
  const { currency } = details || {};
  const beneficiaryFirstName = beneficiary?.firstName;
  const beneficiaryLastName = beneficiary?.lastName;
  const beneficiaryCompleteName =
    beneficiaryFirstName || beneficiaryLastName
      ? `${beneficiaryFirstName} ${beneficiaryLastName}`
      : t('general.noName');
  return (
    <LandingDraftLayout
      project={project}
      projectId={project?.id}
      showPreviewAlert={preview && isAdmin}
      header={
        <ProjectHeroSection
          inReview={inReview}
          title={projectName}
          status={status}
          cloneStatus={cloneStatus}
          subtitle={process.env.NEXT_PUBLIC_ORGANIZATION_NAME}
          country={location}
          beneficiary={beneficiaryCompleteName}
          timeframe={formatTimeframeValue({ timeframe, timeframeUnit, t })}
          budget={formatCurrencyAtTheBeginning(currency, budget)}
          thumbnailPhoto={thumbnailPhoto}
          revision={revision}
        />
      }
      thumbnailPhoto={thumbnailPhoto}
    >
      PreviewDraftProject
    </LandingDraftLayout>
  );
};
