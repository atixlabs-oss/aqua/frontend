/**
 * AGUA Copyright (C) 2023  Atix Labs
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import React from 'react';
import { Typography } from 'antd';
import PropTypes from 'prop-types';
import { useRouter } from 'next/router';
import { CoaDialogModal } from '../CoaModals/CoaDialogModal/CoaDialogModal';
import TitlePage from 'components/atoms/TitlePage/TitlePage';

function ModalFirstLoginSuccess({ modalOpen, admin }) {
  const router = useRouter();

  return (
    <CoaDialogModal
      withLogo
      visible={modalOpen}
      mask={false}
      maskClosable={false}
      okText="Continue"
      withoutCancelButton
      buttonsPosition="center"
      onSave={() => {
        const url = admin ? '/back-office/projects' : '/';
        router.push(url);
      }}
      className={`ChangePasswordSuccess ${!admin ? 'First' : ''}`}
      title={<TitlePage centeredText textTitle="Thanks for you registration!" />}
    >
      <Typography.Paragraph className="ChangePasswordParagraph">
        {!admin
          ? `
    Your account was set successfully.
          Once the project is published, we will send you the next steps
          so that you can make your first login to the platform.
      `
          : 'Use your new password to login'}
      </Typography.Paragraph>
    </CoaDialogModal>
  );
}

ModalFirstLoginSuccess.defaultProps = {
  modalOpen: false,
  admin: false
};

ModalFirstLoginSuccess.propTypes = {
  modalOpen: PropTypes.bool,
  admin: PropTypes.bool
};

export default ModalFirstLoginSuccess;
