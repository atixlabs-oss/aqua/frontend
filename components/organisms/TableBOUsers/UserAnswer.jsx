/**
 * AGUA Copyright (C) 2023  Atix Labs
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import React from 'react';
import PropTypes from 'prop-types';
import { Table } from 'antd';

const UserAnswer = ({ user }) => {
  const userAnswers = JSON.parse(user.answers);
  const questions = Object.keys(userAnswers);
  const dataSource = questions.map(question => ({
    question,
    answer: userAnswers[question]
  }));
  const columns = [
    {
      title: 'Question',
      dataIndex: 'question',
      key: 'question'
    },
    {
      title: 'Answer',
      dataIndex: 'answer',
      key: 'answer',
      render: answer => (Array.isArray(answer) ? answer.join(', ') : answer)
    }
  ];

  return (user.phoneNumber && user.company) || user.answers ? (
    <div>
      {user && user.phoneNumber && <p>Phone number: {user.phoneNumber}</p>}
      {user && user.company && <p>Company: {user.company}</p>}
      {user.answers && (
        <div>
          <p>Questionnaire: </p>
          <Table
            dataSource={dataSource}
            className="TableBOProjects"
            columns={columns}
            pagination={false}
            size="small"
          />
        </div>
      )}
    </div>
  ) : (
    <div>This user has no additional information.</div>
  );
};

export default UserAnswer;

UserAnswer.propTypes = {
  user: PropTypes.element.isRequired
};
