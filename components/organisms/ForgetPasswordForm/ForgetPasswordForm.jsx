/**
 * AGUA Copyright (C) 2023  Atix Labs
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import { useState } from 'react';
import { Form } from 'antd';
import PropTypes from 'prop-types';

import { CoaFormItemInput } from 'components/molecules/CoaFormItems/CoaFormItemInput/CoaFormItemInput';
import { checkValidEmail } from 'helpers/utils';
import { CoaButton } from 'components/atoms/CoaButton/CoaButton';
import { useTranslation } from 'react-i18next';
import { useRecoverPasswordMutation } from 'api/react-query/users/usersMutations';
import Styles from './forget-password-form.module.scss';

const ForgetPasswordFormContent = ({ form, onSuccess }) => {
  const { t } = useTranslation();
  const [isSubmitLoading, setIsSubmitLoading] = useState(false);
  const recoverPasswordMutation = useRecoverPasswordMutation();

  const onSubmit = event => {
    event.preventDefault();
    form.validateFields(async err => {
      if (!err) {
        onSuccess?.();
      }
    });
  };

  return (
    <Form onSubmit={onSubmit}>
      <CoaFormItemInput
        form={form}
        name="email"
        withErrorFeedback
        formItemProps={{
          label: t('loginSection.recovery_password.your_email'),
          className: Styles.formItem
        }}
        fieldDecoratorOptions={{
          rules: [
            {
              required: true,
              message: t('loginSection.recovery_password.inputYourEmail')
            },
            {
              validator: async (_, email, callback) => {
                const isValidEmail = checkValidEmail(email);
                if (!isValidEmail && email)
                  callback(t('loginSection.recovery_password.notValidEmail'));
                if (isValidEmail && email) {
                  try {
                    setIsSubmitLoading(true);
                    await recoverPasswordMutation.mutateAsync({ email });
                  } catch (error) {
                    if (error?.status === 400)
                      callback(t('loginSection.recovery_password.noUserAssociated', { email }));
                    if (error?.status === 500) callback(t('general.serverError'));
                  } finally {
                    setIsSubmitLoading(false);
                  }
                }
                callback();
              }
            }
          ],
          validateTrigger: 'onSubmit'
        }}
      />

      <Form.Item>
        <CoaButton
          type="primary"
          style={{ width: '100%' }}
          htmlType="submit"
          loading={isSubmitLoading}
        >
          {t('loginSection.recovery_password.send_mail')}
        </CoaButton>
      </Form.Item>
    </Form>
  );
};
export const ForgetPasswordForm = Form.create({
  name: 'ForgetPasswordForm'
})(ForgetPasswordFormContent);

ForgetPasswordFormContent.propTypes = {
  onSuccess: PropTypes.func,
  form: PropTypes.object
};
