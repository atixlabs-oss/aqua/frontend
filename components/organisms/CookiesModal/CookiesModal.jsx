/**
 * AGUA Copyright (C) 2023  Atix Labs
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import React, { useState } from 'react';
import { COOKIES_ACCEPTED_KEY } from 'constants/constants';
import { CoaDialogModal } from '../CoaModals/CoaDialogModal/CoaDialogModal';
import Styles from './cookies-modal.module.scss';
import { useTranslation } from 'react-i18next';
import { Collapse, Table } from 'antd';

const { Panel } = Collapse;

export const CookiesModal = () => {
  const [areCookiesAccepted, setAreCookiesAccepted] = useState(
    Boolean(localStorage.getItem(COOKIES_ACCEPTED_KEY))
  );
  const { t } = useTranslation('cookies');

  const handleAcceptCookies = () => {
    setAreCookiesAccepted(true);
    localStorage.setItem(COOKIES_ACCEPTED_KEY, 'true');
  };

  return (
    <CoaDialogModal
      width={600}
      visible={!areCookiesAccepted}
      okText={t('accept_all_cookies')}
      onSave={handleAcceptCookies}
      className={Styles.cookiesModal}
      withoutCancelButton
      closable={false}
      wrapClassName={Styles.cookiesModalWrapper}
    >
      <h2 className={Styles.cookiesModal__title}>{t('title')}</h2>
      <Collapse defaultActiveKey={['1']} accordion className={Styles.cookiesModal__collapse}>
        <Panel header={t('about')} key="1" className={Styles.cookiesModal__panel}>
          <div className={Styles.cookiesModal__panelContent}>
            <p>{t('aboutContent.paragraph1')}</p>
            <p>{t('aboutContent.paragraph2')}</p>
            <p>{t('aboutContent.paragraph3')}</p>
            <p>{t('aboutContent.paragraph4')}</p>
          </div>
        </Panel>
        <Panel header={t('whatIsACookie')} key="2" className={Styles.cookiesModal__panel}>
          <div className={Styles.cookiesModal__panelContent}>
            <p>{t('whatIsACookieContent')}</p>
          </div>
        </Panel>
        <Panel
          header={t('howToControlAndDeleteCookies')}
          key="3"
          className={Styles.cookiesModal__panel}
        >
          <div className={Styles.cookiesModal__panelContent}>
            <p>{t('howToControlAndDeleteCookiesContent.description')}</p>
          </div>
        </Panel>
        <Panel header={t('cookieSettings')} key="4" className={Styles.cookiesModal__panel}>
          <div className={Styles.cookiesModal__panelContent}>
            <p>{t('cookieSettingsContent.description')}</p>
            <ul>
              <li>{t('cookieSettingsContent.cookieSettingsInInternetExplorer')}</li>
              <li>{t('cookieSettingsContent.cookieSettingsInFirefox')}</li>
              <li>{t('cookieSettingsContent.cookieSettingsInChrome')}</li>
              <li>{t('cookieSettingsContent.cookieSettingsInSafari')}</li>
            </ul>
            <p>{t('cookieSettingsContent.endNote')}</p>
          </div>
        </Panel>
        <Panel
          header={t('moreInformationOnCookies')}
          key="5"
          className={Styles.cookiesModal__panel}
        >
          <div className={Styles.cookiesModal__panelContent}>
            <p>{t('moreInformationOnCookiesContent')}</p>
          </div>
        </Panel>
        <Panel header={t('whatCookiesDoWeUse')} key="6" className={Styles.cookiesModal__panel}>
          <div className={Styles.cookiesModal__panelContent}>
            <p>{t('whatCookiesDoWeUseContent.description')}</p>
            <h3 className={Styles.cookiesModal__panelContent__subtitle}>
              {t('whatCookiesDoWeUseContent.typesOfCookies.title')}
            </h3>
            <ol>
              <li>{t('whatCookiesDoWeUseContent.typesOfCookies.firstParagraph')}</li>
              <li>{t('whatCookiesDoWeUseContent.typesOfCookies.secondParagraph')}</li>
            </ol>
            <h3 className={Styles.cookiesModal__panelContent__subtitle}>
              {t('whatCookiesDoWeUseContent.durationOfCookies.title')}
            </h3>
            <ol>
              <li>{t('whatCookiesDoWeUseContent.durationOfCookies.firstParagraph')}</li>
              <li>{t('whatCookiesDoWeUseContent.durationOfCookies.secondParagraph')}</li>
            </ol>
          </div>
        </Panel>
        <Panel header={t('listOfCookies')} key="7" className={Styles.cookiesModal__panel}>
          <div className={Styles.cookiesModal__panelContent}>
            <h3 className={Styles.cookiesModal__panelContent__subtitle}>
              {t('listOfCookiesContent.firstPartyCookies')}
            </h3>
            <Table
              className={Styles.cookiesModal__table}
              pagination={false}
              columns={[
                {
                  title: t('listOfCookiesContent.cookie'),
                  dataIndex: 'cookie',
                  key: 'cookie'
                },
                {
                  title: t('listOfCookiesContent.category'),
                  dataIndex: 'category',
                  key: 'category'
                },
                {
                  title: t('listOfCookiesContent.purpose'),
                  dataIndex: 'purpose',
                  key: 'purpose'
                },
                {
                  title: t('listOfCookiesContent.duration'),
                  dataIndex: 'duration',
                  key: 'duration'
                },
                {
                  title: `${t('listOfCookiesContent.expiration')} (${t(
                    'listOfCookiesContent.ifPersistent'
                  )})`,
                  dataIndex: 'expiration',
                  key: 'expiration'
                }
              ]}
              dataSource={[
                {
                  key: '1',
                  cookie: t('listOfCookiesContent.pardot'),
                  category: t('listOfCookiesContent.advertising'),
                  purpose: t('listOfCookiesContent.pardotPurpose'),
                  duration: t('listOfCookiesContent.session'),
                  expiration: 'NA'
                }
              ]}
            />
            <h3 className={Styles.cookiesModal__panelContent__subtitle}>
              {t('listOfCookiesContent.thirdPartyCookies')}
            </h3>
            <Table
              className={Styles.cookiesModal__table}
              pagination={false}
              columns={[
                {
                  title: t('listOfCookiesContent.cookie'),
                  dataIndex: 'cookie',
                  key: 'cookie'
                },
                {
                  title: t('listOfCookiesContent.category'),
                  dataIndex: 'category',
                  key: 'category'
                },
                {
                  title: t('listOfCookiesContent.purpose'),
                  dataIndex: 'purpose',
                  key: 'purpose'
                },
                {
                  title: t('listOfCookiesContent.duration'),
                  dataIndex: 'duration',
                  key: 'duration'
                },
                {
                  title: `${t('listOfCookiesContent.expiration')} (${t(
                    'listOfCookiesContent.ifPersistent'
                  )})`,
                  dataIndex: 'expiration',
                  key: 'expiration'
                }
              ]}
              dataSource={[
                {
                  key: '1',
                  cookie: t('listOfCookiesContent.ANID'),
                  category: t('listOfCookiesContent.advertising'),
                  purpose: t('listOfCookiesContent.anidPurpose'),
                  duration: t('listOfCookiesContent.persistent'),
                  expiration: t('listOfCookiesContent.expiresAfter2Years')
                }
              ]}
            />
          </div>
        </Panel>
      </Collapse>
    </CoaDialogModal>
  );
};
