import { useEffect, useState } from 'react';
import { useTranslation } from 'react-i18next';
import PropTypes from 'prop-types';

import Styles from './profile-change-password-block-form.module.scss';
import { useChangePasswordMutation } from 'api/react-query/users/usersMutations';
import { Form } from 'antd';
import { CoaFormItemPassword } from 'components/molecules/CoaFormItems/CoaFormItemPassword/CoaFormItemPassword';
import { CoaButton } from 'components/atoms/CoaButton/CoaButton';

const ProfileChangePasswordBlock = ({ form, setIsEditing }) => {
  const { t } = useTranslation();
  const { getFieldValue, validateFields, getFieldsError } = form;
  const changePasswordMutation = useChangePasswordMutation();
  const [isButtonDisabled, setIsButtonDisabled] = useState(true);
  const fieldsError = getFieldsError();
  const newPassword = getFieldValue('newPassword');
  const currentPassword = getFieldValue('currentPassword');
  const confirmNewPassword = getFieldValue('confirmNewPassword');

  useEffect(() => {
    const isNewPasswordWrong =
      (newPassword !== undefined && fieldsError?.newPassword?.length > 0) ||
      newPassword === undefined;
    const isCurrentPasswordWrong =
      (currentPassword !== undefined && fieldsError?.currentPassword?.length > 0) ||
      currentPassword === undefined;
    const isConfirmNewPasswordWrong =
      (confirmNewPassword !== undefined && fieldsError?.confirmNewPassword?.length > 0) ||
      confirmNewPassword === undefined;

    setIsButtonDisabled(isNewPasswordWrong || isCurrentPasswordWrong || isConfirmNewPasswordWrong);
  }, [fieldsError, confirmNewPassword, currentPassword, newPassword]);

  const handleFinish = event => {
    event.preventDefault();
    validateFields((err, values) => {
      if (!err) {
        changePasswordMutation
          .mutateAsync({
            currentPassword: values?.currentPassword,
            newPassword: values?.newPassword
          })
          .then(() => setIsEditing(false));
      }
    });
  };

  return (
    <Form form={form} className={Styles.changePasswordBlockForm} onSubmit={handleFinish}>
      <div className={Styles.changePasswordBlockForm__items}>
        <CoaFormItemPassword
          formItemProps={{
            label: t('profile.currentPassword'),
            className: Styles.changePasswordBlockForm__item
          }}
          fieldDecoratorOptions={{
            rules: [{ required: true, message: t('formErrors.currentPasswordRequired') }]
          }}
          form={form}
          name="currentPassword"
        />
        <CoaFormItemPassword
          formItemProps={{
            label: t('profile.newPassword'),
            className: Styles.changePasswordBlockForm__item,
            ErrorsComponent: ({ errors }) =>
              errors?.length > 0 && (
                <div>
                  {getFieldValue('newPassword') && (
                    <div>Your password must have the following:</div>
                  )}
                  <ul className={Styles.changePasswordBlockForm__formErrorsList}>
                    {errors.map(error => (
                      <li key={error}>
                        {getFieldValue('newPassword') && '-'} {error}
                      </li>
                    ))}
                  </ul>
                </div>
              )
          }}
          fieldDecoratorOptions={{
            rules: [
              { required: true, message: t('formErrors.passwordRequired') },
              { min: 8, message: t('formErrors.password8charsAtLeast') },
              {
                pattern: /^(?=.*[a-z])/,
                message: t('formErrors.password1lowerAtLeast')
              },
              {
                pattern: /^(?=.*[A-Z])/,
                message: t('formErrors.password1upperAtLeast')
              },
              {
                pattern: /^(?=.*\d)/,
                message: t('formErrors.password1numericAtLeast')
              }
            ]
          }}
          form={form}
          name="newPassword"
        />
        <CoaFormItemPassword
          formItemProps={{
            label: t('profile.confirmNewPassword'),
            className: Styles.changePasswordBlockForm__item
          }}
          form={form}
          name="confirmNewPassword"
          fieldDecoratorOptions={{
            rules: [
              { required: true, message: t('formErrors.passwordConfirmRequired') },
              {
                validator: (_, value, callback) => {
                  if (value && value !== getFieldValue('newPassword')) {
                    callback(t('formErrors.passwordConfirmNotMatch'));
                  }
                  callback();
                }
              }
            ]
          }}
        />
      </div>
      <CoaButton type="primary" htmlType="submit" disabled={isButtonDisabled}>
        {t('profile.saveChanges')}
      </CoaButton>
    </Form>
  );
};

ProfileChangePasswordBlock.propTypes = {
  setIsEditing: PropTypes.func,
  form: PropTypes.object
};

export const ProfileChangePasswordBlockForm = Form.create({
  name: 'FormChangePassword'
})(ProfileChangePasswordBlock);
