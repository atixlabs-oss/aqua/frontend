import { Icon } from 'antd';
import PropTypes from 'prop-types';

import { SearchIcon } from 'components/atoms/CustomIcons/SearchIcon';
import Styles from './projects-empty-state.module.scss';
import TitlePage from 'components/atoms/TitlePage/TitlePage';

export const ProjectsEmptyState = ({ title }) => (
  <div className={Styles.emptyStateContainer}>
    <Icon component={SearchIcon} />
    <TitlePage
      textTitle={title}
      weight="bolder"
      variant="primary"
      HtmlTag="h3"
      underlinePosition="none"
      className={Styles.emptyStateContainer__text}
      fontSize="big"
    />
  </div>
);

ProjectsEmptyState.defaultProps = {
  title: 'There are no projects to show yet'
};

ProjectsEmptyState.propTypes = {
  title: PropTypes.string
};
