import React from 'react';
import { useTranslation } from 'react-i18next';
import moment from 'moment';
import PropTypes from 'prop-types';

import Styles from './users-addresses-history.module.scss';
import { CustomSimpleCollapse } from 'components/molecules/CustomSimpleCollapse/CustomSimpleCollapse';
import classNames from 'classnames';

export const UserAddressesHistory = ({ addressHistory, className, twoRows }) => {
  const { t } = useTranslation();

  return (
    <CustomSimpleCollapse
      className={classNames(Styles.publicKeyCollapse, className)}
      title={t('profile.publicKeyHistory')}
      content={
        <div className={Styles.publicKeyCollapse__list}>
          {addressHistory?.map(item => (
            <div
              key={item?.address}
              className={classNames(Styles.publicKeyCollapse__list__item, {
                [Styles['--twoRows']]: twoRows
              })}
            >
              <span className={Styles.publicKeyCollapse__list__item__address}>{item?.address}</span>
              <span className={Styles.publicKeyCollapse__list__item__date}>
                {moment(item?.createdAt).format('YYYY-MM-DDTHH:mm:ss.SSS[Z]')}
              </span>
            </div>
          ))}
        </div>
      }
    />
  );
};

UserAddressesHistory.propTypes = {
  addressHistory: PropTypes.arrayOf(PropTypes.object),
  className: PropTypes.string
};
