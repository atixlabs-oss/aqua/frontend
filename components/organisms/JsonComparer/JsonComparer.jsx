import React, { useState, useEffect } from 'react';
import PropTypes from 'prop-types';

import Tree from './tree.js';
import { mergeData, isArray } from './utils.js';
import Styles from './json-comparer.module.scss';

export const JsonComparer = props => {
  const { oldData, newData, comparerType } = props;
  const [data, setMergeData] = useState([]);

  useEffect(() => {
    setMergeData(mergeData(oldData, newData));
  }, [oldData, newData]);

  return (
    <pre className={Styles['c-json-view']}>
      <p className={Styles['c-json-outter']}>{isArray(newData) ? '[' : '{'}</p>
      {data.map((item, index) => (
        <Tree key={index} {...item} comparerType={comparerType} />
      ))}
      <p className={Styles['c-json-outter']}>{isArray(newData) ? ']' : '}'}</p>
    </pre>
  );
};

JsonComparer.defaultProps = {
  comparerType: 'merged'
};

JsonComparer.propTypes = {
  oldData: PropTypes.object,
  newData: PropTypes.object,
  comparerType: PropTypes.oneOf(['merged', 'old', 'new'])
};
