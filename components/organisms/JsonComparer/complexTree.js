import React, { useState } from 'react';
import classNames from 'classnames';

import { isArray, getIndent } from './utils.js';
import Tree from './tree.js';
import Styles from './json-comparer.module.scss';
import { Caret } from './Caret.jsx';

export default function ComplexTree(props) {
  const {
    name,
    value,
    type,
    line,
    showIndex,
    needComma,
    level = 1,
    lineType,
    lastLineType,
    lastLine = null,
    comparerType
  } = props;

  const [visible, setVisible] = useState(true);

  return (
    <div className={Styles['c-json-line']}>
      <p
        className={classNames(Styles['c-json-p'], Styles[`c-line-${lineType}`], {
          [Styles['--withoutLineDel']]: comparerType === 'new',
          [Styles['--withoutLineAdd']]: comparerType === 'old'
        })}
        style={getIndent(level)}
      >
        <span className={Styles['c-json-mark']}>{line}</span>
        <span className={Styles[`c-of-${lineType}`]}></span>
        <span className={Styles['c-json-content']}>
          <div className={Styles['c-caret-container']}>
            <Caret setVisible={setVisible} visible={visible} className={Styles['c-caret']} />
          </div>{' '}
          {showIndex && <span className={Styles['c-json-key']}> {name}: </span>}
          <span className={Styles['c-json-pt']}>{isArray(type) ? '[' : ' {'}</span>
        </span>
        {!visible && (
          <span className={Styles['c-json-pt']}>
            {isArray(type) ? '...]' : '...}'}
            {needComma ? ',' : ''}
          </span>
        )}
      </p>
      <div style={{ display: visible ? 'block' : 'none' }}>
        {value.map((item, index) => (
          <Tree key={index} level={level + 1} {...item} comparerType={comparerType} />
        ))}
        <p
          className={classNames(
            Styles['c-json-feet'],
            Styles['c-json-p'],
            Styles[`c-line-${lineType}`],
            {
              [Styles['--withoutLineDel']]: comparerType === 'new',
              [Styles['--withoutLineAdd']]: comparerType === 'old'
            }
          )}
          style={getIndent(level)}
        >
          {lastLine && <span className={Styles['c-json-mark']}>{lastLine}</span>}
          {lastLineType && <span className={Styles[`c-of-${lastLineType}`]}></span>}
          <span className={Styles['c-json-pt']}>
            {isArray(type) ? ']' : '}'}
            {needComma ? ',' : ''}
          </span>
        </p>
      </div>
    </div>
  );
}
