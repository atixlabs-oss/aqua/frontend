import React from 'react';
import { needFormat } from './utils.js';
import ComplexTree from './complexTree.js';
import NormalTree from './normalTree.js';

export default function Tree(props) {
  const { type } = props;
  if (needFormat(type)) {
    return <ComplexTree {...props} />;
  }
  return <NormalTree {...props} />;
}
