import React from 'react';
import { getIndent } from './utils.js';

import Styles from './json-comparer.module.scss';
import classNames from 'classnames';

const REGEX_LINK = /^(https?:\/\/)?([\w-]+\.)+[\w-]+(\/[\w-.\/?%&=]*)?(\.[\w]{2,4})$/;

export default function NormalTree(props) {
  const {
    name,
    value,
    line,
    showIndex,
    type,
    lineType,
    needComma,
    level = 1,
    comparerType
  } = props;

  const valueWithoutQuotes = value?.replaceAll('"', '');
  const isLink = REGEX_LINK.test(valueWithoutQuotes);

  return (
    <p
      className={classNames(Styles['c-json-p'], Styles[`c-line-${lineType}`], {
        [Styles['--withoutLineDel']]: comparerType === 'new',
        [Styles['--withoutLineAdd']]: comparerType === 'old'
      })}
      style={getIndent(level)}
    >
      <span className={Styles['c-json-mark']}>{line}</span>
      <span className={Styles[`c-of-${lineType}`]}></span>
      <span className={Styles['c-json-content']}>
        {showIndex && <span className={Styles['c-json-key']}>{name}: </span>}
        {!isLink && <span className={Styles[`c-json-${type}`]}>{value}</span>}
        {isLink && (
          <a
            href={valueWithoutQuotes}
            className={Styles[`c-json-${type}`]}
            target="_blank"
            rel="noreferrer"
          >
            {value}
          </a>
        )}
        <span className={Styles['c-json-comma']}>{needComma ? ',' : ''}</span>
      </span>
    </p>
  );
}
