import { Icon } from 'antd';
import PropTypes from 'prop-types';

export const Caret = ({ visible, setVisible, className }) => {
  if (visible) {
    return <Icon type="caret-down" onClick={() => setVisible(!visible)} className={className} />;
  }
  if (!visible) {
    return <Icon type="caret-right" onClick={() => setVisible(!visible)} className={className} />;
  }
};

Caret.propTypes = {
  visible: PropTypes.bool,
  setVisible: PropTypes.func,
  className: PropTypes.string
};
