/**
 * AGUA Copyright (C) 2023  Atix Labs
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import React, { useContext } from 'react';
import { Breadcrumb } from 'antd';
import { CoaButton } from 'components/atoms/CoaButton/CoaButton';
import PropTypes from 'prop-types';
import { PROJECT_STATUS_ENUM } from 'model/projectStatus';
import { CLONE_STATUS_ENUM } from 'model/cloneStatus';
import TitlePage from 'components/atoms/TitlePage/TitlePage';
import { EDITOR_VARIANT, PROJECT_FORM_NAMES } from 'constants/constants';
import { UserContext } from 'components/utils/UserContext';
import { checkIsBeneficiaryOrInvestorByProject } from 'helpers/roles';
import { CoaChangelogContainer } from '../CoaChangelogContainer/CoaChangelogContainer';
import { useRouter } from 'next/router';
import Link from 'next/link';
import { CheckIcon } from 'components/atoms/CustomIcons/CheckIcon/CheckIcon';
import { useTranslation } from 'react-i18next';
import { CoaAlert } from 'components/molecules/CoaAlert/CoaAlert';
import { ALERT_VARIANTS_ENUM } from 'model/alertVariants';
import { CustomHead } from 'components/atoms/CustomHead/CustomHead';

const Items = ({ title, subtitle, onClick, completed, disabled }) => {
  const { t } = useTranslation();
  return (
    <div className="createProject__content__steps__step">
      <div className="createProject__content__steps__step__left">
        <CheckIcon checked={completed} />
        <div>
          <h3>{title}</h3>
          <h4>{subtitle}</h4>
        </div>
      </div>

      <CoaButton
        type="ghost"
        className="createProject__content__steps__step__button"
        onClick={onClick}
        disabled={disabled}
      >
        {t('general.btnEdit')}
      </CoaButton>
    </div>
  );
};

const CreateProject = ({
  project,
  setCurrentWizard,
  completedSteps,
  Footer,
  editorVariant,
  isACloneBeingEdited
}) => {
  const router = useRouter();
  const { t } = useTranslation();
  const { status, basicInformation, isCancelRequested } = project || {};
  const projectName = basicInformation?.projectName || t('createProject.myProject');
  const areAllStepsCompleted =
    completedSteps[PROJECT_FORM_NAMES.THUMBNAILS] &&
    completedSteps[PROJECT_FORM_NAMES.PROPOSAL] &&
    completedSteps[PROJECT_FORM_NAMES.MILESTONES] &&
    completedSteps[PROJECT_FORM_NAMES.DETAILS];

  const { user } = useContext(UserContext);
  const isAdmin = user?.isAdmin;

  const isBeneficiaryOrInvestor =
    project?.users && checkIsBeneficiaryOrInvestorByProject({ user, project });

  const EDITING_LOGIC = {
    [EDITOR_VARIANT.FIRST_EDITING]: {
      basicInformationButtonDisabled:
        status !== PROJECT_STATUS_ENUM.DRAFT && status !== PROJECT_STATUS_ENUM.IN_REVIEW,
      projectDetailsButtonDisabled:
        !completedSteps[PROJECT_FORM_NAMES.THUMBNAILS] ||
        (status !== PROJECT_STATUS_ENUM.DRAFT && status !== PROJECT_STATUS_ENUM.IN_REVIEW),
      projectUsersButtonDisabled:
        !completedSteps[PROJECT_FORM_NAMES.THUMBNAILS] ||
        (status !== PROJECT_STATUS_ENUM.DRAFT && status !== PROJECT_STATUS_ENUM.IN_REVIEW),
      milestonesButtonDisabled:
        !completedSteps[PROJECT_FORM_NAMES.THUMBNAILS] ||
        !completedSteps[PROJECT_FORM_NAMES.DETAILS] ||
        (status !== PROJECT_STATUS_ENUM.DRAFT && status !== PROJECT_STATUS_ENUM.IN_REVIEW)
    },
    [EDITOR_VARIANT.EDITING_CLONE]: {
      basicInformationButtonDisabled:
        !isBeneficiaryOrInvestor ||
        status !== PROJECT_STATUS_ENUM.OPEN_REVIEW ||
        isAdmin ||
        isCancelRequested,
      projectDetailsButtonDisabled:
        !isBeneficiaryOrInvestor ||
        status !== PROJECT_STATUS_ENUM.OPEN_REVIEW ||
        isAdmin ||
        isCancelRequested,
      projectUsersButtonDisabled:
        !isAdmin || (isAdmin && status !== PROJECT_STATUS_ENUM.OPEN_REVIEW) || isCancelRequested,
      milestonesButtonDisabled:
        !isBeneficiaryOrInvestor ||
        status !== PROJECT_STATUS_ENUM.OPEN_REVIEW ||
        isAdmin ||
        isCancelRequested
    }
  };

  return (
    <>
      <CustomHead titleText={`${t('general.BackOffice')} - ${t('createProject.title')}`} />
      <div className="createProject__content">
        <Breadcrumb>
          <Breadcrumb.Item>Dashboard</Breadcrumb.Item>
          <Breadcrumb.Item>
            <Link href="/">{t('createProject.title')}</Link>
          </Breadcrumb.Item>
        </Breadcrumb>
        <div style={{ display: 'flex', justifyContent: 'space-between' }}>
          <TitlePage textTitle={projectName} />
          <CoaButton
            type="primary"
            disabled={!areAllStepsCompleted || (isACloneBeingEdited && !isAdmin)}
            onClick={() =>
              [
                PROJECT_STATUS_ENUM.DRAFT,
                PROJECT_STATUS_ENUM.OPEN_REVIEW,
                PROJECT_STATUS_ENUM.IN_REVIEW
              ].includes(project?.status) ||
              [CLONE_STATUS_ENUM.IN_REVIEW, CLONE_STATUS_ENUM.PENDING_REVIEW].includes(
                project?.cloneStatus
              )
                ? router.push(`/${project?.id}?preview=true`)
                : router.push(`/${project?.parent || project?.id}`)
            }
          >
            {t('createProject.seePreview')}
          </CoaButton>
        </div>

        <CoaAlert
          show={isCancelRequested && isAdmin}
          variant={ALERT_VARIANTS_ENUM.CANCEL_REQUEST}
        />

        <div className="createProject__content__steps">
          <Items
            title={t('createProject.Basic information')}
            subtitle={t('createProject.HereYouCanFillBasicInformationOfTheProject')}
            onClick={() => setCurrentWizard(PROJECT_FORM_NAMES.THUMBNAILS)}
            completed={completedSteps[PROJECT_FORM_NAMES.THUMBNAILS]}
            disabled={EDITING_LOGIC?.[editorVariant]?.basicInformationButtonDisabled}
          />

          <Items
            title={t('createProject.Project details')}
            subtitle={t('createProject.completeInfo')}
            onClick={() => setCurrentWizard(PROJECT_FORM_NAMES.DETAILS)}
            completed={completedSteps[PROJECT_FORM_NAMES.DETAILS]}
            disabled={EDITING_LOGIC?.[editorVariant]?.projectDetailsButtonDisabled}
          />

          <Items
            title={t('createProject.Project users')}
            subtitle={t('createProject.createUserRoles')}
            onClick={() => setCurrentWizard(PROJECT_FORM_NAMES.PROPOSAL)}
            completed={completedSteps[PROJECT_FORM_NAMES.PROPOSAL]}
            disabled={EDITING_LOGIC?.[editorVariant]?.projectUsersButtonDisabled}
          />
          <Items
            title={t('createProject.Project milestones')}
            subtitle={t('createProject.uploadMilestone')}
            onClick={() => setCurrentWizard(PROJECT_FORM_NAMES.MILESTONES)}
            completed={completedSteps[PROJECT_FORM_NAMES.MILESTONES]}
            disabled={EDITING_LOGIC?.[editorVariant]?.milestonesButtonDisabled}
          />
        </div>
        {!!project.id &&
          ((isAdmin &&
            [CLONE_STATUS_ENUM.IN_REVIEW, CLONE_STATUS_ENUM.PENDING_REVIEW].includes(
              project?.cloneStatus
            )) ||
            (isBeneficiaryOrInvestor && isACloneBeingEdited)) && (
            <div className="createProject__content__changelog">
              <TitlePage
                textTitle={t('createProject.changelog')}
                className="createProject__content__changelog__title"
              />
              <CoaChangelogContainer
                title={t('createProject.projectChangelog')}
                projectId={project?.parent || project?.id}
                currency={project?.details?.currency}
                revisionId={project?.revision}
              />
            </div>
          )}
      </div>
      {Footer()}
    </>
  );
};

CreateProject.defaultProps = {
  project: {},
  completedSteps: {
    thumbnails: false,
    details: false,
    proposal: false,
    milestones: false
  },
  Footer: undefined,
  editorVariant: undefined,
  isACloneBeingEdited: undefined
};

CreateProject.propTypes = {
  setCurrentWizard: PropTypes.func.isRequired,
  project: PropTypes.shape({
    projectName: PropTypes.string.isRequired
  }),
  completedSteps: PropTypes.shape({
    thumnails: PropTypes.bool,
    details: PropTypes.bool,
    proposal: PropTypes.bool,
    milestones: PropTypes.bool
  }),
  Footer: PropTypes.oneOfType([PropTypes.arrayOf(PropTypes.node), PropTypes.node]),
  editorVariant: PropTypes.string,
  isACloneBeingEdited: PropTypes.bool
};

Items.defaultProps = {
  disabled: false,
  completed: false
};

Items.propTypes = {
  title: PropTypes.string.isRequired,
  subtitle: PropTypes.string.isRequired,
  onClick: PropTypes.func.isRequired,
  disabled: PropTypes.bool,
  completed: PropTypes.bool
};

export default CreateProject;
