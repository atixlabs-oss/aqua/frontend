/**
 * AGUA Copyright (C) 2023  Atix Labs
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import React from 'react';
import PropTypes from 'prop-types';
import Navbar from 'components/atoms/Navbar/Navbar';
import { Layout } from 'antd';
import SideBar from '../SideBar/SideBar';

const MainLayout = ({ children, user, project }) => (
  <Layout>
    <Navbar project={project} isProtectedRoute />
    <Layout className="mainContent">
      {user?.isAdmin && <SideBar role={user && user.role} />}
      <div className="mainContent__content">{children}</div>
    </Layout>
  </Layout>
);

MainLayout.defaultProps = {
  user: undefined
};

MainLayout.propTypes = {
  user: PropTypes.shape({}),
  children: PropTypes.element.isRequired
};

export default MainLayout;
