/**
 * AGUA Copyright (C) 2023  Atix Labs
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import React from 'react';
import { Spin } from 'antd';
import CoaModal from 'components/atoms/CoaModal/CoaModal';
import LogoWrapper from 'components/atoms/LogoWrapper';
import PropTypes from 'prop-types';
import { LoadingOutlined } from '@ant-design/icons';
import { useTranslation } from 'react-i18next';
import { CoaDialogModal } from '../CoaModals/CoaDialogModal/CoaDialogModal';
import TitlePage from 'components/atoms/TitlePage/TitlePage';

const antIcon = <LoadingOutlined style={{ fontSize: '100px' }} />;

export default function ModalSecretKeyLoading({ visible }) {
  const { t } = useTranslation();
  return (
    <CoaDialogModal
      withLogo
      visible={visible}
      maskClosable={false}
      closable={false}
      mask
      footer={null}
      title={
        <TitlePage
          centeredText
          textTitle={t('general.savingYourStatus')}
          underlinePosition="none"
        />
      }
    >
      <div className="o-ModalWrapper">
        <div className="o-ModalWrapper__SpinWrapper">
          <Spin className="o-ModalWrapper__Spin" indicator={antIcon} />
        </div>
      </div>
    </CoaDialogModal>
  );
}

ModalSecretKeyLoading.defaultProps = {
  visible: false
};

ModalSecretKeyLoading.propTypes = {
  visible: PropTypes.bool
};
