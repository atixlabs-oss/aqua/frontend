/**
 * AGUA Copyright (C) 2023  Atix Labs
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import React from 'react';
import PropTypes from 'prop-types';
import TitlePage from 'components/atoms/TitlePage/TitlePage';
import { CoaDialogModal } from '../CoaModals/CoaDialogModal/CoaDialogModal';

const ModalConfirmProjectPublish = ({
  visible,
  onSuccess,
  onCancel,
  okText,
  cancelText,
  ...rest
}) => (
  <CoaDialogModal
    {...rest}
    withLogo
    visible={visible}
    onCancel={onCancel}
    onSave={onSuccess}
    buttonsPosition="center"
    okText={okText}
    cancelText={cancelText}
    title={
      <TitlePage
        centeredText
        textTitle="Do you want to confirm the creation of the project?"
        underlinePosition="none"
      />
    }
    description="This action cannot be undone"
  />
);

ModalConfirmProjectPublish.defaultProps = {
  visible: false,
  onSuccess: () => undefined,
  onCancel: () => undefined,
  okText: 'Yes',
  cancelText: 'No'
};

ModalConfirmProjectPublish.propTypes = {
  visible: PropTypes.bool,
  onSuccess: PropTypes.func,
  onCancel: PropTypes.func,
  okText: PropTypes.string,
  cancelText: PropTypes.string
};

export default ModalConfirmProjectPublish;
