/**
 * AGUA Copyright (C) 2023  Atix Labs
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import React from 'react';
import { Icon } from 'antd';

import Styles from './menu-items.module.scss';
import { CustomTooltip } from 'components/atoms/CustomTooltip/CustomTooltip';
import { ProjectsIcon } from 'components/atoms/CustomIcons/ProjectsIcon';
import { BarsGraphIcon } from 'components/atoms/CustomIcons/BarsGraphIcon';
import { UserIcon } from 'components/atoms/CustomIcons/UserIcon';
import { UsersIcon } from 'components/atoms/CustomIcons/UsersIcon';

export const MENU_ITEMS_ADMIN = [
  {
    route: '/back-office/projects',
    key: '/back-office/projects',
    content: (
      <CustomTooltip title="Projects" placement="right" variant="white">
        <Icon component={ProjectsIcon} className={Styles.menuItem__icon} />
      </CustomTooltip>
    )
  },
  {
    route: '/back-office/users-administration',
    key: '/back-office/users-administration',
    content: (
      <CustomTooltip title="Users Administration" placement="right" variant="white">
        <Icon component={UsersIcon} className={Styles.menuItem__icon} />
      </CustomTooltip>
    )
  },
  {
    route: '/back-office/profile',
    key: '/back-office/profile',
    content: (
      <CustomTooltip title="Profile" placement="right" variant="white">
        <Icon component={UserIcon} className={Styles.menuItem__icon} />
      </CustomTooltip>
    )
  }
  /* {
    route: '/back-office/balance-and-account',
    key: '/back-office/balance-and-account',
    content: (
      <CustomTooltip title="Balance & Account information" placement="right" variant="white">
        <Icon component={BarsGraphIcon} className={Styles.menuItem__icon} />
      </CustomTooltip>
    )
  } */
];

export const MENU_ITEMS_REGULAR_USER = [
  {
    route: '/my-projects',
    key: '/my-projects',
    content: (
      <CustomTooltip title="Projects" placement="right" variant="white">
        <Icon component={ProjectsIcon} className={Styles.menuItem__icon} />
      </CustomTooltip>
    )
  },
  {
    route: '/back-office/profile',
    key: '/back-office/profile',
    content: (
      <CustomTooltip title="Profile" placement="right" variant="white">
        <Icon component={UserIcon} className={Styles.menuItem__icon} />
      </CustomTooltip>
    )
  }
];
