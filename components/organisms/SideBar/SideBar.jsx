/**
 * AGUA Copyright (C) 2023  Atix Labs
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import React from 'react';
import { Layout, Menu } from 'antd';
import { useRouter } from 'next/router';
import PropTypes from 'prop-types';

import Styles from './sidebar.module.scss';

import { MENU_ITEMS_ADMIN, MENU_ITEMS_REGULAR_USER } from './MenuItems';

const { Sider } = Layout;

const SideBar = ({ isAdmin }) => {
  const router = useRouter();
  const goToRoute = route => router.push(route);

  const menuItems = isAdmin ? MENU_ITEMS_ADMIN : MENU_ITEMS_REGULAR_USER;

  const basePath = menuItems.find(menuItem => router.asPath.includes(menuItem.key));

  return (
    <Sider width="60" collapsedWidth="0" className={Styles.sidebar}>
      <Menu
        inlineIndent={0}
        mode="inline"
        defaultSelectedKeys={basePath?.key}
        className={Styles.sidebar__content}
      >
        {menuItems.map(({ key, route, content }) => (
          <Menu.Item key={key} onClick={() => goToRoute(route)} className={Styles.menuItem}>
            {content}
          </Menu.Item>
        ))}
      </Menu>
    </Sider>
  );
};

SideBar.propTypes = {
  isAdmin: PropTypes.bool
};

export default SideBar;
