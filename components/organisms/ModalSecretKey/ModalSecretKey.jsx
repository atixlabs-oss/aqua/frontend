/**
 * AGUA Copyright (C) 2023  Atix Labs
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import React, { useEffect, useState } from 'react';
import { Typography, Divider, Form, Input, Checkbox } from 'antd';
import PropTypes from 'prop-types';
import jsPDF from 'jspdf';
import { RubikBold } from 'components/utils/Rubik-Bold';
import { RubikRegular } from 'components/utils/Rubik-Regular';
import { CoaButton } from 'components/atoms/CoaButton/CoaButton';
import { CoaDialogModal } from '../CoaModals/CoaDialogModal/CoaDialogModal';
import TitlePage from 'components/atoms/TitlePage/TitlePage';
import { CoaLink } from 'components/atoms/CoaLink/CoaLink';
import { useTranslation } from 'react-i18next';

function FormModalSecretKey({ form, modalOpen, onSuccess, setModalOpen }) {
  const { getFieldDecorator, getFieldProps, validateFields } = form;
  const [pin, setPin] = useState();
  const [doc, setDoc] = useState();
  const { t } = useTranslation();

  const generatePDF = content => {
    const doc = jsPDF();
    const imageUrl = process.env.NEXT_PUBLIC_LARGE_LOGO_289x38_PNG_PATH;

    const imgWidth = 80;
    const imgHeight = 10;
    const pageWidth = doc.internal.pageSize.width;

    const x = (pageWidth - imgWidth) / 2 + 5;

    doc.addImage(imageUrl, 'png', x, 10, imgWidth, imgHeight);

    doc.addFileToVFS('Rubik-Bold.ttf', RubikBold);
    doc.addFont('Rubik-Bold.ttf', 'Rubik', 'bold');
    doc.addFileToVFS('Rubik-Regular.ttf', RubikRegular);
    doc.addFont('Rubik-Regular.ttf', 'Rubik', 'normal');
    doc.setFont('Rubik', 'bold').setFontSize(18);
    doc.text(70, 40, t('secretKey.thisIsYourSecretKey')).setTextColor('#26385B');

    doc
      .setFont('Rubik', 'normal')
      .setFontSize(12)
      .setTextColor('#728099');
    doc.text(t('secretKey.theKeyIsUniqueAndYouWillNeedItToPerform'), 60, 50);
    doc.text(t('secretKey.differentActionsWithinThePlatform'), 70, 55);

    doc.setFont('Rubik', 'bold');
    doc.text(t('secretKey.alwaysRememberToKeepItInASafePlace'), 63, 60);

    doc.setFont('Rubik', 'normal');
    doc.setFontSize(36);
    doc.setTextColor(process.env.NEXT_PUBLIC_PRIMARY_COLOR || '#4C7FF7');
    doc.text(87, 80, content?.toString());

    return doc;
  };

  const validPin = (_rule, value, callback) => {
    if (value && parseInt(value, 10) !== pin) {
      callback('Secret key do not match');
    } else {
      callback();
    }
  };

  const submit = () => {
    validateFields(err => {
      if (!err) {
        return onSuccess(getFieldProps('pin').value);
      }
    });
  };

  useEffect(() => {
    const _pin =
      process.env.NEXT_PUBLIC_NODE_ENV === 'testing'
        ? 111111
        : Math.floor(100000 + Math.random() * 900000);
    setPin(_pin);
    setDoc(generatePDF(_pin));
  }, []);

  const saveSecretKey = () => {
    doc.save(`${process.env.NEXT_PUBLIC_ORGANIZATION_NAME} - secret key.pdf`);
  };

  return (
    <CoaDialogModal
      onCancel={() => setModalOpen(false)}
      visible={modalOpen}
      onSave={submit}
      buttonsPosition="center"
      withoutCancelButton
      okText={t('general.confirm')}
      title={
        <TitlePage
          textTitle={t('secretKey.setUpYourSecretKey')}
          underlinePosition="none"
          centeredText
        />
      }
      withLogo
    >
      <div style={{ textAlign: 'center' }}>
        <Typography.Paragraph>{t('secretKey.downloadSecretKey')}</Typography.Paragraph>
        <CoaButton
          icon="download"
          classNameIcon="iconDisplay"
          type="primary"
          onClick={saveSecretKey}
        >
          {t('general.downloadFile')}
        </CoaButton>
      </div>
      <Divider />
      <Typography.Paragraph>{t('secretKey.enterTheSecretKey')}</Typography.Paragraph>

      <Form>
        <Form.Item label={t('secretKey.secretKey')}>
          {getFieldDecorator('pin', {
            rules: [
              {
                required: true,
                message: t('secretKey.pleaseInputYourSecretKey')
              },
              {
                validator: validPin
              }
            ]
          })(<Input.Password placeholder={t('secretKey.enterYourSecretKey')} />)}
        </Form.Item>
        <Typography.Paragraph>
          {t('secretKey.pleaseReadCarefullyOurTermsAndConditionsInTheLinkBelow')}
        </Typography.Paragraph>
        <Form.Item>
          {getFieldDecorator('termsAndConditions', {
            valuePropName: 'checked',
            rules: [
              {
                message: t('secretKey.pleaseCheckTermsAndConditions'),
                validator: async (_, checked) => {
                  if (!checked) {
                    return Promise.reject(new Error(t('secretKey.pleaseCheckTermsAndConditions')));
                  }
                }
              }
            ]
          })(
            <Checkbox style={{ lineHeight: '20px' }}>
              {t('secretKey.iHaveReadAndAcceptThe')}{' '}
              <CoaLink href="/terms-and-conditions" target="_blank" rel="noopener noreferrer">
                {t('secretKey.termsAndConditionsOfOrganizationName', {
                  organizationName: process.env.NEXT_PUBLIC_ORGANIZATION_NAME
                })}
              </CoaLink>
            </Checkbox>
          )}
        </Form.Item>
      </Form>
    </CoaDialogModal>
  );
}

FormModalSecretKey.defaultProps = {
  form: null,
  modalOpen: false,
  onSuccess: () => undefined
};

FormModalSecretKey.propTypes = {
  form: PropTypes.element,
  modalOpen: PropTypes.bool,
  onSuccess: PropTypes.func
};

const ModalSecretKey = Form.create({ name: 'SecretKeyForm' })(FormModalSecretKey);
export default ModalSecretKey;
