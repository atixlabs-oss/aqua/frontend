/**
 * AGUA Copyright (C) 2023  Atix Labs
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import PropTypes from 'prop-types';

import TitlePage from 'components/atoms/TitlePage/TitlePage';
import Styles from './profile-helper-block.module.scss';

export const ProfileHelperBlock = ({ Icon, title, description }) => (
  <div className={Styles.helperBlockContainer}>
    <div className={Styles.helperBlockContainer__icon}>{Icon}</div>
    <TitlePage
      className={Styles.helperBlockContainer__title}
      textTitle={title}
      HtmlTag="h2"
      variant="primary"
      underlinePosition="none"
      weight="bolder"
      fontSize="medium-biggest"
    />
    <div className={Styles.helperBlockContainer__description}>{description}</div>
  </div>
);

ProfileHelperBlock.propTypes = {
  Icon: PropTypes.element,
  title: PropTypes.string,
  description: PropTypes.element
};
