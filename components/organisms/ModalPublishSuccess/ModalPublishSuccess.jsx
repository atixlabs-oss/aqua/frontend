/**
 * AGUA Copyright (C) 2023  Atix Labs
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import React from 'react';
import TitlePage from 'components/atoms/TitlePage/TitlePage';
import PropTypes from 'prop-types';
import { CoaDialogModal } from '../CoaModals/CoaDialogModal/CoaDialogModal';
import { useTranslation } from 'react-i18next';

const ModalPublishSuccess = ({ visible, onCancel, textTitle, description, onSave, children }) => {
  const { t } = useTranslation();
  return (
    <CoaDialogModal
      visible={visible}
      onCancel={onCancel}
      title={<TitlePage centeredText textTitle={textTitle} underlinePosition="none" />}
      withoutCancelButton
      withLogo
      okText={t('general.btnContinue')}
      buttonsPosition="center"
      onSave={onSave}
      description={description}
    >
      {children}
    </CoaDialogModal>
  );
};

ModalPublishSuccess.defaultProps = {
  visible: false,
  onCancel: () => undefined,
  textTitle: 'The project has been published',
  description: 'The project has been published successfully. you can see it from here.',
  onSave: undefined,
  children: undefined
};
ModalPublishSuccess.propTypes = {
  visible: PropTypes.bool,
  onCancel: PropTypes.func,
  textTitle: PropTypes.string,
  description: PropTypes.string,
  onSave: PropTypes.func,
  children: PropTypes.oneOfType([PropTypes.arrayOf(PropTypes.node), PropTypes.node])
};
export default ModalPublishSuccess;
