/**
 * AGUA Copyright (C) 2023  Atix Labs
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import React from 'react';
import PropTypes from 'prop-types';
import { WarningIcon } from '../../atoms/WarningIcon/WarningIcon';
import { useTranslation } from 'react-i18next';
import { CoaDialogModal } from '../CoaModals/CoaDialogModal/CoaDialogModal';
import TitlePage from 'components/atoms/TitlePage/TitlePage';
import Styles from './modal-not-compatible.module.scss';

const ModalMyProjects = ({ isVisible, onClick }) => {
  const { t } = useTranslation();
  return (
    <CoaDialogModal
      visible={isVisible}
      onOk={onClick}
      closable={false}
      withoutCancelButton
      okText={t('notCompatibleModal.continueAnyway')}
      onSave={onClick}
      buttonsPosition="center"
      title={
        <div className={Styles.titleContainer}>
          <WarningIcon className={Styles.titleContainer__icon} />
          <TitlePage
            centeredText
            textTitle={t('notCompatibleModal.warning')}
            underlinePosition="none"
          />
        </div>
      }
    >
      <p className={Styles.paragraph}>
        {t('notCompatibleModal.theCurrentVersionOfTheApplicationIsDesignedForDesktopDevicesOnly')}
      </p>
    </CoaDialogModal>
  );
};

export default ModalMyProjects;

ModalMyProjects.defaultProps = {
  isVisible: false
};
ModalMyProjects.propTypes = {
  isVisible: PropTypes.bool,
  onClick: PropTypes.func.isRequired
};
