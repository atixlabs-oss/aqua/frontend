/**
 * AGUA Copyright (C) 2023  Atix Labs
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import React, { useState, useContext, useCallback, useEffect } from 'react';
import PropTypes from 'prop-types';
import { Divider, message } from 'antd';
import { uuid } from 'uuidv4';
import classNames from 'classnames';
import { CoaTag } from 'components/atoms/CoaTag/CoaTag';
import { UserContext } from 'components/utils/UserContext';
import activityStatusMap, { ACTIVITY_STATUS_ENUM } from 'model/activityStatus';
import { checkIsActivityAuditor, checkIsBeneficiaryOrInvestorByProject } from 'helpers/roles';
import CoaRejectButton from 'components/atoms/CoaRejectButton/CoaRejectButton';
import CoaApproveButton from 'components/atoms/CoaApproveButton/CoaApproveButton';
import { CoaButton } from 'components/atoms/CoaButton/CoaButton';
import Loading from 'components/molecules/Loading/Loading';
import { sortArrayByDate } from 'components/utils';
import { getUsersByRole } from 'helpers/modules/projectUsers';
import { ActivityTypeText } from 'components/atoms/ActivityTypeText/ActivityTypeText';
import { ACTIVITY_TYPES_COLORS } from 'model/activityTypes';
import { CoaActivityIndicators } from 'components/molecules/CoaActivityIndicators/CoaActivityIndicators';
import { signMessage } from 'helpers/blockchain/wallet';
import { PROJECT_STATUS_ENUM } from 'model/projectStatus';
import EvidenceCard from '../../atoms/EvidenceCard/EvidenceCard';
import ModalConfirmWithSK from '../ModalConfirmWithSK/ModalConfirmWithSK';
import ModalPublishLoading from '../ModalPublishLoading/ModalPublishLoading';
import ModalEvidencesReviewSuccess from '../ModalEvidencesReviewSuccess/ModalEvidencesReviewSuccess';
import { canAddEvidences } from '../../../helpers/canAddEvidence';
import { AddEvidenceButton } from '../../atoms/AddEvidenceButton/AddEvidenceButton';
import { ROLES_IDS } from '../AssignProjectUsers/constants';
import { useRouter } from 'next/router';
import { DocumentIcon2 } from 'components/atoms/CustomIcons/DocumentIcon2/DocumentIcon2';
import { useTranslation } from 'react-i18next';
import { useGetProfileQuery } from 'api/react-query/users/usersQueries';
import { ADDRESS_STATUS_ENUM } from 'model/addressStatus';
import { CustomTooltip } from 'components/atoms/CustomTooltip/CustomTooltip';
import { openGenericErrorModal } from '../CoaModals/ErrorModals/errorModals';
import {
  useSignActivityMutation,
  useUpdateActivityStatusMutation
} from 'api/react-query/activities/activitiesMutations';

const initialSecretKeyModal = {
  visible: false,
  title: 'Secret Key',
  onSuccessAction: null,
  leaveAComment: false
};

const initialLoadingModal = {
  state: false,
  title: ''
};

const getAuditorName = (auditorId, project) => {
  const auditors = getUsersByRole(ROLES_IDS.auditor, project?.users);
  const auditor = auditors?.find(_auditor => _auditor?.id === auditorId);
  return `${auditor?.firstName} ${auditor?.lastName}`;
};

const Evidences = ({
  project,
  activity,
  evidences,
  areEvidencesLoading,
  getEvidences,
  getChangelog,
  preview
}) => {
  const router = useRouter();
  const activityId = activity?.id;
  const activityStatus = activity?.status;
  const activityStep = activity?.step;
  const activityType = activity?.type;
  const projectId = project?.id;
  const projectStatus = project?.status;
  const isProjectEditing = project?.editing;
  const [secretKeyModal, setSecretKeyModal] = useState(initialSecretKeyModal);
  const { t } = useTranslation();
  const { data: profileData } = useGetProfileQuery();
  const signActivityMutation = useSignActivityMutation();
  const updateActivityStatusMutation = useUpdateActivityStatusMutation();

  const pinStatus = profileData?.pinStatus;
  const isPinStatusApproved = pinStatus === ADDRESS_STATUS_ENUM.APPROVED;

  const [loadingModalVisible, setLoadingModalVisible] = useState(initialLoadingModal);
  const [reviewSuccessVisible, setReviewSuccessVisible] = useState(false);
  const { user } = useContext(UserContext);

  const isActivityAuditor = checkIsActivityAuditor({ user, activity });
  const isBeneficiaryOrInvestor = checkIsBeneficiaryOrInvestorByProject({ user, project });

  const mustSignBeneficiaryOrInvestor =
    activityStep === 1 &&
    isBeneficiaryOrInvestor &&
    activityStatus === ACTIVITY_STATUS_ENUM.PENDING_TO_REVIEW;

  const mustSignAuditor =
    activityStep === 1 &&
    isActivityAuditor &&
    [ACTIVITY_STATUS_ENUM.PENDING_TO_APPROVE, ACTIVITY_STATUS_ENUM.PENDING_TO_REJECT].includes(
      activityStatus
    );

  const sendToReview = useCallback(
    async (_pin, _password, wallet, key) => {
      try {
        setSecretKeyModal(initialSecretKeyModal);
        setLoadingModalVisible({
          state: true,
          title: t('modalPublishLoading.sent')
        });
        setSecretKeyModal(initialSecretKeyModal);
        const result = await updateActivityStatusMutation.mutateAsync({
          activityId,
          status: 'to-review',
          txId: `${uuid()}-mocked`
        });
        const messageToSign = result?.data?.toSign;
        await sign(_pin, _password, wallet, key, messageToSign);
        setReviewSuccessVisible(true);
        getEvidences(activityId);
        getChangelog();
      } catch (error) {
        message.error(t('activities.An error occurred while sending the activity to review'));
        openGenericErrorModal();
      } finally {
        setLoadingModalVisible({ ...loadingModalVisible, state: false });
      }
    },
    // eslint-disable-next-line react-hooks/exhaustive-deps
    [activityId, getChangelog, getEvidences, loadingModalVisible]
  );

  const rejectActivity = useCallback(
    async (_pin, _password, wallet, key) => {
      try {
        setSecretKeyModal(initialSecretKeyModal);
        setLoadingModalVisible({
          state: true,
          title: t('modalPublishLoading.rejected')
        });
        const result = await updateActivityStatusMutation.mutateAsync({
          activityId,
          status: 'rejected',
          txId: `${uuid()}-mocked`
        });
        const messageToSign = result?.data?.toSign;
        await sign(_pin, _password, wallet, key, messageToSign);
        getEvidences(activityId);
        getChangelog();
      } catch (error) {
        message.error(t('activities.An error occurred while rejecting the activity'));
        openGenericErrorModal();
      } finally {
        setLoadingModalVisible({ ...loadingModalVisible, state: false });
      }
    },
    // eslint-disable-next-line react-hooks/exhaustive-deps
    [getChangelog, activityId, getEvidences, loadingModalVisible]
  );

  const approveActivity = useCallback(
    async (_pin, _password, wallet, key) => {
      try {
        setSecretKeyModal(initialSecretKeyModal);
        setLoadingModalVisible({
          state: true,
          title: t('modalPublishLoading.approved')
        });
        const result = await updateActivityStatusMutation.mutateAsync({
          activityId,
          status: 'approved',
          txId: `${uuid()}-mocked`
        });
        const messageToSign = result?.data?.toSign;
        await sign(_pin, _password, wallet, key, messageToSign);
        getEvidences(activityId);
        getChangelog();
      } catch (error) {
        message.error(t('activities.An error occurred while approving the activity'));
        openGenericErrorModal();
      } finally {
        setLoadingModalVisible({ ...loadingModalVisible, state: false });
      }
    },
    // eslint-disable-next-line react-hooks/exhaustive-deps
    [activityId, getChangelog, getEvidences, loadingModalVisible]
  );

  const sign = useCallback(
    async (_pin, _password, wallet, key, messageToSign) => {
      try {
        const _messageToSign = messageToSign || activity?.toSign;
        const authorizationSignature = await signMessage(wallet, _messageToSign, key);
        await signActivityMutation.mutateAsync({ authorizationSignature, activityId });
      } catch {
        message.error(t('evidences.There was an error when trying to sign'));
      }
    },
    [activityId, getChangelog, getEvidences, loadingModalVisible, signActivityMutation]
  );

  const areReviewedEvidences =
    evidences.length > 0 &&
    evidences.every(({ status }) => ['approved', 'rejected'].includes(status));

  const isAbleToSeeEvidenceButtons = canAddEvidences({ user, project, activityType });
  const cantUploadEvidences =
    ![
      ACTIVITY_STATUS_ENUM.NEW,
      ACTIVITY_STATUS_ENUM.REJECTED,
      ACTIVITY_STATUS_ENUM.IN_PROGRESS
    ].includes(activityStatus) ||
    isProjectEditing ||
    ![PROJECT_STATUS_ENUM.PUBLISHED, PROJECT_STATUS_ENUM.IN_PROGRESS].includes(projectStatus);

  const auditorName = getAuditorName(activity?.auditor, project);

  const { budget, current } = activity || {};
  const currency = project?.details?.currency;

  const transferQuantity = evidences?.reduce(
    (curr, next) => (next?.type === 'transfer' ? curr + 1 : curr),
    0
  );
  const impactQuantity = evidences?.reduce(
    (curr, next) => (next?.type === 'impact' ? curr + 1 : curr),
    0
  );

  const handleAddEvidence = useCallback(
    e => {
      e.stopPropagation();
      router.push(`/${projectId}/activity/${activityId}/create-evidence`);
    },
    [activityId, router, projectId]
  );

  const handleSendForReview = useCallback(() => {
    setSecretKeyModal({
      visible: true,
      title: 'You are about to send an activity to be reviewed by an auditor',
      onSuccessAction: sendToReview,
      leaveAComment: false
    });
  }, [sendToReview]);

  useEffect(() => {
    if (mustSignBeneficiaryOrInvestor || mustSignAuditor)
      return setSecretKeyModal({
        visible: true,
        title: 'You are about to sign the activity to finish the process',
        description: 'To confirm the process enter your password and secret key',
        okText: 'Sign',
        onSuccessAction: sign,
        leaveAComment: false
      });
    if (
      activity?.step === 1 &&
      (isActivityAuditor &&
        [ACTIVITY_STATUS_ENUM.APPROVED, ACTIVITY_STATUS_ENUM.REJECTED].includes(activityStatus))
    )
      return setSecretKeyModal({
        visible: true,
        title: 'You are about to sign the activity to finish the process',
        description: 'To confirm the process enter your password and secret key',
        okText: 'Sign',
        onSuccessAction: sign,
        leaveAComment: false
      });
  }, [activity?.step, mustSignBeneficiaryOrInvestor, mustSignAuditor]);

  const handleRejectActivity = useCallback(() => {
    setSecretKeyModal({
      visible: true,
      title: 'Are you sure you want to reject the activity?',
      onSuccessAction: rejectActivity,
      leaveAComment: true
    });
  }, [rejectActivity]);

  const handleApproveActivity = useCallback(() => {
    setSecretKeyModal({
      visible: true,
      title: 'Are you sure you want to approve the activity?',
      onSuccessAction: approveActivity,
      leaveAComment: false
    });
  }, [approveActivity]);

  const handleCancelConfirmSk = useCallback(() => {
    setSecretKeyModal(initialSecretKeyModal);
  }, []);

  const handleCancelReviewSuccess = useCallback(() => {
    setReviewSuccessVisible(false);
  }, []);

  return (
    <>
      <div className="evidences">
        <Loading spinning={areEvidencesLoading}>
          <div className="evidences__list">
            <div className="evidences__list__header">
              <div>
                <h3 className="evidences__list__header__title">
                  <ActivityTypeText Component="span" activityType={activity?.type} />
                  {' - '}
                  {activity.title}
                </h3>
                <p className="evidences__list__header__subtitle">
                  {t('roles.auditor')}: {auditorName}
                </p>
              </div>
              <div className="evidences__list__header__right">
                {isAbleToSeeEvidenceButtons && (
                  <AddEvidenceButton
                    onClickAddEvidence={handleAddEvidence}
                    responsiveLayout={false}
                    disabled={cantUploadEvidences}
                  />
                )}
                <CoaTag predefinedColor={activityStatusMap?.[activityStatus]?.color}>
                  {activityStatusMap?.[activityStatus]?.name}
                </CoaTag>
              </div>
            </div>
            <Divider type="horizontal" />

            <div className="evidences__list__body">
              <CoaActivityIndicators
                {...{
                  currency,
                  budget,
                  current,
                  transferQuantity,
                  impactQuantity,
                  predefinedColor: ACTIVITY_TYPES_COLORS[activityType]
                }}
                withEvidences
              />

              <div className="evidences__list__body__info">
                <div>
                  <p className="evidences__indicatorTitle">{t('general.description')}</p>
                  <p className="evidences__indicatorValue">{activity?.description}</p>
                </div>
                <div>
                  <p className="evidences__indicatorTitle">{t('general.acceptanceCriteria')}</p>
                  <p className="evidences__indicatorValue">{activity?.acceptanceCriteria}</p>
                </div>
              </div>

              {evidences?.length > 0 && <Divider type="horizontal" />}

              {!areEvidencesLoading && (
                <div
                  className={classNames('evidences__list__body__evidencesContainer', {
                    '--empty': evidences.length === 0
                  })}
                >
                  {sortArrayByDate(evidences, 'createdAt').map((evidence, index) => (
                    <EvidenceCard
                      activityStatus={activity?.status}
                      activityType={activity?.type}
                      key={evidence.id}
                      evidenceNumber={evidences.length - index}
                      evidence={evidence}
                      currency={project?.details?.currency}
                      isActivityAuditor={isActivityAuditor}
                      preview={preview}
                      withDeleteButton={isAbleToSeeEvidenceButtons}
                    />
                  ))}
                  {evidences?.length === 0 && (
                    <>
                      <DocumentIcon2 />
                      <p className="evidences__list__body__evidencesContainer__emptyText">
                        {t('evidences.emptyEvidences')}
                      </p>
                    </>
                  )}
                </div>
              )}
            </div>

            {(isBeneficiaryOrInvestor ||
              (isActivityAuditor && activityStatus === ACTIVITY_STATUS_ENUM.TO_REVIEW)) && (
              <Divider type="horizontal" />
            )}

            <div className="evidences__list__footer">
              {isAbleToSeeEvidenceButtons && (
                <CustomTooltip title={isPinStatusApproved && t('pin.actionDisabledPinNotSet')}>
                  <CoaButton
                    type="primary"
                    disabled={
                      evidences?.length === 0 ||
                      [
                        ACTIVITY_STATUS_ENUM.TO_REVIEW,
                        ACTIVITY_STATUS_ENUM.APPROVED,
                        ACTIVITY_STATUS_ENUM.REJECTED,
                        ACTIVITY_STATUS_ENUM.PENDING_TO_REVIEW,
                        ACTIVITY_STATUS_ENUM.PENDING_TO_APPROVE,
                        ACTIVITY_STATUS_ENUM.PENDING_TO_REJECT
                      ].includes(activityStatus) ||
                      isProjectEditing ||
                      isPinStatusApproved
                    }
                    onClick={handleSendForReview}
                  >
                    {t('evidences.btnSendReview')}
                  </CoaButton>
                </CustomTooltip>
              )}
              {isActivityAuditor && activityStatus === ACTIVITY_STATUS_ENUM.TO_REVIEW && (
                <>
                  <CustomTooltip title={isPinStatusApproved && t('pin.actionDisabledPinNotSet')}>
                    <CoaRejectButton
                      disabled={
                        !areReviewedEvidences ||
                        isProjectEditing ||
                        activityStep === 1 ||
                        isPinStatusApproved
                      }
                      onClick={handleRejectActivity}
                    >
                      Reject
                    </CoaRejectButton>
                  </CustomTooltip>
                  <CustomTooltip title={isPinStatusApproved && t('pin.actionDisabledPinNotSet')}>
                    <CoaApproveButton
                      disabled={
                        !areReviewedEvidences ||
                        isProjectEditing ||
                        activityStep === 1 ||
                        isPinStatusApproved
                      }
                      onClick={handleApproveActivity}
                    >
                      Approve
                    </CoaApproveButton>
                  </CustomTooltip>
                </>
              )}
            </div>
          </div>
        </Loading>
      </div>
      {user && (
        <>
          <ModalConfirmWithSK
            visible={secretKeyModal.visible}
            title={secretKeyModal.title}
            onCancel={handleCancelConfirmSk}
            onSuccess={secretKeyModal.onSuccessAction}
            leaveAComment={secretKeyModal.leaveAComment}
            okText="Confirm"
            cancelText="Cancel"
          />
          <ModalPublishLoading
            visible={loadingModalVisible.state}
            textTitle={loadingModalVisible.title}
          />
          <ModalEvidencesReviewSuccess
            visible={reviewSuccessVisible}
            onCancel={handleCancelReviewSuccess}
          />
        </>
      )}
    </>
  );
};

Evidences.defaultProps = {
  project: undefined,
  activity: undefined,
  evidences: undefined,
  areEvidencesLoading: undefined,
  getEvidences: undefined,
  getChangelog: undefined
};

Evidences.propTypes = {
  project: PropTypes.objectOf(PropTypes.any),
  activity: PropTypes.objectOf(PropTypes.any),
  evidences: PropTypes.objectOf(PropTypes.any),
  areEvidencesLoading: PropTypes.bool,
  getEvidences: PropTypes.func,
  getChangelog: PropTypes.func
};

export default Evidences;
