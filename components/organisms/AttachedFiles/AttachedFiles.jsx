/**
 * AGUA Copyright (C) 2023  Atix Labs
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import React, { useState } from 'react';
import { lookup } from 'mime-types';
import { PaperClipOutlined } from '@ant-design/icons';
import { CoaTextButton } from 'components/atoms/CoaTextButton/CoaTextButton';
import { Icon } from 'antd';
import { useTranslation } from 'react-i18next';

const getImageBlob = url =>
  new Promise(async resolve => {
    const response = await fetch(url);
    const blob = response.blob();
    resolve(blob);
  });
// convert a blob to base64
const blobToBase64 = blob =>
  new Promise(resolve => {
    const reader = new FileReader();
    reader.onload = () => {
      const dataUrl = reader.result;
      resolve(dataUrl);
    };
    reader.readAsDataURL(blob);
  });

const getBase64File = async url => {
  const blob = await getImageBlob(url);
  const base64 = await blobToBase64(blob);
  return base64;
};

const downloadFile = async _file => {
  const _url = await getBase64File(`${process.env.NEXT_PUBLIC_URL_HOST}${_file.path}`);
  const a = document.createElement('a');
  a.href = _url;
  a.download = _file.name;
  document.body.appendChild(a);
  a.click();
  document.body.removeChild(a);
};

const AttachedFile = ({ file }) => {
  const { t } = useTranslation();
  const [isLoading, setIsLoading] = useState(false);

  const handleDownload = async () => {
    setIsLoading(true);
    await downloadFile(file);
    setIsLoading(false);
  };

  return (
    <div className="attached-files__file">
      {lookup(file.name).includes('image') && (
        <>
          <img src={`${process.env.NEXT_PUBLIC_URL_HOST}${file.path}`} alt={file.name} />
        </>
      )}
      {lookup(file.name).includes('pdf') && (
        <iframe
          title={file.name}
          src={`${process.env.NEXT_PUBLIC_URL_HOST}${file.path}`}
          width="100%"
          height="100%"
        ></iframe>
      )}
      <div className="attached-files__download">
        <div className="attached-files__download__file-left">
          <PaperClipOutlined />
          <h4 className="attached-files__download__file-title">{file.name}</h4>
        </div>
        <CoaTextButton
          variant="primary"
          onClick={handleDownload}
          loading={isLoading}
          className="attached-files__download__file-download"
        >
          {t('general.btnDownload')}
        </CoaTextButton>
      </div>
    </div>
  );
};

export default function AttachedFiles({ files }) {
  const { t } = useTranslation();
  const [isLoading, setIsLoading] = useState(false);

  const handleDownloadAll = async () => {
    setIsLoading(true);
    const promiseArray = files.map(async file => {
      await downloadFile(file);
    });

    await Promise.all(promiseArray);
    setIsLoading(false);
  };
  return (
    <div className="attached-files">
      <div className="attached-files__header">
        <div className="attached-files__header__titleContainer">
          <PaperClipOutlined />
          <h2 className="attached-files__header__title">{t('attachedFiles.title')}</h2>
        </div>
        <CoaTextButton
          variant="primary"
          onClick={handleDownloadAll}
          loading={isLoading}
          className="attached-files__header__download"
        >
          <Icon type="download" />
          Download All
        </CoaTextButton>
      </div>

      {files && (
        <div className="attached-files__filesContainer">
          {files.map(file => (
            <AttachedFile file={file} key={file.name} />
          ))}
        </div>
      )}
    </div>
  );
}
