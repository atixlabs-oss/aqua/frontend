/**
 * AGUA Copyright (C) 2023  Atix Labs
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import React, { useContext } from 'react';
import PropTypes from 'prop-types';
import { Icon } from 'antd';
import { UserContext } from 'components/utils/UserContext';
import { ProjectDetailsIcon } from 'components/atoms/CustomIcons/ProjectDetailsIcon';
import { MilestonesIcon } from 'components/atoms/CustomIcons/MilestonesIcon';
import { BlockchainIcon } from 'components/atoms/CustomIcons/BlockchainIcon';
import { CashFlowIcon } from 'components/atoms/CustomIcons/CashFlowIcon';
import { CoaButton } from 'components/atoms/CoaButton/CoaButton';
import { ProjectScope } from 'components/molecules/ProjectScope/ProjectScope';
import { ProjectStatement } from 'components/molecules/ProjectStatement/ProjectStatement';
import { CoaProjectMembersCard } from 'components/molecules/CoaProjectMembersCard/CoaProjectMembersCard';
import { getUsersByRole } from 'helpers/modules/projectUsers';
import TitlePage from 'components/atoms/TitlePage/TitlePage';
import { scrollToLandingElement } from 'components/utils';
import { PROJECT_STATUS_ENUM } from 'model/projectStatus';
import { MILESTONE_STATUS_ENUM } from 'model/milestoneStatus';
import { ACTIVITY_STATUS_ENUM } from 'model/activityStatus';
import { PROJECT_TYPE_ENUM } from 'model/projectType';
import CashFlow from 'components/molecules/CashFlow/CashFlow';
import Loading from '../../molecules/Loading/Loading';
import { ProjectInfoSection } from '../ProjectInfoSection/ProjectInfoSection';
import { CoaMilestoneItem } from '../CoaMilestones/CoaMilestoneItem/CoaMilestoneItem';
import { ROLES_IDS } from '../AssignProjectUsers/constants';
import { checkIsBeneficiaryOrInvestorByProject } from '../../../helpers/roles';
import { CoaChangelogContainer } from '../CoaChangelogContainer/CoaChangelogContainer';
import { useTranslation } from 'react-i18next';
import { UiContext } from 'components/utils/UiContext';
import { CoaAlert } from 'components/molecules/CoaAlert/CoaAlert';
import { ALERT_VARIANTS_ENUM } from 'model/alertVariants';
import { RequestChangesButton } from 'components/atoms/RequestChangesButton/RequestChangesButton';
import { ADDRESS_STATUS_ENUM } from 'model/addressStatus';
import { useGetProfileQuery } from 'api/react-query/users/usersQueries';

const PreviewProject = ({ id, preview, project, loading, milestones, isAdmin, setMilestones }) => {
  const { user } = useContext(UserContext);
  const { t } = useTranslation();
  const { isHeaderBig } = useContext(UiContext);
  const { data: profileData } = useGetProfileQuery();
  const pinStatus = profileData?.pinStatus;
  const isPinStatusApproved = pinStatus === ADDRESS_STATUS_ENUM.APPROVED;

  const {
    status,
    details,
    users,
    editing,
    step: projectStep,
    type: projectType,
    parent,
    editProposer,
    id: projectId,
    cloneStatus
  } = project || {};

  const {
    currency,
    currencyType,
    problemAddressed,
    mission,
    additionalCurrencyInformation,
    status: statusDetails = {}
  } = details || {};

  const beneficiaryUser = getUsersByRole(ROLES_IDS.beneficiary, users)?.map(usr => ({
    ...usr,
    rol: t('roles.beneficiary')
  }))[0];
  const investorUser = getUsersByRole(ROLES_IDS.investor, users)?.map(usr => ({
    ...usr,
    rol: t('roles.investor')
  }))[0];
  const auditorsUsers =
    getUsersByRole(ROLES_IDS.auditor, users)?.map(usr => ({
      ...usr,
      rol: t('roles.auditor')
    })) || [];
  const members = [beneficiaryUser, investorUser, ...auditorsUsers];

  const isLoanProject = projectType === PROJECT_TYPE_ENUM.LOAN;

  const toggleAreActivitiesOpened = milestoneId => {
    const _milestones = [...milestones];
    const milestoneFound = _milestones.find(milestone => milestone?.id === milestoneId);
    milestoneFound.areActivitiesOpen = !milestoneFound.areActivitiesOpen;
    setMilestones(_milestones);
  };

  const approvedMilestones = milestones?.filter(
    milestone => milestone?.status === MILESTONE_STATUS_ENUM.APPROVED
  );
  const totalActivitiesQuantity = milestones?.reduce(
    (curr, next) => curr + next?.activities?.length,
    0
  );

  const milestonesProgressPercentage = approvedMilestones?.reduce((curr, milestone) => {
    const approvedActivitiesQuantity = milestone?.activities?.filter(
      activities => activities?.status === ACTIVITY_STATUS_ENUM.APPROVED
    ).length;
    const weightedValues = (approvedActivitiesQuantity / totalActivitiesQuantity) * 100;
    return curr + weightedValues;
  }, 0);

  const activityProgressPercentage =
    ((milestones
      ?.flatMap(({ activities }) => activities)
      ?.reduce((acc, act) => (act.status === ACTIVITY_STATUS_ENUM.APPROVED ? acc + 1 : acc), 0) ||
      0) /
      totalActivitiesQuantity) *
    100;

  const isBeneficiaryOrInvestor = checkIsBeneficiaryOrInvestorByProject({ user, project });
  const isPublishedOrInProgressProject = [
    PROJECT_STATUS_ENUM.PUBLISHED,
    PROJECT_STATUS_ENUM.IN_PROGRESS
  ].includes(status);

  if (loading) return <Loading />;

  return (
    <div className="o-previewProject__content">
      <CoaAlert
        show={[PROJECT_STATUS_ENUM.IN_REVIEW, PROJECT_STATUS_ENUM.PENDING_TO_REVIEW].includes(
          status
        )}
        variant={ALERT_VARIANTS_ENUM.PROJECT_BEING_EDITED}
        className="o-previewProject__alert"
      />
      <div className="o-previewProject__buttons__container">
        <div className="o-previewProject__buttons">
          <CoaButton
            type="secondary"
            shape="round"
            className="o-previewProject__buttons__button"
            onClick={() => scrollToLandingElement('project-progress', isHeaderBig)}
          >
            <ProjectDetailsIcon /> {t('general.projectProgress')}
          </CoaButton>
          <CoaButton
            type="secondary"
            shape="round"
            className="o-previewProject__buttons__button"
            onClick={() => scrollToLandingElement('milestones', isHeaderBig)}
          >
            <MilestonesIcon /> {t('landingSubheader.btnMilestones')}
          </CoaButton>
          <CoaButton
            type="secondary"
            shape="round"
            className="o-previewProject__buttons__button"
            onClick={() => scrollToLandingElement('Cashflow', isHeaderBig)}
          >
            <CashFlowIcon /> {t('landingSubheader.btnCashFlow')}
          </CoaButton>

          <CoaButton
            type="secondary"
            shape="round"
            className="o-previewProject__buttons__button"
            onClick={() => scrollToLandingElement('blockchain-changelog', isHeaderBig)}
          >
            <BlockchainIcon /> {t('landingSubheader.btnChangelog')}
          </CoaButton>

          <CoaButton
            type="secondary"
            shape="round"
            className="o-previewProject__buttons__button"
            href={`/audit/${parent ?? projectId}`}
            target="_blank"
          >
            <Icon type="eye" />
            {t('landingSubheader.btnAuditProject')}
          </CoaButton>
        </div>
        {(isBeneficiaryOrInvestor || isAdmin) &&
          (isPublishedOrInProgressProject ||
            (status === PROJECT_STATUS_ENUM.IN_REVIEW && projectStep === 1)) && (
            <RequestChangesButton
              project={project}
              milestones={milestones}
              cloneStatus={cloneStatus}
              isPinStatusApproved={isPinStatusApproved}
              editProposer={editProposer}
              editing={editing}
              projectStatus={status}
            />
          )}
      </div>
      <div className="o-previewProject__infoSection">
        <ProjectInfoSection
          mission={mission}
          about={problemAddressed}
          currency={currency}
          onClickSeeMilestones={() => scrollToLandingElement('milestones', isHeaderBig)}
          projectType={projectType}
          accountInfo={additionalCurrencyInformation}
          currencyType={currencyType}
          externalProgressCurrentValue={statusDetails?.milestones?.completed}
          externalProgressTotalValue={
            statusDetails?.milestones?.completed + statusDetails?.milestones?.incompleted
          }
          internalProgressCurrentValue={statusDetails?.activities?.completed}
          internalProgressTotalValue={
            statusDetails?.activities?.completed + statusDetails?.activities?.incompleted
          }
        />
      </div>
      <div className="o-previewProject__members">
        <TitlePage
          underlinePosition="none"
          textTitle={t('landingProjectMembers.title')}
          className="o-previewProject__title"
        />
        <div className="o-previewProject__members__container">
          {members.map(member => (
            <CoaProjectMembersCard key={member?.id} {...member} />
          ))}
        </div>
      </div>
      <div className="o-previewProject__progressSection" id="project-progress">
        <TitlePage
          underlinePosition="none"
          textTitle={t('general.projectProgress')}
          className="o-previewProject__title"
        />
        <div
          className="o-previewProject__progressSection__cards"
          data-testid="project-progress-cards"
        >
          <ProjectScope
            milestonesProgressPercentage={milestonesProgressPercentage}
            activityProgressPercentage={activityProgressPercentage}
          />
          <ProjectStatement
            showPayback={isLoanProject}
            budget={parseFloat(statusDetails.budget)}
            funding={parseFloat(statusDetails.funding)}
            spending={parseFloat(statusDetails.spending)}
            payback={parseFloat(statusDetails.payback || 0)}
            currency={currency}
          />
        </div>
      </div>
      <div className="o-previewProject__milestonesSection" id="milestones">
        <TitlePage
          underlinePosition="none"
          textTitle={t('landingMilestones.title')}
          className="o-previewProject__title"
        />
        <div className="o-previewProject__milestonesSection__milestones">
          {milestones.map((milestone, index) => (
            <CoaMilestoneItem
              data-testid="milestone-item"
              key={milestone?.id}
              withAddEvidenceButton
              projectType={projectType}
              preview={preview}
              isProjectEditing={editing}
              project={project}
              projectId={id}
              withEvidences
              withStatusTag
              toggleAreActivitiesOpened={toggleAreActivitiesOpened}
              {...{
                currency,
                milestone
              }}
              {...{ milestoneNumber: index + 1 }}
            />
          ))}
        </div>
      </div>
      <section className="o-previewProject__cashFlowSection" id="Cashflow">
        <TitlePage
          underlinePosition="none"
          textTitle={t('general.cashFlow')}
          className="o-previewProject__title"
        />
        <CashFlow projectId={id} currency={currency} />
      </section>
      <div className="o-previewProject__changelogSection" id="blockchain-changelog">
        <TitlePage
          underlinePosition="none"
          textTitle={t('general.blockchainChangelog')}
          className="o-previewProject__title"
          id="changelogTitle"
        />
        <CoaChangelogContainer
          withViewMoreDetailsLink
          title={t('changelog.title')}
          projectId={parent || id}
          currency={currency}
        />
      </div>
    </div>
  );
};

PreviewProject.propTypes = {
  id: PropTypes.string,
  preview: PropTypes.bool
};

PreviewProject.defaultProps = {
  id: undefined,
  preview: false
};

export default PreviewProject;
