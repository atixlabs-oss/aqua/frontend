/**
 * AGUA Copyright (C) 2023  Atix Labs
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import React, { useState, useEffect } from 'react';
import PropTypes from 'prop-types';
import { Icon, message } from 'antd';

import { ROLES_IDS } from 'components/organisms/AssignProjectUsers/constants';
import { CoaButton } from 'components/atoms/CoaButton/CoaButton';
import TitlePage from 'components/atoms/TitlePage/TitlePage';
import { CoaFormMilestoneModal } from 'components/organisms/CoaMilestones/CoaFormMilestoneModal/CoaFormMilestoneModal';
import { CoaFormActivitiesModal } from 'components/organisms/CoaActivities/CoaFormActivitiesModal/CoaFormActivitiesModal';
import { getUsersByRole } from 'helpers/modules/projectUsers';
import { ACTIVITY_STATUS_ENUM } from 'model/activityStatus';
import { MILESTONE_STATUS_ENUM } from 'model/milestoneStatus';
import { CoaMilestoneItem } from '../CoaMilestoneItem/CoaMilestoneItem';
import { MilestoneIcon2 } from 'components/atoms/CustomIcons/MilestoneIcon2/MilestoneIcon2';
import { useTranslation } from 'react-i18next';
import { CoaConfirmDeleteModal } from 'components/organisms/CoaModals/ConfirmModals/confirmModals';
import { CustomHead } from 'components/atoms/CustomHead/CustomHead';
import {
  useDeleteActivityMutation,
  useUpdateActivityMutation
} from 'api/react-query/activities/activitiesMutations';
import {
  useCreateMilestoneActivityMutation,
  useCreateMilestoneMutation,
  useDeleteMilestoneMutation,
  useUpdateMilestoneMutation
} from 'api/react-query/milestones/milestonesMutations';

export const CoaMilestonesView = ({ project, Footer, isACloneBeingEdited }) => {
  const { t } = useTranslation();
  const updateActivityMutation = useUpdateActivityMutation();
  const updateMilestoneMutation = useUpdateMilestoneMutation();
  const deleteMilestoneMutation = useDeleteMilestoneMutation();
  const createMilestoneMutation = useCreateMilestoneMutation();
  const createMilestoneActivityMutation = useCreateMilestoneActivityMutation();
  const deleteActivityMutation = useDeleteActivityMutation();

  const projectId = project?.id;
  const currency = project?.details?.currency || '';
  const auditors = getUsersByRole(ROLES_IDS.auditor, project?.users);
  const auditorsProcessed = auditors?.map(auditor => ({
    label: `${auditor?.firstName} ${auditor?.lastName}`,
    value: auditor?.id
  }));

  const initialMilestones = project?.milestones;

  const [isFormMilestoneModalOpen, setIsFormMilestoneModalOpen] = useState(false);
  const [isFormActivityModalOpen, setIsFormActivityModalOpen] = useState(false);

  const [milestones, setMilestones] = useState([]);
  const [currentEditedMilestone, setCurrentEditedMilestone] = useState();
  const [currentEditedActivity, setCurrentEditedActivity] = useState();

  useEffect(() => {
    setMilestones(initialMilestones);
  }, [initialMilestones]);

  const handleCreateMilestone = async newMilestone => {
    try {
      const { data } = await createMilestoneMutation.mutateAsync({
        projectId,
        data: newMilestone
      });

      setMilestones([
        ...milestones,
        {
          ...newMilestone,
          id: data?.milestoneId,
          activities: [],
          budget: '0',
          areActivitiesOpen: false,
          status: MILESTONE_STATUS_ENUM.NEW,
          funding: {
            budget: 0,
            current: 0
          },
          spending: {
            budget: 0,
            current: 0
          },
          payback: {
            budget: 0,
            current: 0
          }
        }
      ]);
      message.success(t('createProject.milestoneCreated'));
    } catch {}
  };

  const handleRemoveMilestone = async milestoneId => {
    try {
      await deleteMilestoneMutation.mutateAsync({ milestoneId });
      const _milestones = milestones.filter(milestone => milestone?.id !== milestoneId);
      setMilestones(_milestones);
      message.success(t('createProject.milestoneRemoved'));
    } catch {}
  };

  const handleOpenConfirmDeleteMilestoneModal = milestoneId =>
    CoaConfirmDeleteModal({
      onOk: () => handleRemoveMilestone(milestoneId),
      title: t('createProject.deleteMilestoneQuestion'),
      subtitle: t('createProject.deleteMilestoneQuestionSubtitle')
    });

  const handleUpdateMilestone = async updatedMilestone => {
    try {
      await updateMilestoneMutation.mutateAsync({
        milestoneId: currentEditedMilestone?.id,
        data: updatedMilestone
      });

      const _milestones = [...milestones];
      const milestoneToUpdateIndex = _milestones.findIndex(
        milestone => milestone?.id === currentEditedMilestone?.id
      );
      _milestones[milestoneToUpdateIndex] = {
        ..._milestones[milestoneToUpdateIndex],
        ...updatedMilestone
      };
      setMilestones(_milestones);
      setCurrentEditedMilestone(undefined);
      message.success(t('createProject.milestoneUpdated'));
    } catch {}
  };

  const handleOpenEditMilestone = milestone => {
    setIsFormMilestoneModalOpen(true);
    setCurrentEditedMilestone(milestone);
  };

  const handleOpenEditActivity = ({ activity, milestone }) => {
    setIsFormActivityModalOpen(true);
    setCurrentEditedMilestone(milestone);
    setCurrentEditedActivity(activity);
  };

  const handleCreateActivity = async newActivity => {
    try {
      const processedActivity = { ...newActivity, budget: newActivity?.budget || 0 };

      const { data } = await createMilestoneActivityMutation.mutateAsync({
        activity: processedActivity,
        milestoneId: currentEditedMilestone?.id
      });

      const _milestones = [...milestones];
      const milestoneToUpdateIndex = _milestones.findIndex(
        milestone => milestone?.id === currentEditedMilestone?.id
      );
      const milestoneFound = _milestones[milestoneToUpdateIndex];
      const activities = milestoneFound?.activities;

      activities.push({
        ...processedActivity,
        id: data?.activityId,
        auditor: { id: processedActivity?.auditor },
        status: ACTIVITY_STATUS_ENUM.NEW,
        current: 0
      });

      const activitiesByType = activities?.filter(
        activity => activity?.type === processedActivity?.type
      );

      const totalBudget = activitiesByType.reduce(
        (curr, next) => parseFloat(curr) + parseFloat(next?.budget),
        0
      );
      if (milestoneFound[(processedActivity?.type)]) {
        milestoneFound[(processedActivity?.type)].budget = totalBudget;
      }

      milestoneFound.areActivitiesOpen = true;

      setMilestones([..._milestones]);
      message.success(t('createProject.activityCreated'));
    } catch {}
  };

  const handleRemoveActivity = async ({ activityId, milestone }) => {
    try {
      await deleteActivityMutation.mutateAsync({ activityId });

      const _milestones = [...milestones];
      const milestoneOwnerIndex = _milestones.findIndex(
        _milestone => _milestone?.id === milestone?.id
      );
      const milestoneFound = _milestones[milestoneOwnerIndex];

      const activityFound = milestoneFound?.activities?.find(
        activity => activity?.id === activityId
      );
      const activityType = activityFound?.type;

      milestoneFound.activities = milestoneFound?.activities?.filter(
        activity => activity?.id !== activityId
      );

      const activitiesByType = milestoneFound?.activities?.filter(
        activity => activity?.type === activityType
      );

      const totalBudget = activitiesByType.reduce(
        (curr, next) => parseFloat(curr) + parseFloat(next?.budget),
        0
      );

      if (milestoneFound[activityType]) milestoneFound[activityType].budget = totalBudget;

      setMilestones(_milestones);
      message.success(t('createProject.activityDeleted'));
    } catch {}
  };

  const handleOpenConfirmDeleteActivityModal = ({ activityId, milestone }) =>
    CoaConfirmDeleteModal({
      onOk: () => handleRemoveActivity({ activityId, milestone }),
      title: t('createProject.deleteActivityQuestion'),
      subtitle: t('createProject.deleteActivityQuestionSubtitle')
    });

  const handleEditActivity = async updatedActivity => {
    try {
      await updateActivityMutation.mutateAsync({
        activityId: currentEditedActivity?.id,
        updatedActivity
      });

      const _milestones = [...milestones];
      const milestoneOwnerIndex = _milestones.findIndex(
        _milestone => _milestone?.id === currentEditedMilestone?.id
      );
      const milestoneFound = _milestones[milestoneOwnerIndex];
      const activities = milestoneFound?.activities;
      const activityToUpdateIndex = activities.findIndex(
        activity => activity?.id === currentEditedActivity?.id
      );

      const oldType = activities[activityToUpdateIndex]?.type;

      activities[activityToUpdateIndex] = {
        ...activities[activityToUpdateIndex],
        ...updatedActivity,
        auditor: {
          id: updatedActivity.auditor
        }
      };

      if (
        updatedActivity?.budget &&
        parseFloat(currentEditedActivity.budget) !== parseFloat(updatedActivity?.budget)
      ) {
        const activitiesByType = activities?.filter(
          activity => activity?.type === updatedActivity?.type
        );

        const totalBudget = activitiesByType.reduce(
          (curr, next) => parseFloat(curr) + parseFloat(next?.budget),
          0
        );
        if (milestoneFound[oldType]) milestoneFound[(updatedActivity?.type)].budget = totalBudget;

        // recalculate the changed activity
        if (updatedActivity?.type !== oldType) {
          const _activitiesByType = activities?.filter(activity => activity?.type === oldType);

          const _totalBudget = _activitiesByType.reduce(
            (curr, next) => parseFloat(curr) + parseFloat(next?.budget),
            0
          );
          if (milestoneFound[oldType]) milestoneFound[oldType].budget = _totalBudget;
        }
      }

      setMilestones(_milestones);
      setCurrentEditedActivity(undefined);
      setCurrentEditedMilestone(undefined);
      message.success(t('createProject.activityUpdated'));
    } catch {}
  };

  const handleCancelFormMilestoneModal = () => {
    setIsFormMilestoneModalOpen(false);
    setCurrentEditedMilestone(undefined);
  };

  const handleCancelFormActivityModal = () => {
    setIsFormActivityModalOpen(false);
    setCurrentEditedMilestone(undefined);
    setCurrentEditedActivity(undefined);
  };

  const toggleAreActivitiesOpened = milestoneId => {
    const _milestones = [...milestones];
    const milestoneFound = _milestones.find(milestone => milestone?.id === milestoneId);
    milestoneFound.areActivitiesOpen = !milestoneFound.areActivitiesOpen;
    setMilestones(_milestones);
  };

  return (
    <>
      <CustomHead
        titleText={`${t('general.BackOffice')} - ${t('createProject.Project milestones')}`}
      />
      <div className="o-coaMilestonesContainer">
        <div className="o-coaMilestonesContainer__titleContainer">
          <TitlePage textTitle={t('createProject.previewAndEditMilestones')} />
          <CoaButton type="primary" onClick={() => setIsFormMilestoneModalOpen(true)}>
            {t('createProject.addNewMilestone')}
          </CoaButton>
        </div>
        {milestones?.length === 0 && (
          <div className="o-coaMilestonesContainer__emptyContainer">
            <div className="o-coaMilestonesContainer__emptyContainer__empty">
              <Icon component={MilestoneIcon2} />
              <p className="o-coaMilestonesContainer__emptyContainer__empty__title">
                {t('createProject.noMilestones')}
              </p>
              <p className="o-coaMilestonesContainer__emptyContainer__empty__description">
                {t('createProject.noMilestones2')}
              </p>
            </div>
          </div>
        )}
        {milestones?.length > 0 && (
          <div className="o-coaMilestonesContainer__cards">
            {milestones.map((milestone, index) => (
              <CoaMilestoneItem
                data-cy="milestone-card"
                key={milestone?.id}
                project={project}
                projectType={project?.type}
                toggleAreActivitiesOpened={toggleAreActivitiesOpened}
                onRemoveMilestone={
                  [MILESTONE_STATUS_ENUM.NEW].includes(milestone?.status) &&
                  (() => handleOpenConfirmDeleteMilestoneModal(milestone?.id))
                }
                onEditMilestone={
                  [MILESTONE_STATUS_ENUM.NEW, MILESTONE_STATUS_ENUM.IN_PROGRESS].includes(
                    milestone?.status
                  ) && (() => handleOpenEditMilestone(milestone))
                }
                onCreateActivity={
                  milestone?.status !== MILESTONE_STATUS_ENUM.APPROVED &&
                  (() => {
                    setCurrentEditedMilestone(milestone);
                    setIsFormActivityModalOpen(true);
                  })
                }
                onEditActivity={activity => handleOpenEditActivity({ milestone, activity })}
                onRemoveActivity={activityId =>
                  handleOpenConfirmDeleteActivityModal({ milestone, activityId })
                }
                withStatusTag={isACloneBeingEdited}
                {...{
                  currency,
                  milestone
                }}
                {...{ milestoneNumber: index + 1 }}
              />
            ))}
          </div>
        )}
      </div>
      {Footer()}
      <CoaFormMilestoneModal
        visible={isFormMilestoneModalOpen}
        onCancel={handleCancelFormMilestoneModal}
        onSave={currentEditedMilestone ? handleUpdateMilestone : handleCreateMilestone}
        initialData={currentEditedMilestone}
        destroyOnClose
        setVisible={setIsFormMilestoneModalOpen}
      />
      <CoaFormActivitiesModal
        activityStatus={currentEditedActivity?.status}
        visible={isFormActivityModalOpen}
        onCancel={handleCancelFormActivityModal}
        onSave={currentEditedActivity ? handleEditActivity : handleCreateActivity}
        currency={currency}
        auditors={auditorsProcessed}
        initialData={currentEditedActivity}
        destroyOnClose
        projectType={project?.type}
        setVisible={setIsFormActivityModalOpen}
      />
    </>
  );
};

CoaMilestonesView.propTypes = {
  project: PropTypes.objectOf(PropTypes.any),
  Footer: PropTypes.oneOfType([PropTypes.arrayOf(PropTypes.node), PropTypes.node]),
  isACloneBeingEdited: PropTypes.bool
};

CoaMilestonesView.defaultProps = {
  project: undefined,
  Footer: undefined
};
