/**
 * AGUA Copyright (C) 2023  Atix Labs
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import React from 'react';
import { Divider, Icon } from 'antd';
import classNames from 'classnames';
import { CoaTag } from 'components/atoms/CoaTag/CoaTag';
import { CoaTextButton } from 'components/atoms/CoaTextButton/CoaTextButton';
import { CoaMilestoneIndicators } from 'components/molecules/CoaMilestoneIndicators/CoaMilestoneIndicators';
import { useTranslation } from 'react-i18next';

export const CoaMilestoneIndicatorsCard = ({
  additionalBody,
  title,
  onEdit,
  onRemove,
  onCreate,
  entity,
  currency,
  className,
  isCollapsible,
  withStatusTag,
  status,
  statusMap,
  spending,
  payback,
  funding,
  withEvidences,
  impactQuantity,
  transferQuantity,
  projectType,
  ...rest
}) => {
  const { t } = useTranslation();
  return (
    <div className={classNames(className, 'o-coaMilestoneIndicatorsCard')} {...rest}>
      {!isCollapsible && (
        <>
          <div className={classNames('o-coaMilestoneIndicatorsCard__header')}>
            <div className="o-coaMilestoneIndicatorsCard__header__left">
              <div className="o-coaMilestoneIndicatorsCard__header__title">{title}</div>
            </div>
            <div className="o-coaMilestoneIndicatorsCard__header__right">
              {entity && onCreate && (
                <CoaTextButton
                  onClick={e => {
                    e.stopPropagation();
                    onCreate();
                  }}
                >
                  <Icon type="plus" /> {`${t('general.btnAdd')} ${entity}`}
                </CoaTextButton>
              )}
              {onEdit && (
                <CoaTextButton
                  onClick={e => {
                    e.stopPropagation();
                    onEdit();
                  }}
                  variant="muted"
                >
                  <Icon type="edit" /> {t('general.btnEdit')}
                </CoaTextButton>
              )}
              {onRemove && (
                <CoaTextButton
                  onClick={e => {
                    e.stopPropagation();
                    onRemove();
                  }}
                  variant="danger"
                >
                  <Icon type="delete" /> {t('general.btnDelete')}
                </CoaTextButton>
              )}
              {withStatusTag && (
                <CoaTag predefinedColor={statusMap?.[status]?.color}>
                  {statusMap?.[status]?.name}
                </CoaTag>
              )}
            </div>
          </div>
          <Divider className="o-coaMilestoneIndicatorsCard__divider" />
        </>
      )}

      <div className="o-coaMilestoneIndicatorsCard__body">
        <CoaMilestoneIndicators
          {...{
            currency,
            funding,
            spending,
            payback,
            withEvidences,
            impactQuantity,
            transferQuantity,
            projectType
          }}
        />

        {additionalBody}
      </div>
    </div>
  );
};
