/**
 * AGUA Copyright (C) 2023  Atix Labs
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import React from 'react';

import PropTypes from 'prop-types';
import { Collapse, Icon } from 'antd';
import { CoaTextButton } from 'components/atoms/CoaTextButton/CoaTextButton';
import { CoaActivityItem } from 'components/organisms/CoaActivities/CoaActivityItem/CoaActivityItem';
import { sortActivitiesById } from 'helpers/utils';
import milestoneStatusMap, { MILESTONE_STATUS_ENUM } from 'model/milestoneStatus';
import { ACTIVITY_STATUS_ENUM } from 'model/activityStatus';
import { CoaMilestoneIndicatorsCard } from '../CoaMilestoneIndicatorsCard/CoaMilestoneIndicatorsCard';
import { useTranslation } from 'react-i18next';

const { Panel } = Collapse;

export const CoaMilestoneItem = ({
  milestoneNumber,
  currency,
  milestone,
  onRemoveMilestone,
  onEditMilestone,
  onCreateActivity,
  onRemoveActivity,
  onEditActivity,
  toggleAreActivitiesOpened,
  isProjectEditing,
  withStatusTag,
  withEvidences,
  projectId,
  preview,
  projectType,
  project,
  withAddEvidenceButton,
  ...rest
}) => {
  const { t } = useTranslation();
  const description = milestone?.description;
  const status = milestone?.status;
  const title = milestone?.title;
  const spent = milestone?.spent || 0;
  const deposited = milestone?.deposited || 0;
  const { funding, spending, payback } = milestone || {};

  const areActivitiesOpen = milestone?.areActivitiesOpen || false;
  const allEvidences = milestone?.activities?.reduce(
    (curr, next) => [...curr, ...(next?.evidences || [])],
    []
  );
  const transferQuantity = allEvidences?.reduce(
    (curr, next) => (next?.type === 'transfer' ? curr + 1 : curr),
    0
  );
  const impactQuantity = allEvidences?.reduce(
    (curr, next) => (next?.type === 'impact' ? curr + 1 : curr),
    0
  );
  const activitiesSorted = sortActivitiesById({ activities: milestone?.activities });

  return (
    <CoaMilestoneIndicatorsCard
      {...{ currency }}
      statusMap={milestoneStatusMap}
      title={`${t('landingMilestones.milestone')} ${milestoneNumber} - ${title}`}
      entity={t('general.activity')}
      onEdit={onEditMilestone}
      onRemove={onRemoveMilestone}
      onCreate={onCreateActivity}
      spent={spent}
      deposited={deposited}
      className="o-coaMilestoneItem__card"
      withStatusTag={withStatusTag}
      status={status}
      transferQuantity={transferQuantity}
      impactQuantity={impactQuantity}
      withEvidences={withEvidences}
      funding={funding}
      spending={spending}
      payback={payback}
      projectType={projectType}
      additionalBody={
        <>
          <p className="o-coaMilestoneItem__description">{description}</p>
          {milestone?.activities?.length > 0 && (
            <Collapse
              activeKey={areActivitiesOpen ? '1' : '0'}
              defaultActiveKey={['0']}
              className="o-coaMilestoneItem__collapse"
              bordered={false}
              expandIcon={() => null}
            >
              <Panel
                header={
                  <div
                    tabIndex={0}
                    role="button"
                    className="o-coaMilestoneItem__collapse__header"
                    onClick={() => toggleAreActivitiesOpened(milestone?.id)}
                    onKeyDown={() => toggleAreActivitiesOpened(milestone?.id)}
                  >
                    {t('landingMilestones.viewActivities')}
                    {areActivitiesOpen ? <Icon type="down" /> : <Icon type="right" />}
                  </div>
                }
                className="o-coaMilestoneItem__collapse__panel"
                key="1"
              >
                <div className="o-coaMilestoneItem__cardsList">
                  {activitiesSorted.map((activity, index) => (
                    <CoaActivityItem
                      data-testid="activity-item"
                      key={activity?.id}
                      withAddEvidenceButton={withAddEvidenceButton}
                      project={project}
                      preview={preview}
                      projectId={projectId}
                      withEvidences={withEvidences}
                      withStatusTag={withStatusTag}
                      onRemove={
                        [ACTIVITY_STATUS_ENUM.NEW].includes(activity?.status) && onRemoveActivity
                          ? () => onRemoveActivity(activity?.id)
                          : undefined
                      }
                      onEdit={
                        [
                          ACTIVITY_STATUS_ENUM.NEW,
                          ACTIVITY_STATUS_ENUM.IN_PROGRESS,
                          ACTIVITY_STATUS_ENUM.TO_REVIEW
                        ].includes(activity?.status) && onEditActivity
                          ? () => onEditActivity(activity)
                          : undefined
                      }
                      isProjectEditing={isProjectEditing}
                      {...{ milestone, currency, activity }}
                      {...{ activityNumber: index + 1 }}
                    />
                  ))}
                </div>
              </Panel>
            </Collapse>
          )}
          {onCreateActivity && milestone?.status !== MILESTONE_STATUS_ENUM.APPROVED && (
            <CoaTextButton
              className="o-coaMilestoneItem__addActivityButton"
              type="dashed"
              onClick={() => onCreateActivity(milestone)}
            >
              <Icon type="plus" /> {t('landingMilestones.addActivities')}
            </CoaTextButton>
          )}
        </>
      }
      {...rest}
    />
  );
};

CoaMilestoneItem.defaultProps = {
  milestoneNumber: undefined,
  currency: undefined,
  milestone: undefined,
  onRemoveMilestone: undefined,
  onEditMilestone: undefined,
  onCreateActivity: undefined,
  onRemoveActivity: undefined,
  onEditActivity: undefined,
  toggleAreActivitiesOpened: undefined,
  isProjectEditing: false
};

CoaMilestoneItem.propTypes = {
  milestoneNumber: PropTypes.number,
  currency: PropTypes.string,
  milestone: PropTypes.objectOf(PropTypes.any),
  onRemoveMilestone: PropTypes.func,
  onEditMilestone: PropTypes.func,
  onCreateActivity: PropTypes.func,
  onRemoveActivity: PropTypes.func,
  onEditActivity: PropTypes.func,
  toggleAreActivitiesOpened: PropTypes.func,
  isProjectEditing: PropTypes.bool
};
