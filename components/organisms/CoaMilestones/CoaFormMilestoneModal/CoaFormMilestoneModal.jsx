/**
 * AGUA Copyright (C) 2023  Atix Labs
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import React from 'react';
import PropTypes from 'prop-types';
import { Form } from 'antd';
import { CoaFormItemTextArea } from 'components/molecules/CoaFormItems/CoaFormItemTextArea/CoaFormItemTextArea';
import { CoaDialogModal } from 'components/organisms/CoaModals/CoaDialogModal/CoaDialogModal';
import { ERROR_MESSAGES } from 'constants/constants';
import TitlePage from 'components/atoms/TitlePage/TitlePage';
import { CoaFormItemInput } from 'components/molecules/CoaFormItems/CoaFormItemInput/CoaFormItemInput';
import { useTranslation } from 'react-i18next';

export const CoaFormMilestoneModalContent = ({ form, onSave, onCancel, initialData, ...rest }) => {
  const { t } = useTranslation();
  return (
    <CoaDialogModal
      title={
        <TitlePage
          textTitle={
            initialData
              ? t('createProject.editNewMilestone')
              : t('createProject.createNewMilestone')
          }
          underlinePosition="left"
        />
      }
      {...{
        form,
        onSave,
        onCancel,
        okText: t('general.save'),
        cancelText: t('general.btnCancel')
      }}
      width={800}
      {...rest}
    >
      <CoaFormItemInput
        form={form}
        name="title"
        formItemProps={{ label: t('general.title') }}
        fieldDecoratorOptions={{
          rules: [
            {
              required: true,
              message: ERROR_MESSAGES.EMPTY,
              whitespace: true
            }
          ],
          validateTrigger: 'onSubmit',
          initialValue: initialData?.title
        }}
        inputProps={{
          placeholder: t('createProject.enterMilestoneTitle'),
          maxLength: 50,
          showCount: true
        }}
      />
      <CoaFormItemTextArea
        form={form}
        name="description"
        formItemProps={{ label: t('general.description') }}
        fieldDecoratorOptions={{
          rules: [
            {
              required: true,
              message: ERROR_MESSAGES.EMPTY,
              whitespace: true
            }
          ],
          validateTrigger: 'onSubmit',
          initialValue: initialData?.description
        }}
        inputTextAreaProps={{
          placeholder: t('createProject.enterMilestoneDescription'),
          maxLength: 500,
          showCount: true
        }}
      />
    </CoaDialogModal>
  );
};

export const CoaFormMilestoneModal = Form.create({ name: 'CoaFormMilestoneModal' })(
  CoaFormMilestoneModalContent
);

CoaFormMilestoneModalContent.defaultProps = {
  form: undefined,
  onSave: undefined,
  onCancel: undefined,
  initialData: undefined
};

CoaFormMilestoneModalContent.propTypes = {
  form: PropTypes.objectOf(PropTypes.any),
  onSave: PropTypes.func,
  onCancel: PropTypes.func,
  initialData: PropTypes.objectOf(PropTypes.any)
};
