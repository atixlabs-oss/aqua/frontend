/**
 * AGUA Copyright (C) 2023  Atix Labs
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import React, { useContext, useState } from 'react';
import { UserContext } from 'components/utils/UserContext';
import EvidenceDetailBox from 'components/molecules/EvidenceDetailBox/EvidenceDetailBox';
import GoBackButton from 'components/atoms/GoBackButton/GoBackButton';
import EvidenceDetailType from 'components/molecules/EvidenceDetailType/EvidenceDetailType';
import { Divider, Icon, message } from 'antd';
import ModalRejectEvidence from '../ModalRejectEvidence/ModalRejectEvidence';
import ModalApproveEvidence from '../ModalApproveEvidence/ModalApproveEvidence';
import AttachedFiles from '../AttachedFiles/AttachedFiles';
import Breadcrumb from '../../atoms/BreadCrumb/BreadCrumb';
import CoaRejectButton from '../../atoms/CoaRejectButton/CoaRejectButton';
import CoaApproveButton from '../../atoms/CoaApproveButton/CoaApproveButton';
import { CoaChangelogContainer } from '../CoaChangelogContainer/CoaChangelogContainer';
import TransactionLink from '../../molecules/TransactionLink/TransactionLink';
import { useRouter } from 'next/router';
import { useTranslation } from 'react-i18next';
import { canAddEvidences } from 'helpers/canAddEvidence';
import { CoaDeleteEvidenceButton } from 'components/atoms/CoaDeleteEvidenceButton/CoaDeleteEvidenceButton';
import { useQueryClient } from '@tanstack/react-query';
import { DocumentIcon2 } from 'components/atoms/CustomIcons/DocumentIcon2/DocumentIcon2';
import { useUpdateEvidenceStatusMutation } from 'api/react-query/evidences/evidencesMutations';

export default function EvidenceDetail({
  evidence,
  currency,
  currencyType,
  isProjectEditing,
  preview,
  project
}) {
  const { t } = useTranslation();
  const { user } = useContext(UserContext);
  const router = useRouter();
  const { projectId, activityId, detailEvidenceId } = router.query;
  const [approveModalOpen, setApproveModalOpen] = useState(false);
  const [rejectModalOpen, setRejectModalOpen] = useState(false);
  const evidenceStatus = evidence.status;
  const isAuditor = user?.id === evidence?.activity?.auditor?.id;
  const isNewEvidence = evidenceStatus === 'new';
  const isCryptoProject = (currencyType || '').toLowerCase() === 'crypto';
  const activityStatus = evidence?.activity?.status;
  const activityType = evidence?.activity?.type;
  const isDeletedEvidence = evidence?.deleted;
  const evidenceId = evidence?.id?.toString();
  const updateEvidenceStatusMutation = useUpdateEvidenceStatusMutation();

  if (!user && evidenceStatus !== 'approved')
    return router.push(`/${projectId}/activity/${activityId}/evidences`);

  const isTransferEvidence = evidence.type === 'transfer';
  const { transferTxHash } = evidence;

  const rejectEvidence = async reason => {
    try {
      await updateEvidenceStatusMutation.mutateAsync({
        evidenceId,
        status: 'rejected',
        reason,
        projectId
      });
      setRejectModalOpen(false);
    } catch {
      message.error(t('evidences.There was an error when trying to reject the evidence'));
    }
  };

  const approveEvidence = async () => {
    try {
      await updateEvidenceStatusMutation.mutateAsync({
        evidenceId,
        status: 'approved',
        reason: '',
        projectId
      });
      setApproveModalOpen(false);
    } catch {
      message.error(t('evidences.There was an error when trying to approve the evidence'));
    }
  };

  const isAbleToSeeEvidenceButtons = canAddEvidences({ user, project, activityType });

  const evidenceTitle = isDeletedEvidence ? evidence?.evidenceTitle : evidence?.title;
  const activityTitle = isDeletedEvidence ? evidence?.activityTitle : evidence?.activity?.title;
  const milestoneTitle = isDeletedEvidence ? evidence?.milestoneTitle : evidence?.milestone?.title;

  return (
    <>
      <div className="evidenceDetail">
        <div className="evidenceDetail__breadcrumbContainer">
          <GoBackButton
            goBackTo={
              preview
                ? `/${projectId}/activity/${activityId}/evidences?preview=true`
                : `/${projectId}/activity/${activityId}/evidences`
            }
          />
          <Breadcrumb route={`${milestoneTitle} / ${activityTitle} / ${evidenceTitle}`} />
        </div>

        <div className="evidenceDetail__contentContainer">
          {isDeletedEvidence && (
            <div className="evidenceDetail__emptyContainer">
              <Icon component={DocumentIcon2} />
              <p className="evidenceDetail__emptyContainer__text">
                {t('evidences.noCurrentInformation')}
              </p>
            </div>
          )}

          {!isDeletedEvidence && (
            <>
              <EvidenceDetailBox {...evidence} />
              <EvidenceDetailType
                evidenceType={evidence?.type}
                amount={evidence?.amount}
                currency={evidence?.currency}
                activityType={evidence?.activity?.type}
                destinationAccount={evidence?.destinationAccount}
                isCryptoProject={isCryptoProject}
              />
              {isTransferEvidence && isCryptoProject && (
                <>
                  <Divider />
                  <TransactionLink
                    showTitle
                    txHash={transferTxHash}
                    currency={evidence?.currency}
                  />
                </>
              )}
              {evidence?.files?.length > 0 && (
                <>
                  <Divider />
                  <AttachedFiles files={evidence?.files} />
                </>
              )}
              {isAuditor && isNewEvidence && (
                <>
                  <Divider />
                  <div className="evidenceDetail__auditorOptions">
                    <CoaRejectButton
                      onClick={() => setRejectModalOpen(true)}
                      disabled={isProjectEditing}
                      className="evidenceDetail__auditorOptions__rejectButton"
                    >
                      {t('general.btnReject')}
                    </CoaRejectButton>
                    <CoaApproveButton
                      onClick={() => setApproveModalOpen(true)}
                      disabled={isProjectEditing}
                      className="evidenceDetail__auditorOptions__approveButton"
                    >
                      {t('general.btnApprove')}
                    </CoaApproveButton>
                  </div>
                </>
              )}
            </>
          )}
        </div>

        <div className="evidenceDetail__deleteEvidenceButtonContainer">
          {isAbleToSeeEvidenceButtons && (
            <CoaDeleteEvidenceButton
              projectId={projectId}
              evidenceStatus={evidenceStatus}
              activityStatus={activityStatus}
              largeButtonText
              className="evidenceDetail__deleteEvidenceButtonContainer__button"
              urlToRedirectWhenSuccess={`/${projectId}/activity/${activityId}/evidences`}
              activityId={activityId}
              evidenceId={evidenceId}
            />
          )}
        </div>
        <div className="evidenceDetail__changelogContainer">
          <CoaChangelogContainer
            projectId={project?.parent || project?.id}
            evidenceId={detailEvidenceId}
            title={t('changelog.evidence') || 'Evidence Changelog'}
            currency={currency}
            isRequestEnabled={detailEvidenceId && (project?.parent || project?.id)}
          />
        </div>
      </div>
      <ModalApproveEvidence
        visible={approveModalOpen}
        setVisible={setApproveModalOpen}
        onSuccess={approveEvidence}
      />
      <ModalRejectEvidence
        visible={rejectModalOpen}
        setVisible={setRejectModalOpen}
        onSuccess={reason => rejectEvidence(reason)}
      />
    </>
  );
}
