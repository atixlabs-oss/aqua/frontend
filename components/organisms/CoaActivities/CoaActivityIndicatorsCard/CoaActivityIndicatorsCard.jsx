/**
 * AGUA Copyright (C) 2023  Atix Labs
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import React, { useState } from 'react';
import { Collapse, Icon } from 'antd';
import classNames from 'classnames';
import { CoaTextButton } from 'components/atoms/CoaTextButton/CoaTextButton';
import { AddEvidenceButton } from 'components/atoms/AddEvidenceButton/AddEvidenceButton';
import { ACTIVITY_STATUS_ENUM } from 'model/activityStatus';
import { CoaTag } from 'components/atoms/CoaTag/CoaTag';
import { CoaActivityIndicators } from 'components/molecules/CoaActivityIndicators/CoaActivityIndicators';

import { PROJECT_STATUS_ENUM } from 'model/projectStatus';
import { useTranslation } from 'react-i18next';

const { Panel } = Collapse;

const CardHeader = ({
  title,
  entity,
  onCreate,
  onEdit,
  onRemove,
  onClick,
  extra,
  isProjectEditing,
  withStatusTag,
  status,
  statusMap,
  onViewEvidence,
  onAddEvidences,
  projectStatus
}) => {
  const { t } = useTranslation();
  const responsiveLayout = onViewEvidence || onAddEvidences;

  return (
    <div
      className={classNames('o-coaActivityIndicatorsCard__header', {
        cardHeader: responsiveLayout
      })}
      onClick={onClick}
      tabIndex={0}
      role="button"
      onKeyDown={onClick}
    >
      <div className="o-coaActivityIndicatorsCard__header__left">
        <div className="o-coaActivityIndicatorsCard__header__title">{title}</div>
        {extra}
      </div>
      <div className="o-coaActivityIndicatorsCard__header__right">
        {entity && onCreate && (
          <CoaTextButton
            onClick={e => {
              e.stopPropagation();
              onCreate();
            }}
            variant="primary"
          >
            <Icon type="plus" /> {`Add ${entity}`}
          </CoaTextButton>
        )}
        {onEdit && (
          <CoaTextButton
            onClick={e => {
              e.stopPropagation();
              onEdit();
            }}
            variant="muted"
          >
            <Icon type="edit" /> {t('general.btnEdit')}
          </CoaTextButton>
        )}
        {onRemove && (
          <CoaTextButton
            onClick={e => {
              e.stopPropagation();
              onRemove();
            }}
            variant="danger"
          >
            <Icon type="delete" /> {t('general.btnDelete')}
          </CoaTextButton>
        )}
        {onViewEvidence && (
          <CoaTextButton
            onClick={onViewEvidence}
            className={classNames({ 'o-coaActivityIndicatorsCard__header__btn': responsiveLayout })}
            variant="primary"
          >
            <Icon type="eye" /> {t('general.btnView')}&nbsp;
            <span
              className={classNames({
                'o-coaActivityIndicatorsCard__header__evidenceText': responsiveLayout
              })}
            >
              {t('coaIndicator.evidences')}
            </span>
          </CoaTextButton>
        )}
        {onAddEvidences && (
          <AddEvidenceButton
            onClickAddEvidence={onAddEvidences}
            responsiveLayout
            disabled={
              ![
                ACTIVITY_STATUS_ENUM.NEW,
                ACTIVITY_STATUS_ENUM.REJECTED,
                ACTIVITY_STATUS_ENUM.IN_PROGRESS
              ].includes(status) ||
              isProjectEditing ||
              ![PROJECT_STATUS_ENUM.IN_PROGRESS, PROJECT_STATUS_ENUM.PUBLISHED].includes(
                projectStatus
              )
            }
          />
        )}
        {withStatusTag && (
          <CoaTag predefinedColor={statusMap?.[status]?.color}>{statusMap?.[status]?.name}</CoaTag>
        )}
      </div>
    </div>
  );
};

export const CoaActivityIndicatorsCard = ({
  additionalBody,
  title,
  onEdit,
  onRemove,
  onCreate,
  entity,
  currency,
  budget,
  className,
  isProjectEditing,
  withStatusTag,
  status,
  statusMap,
  transferQuantity,
  impactQuantity,
  withEvidences,
  onViewEvidence,
  onAddEvidences,
  color,
  current,
  projectStatus,
  ...rest
}) => {
  const [isCollapseOpen, setIsCollapseOpen] = useState(false);
  return (
    <div className={classNames(className, 'o-coaActivityIndicatorsCard')} {...rest}>
      <Collapse
        activeKey={isCollapseOpen ? '1' : '0'}
        className="o-coaActivityIndicatorsCard__collapse"
        expandIcon={() => null}
      >
        <Panel
          header={
            <CardHeader
              projectStatus={projectStatus}
              onAddEvidences={onAddEvidences}
              onViewEvidence={onViewEvidence}
              withEvidences={withEvidences}
              withStatusTag={withStatusTag}
              status={status}
              statusMap={statusMap}
              isProjectEditing={isProjectEditing}
              extra={
                isCollapseOpen ? (
                  <Icon type="down" className="o-coaActivityIndicatorsCard__header__icon" />
                ) : (
                  <Icon type="right" className="o-coaActivityIndicatorsCard__header__icon" />
                )
              }
              onClick={() => setIsCollapseOpen(!isCollapseOpen)}
              {...{ title, onEdit, onRemove, onCreate, entity }}
            />
          }
          key="1"
        >
          <div className="o-coaActivityIndicatorsCard__body">
            <CoaActivityIndicators
              {...{
                currency,
                budget,
                transferQuantity,
                impactQuantity,
                withEvidences,
                predefinedColor: color,
                current
              }}
            />

            {additionalBody}
          </div>
        </Panel>
      </Collapse>
    </div>
  );
};
