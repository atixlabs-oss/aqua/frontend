/**
 * AGUA Copyright (C) 2023  Atix Labs
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import React from 'react';

import PropTypes from 'prop-types';
import activityStatusMap from 'model/activityStatus';
import { UserContext } from 'components/utils/UserContext';
import { ACTIVITY_TYPES_COLORS } from 'model/activityTypes';
import { ActivityTypeText } from 'components/atoms/ActivityTypeText/ActivityTypeText';
import { canAddEvidences } from 'helpers/canAddEvidence';
import classNames from 'classnames';
import { CoaActivityIndicatorsCard } from '../CoaActivityIndicatorsCard/CoaActivityIndicatorsCard';
import { useRouter } from 'next/router';
import { useTranslation } from 'react-i18next';

export const CoaActivityItem = ({
  currency,
  activity,
  onRemove,
  onEdit,
  isProjectEditing,
  withStatusTag,
  withEvidences,
  projectId,
  preview,
  project,
  withAddEvidenceButton,
  ...rest
}) => {
  const router = useRouter();
  const { t } = useTranslation();
  const { user } = React.useContext(UserContext);

  const _canAddEvidences = canAddEvidences({ user, project, activityType: activity?.type });

  const projectStatus = project?.status;
  const description = activity?.description;
  const acceptanceCriteria = activity?.acceptanceCriteria;
  const title = activity?.title;
  const budget = activity?.budget;
  const activityType = activity?.type;
  const current = activity?.current || 0;
  const status = activity?.status || '-';
  const transferQuantity = activity?.evidences?.reduce(
    (curr, next) => (next?.type === 'transfer' ? curr + 1 : curr),
    0
  );
  const impactQuantity = activity?.evidences?.reduce(
    (curr, next) => (next?.type === 'impact' ? curr + 1 : curr),
    0
  );

  return (
    <CoaActivityIndicatorsCard
      {...{ currency }}
      isProjectEditing={isProjectEditing}
      withEvidences={withEvidences}
      statusMap={activityStatusMap}
      className={classNames('o-coaActivityItem__card', {
        [`--${ACTIVITY_TYPES_COLORS[activityType]}`]: ACTIVITY_TYPES_COLORS[activityType]
      })}
      color={ACTIVITY_TYPES_COLORS[activityType]}
      budget={budget}
      title={
        <>
          <ActivityTypeText activityType={activityType} Component="span">
            {activityType}
          </ActivityTypeText>
          {' - '}
          {title}
        </>
      }
      onEdit={onEdit}
      onRemove={onRemove}
      current={current}
      isCollapsible
      withStatusTag={withStatusTag}
      status={status}
      transferQuantity={transferQuantity}
      impactQuantity={impactQuantity}
      onViewEvidence={
        withEvidences &&
        (e => {
          e.stopPropagation();
          router.push(
            preview
              ? `/${projectId}/activity/${activity?.id}/evidences?preview=true`
              : `/${projectId}/activity/${activity?.id}/evidences`
          );
        })
      }
      onAddEvidences={
        withAddEvidenceButton &&
        _canAddEvidences &&
        (e => {
          e.stopPropagation();
          router.push(`/${projectId}/activity/${activity?.id}/create-evidence`);
        })
      }
      additionalBody={
        <>
          <div>
            <p className="o-coaActivityItem__indicatorTitle">{t('general.description')}</p>
            <p className="o-coaActivityItem__indicatorValue">{description}</p>
          </div>
          <div>
            <p className="o-coaActivityItem__indicatorTitle">{t('general.acceptanceCriteria')}</p>
            <p className="o-coaActivityItem__indicatorValue">{acceptanceCriteria}</p>
          </div>
        </>
      }
      projectStatus={projectStatus}
      {...rest}
    />
  );
};

CoaActivityItem.defaultProps = {
  activityNumber: undefined,
  currency: undefined,
  activity: undefined,
  onRemove: undefined,
  onEdit: undefined,
  isProjectEditing: false
};

CoaActivityItem.propTypes = {
  activityNumber: PropTypes.number,
  currency: PropTypes.string,
  activity: PropTypes.objectOf(PropTypes.any),
  onRemove: PropTypes.func,
  onEdit: PropTypes.func,
  isProjectEditing: PropTypes.bool
};
