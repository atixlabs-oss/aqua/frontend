/**
 * AGUA Copyright (C) 2023  Atix Labs
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import React, { useEffect, useState } from 'react';
import { Form, Input } from 'antd';
import { CoaDialogModal } from 'components/organisms/CoaModals/CoaDialogModal/CoaDialogModal';
import { CoaFormItemTextArea } from 'components/molecules/CoaFormItems/CoaFormItemTextArea/CoaFormItemTextArea';
import { ERROR_MESSAGES } from 'constants/constants';
import { CoaFormItemSelect } from 'components/molecules/CoaFormItems/CoaFormItemSelect/CoaFormItemSelect';

import PropTypes from 'prop-types';
import { CoaFormItem } from 'components/molecules/CoaFormItems/CoaFormItem/CoaFormItem';
import { CoaFormItemInputNumber } from 'components/molecules/CoaFormItems/CoaFormItemInputNumber/CoaFormItemInputNumber';
import TitlePage from 'components/atoms/TitlePage/TitlePage';
import { CoaFormItemRadioButton } from 'components/molecules/CoaFormItems/CoaFormItemRadioButton/CoaFormItemRadioButton';
import { ACTIVITY_TYPES_ENUM } from 'model/activityTypes';
import { PROJECT_TYPE_ENUM } from 'model/projectType';
import { ACTIVITY_STATUS_ENUM } from 'model/activityStatus';
import { useTranslation } from 'react-i18next';

export const CoaFormActivitiesModalContent = ({
  form,
  onSave,
  onCancel,
  initialData,
  currency,
  auditors,
  projectType,
  activityStatus,
  ...rest
}) => {
  const { t } = useTranslation();
  const { getFieldValue } = form;
  const [isBudgetRequiredField, setIsBudgetRequiredField] = useState(false);
  const activityType = getFieldValue('type');

  useEffect(() => {
    if (activityType === ACTIVITY_TYPES_ENUM.SPENDING) setIsBudgetRequiredField(false);
    if ([ACTIVITY_TYPES_ENUM.FUNDING, ACTIVITY_TYPES_ENUM.PAYBACK].includes(activityType))
      setIsBudgetRequiredField(true);
  }, [activityType]);

  return (
    <CoaDialogModal
      title={
        <TitlePage
          textTitle={initialData ? t('general.editActivity') : t('general.createNewActivity')}
          underlinePosition="left"
        />
      }
      {...{
        form,
        onSave,
        onCancel,
        okText: t('general.save'),
        cancelText: t('general.btnCancel')
      }}
      width={800}
      {...rest}
    >
      <CoaFormItemTextArea
        form={form}
        name="title"
        formItemProps={{ label: t('general.title') }}
        fieldDecoratorOptions={{
          rules: [
            {
              required: true,
              message: ERROR_MESSAGES.EMPTY,
              whitespace: true
            }
          ],
          validateTrigger: 'onSubmit',
          initialValue: initialData?.title,
          hidden: activityStatus === ACTIVITY_STATUS_ENUM.TO_REVIEW
        }}
        inputTextAreaProps={{
          placeholder: t('activities.Enter the activity title'),
          maxLength: 50,
          showCount: true,
          disabled: activityStatus === ACTIVITY_STATUS_ENUM.TO_REVIEW
        }}
      />
      <CoaFormItemTextArea
        form={form}
        name="description"
        formItemProps={{ label: t('general.description') }}
        fieldDecoratorOptions={{
          rules: [
            {
              required: true,
              message: ERROR_MESSAGES.EMPTY,
              whitespace: true
            }
          ],
          validateTrigger: 'onSubmit',
          initialValue: initialData?.description,
          hidden: activityStatus === ACTIVITY_STATUS_ENUM.TO_REVIEW
        }}
        inputTextAreaProps={{
          placeholder: t('general.enterDescription'),
          maxLength: 500,
          showCount: true,
          disabled: activityStatus === ACTIVITY_STATUS_ENUM.TO_REVIEW
        }}
      />
      <CoaFormItemRadioButton
        type="1"
        form={form}
        name="type"
        fieldDecoratorOptions={{
          rules: [{ required: true }],
          validateTrigger: 'onSubmit',
          initialValue: initialData?.type || 'funding',
          hidden: [ACTIVITY_STATUS_ENUM.TO_REVIEW, ACTIVITY_STATUS_ENUM.IN_PROGRESS].includes(
            activityStatus
          )
        }}
        formItemProps={{ label: t('general.activityType') }}
        radioProps={{
          disabled: [ACTIVITY_STATUS_ENUM.TO_REVIEW, ACTIVITY_STATUS_ENUM.IN_PROGRESS].includes(
            activityStatus
          )
        }}
        options={
          projectType === PROJECT_TYPE_ENUM.LOAN
            ? [
                { label: t('general.funding'), value: 'funding' },
                { label: t('general.spending'), value: 'spending' },
                { label: t('general.payback'), value: 'payback' }
              ]
            : [
                { label: t('general.funding'), value: 'funding' },
                { label: t('general.spending'), value: 'spending' }
              ]
        }
      />
      <CoaFormItemTextArea
        form={form}
        name="acceptanceCriteria"
        formItemProps={{ label: t('general.acceptanceCriteria') }}
        fieldDecoratorOptions={{
          rules: [
            {
              required: true,
              message: ERROR_MESSAGES.EMPTY,
              whitespace: true
            }
          ],
          validateTrigger: 'onSubmit',
          initialValue: initialData?.acceptanceCriteria,
          hidden: activityStatus === ACTIVITY_STATUS_ENUM.TO_REVIEW
        }}
        inputTextAreaProps={{
          placeholder: t('general.enterAcceptanceCriteria'),
          maxLength: 500,
          showCount: true,
          disabled: activityStatus === ACTIVITY_STATUS_ENUM.TO_REVIEW
        }}
      />
      <div className="coaFormActivitiesModal__budget">
        <CoaFormItemInputNumber
          name="budget"
          form={form}
          formItemProps={{
            label: t('header.budget'),
            className: 'coaFormActivitiesModal__budget__formItem'
          }}
          fieldDecoratorOptions={{
            validateTrigger: 'onSubmit',
            initialValue: initialData?.budget,
            rules: [
              {
                required: isBudgetRequiredField,
                message: ERROR_MESSAGES.EMPTY
              }
            ],
            hidden: activityStatus === ACTIVITY_STATUS_ENUM.TO_REVIEW
          }}
          inputNumberProps={{
            step: '0.1',
            disabled: activityStatus === ACTIVITY_STATUS_ENUM.TO_REVIEW
          }}
        />
        <CoaFormItem
          formItemProps={{
            label: t('general.currency')
          }}
        >
          <Input disabled value={currency} />
        </CoaFormItem>
      </div>
      <CoaFormItemSelect
        name="auditor"
        form={form}
        formItemProps={{ label: 'Auditor', className: 'coaFormActivitiesModal__select' }}
        fieldDecoratorOptions={{
          rules: [
            {
              required: true,
              message: ERROR_MESSAGES.EMPTY
            }
          ],
          validateTrigger: 'onSubmit',
          initialValue: initialData?.auditor?.id
        }}
        selectProps={{
          placeholder: 'Auditor'
        }}
        options={auditors}
      />
    </CoaDialogModal>
  );
};

export const CoaFormActivitiesModal = Form.create({ name: 'CoaFormActivitiesModal' })(
  CoaFormActivitiesModalContent
);

CoaFormActivitiesModalContent.defaultProps = {
  form: undefined,
  onSave: undefined,
  onCancel: undefined,
  initialData: undefined,
  currency: undefined,
  auditors: undefined
};

CoaFormActivitiesModalContent.propTypes = {
  form: PropTypes.objectOf(PropTypes.any),
  onSave: PropTypes.func,
  onCancel: PropTypes.func,
  initialData: PropTypes.objectOf(PropTypes.any),
  currency: PropTypes.string,
  auditors: PropTypes.objectOf(PropTypes.any)
};
