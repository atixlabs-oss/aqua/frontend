/**
 * AGUA Copyright (C) 2023  Atix Labs
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import { useState } from 'react';
import { Icon as AntIcon, Divider } from 'antd';
import classNames from 'classnames';
import PropTypes from 'prop-types';
import { useTranslation } from 'react-i18next';

import Styles from './profile-block-info.module.scss';
import TitlePage from 'components/atoms/TitlePage/TitlePage';
import { CoaButton } from 'components/atoms/CoaButton/CoaButton';

export const ProfileBlockInfo = ({
  iconName,
  title,
  value,
  editable,
  editButtonText,
  EditContent,
  RightExtraContent,
  LeftButton,
  DownExtraContent,
  CustomIconComponent
}) => {
  const { t } = useTranslation();
  const [isEditing, setIsEditing] = useState(false);

  return (
    <div className={Styles.blockInfoContainer}>
      <div className={Styles.blockInfoContainer__header}>
        <AntIcon
          type={iconName}
          component={CustomIconComponent}
          className={Styles.blockInfoContainer__header__icon}
        />
        <div className={Styles.blockInfoContainer__header__text}>
          <TitlePage
            textTitle={title}
            HtmlTag="h3"
            variant="grey"
            underlinePosition="none"
            fontSize="medium"
          />
          {!isEditing && <div className={Styles.blockInfoContainer__header__value}>{value}</div>}
        </div>
        <div className={Styles.blockInfoContainer__header__rightContainer}>
          {RightExtraContent && (
            <>
              {RightExtraContent}
              {editable && (
                <Divider
                  type="vertical"
                  className={Styles.blockInfoContainer__header__rightContainer__divider}
                />
              )}
            </>
          )}
          {editable && (
            <>
              <CoaButton
                className={classNames(
                  Styles.blockInfoContainer__header__button,
                  Styles['--mobile']
                )}
                icon={isEditing ? 'close' : 'edit'}
                type={isEditing ? 'ghost-dark' : 'ghost'}
                onClick={() => setIsEditing(!isEditing)}
              />
              {LeftButton ? (
                <LeftButton
                  className={classNames(
                    Styles.blockInfoContainer__header__button,
                    Styles['--desktop']
                  )}
                />
              ) : (
                <CoaButton
                  type={isEditing ? 'ghost-dark' : 'ghost'}
                  onClick={() => setIsEditing(!isEditing)}
                  className={classNames(
                    Styles.blockInfoContainer__header__button,
                    Styles['--desktop'],
                    {
                      [Styles['--cancel']]: isEditing
                    }
                  )}
                >
                  {isEditing ? t('general.btnCancel') : editButtonText}
                </CoaButton>
              )}
            </>
          )}
        </div>
      </div>
      {DownExtraContent}
      {isEditing && (
        <>
          <Divider />
          <EditContent setIsEditing={setIsEditing} />
        </>
      )}
    </div>
  );
};

ProfileBlockInfo.defaultProps = {
  EditContent: () => <></>
};

ProfileBlockInfo.propTypes = {
  iconName: PropTypes.string,
  title: PropTypes.oneOfType([PropTypes.string, PropTypes.object]),
  value: PropTypes.oneOfType([PropTypes.string, PropTypes.element]),
  editable: PropTypes.bool,
  editButtonText: PropTypes.string,
  EditContent: PropTypes.elementType,
  RightExtraContent: PropTypes.element,
  LeftButton: PropTypes.elementType,
  DownExtraContent: PropTypes.element,
  CustomIconComponent: PropTypes.elementType
};
