/**
 * AGUA Copyright (C) 2023  Atix Labs
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import i18n from 'i18n';
import classNames from 'classnames';
import { Icon } from 'antd';

import { CoaButton } from 'components/atoms/CoaButton/CoaButton';
import { KeyIcon } from 'components/atoms/CustomIcons/KeyIcon';
import { CustomTooltip } from 'components/atoms/CustomTooltip/CustomTooltip';
import { ADDRESS_STATUS_ENUM } from 'model/addressStatus';
import Styles from './users-administration-table.module.scss';
import { openManageSignatureRequestModal } from '../CoaModals/ConfirmModals/confirmModals';
import { CustomToggle } from 'components/atoms/CustomToggle/CustomToggle';

export const usersAdministrationTableColumns = ({
  onApproveSignature,
  onRejectSignature,
  onToggleActive,
  actionsFiltersSelected
}) => {
  const { t } = i18n;
  return [
    {
      title: t('general.E-mail'),
      dataIndex: 'email',
      key: 'email',
      width: 200
    },
    {
      title: t('general.FirstName'),
      dataIndex: 'firstName',
      key: 'firstName',
      width: 180
    },
    {
      title: t('general.LastName'),
      dataIndex: 'lastName',
      key: 'lastName',
      width: 180
    },
    {
      title: t('general.Registration'),
      dataIndex: 'isActive',
      key: 'registration',
      width: 150,
      render: (isActive, data) => (
        <div className={Styles.registrationColumn} key={data?.id}>
          <Icon
            type={isActive ? 'check-circle' : 'minus-circle'}
            theme="filled"
            className={classNames(Styles.registrationColumn__icon, {
              [Styles['--active']]: isActive,
              [Styles['--inactive']]: !isActive
            })}
          />{' '}
          {isActive ? t('general.Active') : t('general.Inactive')}
        </div>
      )
    },
    {
      title: t('general.Actions'),
      dataIndex: 'actions',
      key: 'actions',
      filters: [
        { text: t('general.All'), value: 'all' },
        { text: t('signature.Digital Signature Request'), value: ADDRESS_STATUS_ENUM.IN_REVIEW }
      ],
      filterMultiple: false,
      filteredValue: actionsFiltersSelected,
      width: 100,
      render: (_, data) => (
        <div className={Styles.actionsColumn} key={data?.id}>
          <CustomToggle onToggle={() => onToggleActive(data?.id)} defaultChecked={data?.isActive} />
          {data?.pinStatus === ADDRESS_STATUS_ENUM.IN_REVIEW && (
            <CustomTooltip title={t('signature.manageDigitalSignatureRequest')} variant="black">
              <CoaButton
                shape="circle"
                type="primary"
                onClick={() =>
                  openManageSignatureRequestModal({
                    onApprove: () => onApproveSignature(data?.id),
                    onReject: () => onRejectSignature(data?.id)
                  })
                }
              >
                <KeyIcon />
              </CoaButton>
            </CustomTooltip>
          )}
        </div>
      )
    }
  ];
};
