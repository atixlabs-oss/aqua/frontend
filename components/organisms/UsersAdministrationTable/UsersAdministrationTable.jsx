/**
 * AGUA Copyright (C) 2023  Atix Labs
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import React, { useState } from 'react';
import { Table } from 'antd';

import Styles from './users-administration-table.module.scss';
import { useGetUsersQuery } from 'api/react-query/users/usersQueries';
import { usersAdministrationTableColumns } from '../UsersAdministrationTable/users-administration-table-columns';
import {
  useApprovePinRequestMutation,
  useRejectPinRequestMutation,
  useToggleActiveStatus
} from 'api/react-query/users/usersMutations';

export const UsersAdministrationTable = () => {
  const [actionsFiltersSelected, setActionsFiltersSelected] = useState(['all']);
  const [filters, setFilters] = useState({ page: 1 });
  const { data: usersResponse, isFetching } = useGetUsersQuery({
    filters
  });
  const approvePinRequestMutation = useApprovePinRequestMutation();
  const rejectPinRequestMutation = useRejectPinRequestMutation();
  const toggleActiveStatus = useToggleActiveStatus();

  const handleTableChange = (pagination, filters) => {
    const { current } = pagination;
    const { actions } = filters;
    const optionSelected = actions?.[0];

    if (optionSelected === 'all' || !optionSelected) {
      setActionsFiltersSelected(['all']);
      return setFilters({ page: current });
    }
    setActionsFiltersSelected(actions);
    setFilters({
      filterPinStatus: optionSelected,
      page: current
    });
  };

  return (
    <Table
      onChange={handleTableChange}
      loading={isFetching}
      onHeaderRow={(col, index) => {
        if (index === 0) return { className: Styles.headerRow };
      }}
      rowClassName={Styles.row}
      dataSource={usersResponse?.users}
      columns={usersAdministrationTableColumns({
        onApproveSignature: approvePinRequestMutation.mutateAsync,
        onRejectSignature: rejectPinRequestMutation.mutateAsync,
        onToggleActive: toggleActiveStatus.mutateAsync,
        actionsFiltersSelected
      })}
      pagination={{
        pageSize: usersResponse?.pageSize,
        total: usersResponse?.totalItems
      }}
      className={Styles.table}
    />
  );
};
