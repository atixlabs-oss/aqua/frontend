/**
 * AGUA Copyright (C) 2023  Atix Labs
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/* eslint-disable max-len */
import GoBackButton from 'components/atoms/GoBackButton/GoBackButton';
import React from 'react';
import { useRouter } from 'next/router';
import { useTranslation } from 'react-i18next';

const TermsAndConditionsContainer = () => {
  const router = useRouter();
  const { t } = useTranslation('termsAndConditions');
  return (
    <div className="termAndConditionsContainer">
      <GoBackButton goBackTo={() => router.back()} />
      <div className="termAndConditionsContainer__content">
        <h1 className="termAndConditionsContainer__title">{t('title')}</h1>
        <p>{t('content')}</p>
      </div>
    </div>
  );
};

export default TermsAndConditionsContainer;
