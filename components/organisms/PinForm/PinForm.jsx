import { useState } from 'react';
import { Typography, Divider, Form, message } from 'antd';
import { useTranslation } from 'react-i18next';

import { CoaFormItemCheckbox } from 'components/molecules/CoaFormItems/CoaFormItemCheckbox/CoaFormItemCheckbox';
import { CoaLink } from 'components/atoms/CoaLink/CoaLink';
import { CoaFormItemPassword } from 'components/molecules/CoaFormItems/CoaFormItemPassword/CoaFormItemPassword';
import Loading from 'components/molecules/Loading/Loading';
import { generateWalletFromPin } from 'helpers/blockchain/wallet';
import Styles from './pin-form.module.scss';
import { CoaButton } from 'components/atoms/CoaButton/CoaButton';

const PinFormContent = ({ form, pin, onSuccess, user, setWalletMutation, setPinMutation }) => {
  const { t } = useTranslation();
  const [isLoading, setIsLoading] = useState(false);

  const savePin = async pin => {
    try {
      await setPinMutation.mutateAsync();

      // Wallet generated only after pin validation
      const key = `${pin}-${user.id}-${user.email}`;

      const wallet = await generateWalletFromPin(key);
      const { data } = await setWalletMutation.mutateAsync(wallet);
      if (data?.id) {
        onSuccess?.();
      }
    } catch {}
  };

  const handleSubmit = event => {
    event.preventDefault();
    form.validateFields(async (err, values) => {
      if (!err) {
        const { pin } = values;
        try {
          setIsLoading(true);
          await savePin(pin);
        } catch (error) {
          message.error(t('modals.dialog.pinChange.error'));
        } finally {
          setIsLoading(false);
        }
      }
    });
  };

  const validPin = (_rule, value, callback) => {
    if (value && value.toString() !== pin.toString()) {
      callback(t('modals.dialog.pinChange.secretKeyDoNotMatch'));
    } else {
      callback();
    }
  };

  return (
    <Loading spinning={Boolean(isLoading)}>
      <Form form={form} onSubmit={handleSubmit} className={Styles.form}>
        <div className={Styles.form__content}>
          <Typography.Paragraph className={Styles.form__pinHelper}>
            {t('modals.dialog.pinChange.pinHelper')}
          </Typography.Paragraph>
          <CoaFormItemPassword
            form={form}
            name="pin"
            formItemProps={{
              label: (
                <span className={Styles.form__pinFormItemLabel}>
                  {t('modals.dialog.pinChange.pinLabel')}
                </span>
              )
            }}
            inputProps={{
              placeholder: t('modals.dialog.pinChange.pinPlaceholder')
            }}
            fieldDecoratorOptions={{
              rules: [
                {
                  required: true,
                  message: t('modals.dialog.pinChange.pleaseEnterYourPin')
                },
                {
                  validator: validPin
                }
              ]
            }}
          />

          <CoaFormItemCheckbox
            form={form}
            name="termsAndConditions"
            fieldDecoratorOptions={{
              rules: [
                {
                  required: true,
                  message: t('modals.dialog.pinChange.pleaseCheckTermsAndConditions')
                }
              ]
            }}
            formItemProps={{
              label: (
                <span className={Styles.form__checkboxFormItemLabel}>
                  {t('modals.dialog.pinChange.termsAndConditionsHelper')}
                </span>
              ),
              className: Styles.form__checkboxFormItem
            }}
            checkboxProps={{
              className: Styles.form__checkbox
            }}
            options={[
              {
                value: 'accept',
                label: (
                  <span className={Styles.form__optionLabel}>
                    {t('secretKey.iHaveReadAndAcceptThe')}{' '}
                    <CoaLink href="/terms-and-conditions" target="_blank" rel="noopener noreferrer">
                      {t('modals.dialog.pinChange.termsAndConditionsLink', {
                        organizationName: process.env.NEXT_PUBLIC_ORGANIZATION_NAME
                      })}
                    </CoaLink>
                  </span>
                )
              }
            ]}
          />
        </div>
        <Divider />
        <div className={Styles.form__footer}>
          <CoaButton
            type="primary"
            htmlType="submit"
            className={Styles.form__submitButton}
            loading={isLoading}
          >
            {t('modals.common.confirm')}
          </CoaButton>
        </div>
      </Form>
    </Loading>
  );
};

export const PinForm = Form.create({
  name: 'PinForm'
})(PinFormContent);
