/**
 * AGUA Copyright (C) 2023  Atix Labs
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import React, { useContext, useCallback } from 'react';
import { useTranslation } from 'react-i18next';
import PropTypes from 'prop-types';
import { Form } from 'antd';

import { UserContext } from 'components/utils/UserContext';
import { decryptJsonWallet } from 'helpers/blockchain/wallet';
import { CoaFormItemTextArea } from 'components/molecules/CoaFormItems/CoaFormItemTextArea/CoaFormItemTextArea';
import TitlePage from 'components/atoms/TitlePage/TitlePage';
import { CoaFormItemPassword } from 'components/molecules/CoaFormItems/CoaFormItemPassword/CoaFormItemPassword';
import { CoaDialogModal } from '../CoaModals/CoaDialogModal/CoaDialogModal';
import { useLoginUserMutation } from 'api/react-query/users/usersMutations';
import { useGetWalletQuery } from 'api/react-query/users/usersQueries';

function FormModalConfirmWithSK({
  form,
  visible,
  onCancel,
  onSuccess,
  title,
  description,
  okText,
  cancelText,
  leaveAComment
}) {
  const { user } = useContext(UserContext);
  const loginUserMutation = useLoginUserMutation();
  const { data, isFetching: isWalletFetching } = useGetWalletQuery({
    enabled: visible
  });
  const { getFieldValue } = form;
  const keySuffix = `${user.id}-${user.email}`;
  const { t } = useTranslation();
  const wallet = data?.wallet;

  const validPin = async (_rule, value, callback) => {
    if (!value) callback();
    try {
      const key = `${value}-${keySuffix}`;
      await decryptJsonWallet(wallet, key);
      callback();
    } catch (e) {
      callback(t('modalConfirmWithSK.invalidKey'));
    }
  };
  const validPassword = async (_rule, value, callback) => {
    try {
      const { email } = user;
      await loginUserMutation.mutateAsync({ email, pwd: value });
      callback();
    } catch (e) {
      callback(t('modalConfirmWithSK.invalidPassword'));
    }
    callback();
  };

  const handleSave = useCallback(() => {
    onSuccess(
      getFieldValue('secretKey'),
      getFieldValue('password'),
      wallet,
      `${getFieldValue('secretKey')}-${user.id}-${user.email}`
    );
  }, [getFieldValue, onSuccess, user.email, user.id, wallet]);

  return (
    <CoaDialogModal
      buttonsPosition="center"
      visible={visible}
      onCancel={onCancel}
      form={form}
      onSave={handleSave}
      title={<TitlePage centeredText textTitle={title} underlinePosition="none" />}
      withLogo
      description={description}
      okText={okText}
      cancelText={cancelText}
      okButtonProps={{ loading: isWalletFetching }}
    >
      <Form>
        {leaveAComment && (
          <CoaFormItemTextArea
            form={form}
            formItemProps={{
              label: 'Leave a comment'
            }}
            name="comment"
            fieldDecoratorOptions={{
              rules: [
                {
                  required: true,
                  message: 'Must write a reason for rejection'
                }
              ],
              validateTrigger: 'onSubmit'
            }}
            inputTextAreaProps={{
              rows: 4
            }}
          />
        )}
        <CoaFormItemPassword
          form={form}
          name="password"
          formItemProps={{
            label: 'Password'
          }}
          fieldDecoratorOptions={{
            rules: [
              {
                required: true,
                message: t('modalConfirmWithSK.rulePassword')
              },
              {
                validator: validPassword
              }
            ],
            validateTrigger: 'onSubmit'
          }}
          inputProps={{
            placeholder: t('modalConfirmWithSK.enterPassword')
          }}
        />
        <CoaFormItemPassword
          form={form}
          name="secretKey"
          formItemProps={{
            label: 'Secret Key'
          }}
          fieldDecoratorOptions={{
            rules: [
              {
                required: true,
                message: t('modalConfirmWithSK.ruleSecretKey')
              },
              {
                validator: validPin
              }
            ],
            validateTrigger: 'onSubmit'
          }}
          inputProps={{
            placeholder: t('modalConfirmWithSK.enterPin')
          }}
        />
      </Form>
    </CoaDialogModal>
  );
}

FormModalConfirmWithSK.defaultProps = {
  form: null,
  visible: false,
  leaveAComment: false,
  title: 'Do you want to confirm the creation of the project?',
  onCancel: () => undefined,
  onSuccess: () => undefined,
  description: 'To confirm the process enter your administrator password and secret key',
  okText: 'Yes',
  cancelText: 'No'
};

FormModalConfirmWithSK.propTypes = {
  form: PropTypes.element,
  title: PropTypes.string,
  visible: PropTypes.bool,
  leaveAComment: PropTypes.bool,
  onCancel: PropTypes.func,
  onSuccess: PropTypes.func,
  description: PropTypes.string,
  okText: PropTypes.string,
  cancelText: PropTypes.string
};
const ModalConfirmWithSK = Form.create({ name: 'FormConfirmWithSK' })(FormModalConfirmWithSK);
export default ModalConfirmWithSK;
