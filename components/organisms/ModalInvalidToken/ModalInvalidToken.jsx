/**
 * AGUA Copyright (C) 2023  Atix Labs
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import React from 'react';
import { Typography } from 'antd';
import LogoWrapper from 'components/atoms/LogoWrapper';
import { useRouter } from 'next/router';
import { CoaDialogModal } from '../CoaModals/CoaDialogModal/CoaDialogModal';
import { useTranslation } from 'react-i18next';

export default function ModalInvalidToken() {
  const router = useRouter();
  const { t } = useTranslation();

  const goToLogin = () => {
    router.push('/login');
  };
  return (
    <CoaDialogModal
      visible
      okText={t('resetPassword.goToLogin')}
      buttonsPosition="center"
      withoutCancelButton
      onSave={goToLogin}
      closable={false}
    >
      <LogoWrapper textTitle={t('resetPassword.thisTokenIsExpired')} />
      <Typography className="CoaModal__Paragraph--centered">
        {t('resetPassword.loginUsingYourUserAndPassword')}
      </Typography>
    </CoaDialogModal>
  );
}
