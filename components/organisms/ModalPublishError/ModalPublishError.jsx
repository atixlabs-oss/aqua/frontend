/**
 * AGUA Copyright (C) 2023  Atix Labs
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import React from 'react';
import PropTypes from 'prop-types';
import TitlePage from 'components/atoms/TitlePage/TitlePage';
import { CoaDialogModal } from '../CoaModals/CoaDialogModal/CoaDialogModal';

const ModalPublishError = ({ visible, onCancel, description, onOk }) => (
  <CoaDialogModal
    buttonsPosition="center"
    withLogo
    title={<TitlePage centeredText textTitle="An error ocurred" underlinePosition="none" />}
    description={description}
    visible={visible}
    closable={false}
    onCancel={onCancel}
    mask
    maskClosable
    withoutCancelButton
    okText="OK"
    onSave={onOk}
  />
);

ModalPublishError.defaultProps = {
  visible: false,
  onCancel: () => undefined,
  description: 'Try again later'
};

ModalPublishError.propTypes = {
  visible: PropTypes.bool,
  onCancel: PropTypes.func,
  description: PropTypes.string
};

export default ModalPublishError;
