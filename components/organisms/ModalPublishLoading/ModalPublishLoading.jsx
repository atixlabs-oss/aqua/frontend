/**
 * AGUA Copyright (C) 2023  Atix Labs
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import React from 'react';
import PropTypes from 'prop-types';
import { LoadingOutlined } from '@ant-design/icons';
import { Spin } from 'antd';
import TitlePage from 'components/atoms/TitlePage/TitlePage';
import { CoaBaseModal } from '../CoaModals/CoaBaseModal/CoaBaseModal';

const antIcon = <LoadingOutlined style={{ fontSize: '100px' }} />;

const ModalPublishLoading = ({ visible, textTitle, ...rest }) => (
  <CoaBaseModal
    visible={visible}
    maskClosable={false}
    closable={false}
    mask
    footer={null}
    title={<TitlePage centeredText textTitle={textTitle} underlinePosition="none" />}
    withLogo
    {...rest}
  >
    <div className="o-ModalWrapper">
      <div className="o-ModalWrapper__SpinWrapper">
        <Spin className="o-ModalWrapper__Spin" indicator={antIcon} />
      </div>
    </div>
  </CoaBaseModal>
);

ModalPublishLoading.defaultProps = {
  visible: false,
  textTitle: undefined
};

ModalPublishLoading.propTypes = {
  visible: PropTypes.bool,
  textTitle: PropTypes.string
};

export default ModalPublishLoading;
