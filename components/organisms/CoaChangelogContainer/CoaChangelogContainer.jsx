/**
 * AGUA Copyright (C) 2023  Atix Labs
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import React, { useEffect, useState } from 'react';
import { useTranslation } from 'react-i18next';
import PropTypes from 'prop-types';
import { Divider, Icon } from 'antd';
import classNames from 'classnames';
import autoTable from 'jspdf-autotable';
import jsPDF from 'jspdf';

import { CoaTextButton } from 'components/atoms/CoaTextButton/CoaTextButton';
import { getDateAndTime } from 'helpers/utils';
import changelogActions from 'constants/ChangelogActions';
import { RubikMedium } from 'components/utils/Rubik-Medium';
import Loading from '../../molecules/Loading/Loading';
import CoaChangelogItem from '../../atoms/ChangelogItem/CoaChangelogItem';
import WindowIcon from 'components/atoms/CustomIcons/WindowIcon/WindowIcon';
import { CoaLink } from 'components/atoms/CoaLink/CoaLink';
import { useGetChangelogQuery } from 'api/react-query/changelog/changelogQueries';
import { useRouter } from 'next/router';

export const CoaChangelogContainer = ({
  title,
  projectId,
  milestoneId,
  activityId,
  revisionId,
  evidenceId,
  userId,
  emptyText,
  withInfinityHeight,
  currency,
  withViewMoreDetailsLink,
  isRequestEnabled
}) => {
  const [processedChangeLogs, setProcessedChangeLogs] = useState([]);
  const router = useRouter();
  const { t } = useTranslation();
  const { preview } = router.query;

  const { data: changeLogs, isFetching } = useGetChangelogQuery({
    projectId,
    params: {
      milestoneId,
      activityId,
      revisionId,
      evidenceId,
      userId
    },
    enabled: isRequestEnabled
  });

  useEffect(() => {
    const _processedChangeLogs = changeLogs?.map(changelog => [
      getDateAndTime(changelog?.datetime, 'minimal'),
      changelogActions(changelog, t)?.[changelog?.action]?.titleText,
      changelogActions(changelog, t)?.[changelog?.action]?.descriptionText,
      changelog?.transaction,
      changelog?.revision
    ]);
    setProcessedChangeLogs(_processedChangeLogs);
  }, [changeLogs, t]);

  async function generatePDF() {
    const doc = new jsPDF();

    const imageUrl = process.env.NEXT_PUBLIC_LARGE_LOGO_289x38_PNG_PATH;
    doc.addFileToVFS('Rubik-Regular.ttf', RubikMedium);
    doc.addFont('Rubik-Regular.ttf', 'Rubik', 'medium');

    const imgWidth = 80;
    const imgHeight = 10;
    const pageWidth = doc.internal.pageSize.width;

    const x = (pageWidth - imgWidth) / 2 + 5;

    doc.addImage(imageUrl, 'png', x, 5, imgWidth, imgHeight);

    doc
      .setFont('Rubik', 'medium')
      .setFontSize(16)
      .setTextColor(process.env.NEXT_PUBLIC_PRIMARY_COLOR || '#4C7FF7');
    doc.text(t('changelog.blockchainChangelog'), 10, 27);

    autoTable(doc, {
      startY: 33,
      margin: { horizontal: 10 },
      head: [
        [
          t('general.date'),
          t('general.title'),
          t('general.description'),
          t('general.transaction'),
          t('general.revision')
        ]
      ],
      body: processedChangeLogs,
      rowPageBreak: 'avoid',
      headStyles: {
        fillColor: [241, 243, 255],
        textColor: [82, 91, 107],
        halign: 'center'
      },
      bodyStyles: {
        fillColor: [241, 243, 255]
      },
      alternateRowStyles: { fillColor: [255, 255, 255] },
      columnStyles: {
        0: { halign: 'center', cellWidth: 30 },
        1: { cellWidth: 40 },
        2: { minCellWidth: 40 },
        3: { cellWidth: 30 },
        4: { halign: 'center', cellWidth: 20 }
      }
    });

    doc.save(`${process.env.NEXT_PUBLIC_ORGANIZATION_NAME} - changelog.pdf`);
  }

  return (
    <Loading spinning={isFetching}>
      <div
        className={classNames('o-coaChangelogContainer', {
          '--infinityHeight': withInfinityHeight
        })}
      >
        <div className="o-coaChangelogContainer__header">
          <div className="o-coaChangelogContainer__header__left">
            <h3>{title}</h3>
            {withViewMoreDetailsLink && (
              <CoaLink
                variant="primary"
                href={{
                  pathname: '/[projectId]/changelog',
                  query: { projectId, preview: preview ?? null }
                }}
              >
                {t('changelog.viewMoreDetails')}
              </CoaLink>
            )}
          </div>
          <CoaTextButton onClick={generatePDF} variant="primary">
            <Icon type="download" />
            {t('general.btnDownload')}
          </CoaTextButton>
        </div>
        <Divider type="horizontal" />

        <div
          className={classNames('o-coaChangelogContainer__body', {
            '--empty': changeLogs.length === 0
          })}
        >
          {changeLogs.length === 0 && (
            <>
              <WindowIcon />
              <p className="o-coaChangelogContainer__emptyText">
                {t('changelog.empty') || emptyText}
              </p>
            </>
          )}
          {changeLogs.length > 0 &&
            changeLogs.map(changelog => (
              <CoaChangelogItem changelog={changelog} key={changelog.id} currency={currency} />
            ))}
        </div>
      </div>
    </Loading>
  );
};

CoaChangelogContainer.displayName = 'CoaChangelogContainer';

CoaChangelogContainer.defaultProps = {
  title: 'Project Changelog',
  milestoneId: undefined,
  activityId: undefined,
  revisionId: undefined,
  evidenceId: undefined,
  userId: undefined,
  emptyText: 'No entries in the changelog yet',
  withInfinityHeight: false,
  currency: undefined,
  isRequestEnabled: true
};

CoaChangelogContainer.propTypes = {
  title: PropTypes.string,
  projectId: PropTypes.string.isRequired,
  milestoneId: PropTypes.string,
  activityId: PropTypes.string,
  revisionId: PropTypes.string,
  evidenceId: PropTypes.string,
  userId: PropTypes.string,
  emptyText: PropTypes.string,
  withInfinityHeight: PropTypes.bool,
  currency: PropTypes.string,
  withViewMoreDetailsLink: PropTypes.bool,
  isRequestEnabled: PropTypes.bool
};
