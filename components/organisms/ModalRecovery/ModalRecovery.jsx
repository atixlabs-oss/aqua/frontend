/**
 * AGUA Copyright (C) 2023  Atix Labs
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import React, { useState } from 'react';
import { useTranslation } from 'react-i18next';
import { Divider } from 'antd';

import TitlePage from '../../atoms/TitlePage/TitlePage';
import { CoaTextButton } from 'components/atoms/CoaTextButton/CoaTextButton';
import { CoaDialogModal } from '../CoaModals/CoaDialogModal/CoaDialogModal';
import { ForgetPasswordForm } from '../ForgetPasswordForm/ForgetPasswordForm';

const ModalRecovery = () => {
  const [visibility, setVisibility] = useState(false);
  const [successfulSent, setSuccessfulSent] = useState(false);
  const { t } = useTranslation();

  const showModal = () => {
    setVisibility(true);
  };

  const handleOk = () => {
    setVisibility(false);
  };

  const handleCancel = () => {
    setVisibility(false);
    setSuccessfulSent(false);
  };

  const handleResend = () => {
    setSuccessfulSent(false);
  };

  return (
    <div data-testid="recovery-password-modal">
      <CoaTextButton variant="primary" onClick={showModal}>
        {t('loginSection.forgot_your_password')}
      </CoaTextButton>
      <CoaDialogModal
        onSave={handleOk}
        visible={visibility}
        onCancel={handleCancel}
        className="ModalLogin"
        width="400"
        footer={null}
      >
        {!successfulSent && (
          <div>
            <TitlePage
              centeredText
              underlinePosition="none"
              textTitle={t('loginSection.recovery_password.title')}
            />
            <ForgetPasswordForm onSuccess={() => setSuccessfulSent(true)} />
            <p>{t('loginSection.recovery_password.we_will_send_you_an_email')}</p>
          </div>
        )}
        {successfulSent && (
          <div>
            <TitlePage textTitle={t('loginSection.recovery_password.emailSent')} />
            <h4>{t('loginSection.recovery_password.check_your_inbox')} </h4>
            <p>{t('loginSection.recovery_password.we_have_send_an_email')}</p>
            <Divider />
            <h4>{t('loginSection.recovery_password.did_not_receive_the_email')}</h4>
            <p>
              {t('loginSection.recovery_password.check_your_spam_folder')}{' '}
              <CoaTextButton variant="primary" onClick={handleResend}>
                {t('loginSection.recovery_password.click_here_to_send_link')}
              </CoaTextButton>
            </p>
          </div>
        )}
      </CoaDialogModal>
    </div>
  );
};

export default ModalRecovery;
