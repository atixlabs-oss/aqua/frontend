/**
 * AGUA Copyright (C) 2023  Atix Labs
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import React from 'react';
import TitlePage from 'components/atoms/TitlePage/TitlePage';

import PropTypes from 'prop-types';
import { ProjectProgressCard } from 'components/molecules/ProjectProgressCard/ProjectProgressCard';
import { Divider, Icon } from 'antd';
import classNames from 'classnames';
import { DocumentIcon } from 'components/atoms/CustomIcons/DocumentIcon/DocumentIcon';
import { BankIcon } from 'components/atoms/CustomIcons/BankIcon';
import { CoaLink } from 'components/atoms/CoaLink/CoaLink';
import { useTranslation } from 'react-i18next';

const HorizontalSmallBlockText = ({ subtitle, content, icon, currencyType, hasAddress }) => {
  const isCryptoProjectWithAddress = (currencyType || '').toLowerCase() === 'crypto' && hasAddress;
  return (
    <div className="horizontalSmallBlockText">
      <div className="horizontalSmallBlockText__subContainer">
        <span className="horizontalSmallBlockText__icon">{icon}</span>{' '}
        <span className="horizontalSmallBlockText__subtitle">{subtitle}</span>
      </div>
      <div
        className={classNames('horizontalSmallBlockText__content', {
          horizontalSmallBlockText__contentWrap: isCryptoProjectWithAddress
        })}
      >
        {isCryptoProjectWithAddress ? (
          <CoaLink
            href={{ pathname: `https://etherscan.io/address/${content}` }}
            target="_blank"
            variant="primary"
          >
            {content}
          </CoaLink>
        ) : (
          content
        )}
      </div>
    </div>
  );
};

const HorizontalBlockText = ({
  title,
  content,
  orderNumber,
  projectType,
  accountInfo,
  currencyType
}) => {
  const { t } = useTranslation();
  return (
    <div className="m-horizontalBlockText">
      <TitlePage
        className="m-horizontalBlockText__title"
        textTitle={title}
        orderNumber={orderNumber}
      />
      <div className="m-horizontalBlockText__content">
        {content}{' '}
        {orderNumber === '01' && (
          <>
            <Divider />
            <HorizontalSmallBlockText
              subtitle={t('general.projectType')}
              content={
                ((projectType || '')[0] || '').toUpperCase() +
                (projectType || '').slice(1).toLowerCase()
              }
              currencyType={currencyType}
              icon={
                <Icon className="m-horizontalBlockText__content__icon" component={DocumentIcon} />
              }
            />
            <Divider />
            <HorizontalSmallBlockText
              subtitle={t('createProject.accountInfo')}
              content={accountInfo}
              currencyType={currencyType}
              icon={<Icon className="m-horizontalBlockText__content__icon" component={BankIcon} />}
              className
              hasAddress
            />
            <Divider />
          </>
        )}
      </div>
    </div>
  );
};

export const ProjectInfoSection = ({
  about,
  mission,
  externalProgressCurrentValue,
  externalProgressTotalValue,
  internalProgressCurrentValue,
  internalProgressTotalValue,
  currency,
  onClickSeeMilestones,
  projectType,
  accountInfo,
  currencyType
}) => {
  const { t } = useTranslation();

  return (
    <div className="o-projectInfoSection">
      <div className="o-projectInfoSection__text">
        <HorizontalBlockText
          title={t('landingInfoSection.about')}
          content={about}
          orderNumber="01"
          projectType={projectType}
          accountInfo={accountInfo}
          currencyType={currencyType}
        />
        <HorizontalBlockText
          title={t('landingInfoSection.missionVision')}
          content={mission}
          orderNumber="02"
        />
      </div>
      <div className="o-projectInfoSection__progressCard">
        <ProjectProgressCard
          externalProgressCurrentValue={externalProgressCurrentValue}
          externalProgressTotalValue={externalProgressTotalValue}
          internalProgressCurrentValue={internalProgressCurrentValue}
          internalProgressTotalValue={internalProgressTotalValue}
          onClickSeeMilestones={onClickSeeMilestones}
          currency={currency}
        />
      </div>
    </div>
  );
};

ProjectInfoSection.propTypes = {
  about: PropTypes.string,
  mission: PropTypes.string,
  currency: PropTypes.string,
  onClickSeeMilestones: PropTypes.func,
  projectType: PropTypes.string,
  accountInfo: PropTypes.string,
  currencyType: PropTypes.string,
  externalProgressCurrentValue: PropTypes.number,
  externalProgressTotalValue: PropTypes.number,
  internalProgressCurrentValue: PropTypes.number,
  internalProgressTotalValue: PropTypes.number
};

ProjectInfoSection.defaultProps = {
  about:
    'Having spent a lot of time volunteering for different charities across East Africa since 2008, Jacqueline got inspired by the many kids she has met over the years to launch Shule. The foundation’s ultimate goal is to bring quality education to boys who are denied this fundamental human right. \n\n Many of the children she meets on the streets of Uganda are very intelligent, despite having little to no access to education due to their family’s economic status. Resulting in their potential often being unfulfilled.',
  mission:
    'Our mission is to empower families + communities through job creation. Each handwoven Yellow Leaf Hammock is created by an artisan from the hill-tribe communities of rural Thailand. \n\n By creating safe, high-wage weaving jobs, we divert families from toxic slash + burn agriculture and end the cycle of debt slavery. \n\n Good jobs empower + transform communities for generations.',
  currency: undefined,
  onClickSeeMilestones: undefined,
  projectType: '',
  accountInfo: '',
  currencyType: '',
  externalProgressCurrentValue: 0,
  externalProgressTotalValue: 0,
  internalProgressCurrentValue: 0,
  internalProgressTotalValue: 0
};

HorizontalBlockText.propTypes = {
  title: PropTypes.string,
  content: PropTypes.string,
  orderNumber: PropTypes.oneOfType([PropTypes.number, PropTypes.string]),
  projectType: PropTypes.string,
  accountInfo: PropTypes.string,
  currencyType: PropTypes.string
};

HorizontalBlockText.defaultProps = {
  title: undefined,
  content: undefined,
  orderNumber: undefined,
  projectType: '',
  accountInfo: '',
  currencyType: ''
};

HorizontalSmallBlockText.propTypes = {
  subtitle: PropTypes.string,
  content: PropTypes.string,
  icon: PropTypes.oneOfType([PropTypes.arrayOf(PropTypes.node), PropTypes.node]),
  currencyType: PropTypes.string,
  hasAddress: PropTypes.bool
};

HorizontalSmallBlockText.defaultProps = {
  subtitle: '',
  content: '',
  icon: undefined,
  currencyType: '',
  hasAddress: false
};
