/**
 * AGUA Copyright (C) 2023  Atix Labs
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import React from 'react';
import { Modal } from 'antd';
import { CoaButton } from 'components/atoms/CoaButton/CoaButton';

class ModalProjectCreated extends React.Component {
  state = { visible: false };

  showModal = () => {
    this.setState({
      visible: true
    });
  };

  handleOk = () => {
    this.setState({
      visible: false
    });
  };

  handleCancel = () => {
    this.setState({
      visible: false
    });
  };

  render() {
    const { visible } = this.state;

    return (
      <div className="ModalProjectCreated">
        <Modal
          closable={false}
          centered
          visible={visible}
          onOk={this.handleOk}
          onCancel={this.handleCancel}
          className="ModalProjectCreated"
          width="400"
          footer={null}
        >
          <img src="/static/images/icon-project-created.svg" alt="ProjectCreated" />

          <h1>Project Created Successfully!</h1>
          <p>Lorem ipsum dolor sit amet concerquetcut</p>
          <CoaButton type="primary" classNameIcon="none">
            Go to my dashboard
          </CoaButton>
        </Modal>
      </div>
    );
  }
}

export default ModalProjectCreated;
