/**
 * AGUA Copyright (C) 2023  Atix Labs
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import TitlePage from 'components/atoms/TitlePage/TitlePage';
import Styles from './profile-content.module.scss';
import { Divider, Icon as AntIcon, Icon } from 'antd';
import { PersonalInformationIcon } from 'components/atoms/CustomIcons/PersonalInformationIcon';
import { SecurityInformationIcon } from 'components/atoms/CustomIcons/SecurityInformationIcon';
import { useGetAddressHistory, useGetProfileQuery } from 'api/react-query/users/usersQueries';
import { processFirstAndLastName } from 'helpers/formatter';
import { ProfileBlockInfo } from '../ProfileBlockInfo/ProfileBlockInfo';
import { ProfileHelperBlock } from '../ProfileHelperBlock/ProfileHelperBlock';
import { ProfileChangePasswordBlockForm } from '../ProfileChangePasswordBlockForm/ProfileChangePasswordBlockForm';
import { useTranslation } from 'react-i18next';
import { CustomTooltip } from 'components/atoms/CustomTooltip/CustomTooltip';
import { CoaButton } from 'components/atoms/CoaButton/CoaButton';
import { CoaTag } from 'components/atoms/CoaTag/CoaTag';
import classNames from 'classnames';
import { ADDRESS_STATUS_ENUM, ADDRESS_STATUS_MAP } from 'model/addressStatus';
import { useRequestPinChangeMutation } from 'api/react-query/users/usersMutations';
import { useRouter } from 'next/router';
import { useContext, useEffect } from 'react';
import { openPinRequestApprovedModal } from '../CoaModals/SuccessModals/successModals';
import { KeyIcon } from 'components/atoms/CustomIcons/KeyIcon';
import { UserContext } from 'components/utils/UserContext';
import { UserAddressesHistory } from '../UserAddressesHistory/UserAddressesHistory';

export const ProfileContent = () => {
  const { t } = useTranslation();
  const router = useRouter();
  const { user } = useContext(UserContext);
  const { data: userData, isFetching: isProfileFetching } = useGetProfileQuery();
  const { data: addressHistory } = useGetAddressHistory();

  const { address, country, email, firstName, lastName, pinStatus } = userData || {};
  const completeName = processFirstAndLastName({ firstName, lastName });
  const pinStatusMapped = ADDRESS_STATUS_MAP[pinStatus];
  const pinStatusName = pinStatusMapped?.name || pinStatus;
  const pinStatusColor = pinStatusMapped?.color;
  const pinStatusTooltip = pinStatusMapped?.tooltip;
  const isPinApproved = pinStatus === ADDRESS_STATUS_ENUM.APPROVED;
  const isPinActive = pinStatus !== ADDRESS_STATUS_ENUM.APPROVED;
  const pinActiveText = isPinActive ? t('profile.pinReadyToUse') : t('profile.pinNotActiveYet');
  const requestPinChangeMutation = useRequestPinChangeMutation();
  const pinButtonActionText =
    pinStatus === ADDRESS_STATUS_ENUM.APPROVED
      ? t('profile.activateNewPin')
      : t('profile.signatureEdit');
  const pinButtonActionFunc =
    pinStatus === ADDRESS_STATUS_ENUM.APPROVED ? handleActivateNewPin : handleRequestNewPin;

  useEffect(() => {
    if (!isProfileFetching && isPinApproved) {
      openPinRequestApprovedModal();
    }
  }, [isPinApproved, isProfileFetching]);

  function handleActivateNewPin() {
    router.push('/secret-key');
  }

  function handleRequestNewPin() {
    requestPinChangeMutation.mutate();
  }

  return (
    <>
      {/* <TitlePage textTitle={t('profile.myProfile')} variant="dark" weight="boldest" /> */}

      <div className={Styles.contentContainer}>
        <section className={Styles.infoSection}>
          <section className={Styles.infoSection__personalInformation}>
            <TitlePage
              textTitle={t('profile.personalInformation')}
              HtmlTag="h2"
              variant="dark"
              underlinePosition="none"
              fontSize="medium-biggest"
            />
            <div className={Styles.blockInfoItemsContainer}>
              <ProfileBlockInfo
                iconName="user"
                title={t('profile.firstNameAndLastName')}
                value={completeName}
              />
              <ProfileBlockInfo iconName="global" title={t('profile.country')} value={country} />
              <ProfileBlockInfo iconName="mail" title={t('profile.email')} value={email} />
            </div>
          </section>

          <section className={Styles.infoSection__securityInformation}>
            <TitlePage
              textTitle={t('profile.securityInformation')}
              HtmlTag="h2"
              variant="dark"
              underlinePosition="none"
              fontSize="medium-biggest"
            />
            <div className={Styles.blockInfoItemsContainer}>
              <ProfileBlockInfo
                iconName="lock"
                title={t('profile.password')}
                value="************"
                editable
                editButtonText={t('profile.passwordEdit')}
                EditContent={ProfileChangePasswordBlockForm}
              />
              <ProfileBlockInfo
                CustomIconComponent={KeyIcon}
                title={
                  <div className={Styles.blockInfoItemsContainer__titleContainer}>
                    {t('profile.signature')}
                    <CustomTooltip title={t('profile.digitalSignatureInfo')} variant="black">
                      <Icon type="question-circle" theme="filled" />
                    </CustomTooltip>
                  </div>
                }
                value={
                  <div className={Styles.blockInfoItemsContainer__valueContainer}>
                    <div className={Styles.blockInfoItemsContainer__valueContainer__key}>
                      {t('profile.publicKey')}: {address}
                    </div>
                    <div className={Styles.blockInfoItemsContainer__valueContainer__state}>
                      {pinActiveText}
                    </div>
                  </div>
                }
                editable={pinStatus !== ADDRESS_STATUS_ENUM.IN_REVIEW}
                LeftButton={
                  pinStatus !== ADDRESS_STATUS_ENUM.IN_REVIEW &&
                  (({ className }) => (
                    <CustomTooltip
                      title={user?.isAdmin && t('pin.theAdminIsNotAllowedToRequestANewPin')}
                    >
                      <CoaButton
                        disabled={user?.isAdmin}
                        type="link"
                        className={classNames(
                          Styles.blockInfoItemsContainer__newPinButton,
                          className
                        )}
                        onClick={pinButtonActionFunc}
                        loading={requestPinChangeMutation.isLoading}
                      >
                        <Icon component={KeyIcon} />
                        {pinButtonActionText}
                      </CoaButton>
                    </CustomTooltip>
                  ))
                }
                RightExtraContent={
                  <CoaTag predefinedColor={pinStatusColor} tooltipTitle={pinStatusTooltip}>
                    {pinStatusName}
                  </CoaTag>
                }
                DownExtraContent={
                  addressHistory?.length > 0 && (
                    <UserAddressesHistory
                      addressHistory={addressHistory}
                      className={Styles.blockInfoItemsContainer__publicKeyCollapse}
                    />
                  )
                }
              />
            </div>
          </section>
        </section>

        <section className={Styles.helperSection}>
          <ProfileHelperBlock
            Icon={
              <AntIcon component={PersonalInformationIcon} className={Styles.helperSection__icon} />
            }
            title={t('profile.personalInformation')}
            description={<p>{t('profile.personalInformationDescription')}</p>}
          />
          <Divider className={Styles.divider} />

          <ProfileHelperBlock
            Icon={
              <AntIcon component={SecurityInformationIcon} className={Styles.helperSection__icon} />
            }
            title={t('profile.securityInformation')}
            description={
              <>
                <p>{t('profile.securityInformationDescription.firstParagraph')}</p>

                <ul className={Styles.helperSection__list}>
                  <li>
                    <p className={Styles.helperSection__list__title}>
                      {t('profile.securityInformationDescription.secondParagraph.title')}
                    </p>
                    <p>{t('profile.securityInformationDescription.secondParagraph.content')}</p>
                  </li>
                  <li>
                    <p className={Styles.helperSection__list__title}>
                      {t('profile.securityInformationDescription.thirdParagraph.title')}
                    </p>
                    <p>{t('profile.securityInformationDescription.thirdParagraph.content')}</p>
                  </li>
                </ul>
              </>
            }
          />
        </section>
      </div>
    </>
  );
};
