/**
 * AGUA Copyright (C) 2023  Atix Labs
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import React, { useContext } from 'react';
import PropTypes from 'prop-types';
import { Layout } from 'antd';
import classNames from 'classnames';

import Navbar from 'components/atoms/Navbar/Navbar';
import SideBar from 'components/organisms/SideBar/SideBar';
import { CoaAlert } from 'components/molecules/CoaAlert/CoaAlert';
import { AlertContext } from 'components/utils/AlertContext';
import TitlePage from 'components/atoms/TitlePage/TitlePage';
import Styles from './back-office-layout.module.scss';
import { CustomHead } from 'components/atoms/CustomHead/CustomHead';
import { useTranslation } from 'react-i18next';

const BackOfficeLayout = ({
  children,
  user,
  project,
  childrenContainerClassName,
  withPredefinedPadding,
  titleText,
  ExtraRightInfoTitle
}) => {
  const { t } = useTranslation();
  const { alertShowed, alertVariant, closeAlert } = useContext(AlertContext);
  return (
    <>
      {titleText && <CustomHead titleText={`${t('general.BackOffice')} - ${titleText}`} />}
      <Layout className={Styles.backOfficeLayout}>
        <Navbar project={project} isProtectedRoute role="navigation" />
        <CoaAlert variant={alertVariant} show={alertShowed} onClose={closeAlert} fixed />
        <Layout className={Styles.mainContent} role="main">
          <SideBar isAdmin={user?.isAdmin} />
          <div
            className={classNames(Styles.mainContent__content, childrenContainerClassName, {
              [Styles['--withSidebar']]: user?.isAdmin,
              [Styles['--withPredefinedPadding']]: withPredefinedPadding
            })}
          >
            {(titleText || ExtraRightInfoTitle) && (
              <div className={Styles.mainContent__content__titleContainer}>
                {<TitlePage textTitle={titleText} variant="dark" weight="boldest" />}
                {ExtraRightInfoTitle}
              </div>
            )}
            {children}
          </div>
        </Layout>
      </Layout>
    </>
  );
};

BackOfficeLayout.defaultProps = {
  user: undefined
};

BackOfficeLayout.propTypes = {
  user: PropTypes.shape({}),
  children: PropTypes.element.isRequired,
  childrenContainerClassName: PropTypes.string,
  withPredefinedPadding: PropTypes.bool,
  titleText: PropTypes.string,
  ExtraRightInfoTitle: PropTypes.elementType
};

export default BackOfficeLayout;
