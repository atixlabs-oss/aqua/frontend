/**
 * AGUA Copyright (C) 2023  Atix Labs
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import React, { useContext, useEffect } from 'react';
import classNames from 'classnames';
import Navbar from 'components/atoms/Navbar/Navbar';
import { CoaAlert } from 'components/molecules/CoaAlert/CoaAlert';
import Footer from 'components/molecules/Footer/Footer';
import { useRouter } from 'next/router';
import { useTranslation } from 'react-i18next';
import { AlertContext } from 'components/utils/AlertContext';
import { useGetProjectQuery } from 'api/react-query/project/projectQueries';
import ProjectHeroSection from 'components/molecules/ProjectHeroSection/ProjectHeroSection';
import { formatCurrencyAtTheBeginning, formatTimeframeValue } from 'helpers/formatter';
import { UserContext } from 'components/utils/UserContext';
import { PROJECT_STATUS_ENUM } from 'model/projectStatus';
import { UiContext } from 'components/utils/UiContext';
import Head from 'next/head';
import { SimpleLandingLayout } from '../SimpleLandingLayout/SimpleLandingLayout';
import Loading from 'components/molecules/Loading/Loading';
import { ProjectsEmptyState } from 'components/organisms/ProjectsEmptyState/ProjectsEmptyState';
import { CustomHead } from 'components/atoms/CustomHead/CustomHead';

export const LandingLayout = ({
  children,
  disappearHeaderInMobile,
  headerAnimation,
  showEditingAlert,
  title
}) => {
  const { t } = useTranslation();
  const router = useRouter();
  const { alertShowed, alertVariant, closeAlert } = useContext(AlertContext);
  const { projectId, preview } = router.query;
  const {
    data: project,
    error: errorGettingProject,
    isFetching: isLoadingProject
  } = useGetProjectQuery({ projectId });
  const { setIsHeaderBig } = useContext(UiContext);

  const {
    status,
    budget,
    inReview,
    revision,
    cloneStatus,
    basicInformation,
    beneficiaryCompleteName,
    details
  } = project || {};
  const { projectName, location, timeframe, timeframeUnit, thumbnailPhoto } =
    basicInformation || {};
  const { currency, legalAgreementFile, projectProposalFile } = details || {};

  const { user } = useContext(UserContext);
  const isAdmin = user?.isAdmin;
  const isProjectDraftAndNotPreview = !isAdmin && project?.status === PROJECT_STATUS_ENUM.DRAFT;
  const projectStatus = project?.status;

  const handleScroll = () => {
    const landingHeader = document.querySelector('.landingLayout__header');
    if (landingHeader) {
      const landingHeaderHeight = landingHeader.offsetHeight;
      const landingHero = document.querySelector('.hero');
      if (window.scrollY > 0 && landingHeaderHeight <= 720) {
        landingHeader.classList.add('scrolledLadingHeader', 'scrolledLadingHeaderAnimate');
        landingHeader.style.setProperty('--topLadingHeader', `${alertShowed ? 120 : 60}px`);
        landingHero.classList.add('scrolledHero', 'scrolledLadingHeaderAnimate');
        setIsHeaderBig(false);
      } else if (window.scrollY === 0 && landingHeaderHeight === 220) {
        landingHeader.classList.remove('scrolledLadingHeader');
        landingHero.classList.remove('scrolledHero');
        setIsHeaderBig(true);
      }
    }
  };
  useEffect(() => {
    if (isProjectDraftAndNotPreview || !projectStatus) return;
    if (headerAnimation) {
      window.addEventListener('scroll', handleScroll);
    } else {
      const landingHero = document.querySelector('.hero');
      landingHero.classList.add('scrolledHero');
      const landingHeader = document.querySelector('.landingLayout__header');
      landingHeader.classList.add('scrolledLadingHeader');
      landingHeader.style.setProperty('--topLadingHeader', `${alertShowed ? 120 : 60}px`);
    }
    return () => window.removeEventListener('scroll', handleScroll);
  }, [headerAnimation, alertShowed, projectStatus, isProjectDraftAndNotPreview]);

  return (
    <>
      {!errorGettingProject && (
        <>
          <CustomHead
            titleText={isLoadingProject || !router.isReady ? t('general.Loading') : projectName}
          />
          <Loading spinning={isLoadingProject || !router.isReady}>
            <div className="landingLayout">
              <Navbar project={project} />
              <CoaAlert
                className="landingLayout__previewInfoMessage"
                variant={alertVariant}
                show={alertShowed}
                onClose={closeAlert}
                fixed
              />
              {!isProjectDraftAndNotPreview ? (
                <>
                  <div
                    role="heading"
                    id="landing-layout-header"
                    className={classNames('landingLayout__header', {
                      '--notShowInMobile': disappearHeaderInMobile
                    })}
                    style={{
                      '--landingHeight': headerAnimation ? '720px' : '220px',
                      backgroundImage: `linear-gradient(180deg, rgba(0, 0, 0, 0) 6.87%, rgba(0, 0, 0, 0.800) 80.6%), url(${process.env.NEXT_PUBLIC_URL_HOST}${thumbnailPhoto})`
                    }}
                  >
                    <div className="landingLayout__header__content">
                      <ProjectHeroSection
                        cloneStatus={cloneStatus}
                        revision={revision}
                        inReview={inReview}
                        title={projectName}
                        status={status}
                        subtitle={process.env.NEXT_PUBLIC_ORGANIZATION_NAME}
                        country={location}
                        beneficiary={beneficiaryCompleteName}
                        timeframe={formatTimeframeValue({ timeframe, timeframeUnit, t })}
                        budget={formatCurrencyAtTheBeginning(currency, budget)}
                        legalAgreementUrl={`${process.env.NEXT_PUBLIC_URL_HOST}${legalAgreementFile}`}
                        projectProposalUrl={`${process.env.NEXT_PUBLIC_URL_HOST}${projectProposalFile}`}
                        blockchainHistoryUrl={
                          preview
                            ? `/${projectId}/changelog?preview=true`
                            : `/${projectId}/changelog`
                        }
                        preview={preview}
                        projectId={projectId}
                      />
                    </div>
                  </div>
                  <div className="landingLayout__body">
                    <div className="landingLayout__body__content">
                      {showEditingAlert ? (
                        <div className="landingLayout__alertEditedProject">
                          {t('general.alertEditing')}
                        </div>
                      ) : null}
                      {children}
                    </div>
                  </div>
                </>
              ) : (
                <div
                  className="landingDraftLayout__header"
                  style={{
                    backgroundImage: `linear-gradient(180deg, rgba(0, 0, 0, 0) 6.87%, rgba(0, 0, 0, 0.800) 80.6%), url(${process.env.NEXT_PUBLIC_URL_HOST}${thumbnailPhoto})`
                  }}
                >
                  <div className="landingDraftLayout__header__content" role="heading">
                    <ProjectHeroSection
                      inReview={inReview}
                      title={projectName}
                      status={status}
                      cloneStatus={cloneStatus}
                      subtitle={process.env.NEXT_PUBLIC_ORGANIZATION_NAME}
                      country={location}
                      beneficiary={beneficiaryCompleteName}
                      timeframe={formatTimeframeValue({ timeframe, timeframeUnit, t })}
                      budget={formatCurrencyAtTheBeginning(currency, budget)}
                      thumbnailPhoto={thumbnailPhoto}
                      revision={revision}
                    />
                  </div>
                </div>
              )}
              <Footer />
            </div>
          </Loading>
        </>
      )}
      {errorGettingProject && (
        <>
          <CustomHead titleText={t('projects.Project not found')} />
          <SimpleLandingLayout>
            <ProjectsEmptyState title={t('projects.Project not found')} />
          </SimpleLandingLayout>
        </>
      )}
    </>
  );
};
