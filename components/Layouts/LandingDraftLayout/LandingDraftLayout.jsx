/**
 * AGUA Copyright (C) 2023  Atix Labs
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import React from 'react';
import Navbar from 'components/atoms/Navbar/Navbar';
import { CoaAlert } from 'components/molecules/CoaAlert/CoaAlert';
import { Icon } from 'antd';

import Footer from 'components/molecules/Footer/Footer';
import { useTranslation } from 'react-i18next';

export const LandingDraftLayout = props => {
  const { header, thumbnailPhoto, project } = props;
  const { t } = useTranslation();
  return (
    <div className="landingDraftLayout">
      <Navbar project={project} />
      <CoaAlert
        Icon={
          <Icon
            type="exclamation-circle"
            theme="filled"
            style={{ fontSize: '1.5rem', color: '#ffffff' }}
          />
        }
        className="landingDraftLayout__previewInfoMessage"
        message={t('alert.theProjectIsInDraftVersion')}
        closable={false}
        show
      />
      <div
        className="landingDraftLayout__header"
        style={{
          backgroundImage: `linear-gradient(180deg, rgba(0, 0, 0, 0) 6.87%, rgba(0, 0, 0, 0.800) 80.6%), url(${process.env.NEXT_PUBLIC_URL_HOST}${thumbnailPhoto})`
        }}
      >
        <div className="landingDraftLayout__header__content" role="heading">
          {header}
        </div>
      </div>
      <Footer />
    </div>
  );
};
