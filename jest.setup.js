/**
 * AGUA Copyright (C) 2023  Atix Labs
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import { setConfig } from 'next/config';
import { publicRuntimeConfig } from './next.config';
import i18n from 'i18next';
import { initReactI18next } from 'react-i18next';
import { setupServer } from 'msw/lib/node';
import { rest } from 'msw';
import { mockedProject } from 'test/mocks/data/project';
import { mockedEvidences } from 'test/mocks/data/evidences';
import { mockedChangelog } from 'test/mocks/data/changelog';

i18n
  // load translation using http -> see /public/locales (i.e. https://github.com/i18next/react-i18next/tree/master/example/react/public/locales)
  // learn more: https://github.com/i18next/i18next-http-backend
  // want your translations to be loaded from a professional CDN? => https://github.com/locize/react-tutorial#step-2---use-the-locize-cdn
  // detect user language
  // learn more: https://github.com/i18next/i18next-browser-languageDetector
  // pass the i18n instance to react-i18next.
  .use(initReactI18next)
  // init i18next
  // for all options read: https://www.i18next.com/overview/configuration-options
  .init({
    fallbackLng: 'en',
    debug: false,
    resources: {
      en: {
        common: require('./locales/en/common.json'),
        termsAndConditions: require('./locales/en/termsAndConditions.json'),
        cookies: require('./locales/en/cookies.json'),
        privacyPolicies: require('./locales/en/privacy_policies.json')
      },
      es: {
        common: require('./locales/es/common.json'),
        termsAndConditions: require('./locales/es/termsAndConditions.json'),
        cookies: require('./locales/es/cookies.json'),
        privacyPolicies: require('./locales/es/privacy_policies.json')
      }
    },
    defaultNS: 'common',
    interpolation: {
      escapeValue: false // not needed for react as it escapes by default
    }
  });

setConfig({ publicRuntimeConfig });

jest.mock('react-google-recaptcha-v3', () => ({
  ...jest.requireActual('react-google-recaptcha-v3'),
  __esModule: true,
  GoogleReCaptchaProvider: ({ children }) => <>{children}</>,
  GoogleReCaptcha: () => <></>
}));

// En el archivo de setupTests.js
HTMLCanvasElement.prototype.getContext = jest.fn(() =>
  // Devuelve un objeto que tenga los métodos necesarios para que tu componente de prueba funcione
  ({
    drawImage: jest.fn(),
    getImageData: jest.fn(() => ({
      data: new Uint8ClampedArray(4),
      height: 1,
      width: 1
    }))
  })
);

require('dotenv').config({ path: '.env.test' });

const server = setupServer(
  rest.get(
    `https://api.coa2.testing.atixlabs.xyz/projects/${mockedProject.draft.withBasicCompleted.id}`,
    (req, res, ctx) => res(ctx.json(mockedProject.draft.withBasicCompleted))
  ),
  rest.get(
    `https://api.coa2.testing.atixlabs.xyz/projects/${mockedProject.draft.stepsUncompleted.id}`,
    (req, res, ctx) => res(ctx.json(mockedProject.draft.stepsUncompleted))
  ),
  rest.get(
    `https://api.coa2.testing.atixlabs.xyz/projects/${mockedProject.draft.stepsCompleted.id}`,
    (req, res, ctx) => res(ctx.json(mockedProject.draft.stepsCompleted))
  ),
  rest.get(
    `https://api.coa2.testing.atixlabs.xyz/projects/${mockedProject.in_progress.id}`,
    (req, res, ctx) => res(ctx.json(mockedProject.in_progress))
  ),
  rest.get(
    `https://api.coa2.testing.atixlabs.xyz/projects/${mockedProject.in_progress.id}/evidences`,
    (req, res, ctx) => res(ctx.json(mockedEvidences.in_progress))
  ),
  rest.get(
    `https://api.coa2.testing.atixlabs.xyz/projects/${mockedProject.in_progress.parent ||
      mockedProject.in_progress.id}/changelog`,
    (req, res, ctx) => res(ctx.json(mockedChangelog.draft.in_progress))
  ),
  rest.get('https://api.coa2.testing.atixlabs.xyz/users/me/wallet', (req, res, ctx) =>
    res(
      ctx.json({
        wallet:
          '{"address":"a351c209ba549d0af5e1145feb2469937e4f5b5d","id":"6ce9c26d-3ca3-4647-945e-5b323cb60401","version":3,"Crypto":{"cipher":"aes-128-ctr","cipherparams":{"iv":"9b40225b9798646e12c91624c1723660"},"ciphertext":"907e45d96389a33c0ecb3b86ee467e130f99b993498a66037123a5afb5231e2a","kdf":"scrypt","kdfparams":{"salt":"2c73292ddb0029e488257afd8d443a36411b77d73643cf2bd60620b1f12fe8bd","n":131072,"dklen":32,"p":1,"r":8},"mac":"9161890386330a85024662b86a711a9bc734d55b4325a100cfb7260dd8914e1e"},"x-ethers":{"client":"ethers.js","gethFilename":"UTC--2023-03-02T16-31-26.0Z--a351c209ba549d0af5e1145feb2469937e4f5b5d","mnemonicCounter":"4aff0ca25325ed4ee5dbeb625aa2df7a","mnemonicCiphertext":"182bf4f97dd79d0588780e22d0afbd0d","path":"m/44\'/60\'/0\'/0/0","locale":"en","version":"0.1"}}'
      })
    )
  )
);

beforeAll(() => server.listen());
afterEach(() => server.resetHandlers());
afterAll(() => server.close());
