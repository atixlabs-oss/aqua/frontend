const fs = require('fs');
const _ = require('lodash');

const sassEnvVariables = ['NEXT_PUBLIC_PRIMARY_COLOR', 'NEXT_PUBLIC_SECONDARY_COLOR'];
const envFilePath = '.env';
const requiredServerFilesPath = '.next/required-server-files.json';

function parseEnvFile(filePath) {
  const envContent = fs.readFileSync(filePath, 'utf8');
  const envLines = envContent.split('\n');
  const envVariables = {};

  for (const line of envLines) {
    const trimmedLine = line.trim();
    if (trimmedLine && !trimmedLine.startsWith('#')) {
      const [key, value] = trimmedLine.split('=');
      envVariables[key] = value;
    }
  }

  return envVariables;
}

function findCurrentSassEnvVariables(envVariables) {
  const sassEnvVariablesWithValue = {};
  sassEnvVariables.forEach(
    sassEnvVariable => (sassEnvVariablesWithValue[sassEnvVariable] = envVariables[sassEnvVariable])
  );
  return sassEnvVariablesWithValue;
}

function parseAdditionalDataString(envString) {
  const lines = envString.split(';\n');
  const outputObject = {};

  lines.forEach(line => {
    let [property, value] = line.split(': ');
    property = property.trim().slice(1);
    if (value) value = value.trim();
    if (property) outputObject[property] = value;
  });

  return outputObject;
}

function findExistingSassEnvVariables() {
  if (fs.existsSync(requiredServerFilesPath)) {
    const fileContent = fs.readFileSync(requiredServerFilesPath, 'utf-8');
    const requiredServerFiles = JSON.parse(fileContent);

    if (
      requiredServerFiles &&
      requiredServerFiles.config &&
      requiredServerFiles.config.sassOptions
    ) {
      const additionalData = requiredServerFiles.config.sassOptions.additionalData;
      return parseAdditionalDataString(additionalData);
    }
  }
}

const existingSassEnvVars = findExistingSassEnvVariables() || {};
const envVariables = parseEnvFile(envFilePath);
const currentSassEnvVars = findCurrentSassEnvVariables(envVariables);

const shouldDoACleanBuild = !_.isEqual(existingSassEnvVars, currentSassEnvVars);
(shouldDoACleanBuild && process.exit(0)) || process.exit(1);
