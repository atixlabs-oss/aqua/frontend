/**
 * AGUA Copyright (C) 2023  Atix Labs
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

module.exports = {
  setupFilesAfterEnv: ['./jest.setup.js'],
  testMatch: ['<rootDir>/test/**/*.js'],
  testPathIgnorePatterns: ['/node_modules/', '/test/mocks/'],
  moduleDirectories: ['node_modules', __dirname],
  moduleNameMapper: {
    '\\.(jpg|jpeg|png|gif|eot|otf|webp|svg|ttf|woff|woff2|mp4|webm|wav|mp3|m4a|aac|oga)$':
      '<rootDir>//test/mocks/files.js',
    '\\.(css|sass|scss)$': '<rootDir>/test/mocks/styles.js'
  },
  collectCoverage: true,
  collectCoverageFrom: ['**/*.{js,jsx}'],
  coveragePathIgnorePatterns: [
    '/node_modules/',
    '/.next/',
    '/api/',
    '/constants',
    '/coverage',
    '/helpers/getTransactionDetail.js',
    '/helpers/proptypes.js',
    '/cypress'
  ],
  testEnvironment: 'jsdom',
  testTimeout: 25000
};
