/**
 * AGUA Copyright (C) 2023  Atix Labs
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import React from 'react';
import 'antd/dist/antd.css';
import { GoogleReCaptchaProvider } from 'react-google-recaptcha-v3';

import '../css/app.scss';
import { UserProvider } from '../components/utils/UserContext';
import withReactRouter from './with-react-router';
import { EvidenceProvider } from '../components/utils/EvidenceContext';
import Head from 'next/head';
import PrivateRoute from 'components/utils/PrivateRoute';
import 'i18n';
import { AlertProvider } from 'components/utils/AlertContext';
import { UiProvider } from 'components/utils/UiContext';
import { ReactQueryProvider } from 'components/utils/ReactQueryProvider';

const MyApp = ({ Component, pageProps }) => {
  const getLayout = Component.getLayout ?? (page => page);

  return (
    <>
      <Head>
        <title>{process.env.NEXT_PUBLIC_ORGANIZATION_NAME}</title>
        <link
          rel="apple-touch-icon"
          sizes="57x57"
          href={process.env.NEXT_PUBLIC_FAVICON_APPLE_57x57_PNG_PATH}
        />
        <link
          rel="apple-touch-icon"
          sizes="60x60"
          href={process.env.NEXT_PUBLIC_FAVICON_APPLE_60x60_PNG_PATH}
        />
        <link
          rel="apple-touch-icon"
          sizes="72x72"
          href={process.env.NEXT_PUBLIC_FAVICON_APPLE_72x72_PNG_PATH}
        />
        <link
          rel="apple-touch-icon"
          sizes="76x76"
          href={process.env.NEXT_PUBLIC_FAVICON_APPLE_76x76_PNG_PATH}
        />
        <link
          rel="apple-touch-icon"
          sizes="114x114"
          href={process.env.NEXT_PUBLIC_FAVICON_APPLE_114x114_PNG_PATH}
        />
        <link
          rel="apple-touch-icon"
          sizes="120x120"
          href={process.env.NEXT_PUBLIC_FAVICON_APPLE_120x120_PNG_PATH}
        />
        <link
          rel="apple-touch-icon"
          sizes="144x144"
          href={process.env.NEXT_PUBLIC_FAVICON_APPLE_144x144_PNG_PATH}
        />
        <link
          rel="apple-touch-icon"
          sizes="152x152"
          href={process.env.NEXT_PUBLIC_FAVICON_APPLE_152x152_PNG_PATH}
        />
        <link
          rel="apple-touch-icon"
          sizes="180x180"
          href={process.env.NEXT_PUBLIC_FAVICON_APPLE_180x180_PNG_PATH}
        />
        <link
          rel="icon"
          type="image/png"
          sizes="192x192"
          href={process.env.NEXT_PUBLIC_FAVICON_ANDROID_192x192_PNG_PATH}
        />
        <link
          rel="icon"
          type="image/png"
          sizes="16x16"
          href={process.env.NEXT_PUBLIC_FAVICON_16x16_PNG_PATH}
        />
        <link
          rel="icon"
          type="image/png"
          sizes="32x32"
          href={process.env.NEXT_PUBLIC_FAVICON_32x32_PNG_PATH}
        />
        <link
          rel="icon"
          type="image/png"
          sizes="96x96"
          href={process.env.NEXT_PUBLIC_FAVICON_96x96_PNG_PATH}
        />
        <link rel="shortcut icon" href={process.env.NEXT_PUBLIC_FAVICON_ICO_PATH} />
        <link rel="manifest" href={process.env.NEXT_PUBLIC_FAVICON_MANIFEST_PATH} />
        <meta name="msapplication-TileColor" content="#ffffff" />
        <meta
          name="msapplication-TileImage"
          content={process.env.NEXT_PUBLIC_FAVICON_MSAPPLICATION_144x144_PNG_PATH}
        />
        <meta name="theme-color" content="#ffffff" />
        <meta
          name="author"
          content={`${process.env.NEXT_PUBLIC_ORGANIZATION_NAME} - powered by Agua`}
        ></meta>
      </Head>
      <GoogleReCaptchaProvider
        reCaptchaKey={process.env.NEXT_PUBLIC_RECAPTCHA_SITE_KEY}
        useEnterprise
        useRecaptchaNet
        scriptProps={{
          async: false, // optional, default to false,
          defer: false, // optional, default to false
          appendTo: 'head', // optional, default to "head", can be "head" or "body",
          nonce: undefined // optional, default undefined
        }}
        container={{
          // optional to render inside custom element
          element: 'reCapchaOwnComponent',
          parameters: {
            badge: 'bottomleft', // optional, default undefined
            theme: 'dark' // optional, default undefined
          }
        }}
      >
        <UiProvider>
          <UserProvider>
            <EvidenceProvider>
              <AlertProvider>
                <ReactQueryProvider>
                  <PrivateRoute>{getLayout(<Component {...pageProps} />)}</PrivateRoute>
                </ReactQueryProvider>
              </AlertProvider>
            </EvidenceProvider>
          </UserProvider>
        </UiProvider>
      </GoogleReCaptchaProvider>
      <div id="reCapchaOwnComponent" />
    </>
  );
};

export default withReactRouter(MyApp);
