/**
 * AGUA Copyright (C) 2023  Atix Labs
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import React, { useContext } from 'react';
import BackgroundLanding from 'components/atoms/BackgroundLanding/BackgroundLanding';
import ProjectBrowser from 'components/organisms/ProjectBrowser/ProjectBrowser';
import { UserContext } from 'components/utils/UserContext';
import { useTranslation } from 'react-i18next';
import BackOfficeLayout from 'components/Layouts/BackOfficeLayout/BackOfficeLayout';
import { useGetUserProjectsQuery } from 'api/react-query/users/usersQueries';

function MyProjects() {
  const { user } = useContext(UserContext);
  const userId = user?.id;
  const { t } = useTranslation();

  const { data: myProjects, isLoading } = useGetUserProjectsQuery({
    userId,
    enabled: Boolean(userId)
  });

  const goToProject = _project => {
    const projectId = _project?.parent || _project?.id;
    window.open(`/${projectId}`, '_blank');
  };

  return (
    <BackgroundLanding>
      <BackOfficeLayout user={user} withPredefinedPadding>
        <ProjectBrowser
          title={t('myProjectsSection.my_projects')}
          userRole={user && user.role}
          projects={myProjects}
          onCardClick={goToProject}
          withDescription
          isLoading={isLoading}
        />
      </BackOfficeLayout>
    </BackgroundLanding>
  );
}

export default MyProjects;
