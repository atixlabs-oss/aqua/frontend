/**
 * AGUA Copyright (C) 2023  Atix Labs
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import React, { useContext, useEffect, useState } from 'react';
import BackgroundLanding from 'components/atoms/BackgroundLanding/BackgroundLanding';
import ModalLogin from 'components/organisms/ModalLogin/ModalLogin';
import Navbar from 'components/atoms/Navbar/Navbar';
import { UserContext } from 'components/utils/UserContext';
import { message } from 'antd';
import { useTranslation } from 'react-i18next';
import { INACTIVE_USER_KEY } from 'constants/constants';

function Landing({ withLoginOpen }) {
  const { t } = useTranslation();
  const [modalOpen, setModalOpen] = useState(false);
  const { isInactiveUser } = useContext(UserContext);

  useEffect(() => {
    if (isInactiveUser) {
      message.warning(t('general.user is not active'));
      sessionStorage.removeItem(INACTIVE_USER_KEY);
    }
  }, [isInactiveUser, t]);

  useEffect(() => {
    setModalOpen(withLoginOpen);
  }, [withLoginOpen]);

  return (
    <BackgroundLanding>
      <Navbar setModalOpen={setModalOpen} />
      <ModalLogin visibility={modalOpen} setVisibility={setModalOpen} />
    </BackgroundLanding>
  );
}

Landing.defaultProps = {
  withLoginOpen: false
};

export default Landing;
