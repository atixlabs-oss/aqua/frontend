/**
 * AGUA Copyright (C) 2023  Atix Labs
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import React, { useCallback, useContext, useEffect } from 'react';
import BackgroundLanding from 'components/atoms/BackgroundLanding/BackgroundLanding';
import { useRouter } from 'next/router';
import Navbar from 'components/atoms/Navbar/Navbar';
import { openSetUpSecretKeyModal } from 'components/organisms/CoaModals/DialogModals/dialogModals';
import { openSetUpSecretKeySuccessModal } from 'components/organisms/CoaModals/SuccessModals/successModals';
import { UserContext } from 'components/utils/UserContext';
import { useSetPinMutation, useSetWalletMutation } from 'api/react-query/users/usersMutations';

function SecretKey() {
  const router = useRouter();
  const { projectId } = router.query;
  const { user } = useContext(UserContext);
  const setPinMutation = useSetPinMutation();
  const setWalletMutation = useSetWalletMutation();

  const redirect = useCallback(() => {
    let route = projectId ? `/${projectId}` : '/';
    if (user?.isAdmin) {
      route = '/back-office/projects';
    }
    router.push(route);
  }, [projectId, router, user?.isAdmin]);

  useEffect(() => {
    openSetUpSecretKeyModal(
      () => openSetUpSecretKeySuccessModal(redirect),
      user,
      setPinMutation,
      setWalletMutation
    );
  }, [redirect, user]);

  return (
    <BackgroundLanding>
      <Navbar setModalOpen={openSetUpSecretKeyModal} />
    </BackgroundLanding>
  );
}

export default SecretKey;
