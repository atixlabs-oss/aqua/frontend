/**
 * AGUA Copyright (C) 2023  Atix Labs
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import React, { useEffect, useState } from 'react';
import { useRouter } from 'next/router';
import { Spin } from 'antd';

import BackgroundLanding from 'components/atoms/BackgroundLanding/BackgroundLanding';
import ModalLogin from 'components/organisms/ModalLogin/ModalLogin';
import ModalChangePasswordSuccess from 'components/organisms/ModalChangePasswordSuccess/ModalChangePasswordSuccess';
import ModalInvalidToken from 'components/organisms/ModalInvalidToken/ModalInvalidToken';
import DynamicFormChangePassword from '../../components/organisms/FormLogin/FormChangePassword';
import { showModalError } from '../../components/utils/Modals';
import Navbar from 'components/atoms/Navbar/Navbar';
import { useGetTokenStatusQuery } from 'api/react-query/users/usersQueries';
import { useResetPasswordMutation } from 'api/react-query/users/usersMutations';

function ResetPassword() {
  const { query } = useRouter();
  const resetPasswordMutation = useResetPasswordMutation();

  const [successfulUpdate, setSuccessfulUpdate] = useState(false);
  const [loading, setLoading] = useState(false);
  const [modalOpen, setModalOpen] = useState(false);
  const [successModalOpen, setSuccessModalOpen] = useState(false);
  const [resetModalOpen, setResetModalOpen] = useState(false);
  const [validToken, setValidToken] = useState(false);
  const [showTokenExpirationModal, setShowTokenExpirationModal] = useState(false);
  const [first, setFirst] = useState(false);
  const { token } = query;
  const { data: tokenStatusData, error: tokenStatusError } = useGetTokenStatusQuery({ token });

  useEffect(() => {
    const _isValid = !tokenStatusData?.expired && !tokenStatusError;
    setValidToken(_isValid);
    setShowTokenExpirationModal(!_isValid);
    setResetModalOpen(_isValid);
  }, [tokenStatusData, tokenStatusError]);

  const updatePassword = async newPassword => {
    const data = { token, password: newPassword };
    setLoading(true);
    try {
      const {
        errors,
        data: { first: _first }
      } = await resetPasswordMutation.mutateAsync(data);
      if (!errors) {
        setSuccessModalOpen(true);
        setSuccessfulUpdate(true);
        setFirst(_first);
      } else {
        showModalError('Error', errors);
      }
    } catch (error) {
      const title = 'Error';
      const content = error.response ? error.response.data.error : error.message;
      showModalError(title, content);
    } finally {
      setLoading(false);
    }
  };

  const renderForm = () => (
    <div>
      {!successfulUpdate && validToken && (
        <div>
          <Spin spinning={loading}>
            <DynamicFormChangePassword
              onSubmit={updatePassword}
              setVisible={setResetModalOpen}
              visible={resetModalOpen}
            />
          </Spin>
        </div>
      )}
    </div>
  );

  return (
    <BackgroundLanding>
      <Navbar setModalOpen={setModalOpen} />
      <ModalLogin visibility={modalOpen} setVisibility={setModalOpen} />
      <div>{renderForm()}</div>
      {showTokenExpirationModal && <ModalInvalidToken />}

      <ModalChangePasswordSuccess
        visible={successModalOpen}
        onCancel={() => setSuccessModalOpen(false)}
        first={first}
      />
    </BackgroundLanding>
  );
}

export default ResetPassword;
