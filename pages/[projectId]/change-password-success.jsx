/**
 * AGUA Copyright (C) 2023  Atix Labs
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import React, { useState } from 'react';
import { Typography } from 'antd';
import LogoWrapper from 'components/atoms/LogoWrapper';
import { useRouter } from 'next/router';
import Navbar from 'components/atoms/Navbar/Navbar';
import { CoaDialogModal } from 'components/organisms/CoaModals/CoaDialogModal/CoaDialogModal';
import { useTranslation } from 'react-i18next';
import BackgroundLanding from 'components/atoms/BackgroundLanding/BackgroundLanding';
import TitlePage from 'components/atoms/TitlePage/TitlePage';

function ChangePasswordSuccess() {
  const [modalOpen, setModalOpen] = useState(false);
  const router = useRouter();
  const { first, projectId } = router.query;
  const { t } = useTranslation();

  const goToLogin = () => router.push(`/${projectId}/login`);
  const goToHomepage = () => router.push('/');

  return (
    <BackgroundLanding>
      <Navbar setModalOpen={setModalOpen} />
      <CoaDialogModal
        buttonsPosition="center"
        withoutCancelButton
        visible
        className={`ChangePasswordSuccess ${first ? 'First' : ''}`}
        okText={t('general.continue')}
        onSave={!first ? goToLogin : goToHomepage}
        withLogo
        closable={false}
        title={
          <TitlePage
            textTitle={t('resetPassword.thanksForYourRegistration')}
            underlinePosition="none"
            centeredText
          />
        }
      >
        <Typography.Paragraph className="ChangePasswordParagraph">
          {projectId
            ? t('resetPassword.yourAccountWasSetSuccessfully')
            : t('resetPassword.useYourNewPasswordToLogin')}
        </Typography.Paragraph>
      </CoaDialogModal>
    </BackgroundLanding>
  );
}

export default ChangePasswordSuccess;
