/**
 * AGUA Copyright (C) 2023  Atix Labs
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import React from 'react';
import { Icon } from 'antd';
import { useRouter } from 'next/router';
import { useTranslation } from 'react-i18next';

import Loading from 'components/molecules/Loading/Loading';
import { CoaTextButton } from 'components/atoms/CoaTextButton/CoaTextButton';
import TitlePage from 'components/atoms/TitlePage/TitlePage';
import { CoaChangelogContainer } from 'components/organisms/CoaChangelogContainer/CoaChangelogContainer';
import { LandingLayout } from 'components/Layouts/LandingLayout/LandingLayout';
import { useGetProjectQuery } from 'api/react-query/project/projectQueries';

export default function ProjectChangeLog() {
  const router = useRouter();
  const { t } = useTranslation();
  const { projectId, preview } = router.query;
  const { data: project, loading } = useGetProjectQuery({ projectId });

  if (loading) return <Loading />;

  const { details } = project || {};

  const { currency } = details || {};

  return (
    <div className="p-projectChangelog__content">
      <CoaTextButton
        className="p-projectChangelog__goBackButton"
        onClick={() =>
          router.push({
            pathname: '/[projectId]',
            query: {
              projectId,
              preview: preview ?? null
            }
          })
        }
      >
        <Icon type="arrow-left" />
        {t('general.btnGoBack')}
      </CoaTextButton>
      <TitlePage textTitle={t('general.blockchainChangelog')} />
      <CoaChangelogContainer
        title={t('changelog.title')}
        projectId={project?.parent || project?.id}
        withInfinityHeight
        currency={currency}
      />
    </div>
  );
}

ProjectChangeLog.getLayout = function getLayout(page) {
  return <LandingLayout disappearHeaderInMobile>{page}</LandingLayout>;
};
