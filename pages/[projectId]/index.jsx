/**
 * AGUA Copyright (C) 2023  Atix Labs
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import React, { useContext, useEffect, useState } from 'react';
import { useRouter } from 'next/router';

import { PROJECT_STATUS_ENUM } from 'model/projectStatus';
import { UserContext } from 'components/utils/UserContext';
import PreviewProject from '../../components/organisms/PreviewProject/PreviewProject';
import { AlertContext } from 'components/utils/AlertContext';
import { ALERT_VARIANTS_ENUM } from 'model/alertVariants';
import { LandingLayout } from 'components/Layouts/LandingLayout/LandingLayout';
import { useGetProjectQuery } from 'api/react-query/project/projectQueries';

const Preview = () => {
  const router = useRouter();
  const { projectId, preview } = router.query;

  const { user } = useContext(UserContext);
  const { setAlertVariant } = useContext(AlertContext);
  const { data: project = {}, isLoading, errorGettingProject } = useGetProjectQuery({ projectId });

  const isAdmin = user?.isAdmin;

  const [milestones, setMilestones] = useState([]);

  useEffect(() => {
    if (!project?.milestones) return;
    const _milestones = project?.milestones;
    setMilestones([..._milestones]);
  }, [project]);

  const isProjectDraftAndNotPreview = !isAdmin && project?.status === PROJECT_STATUS_ENUM.DRAFT;

  useEffect(() => {
    if (isProjectDraftAndNotPreview) setAlertVariant(ALERT_VARIANTS_ENUM.DRAFT_PROJECT);
    return () => setAlertVariant(null);
  }, [isProjectDraftAndNotPreview, setAlertVariant]);

  return (
    <>
      {!isProjectDraftAndNotPreview && !errorGettingProject && (
        <PreviewProject
          id={projectId}
          preview={Boolean(preview)}
          project={project}
          milestones={milestones}
          loading={isLoading}
          isAdmin={isAdmin}
          setMilestones={setMilestones}
        />
      )}
    </>
  );
};

Preview.getLayout = function getLayout(page) {
  return <LandingLayout headerAnimation>{page}</LandingLayout>;
};

export default Preview;
