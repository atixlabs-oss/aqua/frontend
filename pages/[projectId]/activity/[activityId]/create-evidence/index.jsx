/**
 * AGUA Copyright (C) 2023  Atix Labs
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import React from 'react';

import { LandingLayout } from 'components/Layouts/LandingLayout/LandingLayout';
import { EvidenceForm } from 'components/molecules/EvidenceForm/EvidenceForm';
import Loading from 'components/molecules/Loading/Loading';
import { useRouter } from 'next/router';
import { useGetProjectQuery } from 'api/react-query/project/projectQueries';

const CreateEvidence = () => {
  const router = useRouter();
  const { projectId, activityId } = router.query;
  const { data: project = {}, isLoading: isProjectLoading } = useGetProjectQuery({ projectId });

  if (isProjectLoading) return <Loading />;

  const { milestones } = project;

  const milestone =
    milestones.find(({ activities }) =>
      activities.map(_activity => _activity.id).includes(parseInt(activityId, 10))
    ) || {};
  const activity = milestone?.activities.find(
    _activity => _activity.id === parseInt(activityId, 10)
  );
  const breadCrumbPath = `${milestone?.title} / ${activity?.title} / create-evidence`;

  return <EvidenceForm breadCrumbPath={breadCrumbPath} />;
};

CreateEvidence.getLayout = function getLayout(page) {
  return <LandingLayout disappearHeaderInMobile>{page}</LandingLayout>;
};

export default CreateEvidence;
