/**
 * AGUA Copyright (C) 2023  Atix Labs
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import React from 'react';
import EvidenceDetail from 'components/organisms/EvidenceDetail/EvidenceDetail';
import { LandingLayout } from 'components/Layouts/LandingLayout/LandingLayout';
import Loading from '../../../../../components/molecules/Loading/Loading';
import { useRouter } from 'next/router';
import { useGetProjectQuery } from 'api/react-query/project/projectQueries';
import { useGetEvidenceQuery } from 'api/react-query/evidences/evidencesQueries';

const EvidenceDetailPage = () => {
  const router = useRouter();
  const { projectId, detailEvidenceId, preview } = router.query;
  const { data: project, isFetching: isProjectFetching } = useGetProjectQuery({ projectId });
  const { details, editing } = project || {};
  const { currency, currencyType } = details || {};

  const { data: evidence, isFetching: isEvidenceFetching } = useGetEvidenceQuery({
    evidenceId: detailEvidenceId
  });

  return (
    <>
      {isProjectFetching || isEvidenceFetching ? (
        <Loading />
      ) : (
        <EvidenceDetail
          project={project}
          preview={preview}
          evidence={evidence}
          currency={currency}
          currencyType={currencyType}
          isProjectEditing={editing}
        />
      )}
    </>
  );
};

EvidenceDetailPage.getLayout = function getLayout(page) {
  return <LandingLayout disappearHeaderInMobile>{page}</LandingLayout>;
};

export default EvidenceDetailPage;
