/**
 * AGUA Copyright (C) 2023  Atix Labs
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import React, { useEffect, useRef, useState } from 'react';

// eslint-disable-next-line import/no-named-as-default
import { LandingLayout } from 'components/Layouts/LandingLayout/LandingLayout';
import GoBackButton from 'components/atoms/GoBackButton/GoBackButton';
import Breadcrumb from 'components/atoms/BreadCrumb/BreadCrumb';
import { CoaChangelogContainer } from 'components/organisms/CoaChangelogContainer/CoaChangelogContainer';
import Evidences from '../../../../../components/organisms/Evidences/Evidences';
import Loading from '../../../../../components/molecules/Loading/Loading';
import { useRouter } from 'next/router';
import { useTranslation } from 'react-i18next';
import { useGetProjectQuery } from 'api/react-query/project/projectQueries';
import { useGetActivityEvidencesQuery } from 'api/react-query/activities/activitiesQueries';
import { useQueryClient } from '@tanstack/react-query';

const EvidencesContainer = () => {
  const { t } = useTranslation();
  const router = useRouter();
  const queryClient = useQueryClient();
  const { projectId, activityId, preview } = router.query;
  const ChangelogComponent = useRef();
  const [evidences, setEvidences] = useState([]);
  const [milestone, setMilestone] = useState({});
  const [activity, setActivity] = useState({});
  const { data: project, isLoading: isProjectLoading } = useGetProjectQuery({
    projectId
  });
  const {
    data: evidencesResponse,
    isFetched: isGetEvidencesFetched,
    isFetching: isGetEvidencesFetching
  } = useGetActivityEvidencesQuery({
    activityId
  });

  useEffect(() => {
    if (isGetEvidencesFetched) {
      setEvidences(evidencesResponse.evidences);
      setActivity(evidencesResponse.activity);
      setMilestone(evidencesResponse.milestone);
    }
  }, [isGetEvidencesFetched, evidencesResponse, setEvidences, setActivity, setMilestone]);

  if (isProjectLoading) return <Loading />;

  const { details } = project;
  const { currency } = details || {};

  function getChangelog() {
    queryClient.invalidateQueries(['changelog', projectId, { activityId }]);
  }

  return (
    <>
      <div className="p-evidences__content">
        <div>
          <GoBackButton
            goBackTo={{
              pathname: preview ? '/[projectId]?preview=true' : '/[projectId]',
              query: { projectId }
            }}
          />
          <Breadcrumb
            route={`${milestone?.title} / ${activity?.title} / ${t('general.evidences')}`}
          />
        </div>
        <Evidences
          preview={preview}
          project={project}
          activity={activity}
          evidences={evidences}
          areEvidencesLoading={isGetEvidencesFetching}
          getChangelog={getChangelog}
          getEvidences={() => queryClient.invalidateQueries(['activity', 'evidences', activityId])}
        />
        <CoaChangelogContainer
          title={t('changelog.activity_changelog')}
          emptyText={t('changelog.empty')}
          projectId={project?.parent || project?.id}
          activityId={activityId}
          currency={currency}
          ref={ChangelogComponent}
        />
      </div>
    </>
  );
};

EvidencesContainer.getLayout = function getLayout(page) {
  return <LandingLayout disappearHeaderInMobile>{page}</LandingLayout>;
};

export default EvidencesContainer;
