/**
 * AGUA Copyright (C) 2023  Atix Labs
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/* eslint-disable react-hooks/exhaustive-deps */

import React, { useState, useEffect, useContext } from 'react';
import { Icon, message } from 'antd';
import { AssignProjectUsers } from 'components/organisms/AssignProjectUsers/AssignProjectUsers';
import { FormProjectDetail } from 'components/molecules/FormProjectDetail/FormProjectDetail';
import { FormProjectBasicInformation } from 'components/molecules/FormProjectBasicInformation/FormProjectBasicInformation';
import {
  checkProjectHasAllUsersRoles,
  checkProjectHasAllUsersWithFirstLogin,
  getProjectUsersPerRol
} from 'helpers/modules/projectUsers';
import { CoaMilestonesView } from 'components/organisms/CoaMilestones/CoaMilestonesView/CoaMilestonesView';
import FooterButtons from 'components/organisms/FooterButtons/FooterButtons';
import ModalProjectCreated from 'components/organisms/ModalProjectCreated/ModalProjectCreated';
import { CoaTextButton } from 'components/atoms/CoaTextButton/CoaTextButton';
import { CoaButton } from 'components/atoms/CoaButton/CoaButton';
import ModalConfirmProjectPublish from 'components/organisms/ModalConfirmProjectPublish/ModalConfirmProjectPublish';
import ModalConfirmWithSK from 'components/organisms/ModalConfirmWithSK/ModalConfirmWithSK';
import ModalPublishLoading from 'components/organisms/ModalPublishLoading/ModalPublishLoading';
import ModalPublishSuccess from 'components/organisms/ModalPublishSuccess/ModalPublishSuccess';
import { PROJECT_STATUS_ENUM } from 'model/projectStatus';
import { CLONE_STATUS_ENUM } from 'model/cloneStatus';
import { UserContext } from 'components/utils/UserContext';
import CoaRejectButton from 'components/atoms/CoaRejectButton/CoaRejectButton';
import CoaApproveButton from 'components/atoms/CoaApproveButton/CoaApproveButton';
import BackOfficeLayout from 'components/Layouts/BackOfficeLayout/BackOfficeLayout';
import { signMessage } from 'helpers/blockchain/wallet';
import { checkIsBeneficiaryOrInvestorByProject } from 'helpers/roles';
import Loading from 'components/molecules/Loading/Loading';
import { EDITOR_VARIANT, PROJECT_FORM_NAMES } from '../../../../constants/constants';
import { showModalConfirm } from '../../../../components/utils/Modals';
import CreateProject from '../../../../components/organisms/CreateProject/CreateProject';
import { useRouter } from 'next/router';
import { CoaLink } from 'components/atoms/CoaLink/CoaLink';
import Styles from './_projectEdit.module.scss';
import { useTranslation } from 'react-i18next';
import { useGetProjectQuery } from 'api/react-query/project/projectQueries';
import { useQueryClient } from '@tanstack/react-query';
import { openGenericErrorModal } from 'components/organisms/CoaModals/ErrorModals/errorModals';
import {
  useApproveCloneProjectMutation,
  useCancelReviewMutation,
  useDeleteProjectMutation,
  usePublishProjectMutation,
  useRejectCloneProjectMutation,
  useSendToReviewProjectMutation,
  useSignProjectMutation
} from 'api/react-query/project/projectMutations';

const wizards = {
  main: CreateProject,
  thumbnails: FormProjectBasicInformation,
  details: FormProjectDetail,
  proposal: AssignProjectUsers,
  milestones: CoaMilestonesView
};

const CreateProjectContainer = () => {
  const { user } = useContext(UserContext);
  const { t } = useTranslation();
  const router = useRouter();
  const { projectId } = router.query;
  const [currentWizard, setCurrentWizard] = useState(PROJECT_FORM_NAMES.MAIN);
  const [confirmPublishVisible, setConfirmPublishVisible] = useState(false);

  const [loadingModal, setLoadingModal] = useState({ isVisible: false });
  const [modalConfirmWithSk, setModalConfirmWithSk] = useState({ isVisible: false });
  const [modalSuccess, setModalSuccess] = useState({ isVisible: false });

  const queryClient = useQueryClient();
  const { data: project, isLoading: isLoadingPage } = useGetProjectQuery({ projectId: projectId });
  const signProjectMutation = useSignProjectMutation();
  const rejectCloneProjectMutation = useRejectCloneProjectMutation();
  const approveCloneProjectMutation = useApproveCloneProjectMutation();
  const cancelReviewMutation = useCancelReviewMutation();
  const deleteProjectMutation = useDeleteProjectMutation();
  const publishProjectMutation = usePublishProjectMutation();
  const sendToReviewProjectMutation = useSendToReviewProjectMutation();

  const [completedSteps, setCompletedSteps] = useState({
    thumbnails: false,
    details: false,
    proposal: false,
    milestones: false
  });

  const isACloneBeingEdited = project?.editing;

  const cloneStatus = project?.cloneStatus;
  const isACloneInReview = project?.cloneStatus === CLONE_STATUS_ENUM.IN_REVIEW;
  const { isAdmin } = user;
  const isApprovalRejectionAvailable = isAdmin && isACloneInReview;

  const isBeneficiaryOrInvestor = checkIsBeneficiaryOrInvestorByProject({ project, user });

  const editorVariant = isACloneBeingEdited
    ? EDITOR_VARIANT.EDITING_CLONE
    : EDITOR_VARIANT.FIRST_EDITING;

  const checkStepsStatus = projectToCheck => {
    const { details = {}, basicInformation = {}, users = [], milestones = [] } =
      projectToCheck || {};

    const { beneficiaries = [], investors = [], auditors = [] } = getProjectUsersPerRol(users);

    const projectHasAllUsersRoles = checkProjectHasAllUsersRoles({
      beneficiaries,
      investors,
      auditors
    });

    const projectHasAllUsersWithFirstLogin = checkProjectHasAllUsersWithFirstLogin({
      beneficiaries,
      investors,
      auditors
    });

    const hasAMilestoneWithActivity = milestones?.some(
      milestone => milestone?.activities?.length > 0
    );

    const stepsStatus = {
      thumbnails: basicInformation?.location,
      details: details?.mission,
      proposal: projectHasAllUsersRoles && projectHasAllUsersWithFirstLogin,
      milestones: hasAMilestoneWithActivity
    };

    setCompletedSteps(stepsStatus);
  };

  const handleGoBack = async (
    { withUpdate, toProjects } = { withUpdate: false, toProjects: false }
  ) => {
    if (toProjects) return router.push('/back-office/projects');
    if (withUpdate) await queryClient.invalidateQueries(['project', projectId]);
    setCurrentWizard(PROJECT_FORM_NAMES.MAIN);
  };

  const successCallback = async onSubmit => {
    try {
      await onSubmit?.();
      await handleGoBack({ withUpdate: true });
      message.success(t('createProject.saved'));
    } catch (error) {
      if (!error?.formError) errorCallback(error);
    }
  };

  // TODO validate with UX team
  const errorCallback = () => message.error(t('general.errorSavingInfo'));

  const askDeleteConfirmation = () => {
    if (project && project.id) {
      showModalConfirm(
        t('createProject.warning'),
        t('createProject.deleteProjectConfirmation'),
        deleteCurrentProject
      );
    }
  };

  const deleteCurrentProject = async () => {
    if (!project || !project.id) return;
    try {
      await deleteProjectMutation.mutateAsync(project?.id);
      message.success(t('createProject.rightDeleted'));
      goToBackOfficeProjects();
    } catch {
      message.error(t('createProject.There was an error when trying to delete the project'));
    }
  };

  const askDeleteEditionConfirmation = () => {
    if (!project || !project.id) return;
    showModalConfirm(
      t('createProject.warning'),
      t('createProject.deleteConfirmation'),
      deleteCurrentProjectEdition
    );
  };

  const deleteCurrentProjectEdition = async () => {
    if (!project || !project.id) return;
    try {
      await cancelReviewMutation.mutateAsync(project.id);
      message.success(t('projects.The edition was deleted'));
      router.push(`/${project.parent}`);
    } catch {
      message.error(
        t('createProject.There was an error when trying to delete the project revision')
      );
    }
  };

  const publishProject = async () => {
    setModalConfirmWithSk({ isVisible: false });
    setLoadingModal({ isVisible: true });

    try {
      await publishProjectMutation.mutateAsync(project.id);
      setModalSuccess({
        isVisible: true,
        onCancel: () => setModalSuccess({ ...modalSuccess, isVisible: false }),
        onSave: () => router.push('/back-office/projects'),
        children: (
          <div className={Styles.projectLinkContainer}>
            <CoaLink href={`/${projectId}`} variant="primary">
              {t('createProject.projectLink')}
            </CoaLink>
          </div>
        )
      });
    } catch {
      message.error(t('createProject.An error occurred while publishing the project'));
      openGenericErrorModal();
    } finally {
      setLoadingModal({ isVisible: false });
    }
  };

  const checkFundingAndSpendingComparison = () => {
    const fundingBudgetSum = project?.milestones?.reduce?.(
      (curr, next) => parseFloat(curr) + parseFloat(next?.funding?.budget),
      0
    );
    const spendingBudgetSum = project?.milestones?.reduce?.(
      (curr, next) => parseFloat(curr) + parseFloat(next?.spending?.budget),
      0
    );

    if (fundingBudgetSum !== spendingBudgetSum)
      openGenericErrorModal({
        description: t('createProject.youCannotPublishTheProjectBecauseTheSum')
      });

    const isFundingAndSpendingComparisonOk = fundingBudgetSum === spendingBudgetSum;
    return isFundingAndSpendingComparisonOk;
  };

  const sendToReviewProject = async (_pin, _password, wallet, key) => {
    setModalConfirmWithSk({ isVisible: false });
    setLoadingModal({
      isVisible: true,
      title: t('createProject.sendingToReview')
    });

    try {
      const result = await sendToReviewProjectMutation.mutateAsync(project.id);
      const messageToSign = result?.data?.toSign;
      const authorizationSignature = await signMessage(wallet, messageToSign, key);
      await signProjectMutation.mutateAsync({ authorizationSignature, projectId });

      setModalSuccess({
        isVisible: true,
        onCancel: () => setModalSuccess({ ...modalSuccess, isVisible: false }),
        onSave: () => router.push(`/${project?.parent}`),
        title: t('createProject.ttSent'),
        description: t('createProject.dSent')
      });
    } catch (error) {
      message.error(t('activities.An error occurred while sending the project to review'));
      openGenericErrorModal();
    } finally {
      setLoadingModal({ isVisible: false });
    }
  };

  const approveClonedProject = async () => {
    setModalConfirmWithSk({ isVisible: false });
    setLoadingModal({ isVisible: true });

    try {
      await approveCloneProjectMutation.mutateAsync(project.id);
      setModalSuccess({
        isVisible: true,
        onCancel: () => {
          router.push(`/back-office/projects/edit/${project?.parent}`);
          setModalSuccess({ ...modalSuccess, isVisible: false });
        },
        onSave: () => {
          router.push(`/back-office/projects/edit/${project?.parent}`);
          setModalSuccess({ ...modalSuccess, isVisible: false });
        },
        title: project?.isCancelRequested
          ? t('createProject.requestChanges.modalSuccess.cancelTitle')
          : t('createProject.requestChanges.modalSuccess.title'),
        description: project?.isCancelRequested
          ? t('createProject.requestChanges.modalSuccess.cancelDescription')
          : t('createProject.requestChanges.modalSuccess.description')
      });
    } catch {
      message.error(t('activities.An error occurred while trying to approve the revision'));
      openGenericErrorModal();
    } finally {
      setLoadingModal({ isVisible: false });
    }
  };

  const rejectClonedProject = async () => {
    setModalConfirmWithSk({ isVisible: false });
    setLoadingModal({ isVisible: true });

    try {
      await rejectCloneProjectMutation.mutateAsync(project.id);
      setModalSuccess({
        isVisible: true,
        onCancel: () => {
          router.push(`/back-office/projects/edit/${project?.parent}`);
          setModalSuccess({ ...modalSuccess, isVisible: false });
        },
        onSave: () => {
          router.push(`/back-office/projects/edit/${project?.parent}`);
          setModalSuccess({ ...modalSuccess, isVisible: false });
        },
        title: t('createProject.ttRejected'),
        description: t('createProject.dRejected')
      });
    } catch {
      message.error(t('createProject.An error occurred while trying to reject the revision'));
      openGenericErrorModal();
    } finally {
      setLoadingModal({ isVisible: false });
    }
  };

  const goToBackOfficeProjects = () => router.push('/back-office/projects');
  const goToParentProject = () => router.push(`/${project?.parent}`);

  useEffect(() => {
    checkStepsStatus(project);
    const mustSignBeneficiaryOrInvestor =
      project?.step === 1 &&
      isBeneficiaryOrInvestor &&
      project?.status === PROJECT_STATUS_ENUM.IN_REVIEW;
    setModalConfirmWithSk({
      isVisible: mustSignBeneficiaryOrInvestor,
      title: t('modalConfirmWithSK.youAreAboutToSignTheProjectToFinishTheProcess'),
      description: t('modalConfirmWithSK.confirmDescription'),
      okText: t('modalConfirmWithSK.sign'),
      onSuccess: signProjectInStepOne,
      cancelText: t('general.btnGoBack'),
      onCancel: () => router.push(`/${projectId}`)
    });
  }, [project]);

  const CurrentComponent = wizards[currentWizard];
  const props = {};

  if (currentWizard === PROJECT_FORM_NAMES.MAIN) props.completedSteps = completedSteps;

  const status = project?.status;

  const isMainWizardActive = currentWizard === PROJECT_FORM_NAMES.MAIN;

  const EDITING_LOGIC = {
    FIRST_EDITING: {
      finishButtonDisabled:
        ![PROJECT_STATUS_ENUM.DRAFT, PROJECT_STATUS_ENUM.IN_REVIEW].includes(status) ||
        (isMainWizardActive && Object.values(completedSteps).some(completed => !completed)),
      nextStepButtonDisabled: ![PROJECT_STATUS_ENUM.DRAFT, PROJECT_STATUS_ENUM.IN_REVIEW].includes(
        status
      ),
      prevStepButtonDisabled: ![PROJECT_STATUS_ENUM.DRAFT, PROJECT_STATUS_ENUM.IN_REVIEW].includes(
        status
      ),
      prevButtonText: isMainWizardActive
        ? `${t('createProject.deleteProject')}`
        : `${t('general.back')}`,
      prevButtonOnClick: isMainWizardActive
        ? askDeleteConfirmation
        : () => handleGoBack({ withUpdate: true }),
      nextStepButtonText: isMainWizardActive
        ? `${t('createProject.publishProject')}`
        : `${t('createProject.saveAndContinue')}`,
      prevButtonIcon: isMainWizardActive ? 'delete' : 'arrow-left'
    },
    EDITING_CLONE: {
      finishButtonDisabled:
        ![PROJECT_STATUS_ENUM.OPEN_REVIEW].includes(status) ||
        (isMainWizardActive && Object.values(completedSteps).some(completed => !completed)),
      nextStepButtonDisabled: ![PROJECT_STATUS_ENUM.OPEN_REVIEW].includes(status),
      prevStepButtonDisabled:
        (!isAdmin && ![CLONE_STATUS_ENUM.OPEN_REVIEW].includes(cloneStatus)) ||
        (isAdmin &&
          [
            CLONE_STATUS_ENUM.PENDING_APPROVE_REVIEW,
            CLONE_STATUS_ENUM.PENDING_CANCEL_REVIEW,
            CLONE_STATUS_ENUM.PENDING_REVIEW
          ].includes(cloneStatus)),
      prevButtonText:
        isMainWizardActive && isACloneBeingEdited && !isApprovalRejectionAvailable
          ? `${t('createProject.deleteEdition')}`
          : `${t('general.back') || 'Back'}`,
      prevButtonOnClick:
        isMainWizardActive && isACloneBeingEdited && !isApprovalRejectionAvailable
          ? askDeleteEditionConfirmation
          : () => handleGoBack({ withUpdate: true, toProjects: isMainWizardActive }),
      nextStepButtonText: isMainWizardActive
        ? `${t('createProject.sendToReview')}`
        : `${t('createProject.saveAndContinue')}`,
      prevButtonIcon:
        isMainWizardActive && isACloneBeingEdited && !isApprovalRejectionAvailable
          ? 'delete'
          : 'arrow-left'
    }
  };

  const getFinishButton = onSubmit => {
    const handleFinishEdit = isACloneBeingEdited
      ? () => {
          const isFundingAndSpendingComparisonOk = checkFundingAndSpendingComparison();
          if (!isFundingAndSpendingComparisonOk) return;

          setModalConfirmWithSk({
            isVisible: true,
            onCancel: () => setModalConfirmWithSk({ isVisible: false }),
            onSuccess: sendToReviewProject,
            title: t('createProject.ttSend'),
            description: t('createProject.dConfirmation'),
            okText: t('general.confirm'),
            cancelText: t('general.btnCancel')
          });
        }
      : () => setConfirmPublishVisible(true);

    if (isApprovalRejectionAvailable) {
      const modalConfirmWithSkTitle = project?.isCancelRequested
        ? t('createProject.requestChanges.modalConfirmWithSK.approveCancelTitle')
        : t('createProject.requestChanges.modalConfirmWithSK.approveTitle');

      return (
        <CoaApproveButton
          disabled={
            project?.step === 1 ||
            [
              CLONE_STATUS_ENUM.PENDING_APPROVE_REVIEW,
              CLONE_STATUS_ENUM.PENDING_CANCEL_REVIEW
            ].includes(project?.cloneStatus)
          }
          onClick={() => {
            setModalConfirmWithSk({
              isVisible: true,
              onCancel: () =>
                setModalConfirmWithSk({
                  ...modalConfirmWithSk,
                  isVisible: false
                }),
              onSuccess: approveClonedProject,
              title: modalConfirmWithSkTitle,
              description: t('createProject.dConfirmation'),
              okText: t('general.btnApprove'),
              cancelText: t('general.btnCancel')
            });
          }}
        >
          {project?.isCancelRequested ? t('general.btnApproveCancel') : t('general.btnApprove')}
        </CoaApproveButton>
      );
    }

    return (
      <CoaButton
        type="primary"
        onClick={() => (isMainWizardActive ? handleFinishEdit() : successCallback(onSubmit))}
        disabled={EDITING_LOGIC?.[editorVariant]?.finishButtonDisabled}
      >
        {EDITING_LOGIC?.[editorVariant]?.nextStepButtonText}
        <Icon type="arrow-right" />
      </CoaButton>
    );
  };

  const getContinueLaterButton = () => {
    if (!isMainWizardActive) return;

    if (isApprovalRejectionAvailable)
      return (
        <CoaRejectButton
          disabled={
            project?.step === 1 ||
            [
              CLONE_STATUS_ENUM.PENDING_APPROVE_REVIEW,
              CLONE_STATUS_ENUM.PENDING_CANCEL_REVIEW
            ].includes(project?.cloneStatus)
          }
          onClick={() => {
            setModalConfirmWithSk({
              isVisible: true,
              onCancel: setModalConfirmWithSk({ ...modalConfirmWithSk, isVisible: false }),
              onSuccess: rejectClonedProject,
              title: t('createProject.ttRejectConfirmation'),
              description: t('createProject.dConfirmation'),
              okText: t('general.btnReject'),
              cancelText: t('general.btnCancel'),
              leaveAComment: true
            });
          }}
        >
          {project.isCancelRequested ? t('general.btnRejectCancel') : t('general.btnReject')}
        </CoaRejectButton>
      );

    return (
      <CoaTextButton
        variant="primary"
        disabled={EDITING_LOGIC?.[editorVariant]?.nextStepButtonDisabled}
        onClick={isACloneBeingEdited ? goToParentProject : goToBackOfficeProjects}
      >
        {t('createProject.saveAndContinueLater')}
      </CoaTextButton>
    );
  };

  const getPrevButton = () => (
    <CoaButton
      type="secondary"
      icon={EDITING_LOGIC?.[editorVariant]?.prevButtonIcon}
      onClick={EDITING_LOGIC?.[editorVariant]?.prevButtonOnClick}
      disabled={EDITING_LOGIC?.[editorVariant]?.prevStepButtonDisabled}
    >
      {EDITING_LOGIC?.[editorVariant]?.prevButtonText}
    </CoaButton>
  );

  const signProjectInStepOne = async (_pin, _password, wallet, key) => {
    const messageToSign = project?.toSign;
    if (!messageToSign) {
      message.error(t('modalConfirmWithSK.anErrorOcurredWhileSigningTheProjectSignatureIsMissing'));
      return;
    }
    setModalConfirmWithSk({ ...modalConfirmWithSk, isVisible: false });
    setLoadingModal({
      isVisible: true,
      title: t('createProject.sendingToReview')
    });
    try {
      const authorizationSignature = await signMessage(wallet, messageToSign, key);
      await signProjectMutation.mutateAsync({ authorizationSignature, projectId });
      setLoadingModal({ isVisible: false, ...loadingModal });
    } catch (error) {
      message.error(t('modalConfirmWithSK.anErrorOcurredWhileSigningTheActivity'));
      router.push(`/${projectId}`);
    }
  };

  return (
    <BackOfficeLayout project={project} user={user}>
      {isLoadingPage ? (
        <Loading />
      ) : (
        <div className="p-createProject__container">
          <CurrentComponent
            project={project}
            setCurrentWizard={setCurrentWizard}
            editorVariant={editorVariant}
            isACloneBeingEdited={isACloneBeingEdited}
            Footer={({ errors, onSubmit } = {}) => (
              <FooterButtons
                errors={errors}
                className="p-createProject__footerButtons"
                finishButton={getFinishButton(onSubmit)}
                nextStepButton={getContinueLaterButton()}
                prevStepButton={getPrevButton()}
              >
                <ModalProjectCreated />
              </FooterButtons>
            )}
            {...props}
          />
        </div>
      )}
      <ModalConfirmProjectPublish
        onCancel={() => setConfirmPublishVisible(false)}
        visible={confirmPublishVisible}
        onSuccess={() => {
          setConfirmPublishVisible(false);

          const isFundingAndSpendingComparisonOk = checkFundingAndSpendingComparison();
          if (!isFundingAndSpendingComparisonOk) return;

          setModalConfirmWithSk({
            isVisible: true,
            onCancel: () => setModalConfirmWithSk({ isVisible: true }),
            onSuccess: publishProject
          });
        }}
      />
      <ModalConfirmWithSK
        visible={modalConfirmWithSk?.isVisible}
        onCancel={() => setModalConfirmWithSk({ ...modalConfirmWithSk, isVisible: false })}
        onSuccess={modalConfirmWithSk?.onSuccess}
        title={modalConfirmWithSk?.title}
        description={modalConfirmWithSk?.description}
        okText={modalConfirmWithSk?.okText}
        cancelText={modalConfirmWithSk?.cancelText}
        leaveAComment={modalConfirmWithSk?.leaveAComment}
      />

      <ModalPublishLoading visible={loadingModal?.isVisible} textTitle={loadingModal?.title} />
      <ModalPublishSuccess
        visible={modalSuccess?.isVisible}
        onCancel={modalSuccess?.onCancel}
        onSave={modalSuccess?.onSave}
        textTitle={modalSuccess?.title}
        description={modalSuccess?.description}
      >
        {modalSuccess?.children}
      </ModalPublishSuccess>
    </BackOfficeLayout>
  );
};

export default CreateProjectContainer;
