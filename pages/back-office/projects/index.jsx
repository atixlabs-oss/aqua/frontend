/**
 * AGUA Copyright (C) 2023  Atix Labs
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import React, { useState, useContext } from 'react';

import { isMobile } from 'react-device-detect';
import { PROJECT_STATUS_ENUM } from 'model/projectStatus';
import BackOfficeLayout from 'components/Layouts/BackOfficeLayout/BackOfficeLayout';
import ProjectBrowser from '../../../components/organisms/ProjectBrowser/ProjectBrowser';
import ModalNotCompatible from '../../../components/organisms/ModalNotCompatible/ModalNotCompatible';
import { UserContext } from '../../../components/utils/UserContext';
import { useRouter } from 'next/router';
import { useTranslation } from 'react-i18next';
import { useGetProjectsQuery } from 'api/react-query/project/projectQueries';
import {
  useCreateProjectMutation,
  useUpdateProjectVisibilityMutation
} from 'api/react-query/project/projectMutations';
import { PublishSettingsHelpNote } from 'components/molecules/PublishSettingsHelpNote/PublishSettingsHelpNote';

const MyProjects = () => {
  const router = useRouter();
  const { t } = useTranslation();
  const { data: projects, isLoading } = useGetProjectsQuery();
  const updateProjectVisibilityMutation = useUpdateProjectVisibilityMutation();
  const createProjectMutation = useCreateProjectMutation();

  const context = useContext(UserContext);
  const { setSeenUserModal, user } = context || { setSeenUserModal: () => {} };

  const [visible, setVisible] = useState(!user?.seenModal && isMobile);

  const goToProject = project => {
    const projectId = project.parent || project.id;
    window.open(`/${projectId}`, '_blank');
  };

  const goToProjectEdit = (event, project) => {
    event?.stopPropagation();
    let projectId = project.parent || project.id;
    if (project.status === PROJECT_STATUS_ENUM.IN_REVIEW) {
      projectId = project.id;
    }
    router.push(`/back-office/projects/edit/${projectId}`);
  };

  const goToProjectProgress = () => {
    // TODO: go to project-progress page
  };

  const goToNewProject = async () => {
    const {
      data: { projectId }
    } = await createProjectMutation.mutateAsync();
    router.push(`/back-office/projects/edit/${projectId}`);
  };

  const onClick = () => {
    setSeenUserModal();
    setVisible(false);
  };

  const handleVisibilityChange = async (project, visibility) =>
    updateProjectVisibilityMutation.mutateAsync({
      projectId: project.id,
      visibility
    });

  return (
    <BackOfficeLayout
      user={user}
      withPredefinedPadding
      titleText={t('projectsSection.projects')}
      ExtraRightInfoTitle={<PublishSettingsHelpNote />}
    >
      <ProjectBrowser
        projects={projects}
        onCardClick={goToProject}
        onTagClick={goToProjectProgress}
        onNewProject={goToNewProject}
        withDescription
        isAdmin={user?.isAdmin}
        onClickEdit={goToProjectEdit}
        canExecuteActions
        isLoading={isLoading}
        onVisibilityChange={handleVisibilityChange}
      />
      <ModalNotCompatible isVisible={visible} onClick={onClick} />
    </BackOfficeLayout>
  );
};

export default MyProjects;
