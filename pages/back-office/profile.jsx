import React, { useContext } from 'react';
import { UserContext } from 'components/utils/UserContext';
import BackOfficeLayout from 'components/Layouts/BackOfficeLayout/BackOfficeLayout';
import { ProfileContent } from 'components/organisms/ProfileContent/ProfileContent';
import { useTranslation } from 'react-i18next';

function Profile() {
  const { t } = useTranslation();
  const context = useContext(UserContext);
  const { user } = context;
  return (
    <BackOfficeLayout user={user} withPredefinedPadding titleText={t('profile.myProfile')}>
      <ProfileContent />
    </BackOfficeLayout>
  );
}

export default Profile;
