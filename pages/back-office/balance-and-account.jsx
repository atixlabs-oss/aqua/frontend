import React, { useContext } from 'react';
import { UserContext } from 'components/utils/UserContext';
import BackOfficeLayout from 'components/Layouts/BackOfficeLayout/BackOfficeLayout';
import { useTranslation } from 'react-i18next';

function BalanceAndAccount() {
  const { t } = useTranslation();
  const context = useContext(UserContext);
  const { user } = context;
  return (
    <BackOfficeLayout
      user={user}
      withPredefinedPadding
      titleText={t('balanceAndAccount.title')}
    ></BackOfficeLayout>
  );
}

export default BalanceAndAccount;
