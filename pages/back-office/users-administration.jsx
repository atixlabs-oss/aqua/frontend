/**
 * AGUA Copyright (C) 2023  Atix Labs
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import React, { useContext } from 'react';
import { UserContext } from 'components/utils/UserContext';
import BackOfficeLayout from 'components/Layouts/BackOfficeLayout/BackOfficeLayout';
import { useTranslation } from 'react-i18next';
import { UsersAdministrationContent } from 'components/organisms/UsersAdministrationContent/UsersAdministrationContent';

function UsersAdministration() {
  const { t } = useTranslation();
  const context = useContext(UserContext);
  const { user } = context;
  return (
    <BackOfficeLayout user={user} withPredefinedPadding titleText={t('usersAdministration.title')}>
      <UsersAdministrationContent />
    </BackOfficeLayout>
  );
}

export default UsersAdministration;
