/* eslint-disable react/prop-types */
/* eslint-disable @next/next/no-page-custom-font */
import React, { useEffect, useRef, useState } from 'react';
import Head from 'next/head';
import classNames from 'classnames';
import Image from 'next/image';
import Select from 'react-select';
import { useRouter } from 'next/router';

import {
  getVersionsAndIpfsAuditHash,
  Events,
  getTimestampByBlock,
  getSignerFromTransaction
} from '../../helpers/blockchain/readOnlyConnection';
import _ from 'lodash';
import { Divider } from 'antd';
import { JsonComparer } from 'components/organisms/JsonComparer/JsonComparer';

const emptyImage = (
  <svg width="101" height="91" viewBox="0 0 101 91" fill="none" xmlns="http://www.w3.org/2000/svg">
    <ellipse opacity="0.2" cx="50.5758" cy="86.6322" rx="28.3542" ry="3.97488" fill="#9CA6B8" />
    <path
      d="M31.7586 23.0187C28.4511 30.7865 16.7197 25.5385 11.5676 31.756C-3.93373 42.2946 -3.11829 68.9305 17.2462 72.6769C33.0711 78.6844 48.1372 69.0015 63.4768 68.1297C74.8867 70.7785 90.8439 77.6392 98.6109 64.7525C106.992 48.9627 78.013 49.6396 88.3766 33.1778C102.352 7.25055 40.0339 -19.7335 31.7533 23.0221L31.7586 23.0187Z"
      fill="#9CA6B8"
      fillOpacity="0.2"
    />
    <rect
      x="27.0342"
      y="21.1697"
      width="46.625"
      height="41"
      rx="2"
      stroke="#9CA6B8"
      strokeWidth="4"
    />
    <rect x="28.4092" y="22.5447" width="44.4375" height="10.125" fill="#9CA6B8" />
    <rect x="33.4717" y="37.7322" width="6.75" height="3.22917" rx="1.61458" fill="#9CA6B8" />
    <rect x="44.1592" y="37.7322" width="23.625" height="3.22917" rx="1.61458" fill="#9CA6B8" />
    <rect x="33.4717" y="51.9406" width="6.75" height="3.22917" rx="1.61458" fill="#9CA6B8" />
    <rect x="44.1592" y="51.9406" width="23.625" height="3.22917" rx="1.61458" fill="#9CA6B8" />
    <rect x="33.4717" y="44.8362" width="6.75" height="3.22917" rx="1.61458" fill="#9CA6B8" />
    <rect x="44.1592" y="44.8362" width="18" height="3.22917" rx="1.61458" fill="#9CA6B8" />
  </svg>
);

const IconDown = () => (
  <svg xmlns="http://www.w3.org/2000/svg" width="24" height="25" viewBox="0 0 24 25" fill="none">
    <path d="M11.9998 15.2144L6.99976 10.2394H16.9998L11.9998 15.2144Z" fill="#525B6B" />
  </svg>
);

const IconUp = () => (
  <svg xmlns="http://www.w3.org/2000/svg" width="24" height="25" viewBox="0 0 24 25" fill="none">
    <path d="M12 9.21435L17 14.1894L7 14.1894L12 9.21435Z" fill="#525B6B" />
  </svg>
);

const ArrowRight = () => (
  <svg xmlns="http://www.w3.org/2000/svg" width="21" height="21" viewBox="0 0 21 21" fill="none">
    <path
      d="M17.3118 10.2051L9.93286 3.80078C9.87622 3.75195 9.80396 3.72461 9.72778 3.72461H7.99927C7.85474 3.72461 7.78833 3.9043 7.89771 3.99805L14.7375 9.93555H3.30786C3.22192 9.93555 3.15161 10.0059 3.15161 10.0918V11.2637C3.15161 11.3496 3.22192 11.4199 3.30786 11.4199H14.7356L7.89575 17.3574C7.78638 17.4531 7.85278 17.6309 7.99731 17.6309H9.78442C9.82153 17.6309 9.85864 17.6172 9.88599 17.5918L17.3118 11.1504C17.3794 11.0916 17.4336 11.019 17.4707 10.9374C17.5079 10.8559 17.5271 10.7673 17.5271 10.6777C17.5271 10.5881 17.5079 10.4996 17.4707 10.418C17.4336 10.3365 17.3794 10.2639 17.3118 10.2051Z"
      fill="#728099"
    />
  </svg>
);

const ArrowLeft = () => (
  <svg xmlns="http://www.w3.org/2000/svg" width="21" height="21" viewBox="0 0 21 21" fill="none">
    <path
      d="M3.36719 11.1504L10.7461 17.5547C10.8027 17.6035 10.875 17.6309 10.9512 17.6309L12.6797 17.6309C12.8242 17.6309 12.8906 17.4512 12.7813 17.3574L5.94141 11.4199L17.3711 11.4199C17.457 11.4199 17.5273 11.3496 17.5273 11.2637L17.5273 10.0918C17.5273 10.0059 17.457 9.93555 17.3711 9.93555L5.94336 9.93555L12.7832 3.99805C12.8926 3.90234 12.8262 3.72461 12.6816 3.72461L10.8945 3.72461C10.8574 3.72461 10.8203 3.73828 10.793 3.76367L3.36719 10.2051C3.29957 10.2639 3.24536 10.3365 3.20821 10.418C3.17106 10.4996 3.15183 10.5881 3.15183 10.6777C3.15183 10.7673 3.17106 10.8559 3.20821 10.9374C3.24536 11.019 3.29957 11.0916 3.36719 11.1504Z"
      fill="#728099"
    />
  </svg>
);

const CollapsibleButton = ({
  isCollapsed,
  setIsCollapsed,
  collapsedButtonContent,
  expandedButtonContent,
  className
}) => (
  <button className={className} onClick={setIsCollapsed}>
    {isCollapsed ? collapsedButtonContent : expandedButtonContent}
  </button>
);

const CodeCollapseButton = ({ isCollapsed, setIsCollapsed }) => (
  <CollapsibleButton
    isCollapsed={isCollapsed}
    setIsCollapsed={setIsCollapsed}
    className="audit__collapseButton"
    collapsedButtonContent={
      <>
        <IconDown /> View More
      </>
    }
    expandedButtonContent={
      <>
        <IconUp /> View Less
      </>
    }
  />
);

const AsideCollapseButton = ({ isCollapsed, setIsCollapsed, className }) => (
  <CollapsibleButton
    isCollapsed={isCollapsed}
    setIsCollapsed={setIsCollapsed}
    className={classNames('audit__asideCollapseButton', className)}
    collapsedButtonContent={<ArrowRight />}
    expandedButtonContent={<ArrowLeft />}
  />
);

const EmptyContent = ({ className }) => (
  <div className={classNames(className, 'audit__emptyContentContainer')}>
    {emptyImage}
    <p className="audit__emptyContentContainer__text">No Previous versions to compare</p>
  </div>
);

const AsyncLabelText = ({ isLoading, date, name, signerInfo }) => {
  if (isLoading)
    return (
      <>
        <div className="audit__codeDisplay__titleContainer__loading" /> |{' '}
        <div className="audit__codeDisplay__titleContainer__loading" />
      </>
    );

  if (date && name)
    return (
      <>
        <span className="audit__codeDisplay__title">{name}</span>
        <Divider type="vertical" className="audit__codeDisplay__titleContainer__divider" />
        <span className="audit__codeDisplay__title --date">
          {signerInfo} / {date}
        </span>
      </>
    );

  if (!date) return name;
};

const CodeDisplay = ({
  oldCode,
  newCode,
  className,
  isLoading,
  name,
  date,
  isLabelLoading,
  isVersionToAudit,
  signerInfo
}) => (
  <div className={classNames('audit__codeDisplay', className)}>
    <h3 className="audit__codeDisplay__titleContainer">
      <AsyncLabelText date={date} name={name} isLoading={isLabelLoading} signerInfo={signerInfo} />
    </h3>
    {
      <div className="audit__codeDisplay__code">
        {isLoading && <div className="audit__codeDisplay__loadingContainer" />}
        {((isVersionToAudit && !_.isEmpty(newCode)) ||
          (!isVersionToAudit && !_.isEmpty(oldCode))) && (
          <JsonComparer
            oldData={oldCode}
            newData={newCode}
            comparerType={isVersionToAudit ? 'new' : 'old'}
          />
        )}
        {((isVersionToAudit && _.isEmpty(newCode)) ||
          (!isVersionToAudit && _.isEmpty(oldCode))) && (
          <EmptyContent className="audit__codeDisplay__code --empty" />
        )}
      </div>
    }
  </div>
);

const findRole = (users, publicKey) => {
  const user = users.find(user => user.publicKey === publicKey);
  const role = user?.role;
  return role?.charAt(0).toUpperCase() + role?.slice(1);
};

const getSignerInfo = event => {
  const response = {
    publicKey: '',
    role: ''
  };

  if (event.type === Events.Creation) {
    response.role = 'Admin';
    response.publicKey = null;
  }
  if (event.type === Events.Proposal) {
    response.role = findRole(event.data.users, event.proposer);
    response.publicKey = event.proposer;
  }
  if (event.type === Events.Audit) {
    response.role = 'Admin';
    response.publicKey = null;
  }
  if (event.type === Events.ClaimProposal) {
    response.role = findRole(event.data.users, event.proposer);
    response.publicKey = event.proposer;
  }
  if (event.type === Events.ClaimAudit) {
    response.role = findRole(event.data.users, event.auditor);
    response.publicKey = event.auditor;
  }

  return response;
};

const getLabel = (event, index) => {
  if (event.type === Events.Creation) return `Version ${index} - Project Published`;
  if (event.type === Events.Proposal)
    return `Version ${index} - ${
      event.data.isCancelRequested ? 'Project Cancellation Request' : 'Project Edition Request'
    }`;
  if (event.type === Events.Audit)
    return `Version ${index} - Project ${event.data.isCancelRequested ? 'Cancellation' : 'Edit'} ${
      event.approved ? 'Approved' : 'Rejected'
    }`;
  if (event.type === Events.ClaimProposal)
    return `Version ${index} - Activity In Review - ${event.activityId}`;
  if (event.type === Events.ClaimAudit)
    return `Version ${index} - Activity ${event.approved ? 'Approved' : 'Rejected'} - ${
      event.activityId
    }`;
};

const getNotarizationVersions = async projectId => {
  const events = await getVersionsAndIpfsAuditHash(projectId);
  const options = events.map((e, i) => ({
    ...e,
    label: getLabel(e, events.length - i),
    signerInfo: getSignerInfo(e),
    value: i
  }));
  return options;
};

const getNotarizationVersionData = async (id, options) => {
  const option = options?.find(o => o.value === id);
  return option?.data;
};

const getNotRejectedPreviousVersion = (versionsData, startIndex) => {
  const versionToCompare = versionsData?.[startIndex];
  const versionToCompareData = versionToCompare?.data;
  const versionToCompareRevision = versionToCompareData?.revision;

  let prevVersionOption = versionsData?.[startIndex + 1];
  const prevVersionOptionData = prevVersionOption?.data;
  const prevVersionOptionRevision = prevVersionOptionData?.revision;

  const versionDataSliced = versionsData?.slice(startIndex + 1);

  if (versionToCompareRevision < prevVersionOptionRevision) {
    for (const versionData of versionDataSliced) {
      const revision = versionData?.data?.revision;
      if (revision <= versionToCompareRevision) {
        prevVersionOption = versionData;
        break;
      }
    }
  }

  if (
    /project \w+ request\b/i.test(versionToCompare?.label) &&
    /project \w+ rejected\b/i.test(prevVersionOption?.label)
  ) {
    for (const versionData of versionDataSliced) {
      const revision = versionData?.data?.revision;

      if (revision === versionToCompareRevision - 1) {
        prevVersionOption = versionData;
        break;
      }
    }
  }

  return prevVersionOption;
};

const AuditProject = () => {
  const [asideCollapsed, setAsideCollapsed] = useState(
    window.matchMedia('(max-width: 1380px)').matches
  );

  const router = useRouter();
  const { projectId } = router.query;
  const [versionToAudit, setVersionToAudit] = useState();
  const [previousVersion, setPreviousVersion] = useState();
  const [previousVersionDate, setPreviousVersionDate] = useState('');
  const [currentVersionDate, setCurrentVersionDate] = useState('');
  const [previousSignerInfo, setPreviousSignerInfo] = useState('');
  const [currentSignerInfo, setCurrentSignerInfo] = useState('');
  const [isCodeCollapsed, setIsCodeCollapsed] = useState(true);
  const [isAnyCodeCollapsed, setIsAnyCodeCollapsed] = useState(true);
  const [areOptionsLoading, setAreOptionsLoading] = useState(true);
  const [previousVersionLabel, setPreviousVersionLabel] = useState();
  const [options, setOptions] = useState([]);
  const [selectorCurrentValue, setSelectorCurrentValue] = useState();
  const [isVersionToAuditLoading, setIsVersionToAuditLoading] = useState(true);
  const [isPreviousVersionLoading, setIsPreviousVersionLoading] = useState(true);

  const initialCodeHeight = useRef();

  useEffect(() => {
    const _initialCodeHeight = document.querySelectorAll('.audit__codeDisplay__code')[0]
      ?.clientHeight;
    initialCodeHeight.current = _initialCodeHeight;
  }, []);

  useEffect(() => {
    if (!isVersionToAuditLoading && !isPreviousVersionLoading) {
      const codes = document.querySelectorAll('pre');
      const leftCode = codes[0];
      const rightCode = codes[1];

      const _isAnyCodeCollapsed =
        leftCode?.clientHeight > initialCodeHeight.current ||
        rightCode?.clientHeight > initialCodeHeight.current;

      setIsAnyCodeCollapsed(_isAnyCodeCollapsed);
    }

    if (isVersionToAuditLoading && isPreviousVersionLoading) {
      setIsAnyCodeCollapsed(false);
    }
  }, [isVersionToAuditLoading, isPreviousVersionLoading]);

  useEffect(() => {
    (async () => {
      if (projectId) {
        try {
          const _options = await getNotarizationVersions(projectId);

          if (_options.length === 0) {
            setIsVersionToAuditLoading(false);
            setIsPreviousVersionLoading(false);
            setAreOptionsLoading(false);
          }

          const processedOptions = [..._options.slice(1)];

          const newOptions = [
            {
              ..._options?.[0],
              label: `${_options?.[0].label} (Current Version)`
            },
            ...processedOptions
          ];

          setOptions(newOptions);
          setSelectorCurrentValue(newOptions?.[0]);
          setAreOptionsLoading(false);

          if (newOptions?.[0]) {
            setIsVersionToAuditLoading(true);
            getNotarizationVersionData(newOptions[0]?.value, _options).then(async data => {
              setVersionToAudit(data);
              const _currentVersionDate = await getTimestampByBlock(data?.blockNumber);
              setCurrentVersionDate(_currentVersionDate);
              setCurrentSignerInfo(`
                ${newOptions[0]?.signerInfo?.role} - ${newOptions[0]?.signerInfo.publicKey ??
                (await getSignerFromTransaction(newOptions[0]?.transactionHash))}`);
              setIsVersionToAuditLoading(false);
            });
          }

          const prevVersionOption = getNotRejectedPreviousVersion(newOptions, 0);

          if (prevVersionOption) {
            setIsPreviousVersionLoading(true);
            getNotarizationVersionData(prevVersionOption?.value, _options).then(async data => {
              setPreviousVersion(data);
              setPreviousVersionLabel(`${prevVersionOption?.label} (Previous Version)`);
              const _previousVersionDate = await getTimestampByBlock(data?.blockNumber);
              setPreviousVersionDate(_previousVersionDate);
              setPreviousSignerInfo(
                `${prevVersionOption?.signerInfo?.role} - ${prevVersionOption?.signerInfo
                  .publicKey ??
                  (await getSignerFromTransaction(prevVersionOption?.transactionHash))}`
              );
              setIsPreviousVersionLoading(false);
            });
          } else {
            setIsPreviousVersionLoading(false);
          }
        } catch (e) {
          // eslint-disable-next-line no-console
          console.error(e);
        }
      }
    })();
  }, [projectId]);

  const handleSelectorChange = async valueSelected => {
    setSelectorCurrentValue(valueSelected);
    setIsVersionToAuditLoading(true);
    setIsPreviousVersionLoading(true);
    setPreviousVersionDate('');
    setCurrentVersionDate('');
    setCurrentSignerInfo('');
    setPreviousSignerInfo('');

    const prevVersionOption = getNotRejectedPreviousVersion(options, valueSelected.value);

    const currVersionOption = options?.[valueSelected.value];

    try {
      const toAuditNotarizationVersionData = await getNotarizationVersionData(
        valueSelected?.value,
        options
      );
      setVersionToAudit(toAuditNotarizationVersionData);
      setCurrentVersionDate(await getTimestampByBlock(valueSelected?.blockNumber));
      setCurrentSignerInfo(
        `${currVersionOption.signerInfo?.role} - ${currVersionOption.signerInfo.publicKey ??
          (await getSignerFromTransaction(currVersionOption.transactionHash))}`
      );
    } catch (e) {
    } finally {
      setIsVersionToAuditLoading(false);
    }

    if (prevVersionOption) {
      try {
        const previousNotarizationVersionData = await getNotarizationVersionData(
          prevVersionOption?.value,
          options
        );
        setPreviousVersionLabel(`${prevVersionOption?.label} (Previous Version)`);
        setPreviousVersion(previousNotarizationVersionData);
        setPreviousVersionDate(await getTimestampByBlock(prevVersionOption.blockNumber));
        setPreviousSignerInfo(
          `${prevVersionOption.signerInfo?.role} - ${prevVersionOption.signerInfo.publicKey ??
            (await getSignerFromTransaction(prevVersionOption.transactionHash))}`
        );
      } catch (e) {}
    } else {
      setPreviousVersion(null);
      setPreviousVersionDate('');
    }
    setIsPreviousVersionLoading(false);
  };

  return (
    <>
      <Head>
        <link rel="preconnect" href="https://fonts.googleapis.com" />
        <link rel="preconnect" href="https://fonts.gstatic.com" crossOrigin />
        <link
          href="https://fonts.googleapis.com/css2?family=Roboto+Mono&family=Rubik:ital,wght@0,300;0,400;0,500;0,600;1,300&display=swap"
          rel="stylesheet"
        />
      </Head>
      <header className="audit__header">
        <Image
          src={process.env.NEXT_PUBLIC_LARGE_LOGO_SVG_PATH}
          width={290}
          height={38}
          alt="large-logo"
          className="audit__header__image"
        />
      </header>
      <main className="audit__main">
        <aside
          className={classNames('audit__aside', {
            '--collapsed': asideCollapsed
          })}
        >
          <AsideCollapseButton
            className="audit__aside__asideCollapseButton"
            isCollapsed={asideCollapsed}
            setIsCollapsed={() => setAsideCollapsed(!asideCollapsed)}
          />

          <h1 className="audit__aside__title">Audit Project</h1>
          <p className="audit__aside__description">
            Here you will find all the notarized versions of the Project that you want to audit.
          </p>
          <div className="audit__divider"></div>
          <ol className="audit__stepper">
            <li className="audit__stepper__item">
              <h2 className="audit__stepper__title">Current Version</h2>
              <p className="audit__stepper__description">
                You will find the JSON data from the Current Version of the Project.
              </p>
            </li>
            <li className="audit__stepper__item">
              <h2 className="audit__stepper__title">Select version</h2>
              <p className="audit__stepper__description">
                You can select a Project Version from the selector to compare.
              </p>
            </li>
            <li className="audit__stepper__item">
              <h3 className="audit__stepper__title">Compare version</h3>
              <p className="audit__stepper__description">
                Once you select the version at your right, you will find the previous version to
                compare. You can also check the Merged version at the bottom.
              </p>
            </li>
          </ol>
          <div className="audit__divider"></div>
        </aside>

        <section
          className={classNames('audit__content', {
            '--collapsed': isCodeCollapsed,
            '--asideCollapsed': asideCollapsed
          })}
        >
          <AsideCollapseButton
            isCollapsed={asideCollapsed}
            setIsCollapsed={() => setAsideCollapsed(!asideCollapsed)}
          />

          <div className="audit__content__selectContainer">
            <label htmlFor="my-select" className="audit__content__selectContainer__label">
              Notarization Version
            </label>

            <Select
              inputId="my-select"
              options={options}
              value={selectorCurrentValue}
              isLoading={areOptionsLoading}
              onChange={handleSelectorChange}
            />
          </div>

          <CodeDisplay
            className="--versionToAudit"
            oldCode={previousVersion}
            newCode={versionToAudit}
            isLoading={isVersionToAuditLoading}
            name={selectorCurrentValue?.label || 'Default Version'}
            date={currentVersionDate}
            isLabelLoading={isVersionToAuditLoading}
            isVersionToAudit
            signerInfo={currentSignerInfo}
          />

          <CodeDisplay
            className="--previousVersion"
            oldCode={previousVersion}
            newCode={versionToAudit}
            isLoading={isPreviousVersionLoading}
            name={previousVersionLabel || 'Previous Version'}
            date={previousVersionDate}
            isLabelLoading={isPreviousVersionLoading}
            signerInfo={previousSignerInfo}
          />

          {isAnyCodeCollapsed && (
            <div className="audit__content__collapseCodeButtonContainer">
              <CodeCollapseButton
                isCollapsed={isCodeCollapsed}
                setIsCollapsed={() => setIsCodeCollapsed(!isCodeCollapsed)}
              />
            </div>
          )}

          <div className="audit__content__mergedVersion">
            <h2 className="audit__codeDisplay__title">Merged Version</h2>
            <div className="audit__codeDisplay__code">
              {!isPreviousVersionLoading && !isVersionToAuditLoading && previousVersion && (
                <JsonComparer oldData={previousVersion} newData={versionToAudit} />
              )}
              {!isPreviousVersionLoading && !isVersionToAuditLoading && !previousVersion && (
                <EmptyContent className="audit__content__mergedVersion --empty" />
              )}

              {(isPreviousVersionLoading || isVersionToAuditLoading) && (
                <div className="audit__codeDisplay__loadingContainer" />
              )}
            </div>
          </div>
        </section>
      </main>
    </>
  );
};

export default AuditProject;
