import { useContext } from 'react';
import ProjectBrowser from 'components/organisms/ProjectBrowser/ProjectBrowser';
import { UserContext } from 'components/utils/UserContext';
import { useGetPublicProjectsQuery } from 'api/react-query/project/projectQueries';
import { useRouter } from 'next/router';

function PublicProjects() {
  const router = useRouter();
  const filters = router.query;

  const { user } = useContext(UserContext);
  const { data, isLoading } = useGetPublicProjectsQuery({
    enabled: router.isReady,
    filters
  });

  const handleCardClick = project => window.open(`/${project?.parent || project?.id}`, '_blank');

  return (
    <ProjectBrowser
      userRole={user?.role}
      withDescription
      projects={data}
      onCardClick={handleCardClick}
      isLoading={isLoading}
    />
  );
}

export default PublicProjects;
