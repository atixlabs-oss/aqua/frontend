/**
 * AGUA Copyright (C) 2023  Atix Labs
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/** @type {import('next').NextConfig} */
const nextConfig = {
  reactStrictMode: true,
  swcMinify: false,
  sassOptions: {
    additionalData: `
    $NEXT_PUBLIC_PRIMARY_COLOR: ${process.env.NEXT_PUBLIC_PRIMARY_COLOR};
    $NEXT_PUBLIC_SECONDARY_COLOR: ${process.env.NEXT_PUBLIC_SECONDARY_COLOR};
    `
  }
};

require('dotenv').config();

module.exports = nextConfig;
