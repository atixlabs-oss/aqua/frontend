import { getActivityEvidences } from 'api/activityApi';
import { useQuery } from '@tanstack/react-query';

export const useGetActivityEvidencesQuery = ({ activityId }) => {
  const getActivityEvidencesProcessed = async () => {
    const response = await getActivityEvidences(activityId);
    return response.data;
  };

  return useQuery(['activity', 'evidences', activityId], getActivityEvidencesProcessed, {
    enabled: Boolean(activityId)
  });
};
