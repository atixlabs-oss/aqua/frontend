import { message } from 'antd';
import {
  createActivityEvidence,
  deleteActivity,
  deleteActivityEvidence,
  signActivity,
  updateActivity,
  updateActivityStatus
} from 'api/activityApi';
import { AlertContext } from 'components/utils/AlertContext';
import { ALERT_VARIANTS_ENUM } from 'model/alertVariants';
import { useRouter } from 'next/router';
import { useContext } from 'react';
import { useTranslation } from 'react-i18next';
import { useMutation, useQueryClient } from '@tanstack/react-query';

export const useDeleteActivityEvidenceMutation = ({
  activityId,
  evidenceId,
  urlToRedirectWhenSuccess,
  projectId
}) => {
  const { t } = useTranslation();
  const { setVariantAndShow } = useContext(AlertContext);
  const { push } = useRouter();
  const queryClient = useQueryClient();
  return useMutation(() => deleteActivityEvidence(activityId, evidenceId), {
    onSuccess: () => {
      queryClient.invalidateQueries(['activity', 'evidences', activityId]);
      queryClient.invalidateQueries(['changelog', projectId, { activityId }]);
      if (urlToRedirectWhenSuccess) push(urlToRedirectWhenSuccess);
      setVariantAndShow(ALERT_VARIANTS_ENUM.EVIDENCE_DELETED);
    },
    onError: () =>
      new Promise(async (resolve, reject) => {
        message.error(t('evidences.feedbackDelete.error'));
        reject(t('evidences.feedbackDelete.error'));
      })
  });
};

export const useUpdateActivityMutation = () => {
  const { t } = useTranslation();
  return useMutation(
    ({ activityId, updatedActivity }) => updateActivity(activityId, updatedActivity),
    {
      onError: () => {
        message.error(t('activities.There was an error when trying to update the activity'));
      }
    }
  );
};

export const useCreateActivityEvidenceMutation = () => {
  const { t } = useTranslation();
  return useMutation(({ activityId, data }) => createActivityEvidence(activityId, data), {
    onError: () => {
      message.error(
        t('activities.There was an error when trying to create the evidence of the activity')
      );
    }
  });
};

export const useDeleteActivityMutation = () => {
  const { t } = useTranslation();
  return useMutation(({ activityId }) => deleteActivity(activityId), {
    onError: () => {
      message.error(t('activities.There was an error when trying to delete the activity'));
    }
  });
};

export const useSignActivityMutation = () => {
  const { t } = useTranslation();
  return useMutation(signActivity, {
    onError: () => {
      message.error(t('activities.There was an error when trying to sign the activity'));
    }
  });
};

export const useUpdateActivityStatusMutation = () => {
  const { t } = useTranslation();
  return useMutation(
    ({ activityId, status, txId, reason = '' }) =>
      updateActivityStatus(activityId, status, txId, reason),
    {
      onError: () => {
        message.error(t('activities.There was an error when trying to update the activity status'));
      }
    }
  );
};
