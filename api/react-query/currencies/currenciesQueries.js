import { useQuery } from '@tanstack/react-query';
import { message } from 'antd';

import { getCryptoCurrencies } from 'api/currenciesApi';
import { useTranslation } from 'react-i18next';

const FIAT_CURRENCIES = [
  {
    value: 'USD',
    label: 'USD'
  },
  {
    value: 'EUR',
    label: 'EUR'
  },
  {
    value: 'CHF',
    label: 'CHF'
  },
  {
    value: 'GBP',
    label: 'GBP'
  }
];

export const useGetCurrenciesQuery = ({ enabled } = { enabled: true }) => {
  const { t } = useTranslation();

  const getCountriesProcessed = async () => {
    let currenciesProcessed = await getCryptoCurrencies();

    currenciesProcessed = currenciesProcessed?.data?.tokens?.map(cryptoCurrency => ({
      ...cryptoCurrency,
      value: cryptoCurrency?.symbol,
      label: cryptoCurrency?.symbol
    }));

    return { crypto: [...currenciesProcessed], fiat: FIAT_CURRENCIES };
  };

  const query = useQuery(['currencies'], getCountriesProcessed, {
    enabled,
    onError: () => {
      message.error(t('currencies.There was an error when fetching currencies'));
    }
  });

  return query;
};
