import { getChangelog } from 'api/projectApi';
import { sortArrayByDate } from 'components/utils';
import { CHANGELOG_ACTIONS_ENUM } from 'constants/ChangelogActions';
import { TRANSACTION_STATUS_ENUM } from 'model/transactionStatus';
import { useQuery } from '@tanstack/react-query';

export const useGetChangelogQuery = ({ projectId, params, enabled = true }) => {
  const getChangelogProcessed = async () => {
    const response = await getChangelog(projectId, params);
    const _processedChangelogs = sortArrayByDate(response.data, 'datetime').filter(
      changelog =>
        Object.values(CHANGELOG_ACTIONS_ENUM).includes(changelog?.action) &&
        changelog?.status === TRANSACTION_STATUS_ENUM.CONFIRMED
    );
    return _processedChangelogs;
  };

  const query = useQuery(['changelog', projectId, params], getChangelogProcessed, {
    enabled: enabled && Boolean(projectId),
    initialData: []
  });

  return query;
};
