import { useMutation } from '@tanstack/react-query';
import { useTranslation } from 'react-i18next';
import { message } from 'antd';
import {
  createMilestone,
  createMilestoneActivity,
  deleteMilestone,
  updateMilestone
} from 'api/milestonesApi';

export const useUpdateMilestoneMutation = () => {
  const { t } = useTranslation();
  return useMutation(({ milestoneId, data }) => updateMilestone(milestoneId, data), {
    onError: () => {
      message.error(t('milestones.There was an error when trying to update the milestone'));
    }
  });
};

export const useDeleteMilestoneMutation = () => {
  const { t } = useTranslation();
  return useMutation(({ milestoneId }) => deleteMilestone(milestoneId), {
    onError: () => {
      message.error(t('milestones.There was an error when trying to delete the milestone'));
    }
  });
};

export const useCreateMilestoneMutation = () => {
  const { t } = useTranslation();
  return useMutation(({ projectId, data }) => createMilestone(projectId, data), {
    onError: () => {
      message.error(t('milestones.There was an error when trying to create the milestone'));
    }
  });
};

export const useCreateMilestoneActivityMutation = () => {
  const { t } = useTranslation();
  return useMutation(
    ({ activity, milestoneId }) => createMilestoneActivity(activity, milestoneId),
    {
      onError: () => {
        message.error(
          t('milestones.There was an error when trying to create the activity of the milestone')
        );
      }
    }
  );
};
