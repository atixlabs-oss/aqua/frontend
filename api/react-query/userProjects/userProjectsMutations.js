import { useMutation } from '@tanstack/react-query';
import { message } from 'antd';
import { addUserToProject, removeUserFromProject } from 'api/userProjectApi';
import { useTranslation } from 'react-i18next';

export const useAssignUserToProjectMutation = () => {
  const { t } = useTranslation();

  return useMutation(addUserToProject, {
    onError: () => {
      message.error(t('users.There was an error when trying to assign the user to the project'));
    }
  });
};

export const useRemoveUserFromProjectMutation = () => {
  const { t } = useTranslation();

  return useMutation(removeUserFromProject, {
    onError: () => {
      message.error(t('users.There was an error when trying to remove the user from the project'));
    }
  });
};
