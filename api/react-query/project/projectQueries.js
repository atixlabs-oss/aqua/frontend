import {
  getCashFlow,
  getProject,
  getProjectTransactions,
  getProjects,
  getPublicProjects
} from 'api/projectApi';
import { useQuery } from '@tanstack/react-query';
import { formatCurrencyAtTheBeginning, processFirstAndLastName } from 'helpers/formatter';
import { message } from 'antd';
import { useTranslation } from 'react-i18next';
import { getDateAndTime } from 'helpers/utils';

export const useGetProjectQuery = ({ projectId }) => {
  const getProjectProcessed = async () => {
    const response = await getProject(projectId);
    const data = await response.data;

    const beneficiaryFirstName = data?.basicInformation?.beneficiary?.firstName;
    const beneficiaryLastName = data?.basicInformation?.beneficiary?.lastName;
    const beneficiaryCompleteName = processFirstAndLastName({
      firstName: beneficiaryFirstName,
      lastName: beneficiaryLastName
    });

    return {
      ...data,
      beneficiaryCompleteName
    };
  };

  const query = useQuery(['project', projectId], getProjectProcessed, {
    enabled: Boolean(projectId)
  });

  return query;
};

export const useGetPublicProjectsQuery = ({ filters, enabled } = {}) => {
  const getPublicProjectsProcessed = async () => {
    const response = await getPublicProjects(filters);
    return await response.data;
  };

  return useQuery(['publicProjects', filters], getPublicProjectsProcessed, {
    enabled
  });
};

export const useGetProjectsQuery = ({ enabled } = { enabled: true }) => {
  const getProjectsProcessed = async () => getProjects().then(response => response.data);

  return useQuery(['projects'], getProjectsProcessed, {
    enabled
  });
};

export const useGetCashflow = ({ projectId, currency, enabled } = { enabled: true }) => {
  const { t } = useTranslation();

  const getCashFlowProcessed = async () => {
    const response = await getCashFlow({ projectId });
    return response.data?.movements
      ?.sort((a, b) => {
        const dateA = new Date(a.date);
        const dateB = new Date(b.date);
        return dateA - dateB;
      })
      .map((item, index) => {
        const { type, amount, accountId, date, displayId, otherAccount } = item;
        return {
          date: getDateAndTime(date, 'minimal'),
          type: type || '',
          activityType: '-',
          audited: 'NO',
          displayId,
          currentAmount: formatCurrencyAtTheBeginning(currency, amount),
          accountId: accountId.platformName,
          otherAccount,
          key: `${date}${index}`
        };
      });
  };

  return useQuery(['cashflow', projectId], getCashFlowProcessed, {
    enabled: Boolean(projectId) && Boolean(currency) && enabled,
    onError: () => {
      message.error(
        t('apiErrorMessages.There was an error when trying to get the cashflow of the project')
      );
    }
  });
};

export const useGetProjectTransactionsQuery = (
  { enabled, projectId, type, ...restOptions } = { enabled: true }
) => {
  const { t } = useTranslation();
  const getTransactionsProcessed = async () => {
    const response = await getProjectTransactions(projectId, type);
    return response.data;
  };

  return useQuery(['transactions', { projectId, type }], getTransactionsProcessed, {
    enabled,
    onError: () =>
      message.error(t('projects.There was an error when trying to get the transactions')),
    ...restOptions
  });
};
