import { useMutation, useQueryClient } from '@tanstack/react-query';
import { message } from 'antd';
import {
  approveCloneProject,
  cancelReview,
  cloneProject,
  createProject,
  deleteProject,
  publish,
  putBasicInformation,
  rejectCloneProject,
  sendToReview,
  signProject,
  updateProjectCanceledState,
  updateProjectDetail,
  updateProjectVisibility
} from 'api/projectApi';
import { useTranslation } from 'react-i18next';

export const useUpdateProjectCanceledStateMutation = () => {
  const { t } = useTranslation();
  const queryClient = useQueryClient();

  return useMutation(projectId => updateProjectCanceledState(projectId), {
    onSuccess: projectId => {
      queryClient.invalidateQueries(['project', projectId]);
    },
    onError: () => {
      message.error(t('createProject.cancelProjectConfirmModal.errorMessage'));
    }
  });
};

export const useUpdateProjectVisibilityMutation = () => {
  const { t } = useTranslation();
  const queryClient = useQueryClient();

  return useMutation(
    ({ projectId, visibility }) => updateProjectVisibility({ projectId, visibility }),
    {
      onSuccess: () => {
        queryClient.invalidateQueries(['projects']);
      },
      onError: () => {
        message.error(
          t(
            'apiErrorMessages.There was an error when trying to change the visibility of the project'
          )
        );
      }
    }
  );
};

export const useCreateProjectMutation = () => {
  const { t } = useTranslation();

  return useMutation(createProject, {
    onError: () => {
      message.error(t('apiErrorMessages.There was an error when trying to create a new project'));
    }
  });
};

export const useCloneProjectMutation = () => {
  const { t } = useTranslation();
  const queryClient = useQueryClient();

  return useMutation(projectId => cloneProject(projectId), {
    onSuccess: projectId => {
      queryClient.invalidateQueries(['projects']);
      queryClient.invalidateQueries(['project', projectId]);
    },
    onError: () => {
      message.error(
        t('apiErrorMessages.There was an error when trying to create a new project revision')
      );
    }
  });
};

export const useUpdateBasicInformationMutation = () => {
  const { t } = useTranslation();
  const queryClient = useQueryClient();

  return useMutation(
    async ({ projectId, data }) => {
      await putBasicInformation(projectId, data);
      return projectId;
    },
    {
      onSuccess: projectId => {
        queryClient.invalidateQueries(['projects']);
        queryClient.invalidateQueries(['project', projectId]);
      },
      onError: () => {
        message.error(
          t(
            'projects.There was an error when trying to update the basic information of the project'
          )
        );
      }
    }
  );
};

export const useUpdateProjectDetailsMutation = () => {
  const { t } = useTranslation();
  const queryClient = useQueryClient();

  return useMutation(
    async ({ projectId, data }) => {
      await updateProjectDetail(projectId, data);
      return projectId;
    },
    {
      onSuccess: projectId => {
        queryClient.invalidateQueries(['projects']);
        queryClient.invalidateQueries(['project', projectId]);
      },
      onError: () => {
        message.error(t('projects.There was an error when trying to update the project details'));
      }
    }
  );
};

export const useSignProjectMutation = () => {
  const { t } = useTranslation();

  return useMutation(signProject, {
    onError: () => {
      message.error(t('projects.There was an error when trying to sign the project'));
    }
  });
};

export const useRejectCloneProjectMutation = () => {
  const { t } = useTranslation();

  return useMutation(rejectCloneProject, {
    onError: () => {
      message.error(t('projects.There was an error when trying to reject the project edition'));
    }
  });
};

export const useApproveCloneProjectMutation = () => {
  const { t } = useTranslation();

  return useMutation(approveCloneProject, {
    onError: () => {
      message.error(t('projects.There was an error when trying to approve the project edition'));
    }
  });
};

export const useCancelReviewMutation = () => {
  const { t } = useTranslation();

  return useMutation(cancelReview, {
    onError: () => {
      message.error(t('projects.There was an error when trying to cancel the review'));
    }
  });
};

export const useDeleteProjectMutation = () => {
  const { t } = useTranslation();

  return useMutation(deleteProject, {
    onError: () => {
      message.error(t('projects.There was an error when trying to delete the project'));
    }
  });
};

export const usePublishProjectMutation = () => {
  const { t } = useTranslation();

  return useMutation(publish, {
    onError: () => {
      message.error(t('projects.There was an error when trying to publish the project'));
    }
  });
};

export const useSendToReviewProjectMutation = () => {
  const { t } = useTranslation();

  return useMutation(sendToReview, {
    onError: () => {
      message.error(t('projects.There was an error when trying to send the project to review'));
    }
  });
};
