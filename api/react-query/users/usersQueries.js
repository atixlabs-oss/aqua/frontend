import { useContext } from 'react';
import { useTranslation } from 'react-i18next';
import { useQuery } from '@tanstack/react-query';
import { message } from 'antd';
import {
  getAddressHistory,
  getMyProjects,
  getProfile,
  getTokenStatus,
  getUsers,
  getWallet
} from 'api/userApi';

import { UserContext } from 'components/utils/UserContext';

export const useGetProfileQuery = ({ enabled } = { enabled: true }) => {
  const { user } = useContext(UserContext);
  const getProfileProcessed = async () => {
    const response = await getProfile();
    return response.data;
  };

  return useQuery(['profile'], () => getProfileProcessed(), {
    initialData: {},
    enabled: enabled && !!user
  });
};

export const useGetUserProjectsQuery = ({ userId, enabled } = { enabled: true }) => {
  const { t } = useTranslation();
  const getUserProjectsProcessed = async () => {
    const response = await getMyProjects({ userId });
    return response.data?.projects;
  };

  return useQuery(['projects', { user: 'userId' }], () => getUserProjectsProcessed(), {
    enabled,
    onError: () => message.error(t('general.errorFetchingProject'))
  });
};

export const useGetAddressHistory = ({ enabled } = { enabled: true }) => {
  const getAddressHistoryProcessed = async () => {
    const response = await getAddressHistory();
    return response.data;
  };

  return useQuery(['addressHistory'], getAddressHistoryProcessed, {
    enabled
  });
};

export const useGetUsersQuery = ({ enabled, filters, ...restOptions } = { enabled: true }) => {
  const { t } = useTranslation();
  const getUsersProcessed = async () => {
    const response = await getUsers(filters);
    return response.data;
  };

  return useQuery(['users', filters], getUsersProcessed, {
    enabled,
    onError: () => message.error(t('apiErrors.There was an error when trying to get the users')),
    ...restOptions
  });
};

export const useGetTokenStatusQuery = ({ enabled, token }) => {
  const { t } = useTranslation();

  return useQuery(['tokenStatus', token], () => getTokenStatus(token), {
    enabled: Boolean(token) && enabled,
    onError: () =>
      message.error(t('apiErrors.There was an error when trying to get the token status'))
  });
};

export const useGetWalletQuery = ({ enabled }) => {
  const { t } = useTranslation();

  const getWalletProcessed = async () => {
    const response = await getWallet();
    return response.data;
  };

  return useQuery(['wallet'], getWalletProcessed, {
    enabled,
    onError: () => message.error(t('users.There was an error when trying to get the wallet'))
  });
};
