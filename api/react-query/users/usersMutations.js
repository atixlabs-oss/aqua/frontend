import { useContext } from 'react';
import { useMutation, useQueryClient } from '@tanstack/react-query';
import { message } from 'antd';
import { useTranslation } from 'react-i18next';

import { AlertContext } from 'components/utils/AlertContext';
import {
  approvePinRequest,
  changePassword,
  createUser,
  loginUser,
  recoverPassword,
  rejectPinRequest,
  requestPinChange,
  resetPassword,
  sendWelcomeEmail,
  setPin,
  setWallet,
  toggleActiveStatus
} from 'api/userApi';
import { ALERT_VARIANTS_ENUM } from 'model/alertVariants';
import { openPinRequestSuccessModal } from 'components/organisms/CoaModals/SuccessModals/successModals';

export const useChangePasswordMutation = () => {
  const { t } = useTranslation();
  const { setVariantAndShow } = useContext(AlertContext);
  return useMutation(
    ({ currentPassword, newPassword }) => changePassword({ currentPassword, newPassword }),
    {
      onSuccess: () => {
        setVariantAndShow(ALERT_VARIANTS_ENUM.PASSWORD_CHANGED);
      },
      onError: () =>
        new Promise(async (resolve, reject) => {
          message.error(t('profile.changePasswordError'));
          reject(t('profile.changePasswordError'));
        })
    }
  );
};

export const useRequestPinChangeMutation = () => {
  const { t } = useTranslation();
  const queryClient = useQueryClient();
  return useMutation(requestPinChange, {
    onSuccess: response => {
      if (!response?.data?.success) return message.error(t('profile.changePinCodeRequestError'));
      openPinRequestSuccessModal();
      queryClient.invalidateQueries(['profile']);
    },
    onError: () => {
      message.error(t('profile.changePinCodeRequestError'));
    }
  });
};

export const useApprovePinRequestMutation = () => {
  const { t } = useTranslation();
  const queryClient = useQueryClient();
  const { setVariantAndShow } = useContext(AlertContext);

  return useMutation(approvePinRequest, {
    onSuccess: response => {
      if (!response?.data?.success) {
        throw Error(
          `${t('general.thereWasAnError')} ${t('general.actions.approve')} ${t(
            'pin.thePinRequest'
          )}`
        );
      }
      setVariantAndShow(ALERT_VARIANTS_ENUM.SIGNATURE_APPROVED);
      queryClient.invalidateQueries(['users']);
    },
    onError: () => {
      message.error(
        `${t('general.thereWasAnError')} ${t('general.actions.approve')} ${t('pin.thePinRequest')}`
      );
    }
  });
};

export const useRejectPinRequestMutation = () => {
  const { t } = useTranslation();
  const queryClient = useQueryClient();
  const { setVariantAndShow } = useContext(AlertContext);

  return useMutation(rejectPinRequest, {
    onSuccess: response => {
      if (!response?.data?.success) {
        throw Error(
          `${t('general.thereWasAnError')} ${t('general.actions.reject')} ${t('pin.thePinRequest')}`
        );
      }
      setVariantAndShow(ALERT_VARIANTS_ENUM.SIGNATURE_REJECTED);
      queryClient.invalidateQueries(['users']);
    },
    onError: () => {
      message.error(
        `${t('general.thereWasAnError')} ${t('general.actions.reject')} ${t('pin.thePinRequest')}`
      );
    }
  });
};

export const useRecoverPasswordMutation = () => {
  const { t } = useTranslation();

  return useMutation(recoverPassword, {
    onError: () => {
      message.error(
        `${t('general.thereWasAnError')} ${t('general.actions.recover')} ${t(
          'loginSection.recovery_password.yourPassword'
        )}`
      );
    }
  });
};

export const useToggleActiveStatus = () => {
  const { t } = useTranslation();
  const queryClient = useQueryClient();
  const { setVariantAndShow } = useContext(AlertContext);

  return useMutation(toggleActiveStatus, {
    onSuccess: () => {
      setVariantAndShow(ALERT_VARIANTS_ENUM.ACTIVE_STATUS_CHANGED);
      queryClient.invalidateQueries(['users']);
    },
    onError: () => {
      message.error(
        `${t('general.thereWasAnError')} ${t('general.actions.change')} ${t(
          'usersAdministration.the active status'
        )}`
      );
    }
  });
};

export const useLoginUserMutation = () => {
  const { t } = useTranslation();

  return useMutation(({ email, pwd }) => loginUser(email, pwd), {
    onError: () => {
      message.error(t('users.There was an error when trying to log in'));
    }
  });
};

export const useResetPasswordMutation = () => {
  const { t } = useTranslation();

  return useMutation(resetPassword, {
    onError: () => {
      message.error(t('users.There was an error when trying to reset password'));
    }
  });
};

export const useCreateUserMutation = () => {
  const { t } = useTranslation();

  return useMutation(createUser, {
    onError: () => {
      message.error(t('users.There was an error when trying to create user'));
    }
  });
};

export const useSendWelcomeEmailMutation = () => {
  const { t } = useTranslation();

  return useMutation(sendWelcomeEmail, {
    onError: () => {
      message.error(t('users.There was an error when trying send the welcome email'));
    }
  });
};

export const useSetPinMutation = () => {
  const { t } = useTranslation();

  return useMutation(setPin, {
    onError: () => {
      message.error(t('users.There was an error when trying set the pin'));
    }
  });
};

export const useSetWalletMutation = () => {
  const { t } = useTranslation();

  return useMutation(setWallet, {
    onError: () => {
      message.error(t('users.There was an error when trying set the wallet'));
    }
  });
};
