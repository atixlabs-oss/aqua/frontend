import { useQuery } from '@tanstack/react-query';
import { message } from 'antd';
import { getCountries } from 'api/countriesApi';
import { useTranslation } from 'react-i18next';

export const useGetCountriesQuery = ({ enabled } = { enabled: true }) => {
  const { t } = useTranslation();

  const getCountriesProcessed = async () => {
    let countriesProcessed = await getCountries();

    countriesProcessed = countriesProcessed?.data?.map(country => ({
      ...country,
      value: country?.id
    }));

    return countriesProcessed.sort((a, b) => a.name.localeCompare(b.name));
  };

  const query = useQuery(['countries'], getCountriesProcessed, {
    enabled,
    onError: () => {
      message.error(t('countries.There was an error when fetching countries'));
    }
  });

  return query;
};
