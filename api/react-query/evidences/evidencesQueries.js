import { useQuery } from '@tanstack/react-query';
import { getEvidence } from 'api/activityApi';

export const useGetEvidenceQuery = ({ evidenceId, enabled = true }) => {
  const getEvidenceProcessed = async () => {
    const response = await getEvidence(evidenceId);
    return await response.data;
  };

  const query = useQuery(['evidence', evidenceId], getEvidenceProcessed, {
    enabled: enabled && Boolean(evidenceId),
    initialData: {}
  });

  return query;
};
