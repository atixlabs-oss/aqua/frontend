import { useMutation, useQueryClient } from '@tanstack/react-query';
import { useTranslation } from 'react-i18next';
import { message } from 'antd';

import { updateEvidenceStatus } from 'api/activityApi';

export const useUpdateEvidenceStatusMutation = () => {
  const { t } = useTranslation();
  const queryClient = useQueryClient();
  return useMutation(
    async ({ evidenceId, status, reason, projectId }) => {
      await updateEvidenceStatus(evidenceId, status, reason);
      return { evidenceId, projectId };
    },
    {
      onSuccess: ({ evidenceId, projectId }) => {
        queryClient.invalidateQueries(['evidence', evidenceId]);
        queryClient.invalidateQueries(['changelog', projectId, { evidenceId }]);
      },
      onError: () => {
        message.error(
          t('evidences.There was an error when trying to update the status of the evidence')
        );
      }
    }
  );
};
