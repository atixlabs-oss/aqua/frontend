/**
 * AGUA Copyright (C) 2023  Atix Labs
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import { doPut, doDelete, doPost } from './api';

const baseURL = '/milestones';

export const updateMilestone = (milestoneId, saveData) =>
  doPut(`${baseURL}/${milestoneId}`, saveData);

export const deleteMilestone = milestoneId => doDelete(`${baseURL}/${milestoneId}`);

export const createMilestone = (projectId, saveData) =>
  doPost(`/projects/${projectId}${baseURL}`, saveData);

export const createMilestoneActivity = async (activity, milestoneId) =>
  doPost(`${baseURL}/${milestoneId}/activities`, activity);
