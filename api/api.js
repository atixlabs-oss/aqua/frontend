/**
 * AGUA Copyright (C) 2023  Atix Labs
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import { message } from 'antd';
import axios from 'axios';
import i18n from 'i18n';

import { ACCESS_TOKEN_KEY } from 'constants/constants';
import formatError from '../helpers/errorFormatter';

const { t } = i18n;

export const getBaseURL = () => process.env.NEXT_PUBLIC_URL_HOST;

const baseURL = getBaseURL();

const api = axios.create({
  baseURL,
  timeout: 60000,
  headers: { 'content-type': 'application/json' }
});

let loadingMessage;
let requestsInQueue = 0;

const handleResponseQueue = currentRequestsInQueue => {
  const updatedRequestsInQueue = currentRequestsInQueue - 1;
  if (updatedRequestsInQueue === 0) {
    loadingMessage?.();
  }
  return updatedRequestsInQueue;
};

api.interceptors.request.use(
  config => {
    const accessToken = sessionStorage.getItem(ACCESS_TOKEN_KEY);

    const _config = config;

    if (accessToken) _config.headers.authorization = accessToken;

    if (requestsInQueue === 0) {
      loadingMessage = message.loading(t('general.Loading'), 0);
    }
    requestsInQueue++;

    return _config;
  },
  error => Promise.reject(error)
);

api.interceptors.response.use(
  response => {
    requestsInQueue = handleResponseQueue(requestsInQueue);
    return Promise.resolve(response);
  },
  error => {
    requestsInQueue = handleResponseQueue(requestsInQueue);
    return Promise.reject(error);
  }
);

export const makeApiRequest = async (method, url, body, config) => {
  let data;
  let headers;
  let errors;
  let status;

  try {
    const result = await api.request({
      method,
      url,
      data: body,
      ...config
    });

    data = result?.data;
    headers = result?.headers;
    status = result?.status;
  } catch (error) {
    errors = formatError(error);
    status = error?.response?.status;
    throw { errors, status };
  }

  return { data, headers, errors, body, status };
};

export const doGet = async (url, data, config) => makeApiRequest('get', url, data, config);

export const doPost = async (url, data = {}, config) => makeApiRequest('post', url, data, config);

export const doPut = async (url, data = {}, config) => makeApiRequest('put', url, data, config);

export const doDelete = async (url, data = {}) => makeApiRequest('delete', url, data);

export default api;
