/**
 * AGUA Copyright (C) 2023  Atix Labs
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import { doGet, doPost, doPut, doDelete } from './api';

import queryString from 'query-string';

const projectsBaseURL = '/projects';

export const getProjects = () => doGet(projectsBaseURL);

export const createProject = () => doPost(projectsBaseURL);

export const putBasicInformation = (projectId, saveData) => {
  const config = { headers: { 'Content-Type': 'multipart/form-data' } };
  return doPut(`${projectsBaseURL}/${projectId}/basic-information`, saveData, config);
};

export const updateProjectDetail = (projectId, saveData) => {
  const config = { headers: { 'Content-Type': 'multipart/form-data' } };
  return doPut(`${projectsBaseURL}/${projectId}/details`, saveData, config);
};

export const getProject = async projectId => doGet(`${projectsBaseURL}/${projectId}`);

export const getProjectTransactions = (projectId, type) =>
  doGet(`${projectsBaseURL}/${projectId}/transactions?type=${type}`);

export const sendToReview = projectId => doPut(`${projectsBaseURL}/${projectId}/in-review`);

export const publish = projectId => doPut(`${projectsBaseURL}/${projectId}/publish`);

export const deleteProject = async projectId => doDelete(`${projectsBaseURL}/${projectId}`);

export const updateProjectCanceledState = async projectId =>
  doPut(`${projectsBaseURL}/${projectId}/cancel`);

export const getChangelog = async (projectId, values) => {
  const params = {};
  if (values.milestoneId) params.milestoneId = values.milestoneId;
  if (values.activityId) params.activityId = values.activityId;
  if (values.revisionId) params.revisionId = values.revisionId;
  if (values.evidenceId) params.evidenceId = values.evidenceId;
  if (values.userId) params.userId = values.userId;

  return doGet(`${projectsBaseURL}/${projectId}/changelog`, undefined, { params });
};

export const cloneProject = async projectId => doPost(`${projectsBaseURL}/${projectId}/clone`);

export const cancelReview = async projectId =>
  doPut(`${projectsBaseURL}/${projectId}/cancel-review`);

export const approveCloneProject = async projectId =>
  doPut(`${projectsBaseURL}/${projectId}/review`, { approved: true });

export const rejectCloneProject = async projectId =>
  doPut(`${projectsBaseURL}/${projectId}/review`, { approved: false });

export const signProject = ({ projectId, authorizationSignature }) =>
  doPost(`${projectsBaseURL}/${projectId}/signature`, { authorizationSignature });

export const getCashFlow = ({ projectId }) => doGet(`${projectsBaseURL}/${projectId}/movement`);

export const getPublicProjects = filters => {
  const stringified = queryString.stringify(filters);
  const queries = stringified ? `?${stringified}` : '';

  return doGet(`${projectsBaseURL}/public${queries}`, filters);
};

export const updateProjectVisibility = ({ projectId, visibility }) =>
  doPut(`${projectsBaseURL}/${projectId}/set-visibility`, { visibility });
