/**
 * AGUA Copyright (C) 2023  Atix Labs
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import { doGet, doPut, doPost } from './api';
const baseURL = '/users';

export const getMyProjects = async ({ userId }) => doGet(`${baseURL}/${userId}/projects`);

export const loginUser = (email, pwd) => doPost(`${baseURL}/login`, { email, pwd });

export const getUsers = params =>
  doGet(
    `${baseURL}`,
    {},
    {
      params
    }
  );

export const recoverPassword = data => doPost(`${baseURL}/recoverPassword`, data);

export const changePassword = data => doPut(`${baseURL}/me/password`, data);

export const resetPassword = data => doPut(`${baseURL}/me/reset-password`, data);

export const getWallet = () => doGet(`${baseURL}/me/wallet`);

export const createUser = async user => doPost(`${baseURL}`, user);

export const sendWelcomeEmail = async data => doPost(`${baseURL}/welcome-email`, data);

export const setPin = async () => doPut(`${baseURL}/pin`);

export const setWallet = async data => doPost(`${baseURL}/wallet`, data);

export const getTokenStatus = async token => doGet(`${baseURL}/token/${token}`);

export const getProfile = async () => doGet(`${baseURL}/profile`);

export const requestPinChange = async () => doPost(`${baseURL}/pin`);

export const getAddressHistory = async () => doGet(`${baseURL}/addressHistory`);

export const approvePinRequest = userId => doPut(`${baseURL}/${userId}/approve-pin-request`);

export const rejectPinRequest = userId => doPut(`${baseURL}/${userId}/reject-pin-request`);

export const toggleActiveStatus = userId => doPut(`${baseURL}/${userId}/toggle-active-status`);
