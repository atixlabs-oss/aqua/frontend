/**
 * AGUA Copyright (C) 2023  Atix Labs
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import { doPut, doPost, doGet, doDelete } from './api';

const baseURL = '/activities';

export const getActivityEvidences = activityId => doGet(`${baseURL}/${activityId}/evidences`);

export const getEvidence = evidenceId => doGet(`/evidences/${evidenceId}`);
export const updateEvidenceStatus = (evidenceId, status, reason) =>
  doPut(`/evidences/${evidenceId}`, { status, reason });

export const updateActivity = async (activityId, updatedActivity) =>
  doPut(`${baseURL}/${activityId}`, updatedActivity);

export const deleteActivityEvidence = (activityId, evidenceId) =>
  doDelete(`${baseURL}/${activityId}/evidence/${evidenceId}`);

export const updateActivityStatus = async (activityId, status, txId, reason = '') =>
  doPut(`${baseURL}/${activityId}/status`, {
    status,
    reason,
    txId
  });

export const createActivityEvidence = async (activityId, data) => {
  const config = { headers: { 'Content-Type': 'multipart/form-data' } };
  const fd = new FormData();
  fd.append('title', data.title);
  fd.append('type', data.type);
  fd.append('description', data.description);
  fd.append('amount', data.amount);

  if (data.files.length > 0) {
    data.files.forEach((file, index) => {
      fd.append(`file-${index}`, file);
    });
  }

  if (data.transferTxHash.length > 0) {
    fd.append('transferTxHash', data.transferTxHash);
  }

  if (data.destinationAccount) {
    fd.append('destinationAccount', data.destinationAccount);
  }

  return doPost(`${baseURL}/${activityId}/evidences`, fd, config);
};

export const signActivity = ({ activityId, authorizationSignature }) =>
  doPost(`${baseURL}/${activityId}/signature`, { authorizationSignature });

export const deleteActivity = activityId => doDelete(`activities/${activityId}`);
