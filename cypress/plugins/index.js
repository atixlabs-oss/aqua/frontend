const registerCodeCoverageTasks = require('@cypress/code-coverage/task');
const webpack = require('@cypress/webpack-preprocessor');

module.exports = (on, config) => {
  const options = {
    webpackOptions: {
      resolve: {
        extensions: ['.js', '.json']
      }
    }
  };
  registerCodeCoverageTasks(on, config);
  on('file:preprocessor', webpack(options));

  // aquí puedes agregar otras configuraciones para Cypress
  return config;
};
