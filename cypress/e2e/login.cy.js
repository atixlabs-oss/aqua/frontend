const { mockedUsers } = require('../mocks/users');

describe('Login', () => {
  beforeEach(() => {
    cy.visit('/login', {
      onBeforeLoad(win) {
        win.localStorage.setItem('cookies-accepted', 'true');
      }
    });
  });
  it('Login: Admin', () => {
    cy.get('form')
      .find('[name="email"]')
      .type(mockedUsers.admin.email);
    cy.get('form')
      .find('[name="password"]')
      .type('passwordTest');
    expect(true).to.equal(true);

    cy.get('form')
      .contains(/Log in/i)
      .click();

    cy.url().should('be.equal', 'http://localhost:3000/back-office/projects');

    cy.get('nav')
      .contains(mockedUsers.admin.firstName)
      .contains('Administrator');
  });
});
