const { mockedUsers } = require('../mocks/users');

describe('Home', () => {
  beforeEach(() => {
    cy.visit('/', {
      onBeforeLoad(win) {
        win.localStorage.setItem('cookies-accepted', 'true');
      }
    });
  });

  it('Login: Admin', () => {
    cy.contains(/Log in/i).click();
    cy.get('form')
      .find('[name="email"]')
      .type(mockedUsers.admin.email);

    cy.get('form')
      .find('[name="password"]')
      .type('passwordTest');

    cy.get('form')
      .contains(/Log in/i)
      .click();

    cy.url().should('be.equal', 'http://localhost:3000/back-office/projects');

    cy.get('nav')
      .contains(mockedUsers.admin.firstName)
      .contains('Administrator');
  });
});
