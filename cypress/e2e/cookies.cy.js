describe('Cookies', () => {
  it('When first entering contains a modal showing a message about cookies with button clickable to dissapear it', () => {
    cy.visit('/');
    cy.contains(/This website uses cookies/i);
    cy.contains(/What is a cookie?/i);
    cy.get('[role=dialog]').should('not.have.css', 'display', 'none');
    cy.contains(/accept cookies/i).click();
    cy.get('[role=dialog]').should('have.css', 'display', 'none');
  });
});
