const { mockedProject } = require('../mocks/project');

describe('Login', () => {
  beforeEach(() => {
    cy.visit('/my-projects', {
      onBeforeLoad(win) {
        win.localStorage.setItem('cookies-accepted', 'true');
        win.sessionStorage.setItem(
          'user',
          '{"firstName":"Firstname","lastName":"Beneficiary","email":"federico.catala+20@globant.com","id":"37495329-004d-422b-8ce7-0e8155bb0183","isAdmin":false,"forcePasswordChange":false,"first":false,"pin":true,"seenModal":false}'
        );
      }
    });
  });
  it('A list of projects should be visible', () => {
    cy.get('h1').contains(/my projects/i);
    cy.get('div').contains(/fede 22 feb/i);
    cy.get('[role="main"]')
      .find('[role="button"]')
      .children()
      .should(
        'have.attr',
        'style',
        'background-image: linear-gradient(0.65deg, rgba(0, 0, 0, 0.7) 0.02%, rgba(0, 0, 0, 0) 50.2%), url("/static/images/empty-img.svg");'
      );
  });
  it('When click a project user should be redirected to it', () => {
    cy.window().then(win => {
      cy.stub(win, 'open').as('openStub');
    });

    cy.get('h1').contains(/my projects/i);
    cy.get('div').contains(/fede 22 feb/i);
    cy.get('[role="main"]')
      .find('[role="button"]')
      .click();

    cy.get('@openStub').should('be.calledWithMatch', `/${mockedProject.in_progress.id}`, '_blank');
  });
  it('should has a navbar that show user name', () => {
    cy.get('nav')
      .find('h2')
      .should('have.text', 'Firstname');
  });
});
