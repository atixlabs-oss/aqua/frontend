describe('Secret key page', () => {
  beforeEach(() => {
    cy.visit('/secret-key', {
      onBeforeLoad(win) {
        win.localStorage.setItem('cookies-accepted', 'true');
        win.sessionStorage.setItem(
          'user',
          '{"firstName":"Firstname","lastName":"Beneficiary","email":"federico.catala+20@globant.com","id":"37495329-004d-422b-8ce7-0e8155bb0183","isAdmin":false,"forcePasswordChange":false,"first":false,"pin":true,"seenModal":false}'
        );
      }
    });
  });

  it('The secret key modal should be visible when enter', () => {
    cy.get('[role=dialog]').should('not.have.css', 'display', 'none');
    cy.get('h2')
      .contains(/set up your digital signature/i)
      .should('exist');
    cy.get('div')
      .contains(/download file/i)
      .should('exist');
  });

  it('Validation errors should be visible if needed', () => {
    cy.get('button')
      .contains(/download file/i)
      .should('exist');
    cy.get('form')
      .find('input')
      .should('have.attr', 'placeholder', 'Enter your PIN');
    cy.contains(/Confirm/i).click();
    cy.contains(/please, enter your pin/i).should('exist');
    cy.contains(/please, check terms & conditions/i).should('exist');
  });
});
