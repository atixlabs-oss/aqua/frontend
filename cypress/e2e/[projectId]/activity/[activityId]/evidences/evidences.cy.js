describe('[projectId]/activity/[activityId]/evidences page', () => {
  beforeEach(() => {
    cy.visit('/projectId/activity/activityId/evidences', {
      onBeforeLoad(win) {
        win.localStorage.setItem('cookies-accepted', 'true');
      }
    });
    cy.intercept('POST', 'https://localhost:3001/users/login', { res: 200 });
  });
  it('Does not do much!', () => {
    expect(true).to.equal(true);
  });
});
