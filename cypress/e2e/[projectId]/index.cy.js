const { mockedProject } = require('../../mocks/project');

describe('Login', () => {
  beforeEach(() => {
    cy.visit(`/${mockedProject.in_progress.id}`, {
      onBeforeLoad(win) {
        win.localStorage.setItem('cookies-accepted', 'true');
      }
    });
  });
  it('Does not do much!', () => {
    expect(true).to.equal(true);
  });
});
