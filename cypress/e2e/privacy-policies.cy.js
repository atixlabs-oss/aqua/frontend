describe('Privacy policies page', () => {
  beforeEach(() => {
    cy.visit('/privacy-policies', {
      onBeforeLoad(win) {
        win.localStorage.setItem('cookies-accepted', 'true');
      }
    });
  });

  it('Navbar should be present', () => {
    cy.contains(/Log in/i).click();
    cy.url().should('be.equal', 'http://localhost:3000/login');
    cy.get('form').should('exist');
  });

  it('Should has a go back button', () => {
    cy.get('button').contains(/Go back/i);
  });

  it('Should has a privacy policies title and content', () => {
    cy.get('h1')
      .contains(/privacy policies/i)
      .should('exist');
    cy.get('p').should('exist');
  });
});
