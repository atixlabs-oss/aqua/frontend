const { mockedProject } = require('../../../../mocks/project');

describe('Edit', () => {
  beforeEach(() => {
    cy.visit(`/back-office/projects/edit/${mockedProject.in_progress_clone.id}`, {
      onBeforeLoad(win) {
        win.localStorage.setItem('cookies-accepted', 'true');
        win.sessionStorage.setItem(
          'user',
          '{"firstName":"Firstname","lastName":"Beneficiary","email":"federico.catala+20@globant.com","id":"37495329-004d-422b-8ce7-0e8155bb0183","isAdmin":false,"forcePasswordChange":false,"first":false,"pin":true,"seenModal":false}'
        );
      }
    });
  });

  it('Preview project button should be disabled for beneficiary', () => {
    cy.get('button')
      .contains(/see preview project/i)
      .parent()
      .should('be.disabled');
  });

  it('Only the project users should be disabled if beneficiary is logged in', () => {
    cy.get('button:contains(Edit)').as('editButtons');

    cy.get('@editButtons')
      .eq(0)
      .should('be.enabled');
    cy.get('@editButtons')
      .eq(1)
      .should('be.enabled');
    cy.get('@editButtons')
      .eq(2)
      .should('be.disabled');
    cy.get('@editButtons')
      .eq(3)
      .should('be.enabled');
  });

  it('Project details: cant edit currency type nor currency', () => {
    cy.get('button:contains(Edit)').as('editButtons');

    cy.get('@editButtons')
      .eq(1)
      .click();
    cy.get('label')
      .contains(/currency type/i)
      .parent('div')
      .next('div')
      .find('div[id="FormProjectDetail_currencyType"]')
      .should('have.attr', 'class', 'ant-select ant-select-disabled');

    cy.get('label')
      .contains(/currency$/i)
      .parent('div')
      .next('div')
      .find('div[id="FormProjectDetail_currency"]')
      .should('have.attr', 'class', 'ant-select ant-select-disabled');

    cy.get('button')
      .contains('Save and continue')
      .should('exist');

    cy.get('button')
      .contains(/back/i)
      .click({ force: true });

    cy.get('.ant-breadcrumb')
      .contains(/Create Project/i)
      .should('exist');

    cy.get('.ant-breadcrumb')
      .contains(/Dashboard/i)
      .should('exist');
  });
});
