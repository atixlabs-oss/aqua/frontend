const { mockedProject } = require('../../../../mocks/project');

describe('Edit', () => {
  beforeEach(() => {
    cy.visit(`/back-office/projects/edit/${mockedProject.draft.stepsCompleted.id}`, {
      onBeforeLoad(win) {
        win.localStorage.setItem('cookies-accepted', 'true');
        win.sessionStorage.setItem(
          'user',
          '{"firstName":"Administrator","lastName":"","email":"dialguiba@gmail.com","id":"a2e4bbe4-7655-4633-a2fb-620565c07ab8","isAdmin":true,"forcePasswordChange":false,"first":false,"pin":false,"seenModal":false}'
        );
      }
    });
  });

  /* Skipped temporally for now because there is a problem with the router and the message showed in the preview - Working in local but not in the pipeline  */
  it.skip('Should be able to preview the project in draft', () => {
    cy.get('button')
      .contains(/see preview project/i)
      .click({ force: true });
    cy.url().should(
      'be.equal',
      `http://localhost:3000/${mockedProject.draft.stepsCompleted.id}?preview=true`
    );
    cy.get('span')
      .contains('You are viewing the preview of your project')
      .should('exist');
    cy.get('button')
      .contains('Back to edit')
      .should('exist');
    cy.get('div')
      .contains(/draft/i)
      .should('exist');
    cy.get('h1').contains(/Fede 22 Feb/i);
  });

  it('Have correct sections', () => {
    cy.get('h3')
      .contains(/Basic information/i)
      .should('exist');
    cy.get('h4')
      .contains(/Here you can complete the basic information of the project/i)
      .should('exist');
    cy.get('h3')
      .contains(/Project Detail/i)
      .should('exist');
    cy.get('h4')
      .contains(/Here you can complete the project details/i)
      .should('exist');
    cy.get('h3')
      .contains(/Project Users/i)
      .should('exist');
    cy.get('h4')
      .contains(/Here you can assign or create users to different roles/i)
      .should('exist');
    cy.get('h3')
      .contains(/Project Milestones/i)
      .should('exist');
    cy.get('h4')
      .contains(/Here you can upload the required milestones for your project/i)
      .should('exist');
    cy.get('button').contains(/Edit/i);
  });

  it('should contains footer with correct buttons in footer', () => {
    cy.contains(/delete project/i).click();
    cy.get('[role=dialog]').should('not.have.css', 'display', 'none');
    cy.contains(/warning!/i).should('exist');
    cy.contains(/are you sure you want to delete your project\?/i).should('exist');
    cy.contains(/cancel/i).click();
    cy.get('[role=dialog]').should('not.exist');

    cy.contains(/save & continue later/i).should('exist');
    cy.contains(/publish project/i).should('exist');
  });

  describe('Sections', () => {
    it('Basic information', () => {
      cy.get('button')
        .eq(2)
        .click();
      cy.get('h1')
        .contains(/complete basic information/i)
        .should('exist');
      cy.get('h4')
        .contains(/Fede 22 Feb/i)
        .should('exist');

      cy.contains(/project name/i)
        .parent('div')
        .next('div')
        .find('input')
        .should('have.value', 'Fede 22 Feb');

      cy.contains(/country of impact/i)
        .parent('div')
        .next('div')
        .find('div[title="(Islamic Republic of) Iran"]')
        .should('contain.text', '(Islamic Republic of) Iran');

      cy.contains(/timeframe/i)
        .parent('div')
        .next('div')
        .find('input')
        .should('have.value', '12.0');

      cy.contains(/timeframe/i)
        .parent('div')
        .parent('div')
        .next('div')
        .find('div[role="combobox"]')
        .find('div[title="months"]')
        .should('contain.text', 'months');

      cy.contains(/thumbnail image/i)
        .parent('div')
        .next('div')
        .find('button')
        .contains(/click to upload/i)
        .should('exist');

      cy.contains(/thumbnail image/i)
        .parent('div')
        .next('div')
        .contains(/thumbnail-image\.jpeg/i)
        .should('have.prop', 'href');

      cy.get('button')
        .contains('Save and continue')
        .should('exist');

      cy.get('button')
        .contains(/back/i)
        .click({ force: true });

      cy.get('.ant-breadcrumb')
        .contains(/Create Project/i)
        .should('exist');

      cy.get('.ant-breadcrumb')
        .contains(/Dashboard/i)
        .should('exist');
    });

    it('Project details', () => {
      cy.get('button')
        .eq(3)
        .click();
      cy.get('h1')
        .contains(/complete project's details/i)
        .should('exist');

      cy.contains(/about the project/i)
        .parent('div')
        .next('div')
        .find('textarea')
        .should('have.value', 'asd');

      cy.contains(/our mission and vision/i)
        .parent('div')
        .next('div')
        .find('textarea')
        .should('have.value', 'adssda');

      cy.contains(/project type/i)
        .parent('div')
        .next('div')
        .find('div[role="combobox"]')
        .find('div[title="Grant"]')
        .should('contain.text', 'Grant');

      cy.get('label')
        .contains(/currency type/i)
        .parent('div')
        .next('div')
        .find('div[role="combobox"]')
        .find('div[title="CRYPTO"]')
        .should('contain.text', 'CRYPTO');

      cy.get('label')
        .contains(/^currency$/i)
        .parent('div')
        .next('div')
        .find('div[role="combobox"]')
        .find('div[title="USDT"]')
        .should('contain.text', 'USDT');

      cy.contains(/address/i)
        .parent('div')
        .next('div')
        .find('input')
        .should('have.value', '0xaC4D2aC230876F8bAB54144CEE315bC22250661f');

      cy.contains(/budget/i)
        .parent('div')
        .next('div')
        .find('input')
        .should('have.value', 'USDT 200.00000000')
        .should('be.disabled');

      cy.get('button')
        .contains(/upload legal agreement/i)
        .should('exist');
      cy.get('button')
        .contains(/upload project proposal/i)
        .should('exist');

      cy.get('button')
        .contains('Save and continue')
        .should('exist');

      cy.get('button')
        .contains(/back/i)
        .click({ force: true });

      cy.get('.ant-breadcrumb')
        .contains(/Create Project/i)
        .should('exist');

      cy.get('.ant-breadcrumb')
        .contains(/Dashboard/i)
        .should('exist');
    });

    it('Project users', () => {
      cy.get('button')
        .eq(4)
        .click();

      cy.get('h1')
        .contains(/Assign project users/i)
        .should('exist');

      cy.get('h3')
        .contains(/beneficiary/i)
        .parent('div')
        .parent('div')
        .as('beneficiarySection')
        .should('exist');

      cy.get('@beneficiarySection')
        .find('label[title="Beneficiary email"]')
        .should('exist');

      cy.get('@beneficiarySection')
        .find('input[value="federico.catala+20@globant.com"]')
        .should('have.value', 'federico.catala+20@globant.com');

      cy.get('@beneficiarySection').click();

      cy.get('@beneficiarySection')
        .contains(/unassign user/i)
        .click();

      cy.get('@beneficiarySection')
        .find('input[value=""]')
        .as('beneficiaryInputs')
        .should('have.value', '');

      cy.get('@beneficiaryInputs')
        .eq(1)
        .type('test@gmail.com');

      cy.contains(/The beneficiary is not registered. Fill in the information below/i);

      cy.get('h3')
        .contains(/investor/i)
        .parent('div')
        .parent('div')
        .as('investorSection')
        .should('exist');

      cy.get('@investorSection').find('label[title="Investor email"]');

      cy.get('@investorSection')
        .find('input[value="federico.catala+21@globant.com"]')
        .should('have.value', 'federico.catala+21@globant.com');

      cy.get('@investorSection').click();

      cy.get('@investorSection')
        .contains(/unassign user/i)
        .click();

      cy.get('@investorSection')
        .find('input[value=""]')
        .as('investorInputs')
        .should('have.value', '');

      cy.get('@investorInputs')
        .eq(1)
        .type('test@gmail.com');

      cy.contains(/The investor is not registered. Fill in the information below/i);

      cy.get('h3')
        .contains(/auditor/i)
        .parent('div')
        .parent('div')
        .as('auditorSection')
        .should('exist');

      cy.get('@auditorSection').find('label[title="Auditor email"]');

      cy.get('@auditorSection')
        .find('input[value="federico.catala+22@globant.com"]')
        .should('have.value', 'federico.catala+22@globant.com');

      cy.get('@auditorSection').click();

      cy.get('@auditorSection')
        .contains(/unassign user/i)
        .should('be.disabled');

      cy.contains(/add auditor/i).click();

      cy.get('@auditorSection')
        .find('input[value=""]')
        .should('exist');

      cy.contains(/add auditor/i).should('be.disabled');

      cy.get('button')
        .contains('Save and continue')
        .should('exist');

      cy.get('button')
        .contains(/back/i)
        .click({ force: true });

      cy.get('.ant-breadcrumb')
        .contains(/Create Project/i)
        .should('exist');

      cy.get('.ant-breadcrumb')
        .contains(/Dashboard/i)
        .should('exist');
    });

    it('Project milestones', () => {
      cy.get('button')
        .eq(5)
        .click();
      cy.get('h1')
        .contains(/preview and edit milestones/i)
        .as('sectionTitle')
        .should('exist');

      cy.contains(/milestone 1 - milestone 1/i).should('exist');
      cy.contains(/milestone 2 - milestone 2/i).should('exist');

      cy.get('[data-cy="milestone-card"]')
        .as('milestonesCards')
        .should('have.length', 2);

      cy.contains(/add new milestone/i).click();

      cy.get('label[title="Title"]')
        .parent()
        .next()
        .find('input')
        .type('test title');

      cy.get('label[title="Description"]')
        .parent()
        .next()
        .find('textarea')
        .type('test description');

      cy.get('[role=dialog]')
        .find('button')
        .contains('Save')
        .click({ force: true });

      cy.get('@milestonesCards').should('have.length', 3);

      cy.contains(/milestone 3 - test title/i).should('exist');

      cy.contains(/milestone 1 - milestone 1/i)
        .parent()
        .parent()
        .contains(/edit/i)
        .click();

      cy.get('label[title="Title"]')
        .parent()
        .next()
        .find('input')
        .should('have.value', 'Milestone 1');

      cy.get('label[title="Description"]')
        .parent()
        .next()
        .find('textarea')
        .should('have.value', 'asddsa');

      cy.get('label[title="Title"]')
        .parent()
        .next()
        .find('input')
        .type(' edited');

      cy.get('[role=dialog]')
        .find('button')
        .contains('Save')
        .click({ force: true });

      cy.contains(/milestone 1 - milestone 1 edited/i).should('exist');

      cy.get('button')
        .contains('Save and continue')
        .should('exist');

      cy.get('button')
        .contains(/back/i)
        .click({ force: true });

      cy.get('.ant-breadcrumb')
        .contains(/Create Project/i)
        .should('exist');

      cy.get('.ant-breadcrumb')
        .contains(/Dashboard/i)
        .should('exist');
    });
  });

  it('should be able to logout', () => {
    cy.get('nav')
      .find('i[aria-label="icon: down"]')
      .click();
    cy.contains(/logout/i).click({ force: true });
    cy.get('nav')
      .contains(/log in/i)
      .should('exist');
  });
});
