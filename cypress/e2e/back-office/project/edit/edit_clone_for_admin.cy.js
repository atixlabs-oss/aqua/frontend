const { mockedProject } = require('../../../../mocks/project');

describe('Edit', () => {
  beforeEach(() => {
    cy.visit(`/back-office/projects/edit/${mockedProject.in_progress_clone.id}`, {
      onBeforeLoad(win) {
        win.localStorage.setItem('cookies-accepted', 'true');
        win.sessionStorage.setItem(
          'user',
          '{"firstName":"Administrator","lastName":"","email":"dialguiba@gmail.com","id":"a2e4bbe4-7655-4633-a2fb-620565c07ab8","isAdmin":true,"forcePasswordChange":false,"first":false,"pin":false,"seenModal":false}'
        );
      }
    });
    cy.get('button:contains(Edit)').as('editButtons');
  });

  /* Skipped temporally for now because there is a problem with the router and the message showed in the preview - Working in local but not in the pipeline  */
  it.skip('Should be able to preview the project being edited', () => {
    cy.get('button')
      .contains(/see preview project/i)
      .click({ force: true });
    cy.url().then(() => {
      cy.url().should(
        'be.equal',
        `http://localhost:3000/${mockedProject.in_progress_clone.id}?preview=true`
      );
    });
    cy.get('span')
      .contains('You are viewing the preview of your project')
      .should('exist');
    cy.get('button')
      .contains('Back to edit')
      .should('exist');
    cy.get('div')
      .contains(/open for review/i)
      .should('exist');
    cy.get('h1').contains(/Fede 22 Feb/i);
    cy.contains(/back to edit/i).click();
    cy.url().then(() => {
      cy.url().should(
        'be.equal',
        `http://localhost:3000/back-office/projects/edit/${mockedProject.in_progress_clone.id}`
      );
    });
  });

  it('Only the project users should be enabled if administrator is logged in', () => {
    cy.get('@editButtons')
      .eq(0)
      .should('be.disabled');
    cy.get('@editButtons')
      .eq(1)
      .should('be.disabled');
    cy.get('@editButtons')
      .eq(2)
      .should('be.enabled');
    cy.get('@editButtons')
      .eq(3)
      .should('be.disabled');
  });

  it('Should be able to unassign a beneficiary and auditor', () => {
    cy.get('@editButtons')
      .eq(2)
      .click();

    cy.get('h3')
      .contains(/beneficiary/i)
      .parent('div')
      .parent('div')
      .as('beneficiarySection')
      .should('exist');

    cy.get('@beneficiarySection')
      .find('label[title="Beneficiary email"]')
      .should('exist');

    cy.get('@beneficiarySection')
      .find('input[value="federico.catala+20@globant.com"]')
      .should('have.value', 'federico.catala+20@globant.com');

    cy.get('@beneficiarySection').click();

    cy.get('@beneficiarySection')
      .contains(/unassign user/i)
      .click();

    cy.get('@beneficiarySection')
      .find('input[value=""]')
      .as('beneficiaryInputs')
      .should('have.value', '');

    cy.get('h3')
      .contains(/investor/i)
      .parent('div')
      .parent('div')
      .as('investorSection')
      .should('exist');

    cy.get('@investorSection').find('label[title="Investor email"]');

    cy.get('@investorSection')
      .find('input[value="federico.catala+21@globant.com"]')
      .should('have.value', 'federico.catala+21@globant.com');

    cy.get('@investorSection').click();

    cy.get('@investorSection')
      .contains(/unassign user/i)
      .click();

    cy.get('@investorSection')
      .find('input[value=""]')
      .as('investorInputs')
      .should('have.value', '');
  });

  it('If an auditor is assigned to an activity, he cant be unassigned from the project', () => {
    cy.get('@editButtons')
      .eq(2)
      .click();

    cy.get('h3')
      .contains(/auditor/i)
      .parent('div')
      .parent('div')
      .as('auditorSection')
      .should('exist');

    cy.get('@auditorSection').find('label[title="Auditor email"]');

    cy.get('@auditorSection')
      .find('input[value="federico.catala+22@globant.com"]')
      .should('have.value', 'federico.catala+22@globant.com');

    cy.get('@auditorSection').click();

    cy.get('@auditorSection')
      .contains(/unassign user/i)
      .should('be.disabled');
  });

  it('should be able to continue and go back', () => {
    cy.get('button')
      .contains(/Save & continue later/i)
      .should('exist');

    cy.get('.ant-breadcrumb')
      .contains(/Create Project/i)
      .should('exist');

    cy.get('.ant-breadcrumb')
      .contains(/Dashboard/i)
      .should('exist');

    cy.get('button')
      .contains(/Save & continue later/i)
      .click({ force: true });

    /* cy.url().should('be.equal', `http://localhost:3000/${mockedProject.in_progress_clone.parent}`); */
  });
});
