/* /activities/activityId/evidences */

export const MOCKED_ACTIVITY_EVIDENCES = {
  22: {
    milestone: {
      id: 10,
      title: 'First Miletone'
    },
    activity: {
      id: 22,
      title: 'Specific project activity ',
      description: 'project activity with associated budget and a clear acceptance criteria',
      acceptanceCriteria: 'invoice, photo',
      status: 'in_progress',
      auditor: '83a8ad9f-8b80-493c-8e8f-0ea071d62747',
      budget: '1000',
      current: '0',
      step: 0,
      activityHash: null,
      reason: null,
      createdAt: '2023-05-09T00:00:00.000Z',
      deleted: false,
      toSign: null,
      type: 'spending',
      claimCounter: 1,
      milestone: 10,
      proposer: null,
      parent: null
    },
    evidences: [
      {
        id: 15,
        title: 'Transfer to exchange',
        description: 'Transfer to exchange',
        type: 'transfer',
        amount: '1000',
        destinationAccount: '0x5324d8674d3152bed17cbd7fed89924777bd7fa2',
        txHash: '',
        status: 'new',
        reason: '',
        files: [],
        createdAt: '2023-05-15T19:43:22.411Z'
      },
      {
        id: 16,
        title: 'Invoice',
        description: 'Purchased manual harvester invoice',
        type: 'impact',
        amount: '0',
        destinationAccount: '',
        txHash: '',
        status: 'new',
        reason: '',
        files: [
          {
            id: 6,
            name: 'harvesterinvoice.jpg',
            path: '/evidence/2/26693d828602befb5492da75089064fa.jpeg'
          },
          {
            id: 6,
            name: 'harvesterinvoice.jpg',
            path: '/evidence/2/26693d828602befb5492da75089064fa.jpeg'
          }
        ],
        createdAt: '2023-05-15T19:43:55.586Z'
      },
      {
        id: 17,
        title: 'Harvester photo',
        description:
          'Manual harvester model AB323, purchased, unpacked and operating in the field.',
        type: 'impact',
        amount: '0',
        destinationAccount: '',
        txHash: '',
        status: 'new',
        reason: '',
        files: [
          {
            id: 7,
            name: 'Rice-Wheat-Mini-Manual-Harvesting-Machine.jpg',
            path: '/evidence/6/612f6c66abf1e7d414884de94e4cf01b.jpeg'
          },
          {
            id: 7,
            name: 'Rice-Wheat-Mini-Manual-Harvesting-Machine.jpg',
            path: '/evidence/6/612f6c66abf1e7d414884de94e4cf01b.jpeg'
          }
        ],
        createdAt: '2023-05-15T19:44:22.436Z'
      },
      {
        id: 18,
        title: 'photo of combine harvester in use',
        description: 'Ronaldo, a member of the team, testing the purchased harvester',
        type: 'impact',
        amount: '0',
        destinationAccount: '',
        txHash: '',
        status: 'new',
        reason: '',
        files: [
          {
            id: 8,
            name: 'Rice-Wheat-Mini-Manual-Harvesting-Machine.jpg',
            path: '/evidence/6/612f6c66abf1e7d414884de94e4cf01b.jpeg'
          },
          {
            id: 8,
            name: 'Rice-Wheat-Mini-Manual-Harvesting-Machine.jpg',
            path: '/evidence/6/612f6c66abf1e7d414884de94e4cf01b.jpeg'
          }
        ],
        createdAt: '2023-05-15T19:44:49.707Z'
      }
    ]
  }
};
