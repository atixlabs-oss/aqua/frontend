import { mockedProject } from './project';

export const mockedProjects = [
  {
    id: mockedProject.draft.stepsCompleted.id,
    cardPhotoPath: 'http://fakeimg.pl/600x600?text=test&font=museo',
    goalAmount: mockedProject.draft.stepsCompleted.budget,
    location: mockedProject.draft.stepsCompleted.basicInformation.location,
    projectName: mockedProject.draft.stepsCompleted.basicInformation.projectName,
    timeframe: mockedProject.draft.stepsCompleted.basicInformation.timeframe,
    status: mockedProject.draft.stepsCompleted.status,
    beneficiary: mockedProject.draft.stepsCompleted.basicInformation.beneficiary,
    currency: mockedProject.draft.stepsCompleted.details.currency,
    timeframeUnit: mockedProject.draft.stepsCompleted.basicInformation.timeframeUnit,
    revision: mockedProject.draft.stepsCompleted.revision,
    type: mockedProject.draft.stepsCompleted.type,
    cloneStatus: null
  }
];
