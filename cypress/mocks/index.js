import { MOCKED_ACTIVITY_EVIDENCES } from './activities';
import { mockedChangelog } from './changelog';
import { mockedCountries } from './countries';
import { mockedEvidences, MOCKED_EVIDENCE } from './evidences';
import { mockedProject } from './project';
import { mockedProjects } from './projects';
import { mockedUsers } from './users';
const apiUrl = Cypress.env('API_URL');

export const serverMocked = cy => {
  cy.intercept('GET', `${apiUrl}/projects`, mockedProjects);
  cy.intercept('GET', `${apiUrl}/countries`, mockedCountries);
  cy.intercept('POST', `${apiUrl}/users/login`, mockedUsers.admin);
  cy.intercept(
    'GET',
    `${apiUrl}/projects/${mockedProject.draft.stepsCompleted.id}`,
    mockedProject.draft.stepsCompleted
  );
  cy.intercept(
    'GET',
    `${apiUrl}/projects/${mockedProject.in_progress.id}`,
    mockedProject.in_progress
  );
  cy.intercept(
    'GET',
    `${apiUrl}/projects/${mockedProject.in_progress_clone.id}`,
    mockedProject.in_progress_clone
  );

  cy.intercept('GET', '${apiUrl}/users/me/wallet', {
    wallet:
      '{"address":"8bf0b6bc1d0e850da4f5d64662a0b958ed92d5f0","id":"0919a13b-606a-4f6c-8d9f-14c154d49687","version":3,"Crypto":{"cipher":"aes-128-ctr","cipherparams":{"iv":"c32f022dee166c18331e250f0288fe0b"},"ciphertext":"f106ffbbfd598080629a42a26ba5240ca9acc0e88877782e374ac333fa6d30c2","kdf":"scrypt","kdfparams":{"salt":"69fab56a2530ae04776308cfa7340063e2de07bebbefa8f2a2e488a0fb82031b","n":131072,"dklen":32,"p":1,"r":8},"mac":"7e27bd07cb5cb8f2940c18b43b8754899fc43ff7f759d05deabb799c323e059d"},"x-ethers":{"client":"ethers.js","gethFilename":"UTC--2023-03-08T18-57-40.0Z--8bf0b6bc1d0e850da4f5d64662a0b958ed92d5f0","mnemonicCounter":"1218a02faddddb90036fd3f0a0b1b6a2","mnemonicCiphertext":"79b4e7cbf03c09ce6f3b781f789cff5f","path":"m/44\'/60\'/0\'/0/0","locale":"en","version":"0.1"}}'
  });
  cy.intercept(
    'GET',
    `${apiUrl}/projects/${mockedProject.draft.stepsCompleted.id}/evidences`,
    mockedEvidences.draft
  );
  cy.intercept(
    'GET',
    `${apiUrl}/projects/${mockedProject.in_progress.id}/evidences`,
    mockedEvidences.in_progress
  );
  cy.intercept(
    'GET',
    `${apiUrl}/projects/${mockedProject.in_progress_clone.id}/evidences`,
    mockedEvidences.in_progress
  );
  cy.intercept(
    'GET',
    `${apiUrl}/projects/${mockedProject.draft.stepsCompleted.id}/changelog`,
    mockedChangelog.draft
  );
  cy.intercept(
    'GET',
    `${apiUrl}/projects/${mockedProject.in_progress.id}/changelog`,
    mockedChangelog.in_progress
  );
  cy.intercept(
    'GET',
    `${apiUrl}/projects/${mockedProject.in_progress.id}/changelog?revisionId=2`,
    mockedChangelog.in_progress
  );

  cy.intercept('GET', `${apiUrl}/tokens`, {
    tokens: [
      {
        id: '1',
        name: 'ETH',
        symbol: 'ETH'
      },
      {
        id: '2',
        name: 'ETC',
        symbol: 'ETC'
      },
      {
        id: '3',
        name: 'Tether USD',
        symbol: 'USDT'
      },
      {
        id: '4',
        name: 'RBTC',
        symbol: 'RBTC'
      },
      {
        id: '5',
        name: 'Dollar on Chain',
        symbol: 'DOC'
      },
      {
        id: '6',
        name: 'RIF Dollar on Chain',
        symbol: 'RDOC'
      }
    ]
  });

  cy.intercept('DELETE', `${apiUrl}/user-project`, {});
  cy.intercept('GET', `${apiUrl}/users?email=test@gmail.com`, {
    users: []
  });
  cy.intercept('POST', `${apiUrl}/projects/${mockedProject.draft.stepsCompleted.id}/milestones`, {
    statusCode: 201,
    body: { milestoneId: 3 }
  });
  cy.intercept(
    'PUT',
    `${apiUrl}/projects/${mockedProject.in_progress_clone.id}/basic-information`,
    {
      statusCode: 201,
      body: {}
    }
  );
  cy.intercept('PUT', `${apiUrl}/projects/${mockedProject.in_progress_clone.id}/details`, {
    statusCode: 201,
    body: {}
  });

  cy.intercept('GET', `${apiUrl}/users/${mockedUsers.beneficiary.id}/projects`, {
    projects: [
      {
        id: mockedProject.in_progress.id,
        projectName: mockedProject.in_progress.basicInformation.projectName,
        parent: null
      }
    ]
  });

  cy.intercept('PUT', `${apiUrl}/milestones/65`, {
    statusCode: 200,
    body: { milestoneId: 65 }
  });

  cy.intercept('PUT', `${apiUrl}/users/pin`, {});

  cy.intercept('POST', `${apiUrl}/users/wallet`, { id: '111111' });
  cy.intercept('GET', `${apiUrl}/users/me/wallet`, { id: '111111' });

  cy.intercept('GET', `${apiUrl}/evidences/detailEvidenceId`, MOCKED_EVIDENCE[45]);
  cy.intercept('GET', `${apiUrl}/projects/projectId`, {});
  cy.intercept('GET', `${apiUrl}/activities/activityId/evidences`, MOCKED_ACTIVITY_EVIDENCES[22]);
  cy.intercept('GET', '/cardPhotos/0/07621fa4dbb59c0a69fd1bf52ff96338.jpeg', {
    fixture: 'image.jpg'
  });
};
