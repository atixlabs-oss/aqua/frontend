/**
 * AGUA Copyright (C) 2023  Atix Labs
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import { Wallet, utils } from 'ethers';

export const decryptJsonWallet = async (jsonWallet, password) => {
  if (!password) throw new Error('a password is required to decrypt a wallet');
  return Wallet.fromEncryptedJson(jsonWallet, password);
};

export const signTransaction = async (jsonWallet, transaction, password) => {
  if (!transaction) throw new Error('a transaction is required');
  // eslint-disable-next-line no-param-reassign
  delete transaction.from;
  const wallet = await decryptJsonWallet(jsonWallet, password);
  return wallet.signTransaction(transaction);
};

export const generateWalletFromPin = async pin => {
  if (!pin && pin.length >= 12) throw new Error('Pin must be length 12 or longer');
  const random = Wallet.createRandom();
  const { address } = random;
  // Until we find the real cause mnemonic is inconsistent between environment
  const encrypted = await random.encrypt(pin);
  const {
    Crypto: {
      cipherparams: { iv }
    }
  } = JSON.parse(encrypted);
  return {
    address,
    wallet: encrypted,
    iv
  };
};

export const signMessage = async (jsonWallet, message, password) => {
  if (!message) throw new Error('Message is required');

  const wallet = await decryptJsonWallet(jsonWallet, password);
  return wallet.signMessage(utils.arrayify(message));
};
