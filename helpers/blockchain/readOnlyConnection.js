/**
 * AGUA Copyright (C) 2023  Atix Labs
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import ethers from 'ethers';
import projectAbi from 'public/static/abis/ProjectsRegistry.json';
import claimsAbi from 'public/static/abis/ClaimsRegistry.json';
import { links } from '../../constants/links';

export const Events = {
  Creation: 'creation',
  Proposal: 'proposal',
  Audit: 'audit',
  ClaimProposal: 'claimProposal',
  ClaimAudit: 'claimAudit'
};
const failure = 'Could not resolve';

const wait = ms =>
  new Promise(resolve => {
    setTimeout(() => {
      resolve(failure);
    }, ms);
  });

const getProviders = () => {
  const providersUrl = links.providers();
  return providersUrl.map(url => new ethers.providers.JsonRpcProvider(url));
};

export const getTransaction = async txHash => {
  const providers = getProviders();

  for (const provider of providers) {
    try {
      return await provider.getTransaction(txHash);
    } catch (error) {
      continue;
    }
    break;
  }
};

const generateConnections = () => {
  const providers = getProviders();

  const projectRegistryConnections = providers.map(
    p => new ethers.Contract(process.env.NEXT_PUBLIC_PROJECTS_REGISTRY_ADDRESS, projectAbi.abi, p)
  );

  const claimsRegistryConnections = providers.map(
    p => new ethers.Contract(process.env.NEXT_PUBLIC_CLAIMS_REGISTRY_ADDRESS, claimsAbi.abi, p)
  );

  return { projectRegistryConnections, claimsRegistryConnections };
};

const retryingProjectsFilter = async (connections, eventName) => {
  // shuffle
  for (let i = 0; i < connections.projectRegistryConnections.length; i++) {
    const filter = connections.projectRegistryConnections[i].filters[eventName]();
    const result = await Promise.race([
      connections.projectRegistryConnections[i]
        .queryFilter(
          filter,
          parseInt(process.env.NEXT_PUBLIC_BLOCK_NUMBER_AUDIT_BEGIN, 10),
          'latest'
        )
        .catch(e => failure),
      wait(2000)
    ]);
    if (result !== failure) return result;
  }
  throw new Error(failure);
};

const retryingClaimsFilter = async (connections, eventName, argsForFilter) => {
  for (let i = 0; i < connections.claimsRegistryConnections.length; i++) {
    const filter = connections.claimsRegistryConnections[i].filters[eventName](...argsForFilter);
    const result = await Promise.race([
      connections.claimsRegistryConnections[i]
        .queryFilter(
          filter,
          parseInt(process.env.NEXT_PUBLIC_BLOCK_NUMBER_AUDIT_BEGIN, 10),
          'latest'
        )
        .catch(e => failure),
      wait(2000)
    ]);
    if (result !== failure) return result;
  }
  throw new Error(failure);
};

export const getTimestampByBlock = async blockNumber => {
  const providers = getProviders();

  const block = await providers[0].getBlock(blockNumber);
  const toMiliseconds = 1000;
  return new Date(block.timestamp * toMiliseconds).toISOString();
};

export const getSignerFromTransaction = async txHash => {
  const providers = getProviders();

  const pendingTx = await providers[0].getTransaction(txHash);
  return pendingTx.from;
};

export const getVersionsAndIpfsAuditHash = async projectId => {
  const connections = generateConnections();

  const auditRawEvents = await retryingProjectsFilter(connections, 'ProjectEditAudited');
  const proposalRawEvents = await retryingProjectsFilter(connections, 'ProjectEditProposed');
  const creationRawEvents = await retryingProjectsFilter(connections, 'ProjectCreated');

  const claimProposalRawEvents = await retryingClaimsFilter(connections, 'ClaimProposed', [
    projectId,
    null,
    null
  ]);
  const claimAuditRawEvents = await retryingClaimsFilter(connections, 'ClaimAudited', [
    projectId,
    null,
    null
  ]);

  const auditEvents = auditRawEvents
    .filter(e => e.args.id === projectId)
    .map(e => ({
      blockNumber: e.blockNumber,
      fileHash: e.args.auditIpfsHash,
      transactionHash: e.transactionHash,
      type: Events.Audit,
      approved: e.args.approved,
      proposer: e.args.proposer
    }));

  const proposalEvents = proposalRawEvents
    .filter(e => e.args.id === projectId)
    .map(e => ({
      blockNumber: e.blockNumber,
      fileHash: e.args.proposedIpfsHash,
      transactionHash: e.transactionHash,
      type: Events.Proposal,
      proposer: e.args.proposer
    }));

  const creationEvents = creationRawEvents
    .filter(e => e.args.id === projectId)
    .map(e => ({
      blockNumber: e.blockNumber,
      fileHash: e.args.ipfsHash,
      transactionHash: e.transactionHash,
      type: Events.Creation
    }));

  const claimAuditEvents = claimAuditRawEvents.map(e => ({
    blockNumber: e.blockNumber,
    fileHash: e.args.proofHash,
    transactionHash: e.transactionHash,
    type: Events.ClaimAudit,
    auditor: e.args.auditor,
    activityId: e.args.activityId.toString(),
    date: e.args.verifiedAt.toString(),
    approved: e.args._approved
  }));

  const claimProposalEvents = claimProposalRawEvents.map(e => ({
    blockNumber: e.blockNumber,
    fileHash: e.args.proofHash,
    transactionHash: e.transactionHash,
    type: Events.ClaimProposal,
    proposer: e.args.proposer,
    activityId: e.args.activityId.toString(),
    date: e.args.proposedAt.toString()
  }));

  const events = auditEvents
    .concat(proposalEvents)
    .concat(creationEvents)
    .concat(claimAuditEvents)
    .concat(claimProposalEvents);

  const sortedEvents = events.sort((e2, e1) => e1.blockNumber - e2.blockNumber);

  const populatedEvents = await Promise.all(
    sortedEvents.map(async e => {
      const response = await fetch(`${process.env.NEXT_PUBLIC_IPFS_PROVIDER}/${e.fileHash}`);
      const json = await response.json();
      return { data: json, ...e };
    })
  );

  return populatedEvents;
};
