/**
 * AGUA Copyright (C) 2023  Atix Labs
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import { ROLES_IDS } from 'components/organisms/AssignProjectUsers/constants';

export const getProjectUsersPerRol = users => {
  const beneficiaries = users?.find(user => user?.role === ROLES_IDS.beneficiary.toString())?.users;
  const investors = users?.find(user => user?.role === ROLES_IDS.investor.toString())?.users;
  const auditors = users?.find(user => user?.role === ROLES_IDS.auditor.toString())?.users;
  return { beneficiaries, investors, auditors };
};

export const checkProjectHasAllUsersRoles = ({ beneficiaries, investors, auditors }) =>
  beneficiaries?.length > 0 && investors?.length > 0 && auditors?.length > 0;

export const checkProjectHasAllUsersWithFirstLogin = ({ beneficiaries, investors, auditors }) => {
  const allBeneficiariesWithFirstLogin = beneficiaries?.every(beneficiary => !beneficiary?.first);
  const allInvestorsWithFirstLogin = investors?.every(investor => !investor?.first);
  const allAuditorsWithFirstLogin = auditors?.every(auditor => !auditor?.first);

  return allBeneficiariesWithFirstLogin && allInvestorsWithFirstLogin && allAuditorsWithFirstLogin;
};

export const checkProjectHasAnyUserWithoutFirstLogin = ({ beneficiaries, investors, auditors }) => {
  const allBeneficiariesWithFirstLogin = beneficiaries?.some(beneficiary => beneficiary?.first);
  const allInvestorsWithFirstLogin = investors?.some(investor => investor?.first);
  const allAuditorsWithFirstLogin = auditors?.some(auditor => auditor?.first);
  return allBeneficiariesWithFirstLogin || allInvestorsWithFirstLogin || allAuditorsWithFirstLogin;
};

export const getUsersByRole = (role, users) =>
  users?.filter(user => user?.role === role.toString())?.[0]?.users;
