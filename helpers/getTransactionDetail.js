/**
 * AGUA Copyright (C) 2023  Atix Labs
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import errorFormatter from './errorFormatter';
import activityAbi from 'public/static/abis/ClaimsRegistry.json';
import projectAbi from 'public/static/abis/ProjectsRegistry.json';
import abiDecoder from 'abi-decoder';
import { getTransaction } from './blockchain/readOnlyConnection';

export const mapOperationAbi = {
  publish_project: projectAbi.abi,
  send_project_to_review: projectAbi.abi,
  approve_review: projectAbi.abi,
  cancel_review: projectAbi.abi,
  activity_to_review: activityAbi.abi,
  approve_activity: activityAbi.abi,
  reject_activity: activityAbi.abi
};

const getInputStringFromTransaction = async transactionHash => {
  try {
    const transactionDetails = await getTransaction(transactionHash);
    if (!transactionDetails?.data) throw new Error('Data is not defined');
    return transactionDetails.data;
  } catch (error) {
    throw new Error(errorFormatter(error));
  }
};

export const getTransactionDetails = async ({ action, transactionHash }) => {
  const input = await getInputStringFromTransaction(transactionHash);
  const abi = mapOperationAbi[action];

  abiDecoder.addABI(abi);

  const result = abiDecoder.decodeMethod(input);

  const params = result.params;

  if (!params) throw new Error('Details of the transaction are undefined');

  const txDetails = {};

  params.forEach(({ name, value }) => {
    if (!name || !value) return;
    txDetails[name] = value;
  });

  return txDetails;
};
