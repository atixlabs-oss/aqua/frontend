/**
 * AGUA Copyright (C) 2023  Atix Labs
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import { VALID_EMAIL_REGEX } from 'constants/Regex';
import { ERROR_MESSAGES } from '../constants/constants';

export const getErrorTypes = (errors = []) => {
  const typeOfErrors = [];

  Object.entries(errors).forEach(([, value]) => {
    if (value) typeOfErrors.push(...value);
  });
  const uniqueErrors = [...new Set(typeOfErrors)];
  return uniqueErrors;
};

export const getErrorMessagesFields = (currentErrors, errorsToShow = []) => {
  const typesOfErrors = getErrorTypes(currentErrors);

  const errors = typesOfErrors.map(typeOfError => {
    errorsToShow.includes(typeOfError); //
    if (errorsToShow.includes(typeOfError)) return ERROR_MESSAGES[typeOfError];
    return undefined;
  });

  const cleanedErrors = errors.filter(error => error);

  return cleanedErrors;
};

export const getErrorMessagesField = (currentErrorState = [], errorsToShow = []) => {
  const errors = currentErrorState.map(typeOfError => {
    errorsToShow.includes(typeOfError);
    if (errorsToShow.includes(typeOfError)) return ERROR_MESSAGES[typeOfError];
    return undefined;
  });

  const cleanedErrors = errors.filter(error => error);

  return cleanedErrors;
};

export const checkValidEmail = input => VALID_EMAIL_REGEX.test(input);

export const getExtensionFromUrl = (url = '') => {
  const urlSplitted = url.split('.');
  const lastElement = urlSplitted[urlSplitted.length - 1];
  return lastElement;
};

export const decimalCount = num => {
  const numStr = String(num);
  if (numStr.includes('.')) {
    return numStr.split('.')[1].length;
  }
  return 0;
};

export const capitalizeFirstLetter = (string = '') =>
  string.charAt?.(0)?.toUpperCase() + string?.slice(1);

const months = [
  'January',
  'February',
  'March',
  'April',
  'May',
  'June',
  'July',
  'August',
  'September',
  'October',
  'November',
  'December'
];

const nth = d => {
  if (d > 3 && d < 21) return 'th';
  switch (d % 10) {
    case 1:
      return 'st';
    case 2:
      return 'nd';
    case 3:
      return 'rd';
    default:
      return 'th';
  }
};

export const getDateAndTime = (date, format) => {
  if (!date) return '';
  const newDate = new Date(date).toLocaleString('en-us', { hour12: false });

  const [_date, time] = newDate?.split(',');

  const localeDateParsed = new Date(_date);

  const [hours, minutes] = time?.split?.(' ')[1]?.split(':');

  const month = localeDateParsed.getMonth() + 1;
  const monthLeadWithZero = String(month).padStart(2, '0');
  const day = localeDateParsed.getDate();
  const dayLeadWithZero = String(day).padStart(2, '0');
  const year = localeDateParsed.getFullYear();

  if (format === 'minimal')
    return `${dayLeadWithZero}/${monthLeadWithZero}/${year} - ${hours}:${minutes}`;
  return `${months[month]}, ${day}${nth(day)} ${year} - ${hours}:${minutes}`;
};

export const checkHasActivityPending = ({ milestones }) => {
  const allActivities = milestones.reduce((acc, curr) => [...acc, ...curr?.activities], []);
  return allActivities.some(activity => activity?.status.includes('pending'));
};

export const checkIsAuditorOfAnActivity = ({ milestones, auditorId }) => {
  const allActivities = milestones?.reduce((acc, curr) => [...acc, ...curr.activities], []);
  return allActivities.some(activity => activity?.auditor?.id === auditorId);
};

export const sortActivitiesById = ({ activities }) => activities.sort((a, b) => a?.id - b?.id);
