/**
 * AGUA Copyright (C) 2023  Atix Labs
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import { startCase } from 'lodash';
import { CURRENCIES } from '../constants/constants';

export const getInitials = fullName => {
  if (!fullName) return;
  let initials = fullName.match(/\b\w/g) || [];
  initials = ((initials.shift() || '') + (initials.pop() || '')).toUpperCase();
  return initials;
};

export const formatTimeframeValue = ({ timeframe, timeframeUnit = '', t }) =>
  `${parseFloat(timeframe)?.toFixed(1)} ${t?.(`general.${timeframeUnit}`)}`;

export const formatCurrencyAtTheBeginning = (currency, value) => {
  if (!value && value !== 0) return (0).toFixed(2);
  if (!currency) return Number(value).toFixed(2);

  const isFiat = CURRENCIES.fiat.find(_currency => _currency.value === currency?.toUpperCase());
  const decimals = isFiat ? 2 : 8;

  return `${currency} ${Number(value).toFixed(decimals)}`;
};

export const formatCurrency = (currency, value, isCurrencyLabelAtEnd = false) => {
  if (!value && value !== 0) return (0).toFixed(2);
  if (!currency) return Number(value).toFixed(2);

  const isFiat = CURRENCIES.fiat.find(_currency => _currency.value === currency?.toUpperCase());
  const decimals = isFiat ? 2 : 8;
  let finalFormat = '';

  if (isCurrencyLabelAtEnd) {
    finalFormat = `${Number(value).toFixed(decimals)} ${currency}`;
  } else {
    try {
      finalFormat = new Intl.NumberFormat('en-US', {
        style: 'currency',
        currency,
        minimumFractionDigits: 2,
        maximumFractionDigits: 8
      }).format(Number(value).toFixed(decimals));
    } catch (error) {
      finalFormat = `${currency} ${Number(value).toFixed(decimals)}`;
    }
  }

  const [integerPart, decimalPart] = finalFormat.split('.');
  return `${integerPart}.${isFiat ? decimalPart.padEnd(2, '0') : decimalPart.padEnd(8, '0')}`;
};

export const removeDecimals = number => number?.toString().split('.')[0];

export const formatLeadWithZero = number => (number < 10 ? `0${number}`.slice(-2) : number);

export const addMiddleEllipsis = (text, maxLength = 8) => {
  if (!text) return;
  if (text.length < maxLength) return text;
  const start = text.slice(0, 5);
  const end = text.slice(-6);
  return `${start}...${end}`;
};

export const processFirstAndLastName = ({ firstName, lastName }) => {
  if (!firstName && !lastName) return 'No name';
  const noLastName = !lastName || lastName === '';
  const noFirstName = !firstName || firstName === '';
  if (noLastName && !noFirstName) return startCase(firstName);
  if (noFirstName && !noLastName) return startCase(lastName);
  return `${startCase(firstName)} ${startCase(lastName)}`;
};
