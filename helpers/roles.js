/**
 * AGUA Copyright (C) 2023  Atix Labs
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

const { ROLES_IDS } = require('components/organisms/AssignProjectUsers/constants');

export const checkIsActivityAuditor = ({ user, activity }) => {
  if (!user) return false;
  return user?.id === activity?.auditor;
};

export const checkIsBeneficiaryByProject = ({ user, project }) => {
  const userId = user?.id;
  const projectUsers = project?.users;
  const beneficiary = projectUsers?.find(
    projectUser => parseInt(projectUser?.role, 10) === ROLES_IDS.beneficiary
  )?.users?.[0];
  return beneficiary?.id === userId;
};

export const checkIsInvestorByProject = ({ user, project }) => {
  const userId = user?.id;
  const projectUsers = project?.users;
  const investor = projectUsers?.find(
    projectUser => parseInt(projectUser?.role, 10) === ROLES_IDS.investor
  )?.users?.[0];
  return investor?.id === userId;
};

export const checkIsAuditorByProject = ({ user, project }) => {
  const userId = user?.id;
  const projectUsers = project?.users;
  const auditors = projectUsers?.find(
    projectUser => parseInt(projectUser?.role, 10) === ROLES_IDS.auditor
  )?.users;
  return auditors?.some(auditor => auditor?.id === userId);
};

export const checkIsBeneficiaryOrInvestorByProject = ({ user, project }) =>
  checkIsBeneficiaryByProject({ user, project }) || checkIsInvestorByProject({ user, project });

export const checkRoleByProject = ({ user, project }) => {
  if (user?.isAdmin) return { name: 'Administrator' };
  if (checkIsBeneficiaryByProject({ user, project }))
    return { id: ROLES_IDS.beneficiary, name: 'Beneficiary' };
  if (checkIsInvestorByProject({ user, project }))
    return { id: ROLES_IDS.investor, name: 'Investor' };
  if (checkIsAuditorByProject({ user, project })) return { id: ROLES_IDS.auditor, name: 'Auditor' };
};
