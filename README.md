# AGUA - Frontend

[![License: AGPL v3](https://img.shields.io/badge/License-AGPL_v3-blue.svg)](https://www.gnu.org/licenses/agpl-3.0)
[![Coverage Status](https://coveralls.io/repos/gitlab/atixlabs-oss/agua/frontend/badge.svg?branch=develop)](https://coveralls.io/repos/gitlab/atixlabs-oss/agua/frontend?branch=develop)

AGUA seeks to help organizations to have a clear and transparent framework for the definition and execution of projects



## Prerequisites

- Node v14.18.1

## Tools and frameworks

- react@17.0.2

- next@12.1.0

- antd@3.17.0

## Contributing

Clone the repository by running `git@gitlab.com:atixlabs-oss/agua/frontend.git` and create a new branch from the latest development branch

## Install

- Clone repository

```bash
$ git clone https://gitlab.com/atixlabs-oss/agua/frontend.git
$ cd frontend
$ nvm use
$ npm install
```

- Edit .env file

```
NEXT_PUBLIC_RECAPTCHA_SITE_KEY='xxxxxx'
NEXT_PUBLIC_URL_HOST=https://yourhost.com
```

You can use the .env.example in order to see what variables are you able to set

- Build

```bash
$ npm run build
```

Also, take into account that if you are building the project for a second time and you have changed .env variables related to colors you should use this script instead

```bash
$ npm run clean-build
```

NOTE: There is also an script provided that will check if the .env scss variables have changed and decide what type of build need to be executed (recommended)

```bash
$ npm run build-decision
```

## Development

- Run `npm run dev` to run the app in development mode
- Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

## Running a Production build

- Run `npm run build` to create an optimized production build
- Run `npm start` to start the app
- Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

## Build Docker image.

```bash
$ docker build --pull --rm -f "Dockerfile" -t agua-frontend:latest "."
```


## TODO:

- Try to stop using global styles progressively
- Create a wrapper for routes in order to limit the access to private routes
