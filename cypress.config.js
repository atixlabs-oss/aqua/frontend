const { defineConfig } = require('cypress');

module.exports = defineConfig({
  env: {
    codeCoverage: {
      url: '/api/__coverage__'
    },
    API_URL: 'http://localhost:3001'
  },
  e2e: {
    setupNodeEvents(on, config) {
      // implement node event listeners here
      require('@cypress/code-coverage/task')(on, config);
      return config;
    },
    baseUrl: 'http://localhost:3000',
    viewportWidth: 1920,
    viewportHeight: 1080,
    video: false
  }
});
