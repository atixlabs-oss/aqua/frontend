/**
 * AGUA Copyright (C) 2023  Atix Labs
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import i18n from 'i18n';

const { t } = i18n;

const activityStatusMap = {
  new: { name: t('activities.Not Started'), color: 'gray' },
  approved: { name: t('activities.Approved'), color: 'green' },
  'to-review': { name: t('activities.In review'), color: 'violet' },
  rejected: { name: t('activities.Rejected'), color: 'red' },
  in_progress: { name: t('activities.In Progress'), color: 'orange' },
  'pending to review': { name: t('activities.Changing to In Review'), color: 'pink' },
  'pending to approve': { name: t('activities.Changing to Approved'), color: 'pink' },
  'pending to reject': { name: t('activities.Changing to Rejected'), color: 'pink' }
};

export const ACTIVITY_STATUS_ENUM = {
  NEW: 'new',
  APPROVED: 'approved',
  TO_REVIEW: 'to-review',
  REJECTED: 'rejected',
  IN_PROGRESS: 'in_progress',
  PENDING_TO_REVIEW: 'pending to review',
  PENDING_TO_APPROVE: 'pending to approve',
  PENDING_TO_REJECT: 'pending to reject'
};

export default activityStatusMap;
