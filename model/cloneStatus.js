/**
 * AGUA Copyright (C) 2023  Atix Labs
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import i18n from 'i18n';

const { t } = i18n;

export const CLONE_STATUS_ENUM = {
  OPEN_REVIEW: 'open review',
  IN_REVIEW: 'in review',
  PENDING_REVIEW: 'pending review',
  PENDING_APPROVE_REVIEW: 'pending approve review',
  PENDING_CANCEL_REVIEW: 'pending cancel review'
};

export const CLONE_STATUS_MAP = {
  [CLONE_STATUS_ENUM.OPEN_REVIEW]: { name: t('projects.Open for review'), color: 'gray' },
  [CLONE_STATUS_ENUM.IN_REVIEW]: { name: t('projects.In review'), color: 'violet' },
  [CLONE_STATUS_ENUM.PENDING_REVIEW]: { name: t('projects.Pending review'), color: 'pink' },
  [CLONE_STATUS_ENUM.PENDING_APPROVE_REVIEW]: {
    name: t('projects.Pending approve review'),
    color: 'pink'
  },
  [CLONE_STATUS_ENUM.PENDING_CANCEL_REVIEW]: {
    name: t('projects.Pending cancel review'),
    color: 'pink'
  }
};
