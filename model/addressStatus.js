/**
 * AGUA Copyright (C) 2023  Atix Labs
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import i18n from 'i18n';

const { t } = i18n;

export const ADDRESS_STATUS_ENUM = {
  IN_REVIEW: 'in review',
  APPROVED: 'approved',
  REJECTED: 'rejected',
  ACTIVE: 'active',
  INACTIVE: 'inactive'
};

export const ADDRESS_STATUS_MAP = {
  [ADDRESS_STATUS_ENUM.IN_REVIEW]: {
    name: t('profile.pinStatus.requestInReview'),
    color: 'violet',
    tooltip: t('profile.pinStatus.requestInReviewInfo')
  },
  [ADDRESS_STATUS_ENUM.APPROVED]: {
    name: t('profile.pinStatus.requestApproved'),
    color: 'yellow',
    tooltip: t('profile.pinStatus.requestApprovedInfo')
  },
  [ADDRESS_STATUS_ENUM.REJECTED]: {
    name: t('profile.pinStatus.requestRejected'),
    color: 'red',
    tooltip: t('profile.pinStatus.requestRejectedInfo')
  },
  [ADDRESS_STATUS_ENUM.ACTIVE]: {
    name: t('profile.pinStatus.active'),
    color: 'blue',
    tooltip: t('profile.pinStatus.activeInfo')
  },
  [ADDRESS_STATUS_ENUM.INACTIVE]: {
    name: t('profile.pinStatus.inactive'),
    color: 'gray',
    tooltip: t('profile.pinStatus.inactiveInfo')
  }
};
