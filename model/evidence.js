/**
 * AGUA Copyright (C) 2023  Atix Labs
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import i18n from 'i18n';

const { t } = i18n;

export const EVIDENCE_STATUS_ENUM = {
  NEW: 'new',
  APPROVED: 'approved',
  REJECTED: 'rejected'
};

export const EVIDENCE_TYPES_ENUM = {
  TRANSFER: 'transfer',
  IMPACT: 'impact'
};

export const EVIDENCE_STATUS_MAP = {
  new: { name: t('evidences.New'), color: 'blue' },
  approved: { name: t('evidences.Approved'), color: 'green' },
  rejected: { name: t('evidences.Rejected'), color: 'red' }
};
