export const VISIBILITY_TYPES_ENUM = {
  PRIVATE: 'private',
  PUBLISHED: 'published',
  HIGHLIGHTED: 'highlighted'
};
