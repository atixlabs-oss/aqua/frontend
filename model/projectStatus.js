/**
 * AGUA Copyright (C) 2023  Atix Labs
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import i18n from 'i18n';

const { t } = i18n;

export const PROJECT_STATUS_ENUM = {
  DRAFT: 'draft',
  IN_PROGRESS: 'in progress',
  OPEN_REVIEW: 'open review',
  IN_REVIEW: 'in review',
  CANCELLED: 'cancelled',
  COMPLETED: 'completed',
  PUBLISHED: 'published',
  PENDING_PUBLISHED: 'pending published',
  PENDING_TO_REVIEW: 'pending to review'
};

export const PROJECT_STATUS_MAP = {
  draft: { name: t('projects.Draft'), color: 'gray' },
  'in progress': { name: t('projects.In Progress'), color: 'yellow' },
  'in review': { name: t('projects.In review'), color: 'violet' },
  'open review': { name: t('projects.Open for review'), color: 'brown' },
  cancelled: { name: t('projects.Cancelled'), color: 'red' },
  completed: { name: t('projects.Completed'), color: 'green' },
  published: { name: t('projects.Published'), color: 'blue' },
  'pending published': { name: t('projects.Changing to Published'), color: 'pink' },
  'pending to review': { name: t('projects.Changing to In Review'), color: 'pink' }
};
