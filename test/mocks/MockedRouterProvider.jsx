/**
 * AGUA Copyright (C) 2023  Atix Labs
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import React from 'react';
import { RouterContext } from 'next/dist/shared/lib/router-context';

const _router = {
  basePath: '',
  pathname: '/',
  query: {},
  asPath: '/',
  push: () => {},
  replace: () => {},
  reload: () => {},
  back: () => {},
  prefetch: async () => {},
  beforePopState: () => {},
  isFallback: false,
  events: {
    on: () => {},
    off: () => {},
    emit: () => {}
  }
};

export const MockedRouterProvider = ({ router, children }) => (
  <RouterContext.Provider value={{ ..._router, ...router }}>{children}</RouterContext.Provider>
);
