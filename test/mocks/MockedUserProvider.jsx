/**
 * AGUA Copyright (C) 2023  Atix Labs
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

const { UserContext } = require('components/utils/UserContext');

const mockedUser = {
  firstName: 'Administrator',
  lastName: '',
  email: 'federico.catala@globant.com',
  id: '28b3a91d-c7e3-44c6-b8ea-5b72650694bd',
  isAdmin: true,
  forcePasswordChange: false,
  first: false,
  pin: true,
  seenModal: false
};

export const MockedUserProvider = ({ children, user, withUser }) => (
  <UserContext.Provider value={{ user: user ? user : withUser && mockedUser }}>
    {children}
  </UserContext.Provider>
);
