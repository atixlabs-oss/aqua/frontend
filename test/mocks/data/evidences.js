/**
 * AGUA Copyright (C) 2023  Atix Labs
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

export const mockedEvidences = {
  in_progress: {
    evidences: [
      {
        date: '2023-02-22T20:01:16.009Z',
        role: 'beneficiary',
        userName: 'Firstname Beneficiary',
        activityType: 'spending',
        amount: '25',
        destinationAccount: '0xceddc4a2a24f04a59194e7244f0475d78c10b332'
      }
    ]
  },
  draft: {}
};

export const mockedEvidencesOfActivity = {
  in_progress: {
    milestone: {
      id: 65,
      title: 'Milestone 1'
    },
    activity: {
      id: 172,
      title: 'Activity 1',
      description: 'adssad',
      acceptanceCriteria: 'asddsa',
      budget: '100',
      currency: 'USDT',
      auditor: '4d3aa496-943b-474f-a936-a037ab325b75',
      status: 'to-review',
      type: 'funding',
      current: '25',
      parent: null
    },
    evidences: [
      {
        id: 144,
        title: 'transfer',
        description: 'asdds',
        type: 'transfer',
        transferTxHash: '0x620b0b9b5d5f5c66ca6a3ee2d3059e123f3a3e5d82984d154633291e893a994b',
        proof: null,
        approved: null,
        txHash: null,
        status: 'rejected',
        reason: 'ads',
        createdAt: '2023-02-22T18:51:30.041Z',
        amount: '25',
        destinationAccount: '0xac4d2ac230876f8bab54144cee315bc22250661f',
        activity: 172,
        user: 'b067444b-1cae-4059-9d1c-576c24166f3d',
        auditor: '4d3aa496-943b-474f-a936-a037ab325b75',
        parent: null
      },
      {
        id: 145,
        title: 'dasdas',
        description: 'adsdsa',
        type: 'transfer',
        transferTxHash: '0x90778c3dd9b5f5edd724b171ef256453291d733b96d30a95c5b8697dd5ed8eb4',
        proof: null,
        approved: null,
        txHash: null,
        status: 'approved',
        reason: null,
        createdAt: '2023-02-22T19:22:48.642Z',
        amount: '25',
        destinationAccount: '0xac4d2ac230876f8bab54144cee315bc22250661f',
        activity: 172,
        user: 'b067444b-1cae-4059-9d1c-576c24166f3d',
        auditor: '4d3aa496-943b-474f-a936-a037ab325b75',
        parent: null
      }
    ]
  }
};
