/**
 * AGUA Copyright (C) 2023  Atix Labs
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

export const mockedProject = {
  in_progress: {
    milestones: [
      {
        id: 65,
        title: 'Milestone 1',
        description: 'asddsa',
        funding: {
          budget: '100',
          current: '25'
        },
        spending: {
          budget: '100',
          current: '50'
        },
        payback: {
          budget: '0',
          current: '0'
        },
        status: 'in progress',
        activities: [
          {
            id: 172,
            title: 'Activity 1',
            description: 'adssad',
            acceptanceCriteria: 'asddsa',
            budget: '100',
            currency: 'USDT',
            auditor: {
              id: '4d3aa496-943b-474f-a936-a037ab325b75',
              firstName: 'Firstname',
              lastName: 'Auditor 1'
            },
            status: 'to-review',
            type: 'funding',
            current: '25',
            parent: null,
            evidences: [
              {
                id: 144,
                title: 'transfer',
                description: 'asdds',
                type: 'transfer',
                transferTxHash:
                  '0x620b0b9b5d5f5c66ca6a3ee2d3059e123f3a3e5d82984d154633291e893a994b',
                proof: null,
                approved: null,
                txHash: null,
                status: 'rejected',
                reason: 'ads',
                createdAt: '2023-02-22T18:51:30.041Z',
                amount: '25',
                destinationAccount: '0xac4d2ac230876f8bab54144cee315bc22250661f',
                activity: 172,
                user: 'b067444b-1cae-4059-9d1c-576c24166f3d',
                auditor: '4d3aa496-943b-474f-a936-a037ab325b75',
                parent: null
              },
              {
                id: 145,
                title: 'dasdas',
                description: 'adsdsa',
                type: 'transfer',
                transferTxHash:
                  '0x90778c3dd9b5f5edd724b171ef256453291d733b96d30a95c5b8697dd5ed8eb4',
                proof: null,
                approved: null,
                txHash: null,
                status: 'approved',
                reason: null,
                createdAt: '2023-02-22T19:22:48.642Z',
                amount: '25',
                destinationAccount: '0xac4d2ac230876f8bab54144cee315bc22250661f',
                activity: 172,
                user: 'b067444b-1cae-4059-9d1c-576c24166f3d',
                auditor: '4d3aa496-943b-474f-a936-a037ab325b75',
                parent: null
              }
            ]
          },
          {
            id: 173,
            title: 'Activity 2',
            description: 'asdas',
            acceptanceCriteria: 'asdsa',
            budget: '100',
            currency: 'USDT',
            auditor: {
              id: '4d3aa496-943b-474f-a936-a037ab325b75',
              firstName: 'Firstname',
              lastName: 'Auditor 1'
            },
            status: 'to-review',
            type: 'spending',
            current: '50',
            parent: null,
            evidences: [
              {
                id: 143,
                title: 'Transfer',
                description: 'addsa',
                type: 'transfer',
                transferTxHash:
                  '0xb5a94a017aa9957d6768f3594b78146331da41f2184a339a4ae7367c13a87825',
                proof: null,
                approved: null,
                txHash: null,
                status: 'rejected',
                reason: 'asd',
                createdAt: '2023-02-22T18:25:25.582Z',
                amount: '50',
                destinationAccount: '0xceddc4a2a24f04a59194e7244f0475d78c10b332',
                activity: 173,
                user: '37495329-004d-422b-8ce7-0e8155bb0183',
                auditor: '4d3aa496-943b-474f-a936-a037ab325b75',
                parent: null
              },
              {
                id: 146,
                title: 'transfer 2',
                description: 'asddsa',
                type: 'transfer',
                transferTxHash:
                  '0xa9e8848d39ebcdaa4ff837e4f100b50ab3f8b0f646dd3b97638900934f45eb8f',
                proof: null,
                approved: null,
                txHash: null,
                status: 'rejected',
                reason: 'asd',
                createdAt: '2023-02-22T19:32:37.598Z',
                amount: '25',
                destinationAccount: '0xceddc4a2a24f04a59194e7244f0475d78c10b332',
                activity: 173,
                user: '37495329-004d-422b-8ce7-0e8155bb0183',
                auditor: '4d3aa496-943b-474f-a936-a037ab325b75',
                parent: null
              },
              {
                id: 147,
                title: 'transfer 3',
                description: 'asd',
                type: 'transfer',
                transferTxHash:
                  '0xb5a94a017aa9957d6768f3594b78146331da41f2184a339a4ae7367c13a87825',
                proof: null,
                approved: null,
                txHash: null,
                status: 'approved',
                reason: null,
                createdAt: '2023-02-22T19:39:33.037Z',
                amount: '50',
                destinationAccount: '0xceddc4a2a24f04a59194e7244f0475d78c10b332',
                activity: 173,
                user: '37495329-004d-422b-8ce7-0e8155bb0183',
                auditor: '4d3aa496-943b-474f-a936-a037ab325b75',
                parent: null
              }
            ]
          }
        ],
        parent: null
      },
      {
        id: 66,
        title: 'Milestone 2',
        description: 'asddsa',
        funding: {
          budget: '100',
          current: '0'
        },
        spending: {
          budget: '100',
          current: '25'
        },
        payback: {
          budget: '0',
          current: '0'
        },
        status: 'in progress',
        activities: [
          {
            id: 174,
            title: 'Activity 3',
            description: 'asda',
            acceptanceCriteria: 'adsdsa',
            budget: '100',
            currency: 'USDT',
            auditor: {
              id: '4d3aa496-943b-474f-a936-a037ab325b75',
              firstName: 'Firstname',
              lastName: 'Auditor 1'
            },
            status: 'to-review',
            type: 'funding',
            current: '0',
            parent: null,
            evidences: [
              {
                id: 150,
                title: 'Transfer 1',
                description: 'asddsa',
                type: 'transfer',
                transferTxHash:
                  '0x620b0b9b5d5f5c66ca6a3ee2d3059e123f3a3e5d82984d154633291e893a994b',
                proof: null,
                approved: null,
                txHash: null,
                status: 'new',
                reason: null,
                createdAt: '2023-02-22T20:21:26.149Z',
                amount: '25',
                destinationAccount: '0xac4d2ac230876f8bab54144cee315bc22250661f',
                activity: 174,
                user: 'b067444b-1cae-4059-9d1c-576c24166f3d',
                auditor: null,
                parent: null
              }
            ]
          },
          {
            id: 175,
            title: 'Activity 4',
            description: 'asdsda',
            acceptanceCriteria: 'adsdas',
            budget: '100',
            currency: 'USDT',
            auditor: {
              id: '4d3aa496-943b-474f-a936-a037ab325b75',
              firstName: 'Firstname',
              lastName: 'Auditor 1'
            },
            status: 'approved',
            type: 'spending',
            current: '25',
            parent: null,
            evidences: [
              {
                id: 148,
                title: 'asd',
                description: 'asd',
                type: 'transfer',
                transferTxHash:
                  '0xb5a94a017aa9957d6768f3594b78146331da41f2184a339a4ae7367c13a87825',
                proof: null,
                approved: null,
                txHash: null,
                status: 'rejected',
                reason: 'ad',
                createdAt: '2023-02-22T19:52:17.806Z',
                amount: '50',
                destinationAccount: '0xceddc4a2a24f04a59194e7244f0475d78c10b332',
                activity: 175,
                user: '37495329-004d-422b-8ce7-0e8155bb0183',
                auditor: '4d3aa496-943b-474f-a936-a037ab325b75',
                parent: null
              },
              {
                id: 149,
                title: 'TRANSFER 2',
                description: 'ASD',
                type: 'transfer',
                transferTxHash:
                  '0xa9e8848d39ebcdaa4ff837e4f100b50ab3f8b0f646dd3b97638900934f45eb8f',
                proof: null,
                approved: null,
                txHash: null,
                status: 'approved',
                reason: null,
                createdAt: '2023-02-22T20:01:16.009Z',
                amount: '25',
                destinationAccount: '0xceddc4a2a24f04a59194e7244f0475d78c10b332',
                activity: 175,
                user: '37495329-004d-422b-8ce7-0e8155bb0183',
                auditor: '4d3aa496-943b-474f-a936-a037ab325b75',
                parent: null
              }
            ]
          }
        ],
        parent: null
      }
    ],
    dataComplete: 11,
    proposal: null,
    faqLink: null,
    agreementJson: null,
    coverPhotoPath: null,
    milestonePath: null,
    agreementFileHash: 'QmPDW9VA848kJ1uypQKFeupKFPnaQysAgaVg4DBjTsdEgQ',
    proposalFileHash: 'QmSeRu4tMJH7ZsqncDNz4Ct5a8aMsaAmCnco8iVyEGY5Sv',
    status: 'in progress',
    createdAt: '2023-02-22T18:15:25.753Z',
    address: null,
    lastUpdatedStatusAt: '2023-02-22T20:21:26.154Z',
    id: 'mockedId_inProgress',
    txHash: null,
    rejectionReason: null,
    ipfsHash: 'QmUHqWNzoXCYuTmBBAC4etsKWYfTYAiodimc1BzBxxAaUm',
    revision: 1,
    step: 0,
    type: 'grant',
    owner: '28b3a91d-c7e3-44c6-b8ea-5b72650694bd',
    proposer: null,
    parent: null,
    budget: '200',
    basicInformation: {
      projectName: 'Fede 22 Feb',
      location: '(Islamic Republic of) Iran',
      timeframe: '12.000',
      timeframeUnit: 'months',
      thumbnailPhoto: '/cardPhotos/0/07621fa4dbb59c0a69fd1bf52ff96338.jpeg',
      beneficiary: {
        id: '37495329-004d-422b-8ce7-0e8155bb0183',
        lastName: 'Beneficiary',
        firstName: 'Firstname'
      }
    },
    users: [
      {
        roleDescription: 'beneficiary',
        role: '1',
        users: [
          {
            id: '37495329-004d-422b-8ce7-0e8155bb0183',
            firstName: 'Firstname',
            lastName: 'Beneficiary',
            email: 'federico.catala+20@globant.com',
            country: 31,
            first: false,
            address: '0xCCA834c18DF8208a4d4FacBFeeCe31af55DF9346'
          }
        ]
      },
      {
        roleDescription: 'investor',
        role: '2',
        users: [
          {
            id: 'b067444b-1cae-4059-9d1c-576c24166f3d',
            firstName: 'Firstname',
            lastName: 'Investor',
            email: 'federico.catala+21@globant.com',
            country: 31,
            first: false,
            address: '0x56129104491774918F140F023b8BE131fAAffEAD'
          }
        ]
      },
      {
        roleDescription: 'auditor',
        role: '3',
        users: [
          {
            id: '4d3aa496-943b-474f-a936-a037ab325b75',
            firstName: 'Firstname',
            lastName: 'Auditor 1',
            email: 'federico.catala+22@globant.com',
            country: 77,
            first: false,
            address: '0xDc6a8e732e371B1BD857e04353Dc0FCDa0029C53'
          }
        ]
      }
    ],
    details: {
      mission: 'adssda',
      problemAddressed: 'asd',
      currency: 'USDT',
      currencyType: 'Crypto',
      additionalCurrencyInformation: '0xaC4D2aC230876F8bAB54144CEE315bC22250661f',
      legalAgreementFile: '/agreement/7/76e0e837192ebba23502a2d164b53347.pdf',
      projectProposalFile: '/proposal/7/76e0e837192ebba23502a2d164b53347.pdf',
      status: {
        milestones: {
          completed: 0,
          incompleted: 2
        },
        activities: {
          completed: 1,
          incompleted: 3
        },
        budget: '200',
        funding: '0',
        spending: '25',
        payback: '0'
      }
    },
    editing: false,
    cloneId: null
  },
  draft: {
    stepsCompleted: {
      milestones: [
        {
          id: 65,
          title: 'Milestone 1',
          description: 'asddsa',
          funding: {
            budget: '100',
            current: '25'
          },
          spending: {
            budget: '100',
            current: '50'
          },
          payback: {
            budget: '0',
            current: '0'
          },
          status: 'not started',
          activities: [
            {
              id: 172,
              title: 'Activity 1',
              description: 'adssad',
              acceptanceCriteria: 'asddsa',
              budget: '100',
              currency: 'USDT',
              auditor: {
                id: '4d3aa496-943b-474f-a936-a037ab325b75',
                firstName: 'Firstname',
                lastName: 'Auditor 1'
              },
              status: 'new',
              type: 'funding',
              current: '25',
              parent: null,
              evidences: []
            },
            {
              id: 173,
              title: 'Activity 2',
              description: 'asdas',
              acceptanceCriteria: 'asdsa',
              budget: '100',
              currency: 'USDT',
              auditor: {
                id: '4d3aa496-943b-474f-a936-a037ab325b75',
                firstName: 'Firstname',
                lastName: 'Auditor 1'
              },
              status: 'new',
              type: 'spending',
              current: '50',
              parent: null,
              evidences: []
            }
          ],
          parent: null
        },
        {
          id: 66,
          title: 'Milestone 2',
          description: 'asddsa',
          funding: {
            budget: '100',
            current: '0'
          },
          spending: {
            budget: '100',
            current: '25'
          },
          payback: {
            budget: '0',
            current: '0'
          },
          status: 'draft',
          activities: [
            {
              id: 174,
              title: 'Activity 3',
              description: 'asda',
              acceptanceCriteria: 'adsdsa',
              budget: '100',
              currency: 'USDT',
              auditor: {
                id: '4d3aa496-943b-474f-a936-a037ab325b75',
                firstName: 'Firstname',
                lastName: 'Auditor 1'
              },
              status: 'new',
              type: 'funding',
              current: '0',
              parent: null,
              evidences: []
            },
            {
              id: 175,
              title: 'Activity 4',
              description: 'asdsda',
              acceptanceCriteria: 'adsdas',
              budget: '100',
              currency: 'USDT',
              auditor: {
                id: '4d3aa496-943b-474f-a936-a037ab325b75',
                firstName: 'Firstname',
                lastName: 'Auditor 1'
              },
              status: 'new',
              type: 'spending',
              current: '25',
              parent: null,
              evidences: []
            }
          ],
          parent: null
        }
      ],
      dataComplete: 11,
      proposal: null,
      faqLink: null,
      agreementJson: null,
      coverPhotoPath: null,
      milestonePath: null,
      agreementFileHash: 'QmPDW9VA848kJ1uypQKFeupKFPnaQysAgaVg4DBjTsdEgQ',
      proposalFileHash: 'QmSeRu4tMJH7ZsqncDNz4Ct5a8aMsaAmCnco8iVyEGY5Sv',
      status: 'draft',
      createdAt: '2023-02-22T18:15:25.753Z',
      address: null,
      lastUpdatedStatusAt: '2023-02-22T20:21:26.154Z',
      id: 'mockedId_draft_stepsCompleted',
      txHash: null,
      rejectionReason: null,
      ipfsHash: 'QmUHqWNzoXCYuTmBBAC4etsKWYfTYAiodimc1BzBxxAaUm',
      revision: 1,
      step: 0,
      type: 'grant',
      owner: '28b3a91d-c7e3-44c6-b8ea-5b72650694bd',
      proposer: null,
      parent: null,
      budget: '200',
      basicInformation: {
        projectName: 'Fede 22 Feb',
        location: '(Islamic Republic of) Iran',
        timeframe: '12.000',
        timeframeUnit: 'months',
        thumbnailPhoto: '/cardPhotos/0/07621fa4dbb59c0a69fd1bf52ff96338.jpeg',
        beneficiary: {
          id: '37495329-004d-422b-8ce7-0e8155bb0183',
          lastName: 'Beneficiary',
          firstName: 'Firstname'
        }
      },
      users: [
        {
          roleDescription: 'beneficiary',
          role: '1',
          users: [
            {
              id: '37495329-004d-422b-8ce7-0e8155bb0183',
              firstName: 'Firstname',
              lastName: 'Beneficiary',
              email: 'federico.catala+20@globant.com',
              country: 31,
              first: false,
              address: '0xCCA834c18DF8208a4d4FacBFeeCe31af55DF9346'
            }
          ]
        },
        {
          roleDescription: 'investor',
          role: '2',
          users: [
            {
              id: 'b067444b-1cae-4059-9d1c-576c24166f3d',
              firstName: 'Firstname',
              lastName: 'Investor',
              email: 'federico.catala+21@globant.com',
              country: 31,
              first: false,
              address: '0x56129104491774918F140F023b8BE131fAAffEAD'
            }
          ]
        },
        {
          roleDescription: 'auditor',
          role: '3',
          users: [
            {
              id: '4d3aa496-943b-474f-a936-a037ab325b75',
              firstName: 'Firstname',
              lastName: 'Auditor 1',
              email: 'federico.catala+22@globant.com',
              country: 77,
              first: false,
              address: '0xDc6a8e732e371B1BD857e04353Dc0FCDa0029C53'
            }
          ]
        }
      ],
      details: {
        mission: 'adssda',
        problemAddressed: 'asd',
        currency: 'USDT',
        currencyType: 'Crypto',
        additionalCurrencyInformation: '0xaC4D2aC230876F8bAB54144CEE315bC22250661f',
        legalAgreementFile: '/agreement/7/76e0e837192ebba23502a2d164b53347.pdf',
        projectProposalFile: '/proposal/7/76e0e837192ebba23502a2d164b53347.pdf',
        status: {
          milestones: {
            completed: 0,
            incompleted: 2
          },
          activities: {
            completed: 1,
            incompleted: 3
          },
          budget: '200',
          funding: '0',
          spending: '25',
          payback: '0'
        }
      },
      editing: false,
      cloneId: null
    },
    withBasicCompleted: {
      milestones: [],
      dataComplete: 11,
      proposal: null,
      faqLink: null,
      agreementJson: null,
      coverPhotoPath: null,
      milestonePath: null,
      agreementFileHash: '',
      proposalFileHash: '',
      status: 'draft',
      createdAt: '2023-02-22T18:15:25.753Z',
      address: null,
      lastUpdatedStatusAt: '2023-02-22T20:21:26.154Z',
      id: 'mockedId_draft_withBasicCompleted',
      txHash: null,
      rejectionReason: null,
      ipfsHash: 'QmUHqWNzoXCYuTmBBAC4etsKWYfTYAiodimc1BzBxxAaUm',
      revision: 1,
      step: 0,
      type: 'grant',
      owner: '28b3a91d-c7e3-44c6-b8ea-5b72650694bd',
      proposer: null,
      parent: null,
      budget: '',
      basicInformation: {
        projectName: 'Fede 22 Feb',
        location: '(Islamic Republic of) Iran',
        timeframe: '12.000',
        timeframeUnit: 'months',
        thumbnailPhoto: '/cardPhotos/0/07621fa4dbb59c0a69fd1bf52ff96338.jpeg',
        beneficiary: {
          id: '37495329-004d-422b-8ce7-0e8155bb0183',
          lastName: 'Beneficiary',
          firstName: 'Firstname'
        }
      },
      users: [],
      details: {},
      editing: false,
      cloneId: null
    },
    withBasicAndDetailsCompleted: {
      milestones: [],
      dataComplete: 11,
      proposal: null,
      faqLink: null,
      agreementJson: null,
      coverPhotoPath: null,
      milestonePath: null,
      agreementFileHash: 'QmPDW9VA848kJ1uypQKFeupKFPnaQysAgaVg4DBjTsdEgQ',
      proposalFileHash: 'QmSeRu4tMJH7ZsqncDNz4Ct5a8aMsaAmCnco8iVyEGY5Sv',
      status: 'draft',
      createdAt: '2023-02-22T18:15:25.753Z',
      address: null,
      lastUpdatedStatusAt: '2023-02-22T20:21:26.154Z',
      id: 'mockedId_draft_withBasicAndDetailsCompleted',
      txHash: null,
      rejectionReason: null,
      ipfsHash: 'QmUHqWNzoXCYuTmBBAC4etsKWYfTYAiodimc1BzBxxAaUm',
      revision: 1,
      step: 0,
      type: 'grant',
      owner: '28b3a91d-c7e3-44c6-b8ea-5b72650694bd',
      proposer: null,
      parent: null,
      budget: '200',
      basicInformation: {
        projectName: 'Fede 22 Feb',
        location: '(Islamic Republic of) Iran',
        timeframe: '12.000',
        timeframeUnit: 'months',
        thumbnailPhoto: '/cardPhotos/0/07621fa4dbb59c0a69fd1bf52ff96338.jpeg',
        beneficiary: {
          id: '37495329-004d-422b-8ce7-0e8155bb0183',
          lastName: 'Beneficiary',
          firstName: 'Firstname'
        }
      },
      users: [],
      details: {
        mission: 'adssda',
        problemAddressed: 'asd',
        currency: 'USDT',
        currencyType: 'Crypto',
        additionalCurrencyInformation: '0xaC4D2aC230876F8bAB54144CEE315bC22250661f',
        legalAgreementFile: '/agreement/7/76e0e837192ebba23502a2d164b53347.pdf',
        projectProposalFile: '/proposal/7/76e0e837192ebba23502a2d164b53347.pdf',
        status: {
          milestones: {
            completed: 0,
            incompleted: 2
          },
          activities: {
            completed: 1,
            incompleted: 3
          },
          budget: '200',
          funding: '0',
          spending: '25',
          payback: '0'
        }
      },
      editing: false,
      cloneId: null
    },
    withBasicDetailsAndUsersCompleted: {
      milestones: [],
      dataComplete: 11,
      proposal: null,
      faqLink: null,
      agreementJson: null,
      coverPhotoPath: null,
      milestonePath: null,
      agreementFileHash: 'QmPDW9VA848kJ1uypQKFeupKFPnaQysAgaVg4DBjTsdEgQ',
      proposalFileHash: 'QmSeRu4tMJH7ZsqncDNz4Ct5a8aMsaAmCnco8iVyEGY5Sv',
      status: 'draft',
      createdAt: '2023-02-22T18:15:25.753Z',
      address: null,
      lastUpdatedStatusAt: '2023-02-22T20:21:26.154Z',
      id: 'mockedId_draft_withBasicDetailsAndUserCompleted',
      txHash: null,
      rejectionReason: null,
      ipfsHash: 'QmUHqWNzoXCYuTmBBAC4etsKWYfTYAiodimc1BzBxxAaUm',
      revision: 1,
      step: 0,
      type: 'grant',
      owner: '28b3a91d-c7e3-44c6-b8ea-5b72650694bd',
      proposer: null,
      parent: null,
      budget: '200',
      basicInformation: {
        projectName: 'Fede 22 Feb',
        location: '(Islamic Republic of) Iran',
        timeframe: '12.000',
        timeframeUnit: 'months',
        thumbnailPhoto: '/cardPhotos/0/07621fa4dbb59c0a69fd1bf52ff96338.jpeg',
        beneficiary: {
          id: '37495329-004d-422b-8ce7-0e8155bb0183',
          lastName: 'Beneficiary',
          firstName: 'Firstname'
        }
      },
      users: [
        {
          roleDescription: 'beneficiary',
          role: '1',
          users: [
            {
              id: '37495329-004d-422b-8ce7-0e8155bb0183',
              firstName: 'Firstname',
              lastName: 'Beneficiary',
              email: 'federico.catala+20@globant.com',
              country: 31,
              first: false,
              address: '0xCCA834c18DF8208a4d4FacBFeeCe31af55DF9346'
            }
          ]
        },
        {
          roleDescription: 'investor',
          role: '2',
          users: [
            {
              id: 'b067444b-1cae-4059-9d1c-576c24166f3d',
              firstName: 'Firstname',
              lastName: 'Investor',
              email: 'federico.catala+21@globant.com',
              country: 31,
              first: false,
              address: '0x56129104491774918F140F023b8BE131fAAffEAD'
            }
          ]
        },
        {
          roleDescription: 'auditor',
          role: '3',
          users: [
            {
              id: '4d3aa496-943b-474f-a936-a037ab325b75',
              firstName: 'Firstname',
              lastName: 'Auditor 1',
              email: 'federico.catala+22@globant.com',
              country: 77,
              first: false,
              address: '0xDc6a8e732e371B1BD857e04353Dc0FCDa0029C53'
            }
          ]
        }
      ],
      details: {
        mission: 'adssda',
        problemAddressed: 'asd',
        currency: 'USDT',
        currencyType: 'Crypto',
        additionalCurrencyInformation: '0xaC4D2aC230876F8bAB54144CEE315bC22250661f',
        legalAgreementFile: '/agreement/7/76e0e837192ebba23502a2d164b53347.pdf',
        projectProposalFile: '/proposal/7/76e0e837192ebba23502a2d164b53347.pdf',
        status: {
          milestones: {
            completed: 0,
            incompleted: 2
          },
          activities: {
            completed: 1,
            incompleted: 3
          },
          budget: '200',
          funding: '0',
          spending: '25',
          payback: '0'
        }
      },
      editing: false,
      cloneId: null
    },
    stepsUncompleted: {
      milestones: [],
      dataComplete: 0,
      proposal: null,
      faqLink: null,
      agreementJson: null,
      coverPhotoPath: null,
      milestonePath: null,
      agreementFileHash: '',
      proposalFileHash: '',
      status: 'draft',
      createdAt: '2023-02-22T18:15:25.753Z',
      address: null,
      lastUpdatedStatusAt: '2023-02-22T20:21:26.154Z',
      id: 'mockedId_draft_withBasicCompleted',
      txHash: null,
      rejectionReason: null,
      ipfsHash: 'QmUHqWNzoXCYuTmBBAC4etsKWYfTYAiodimc1BzBxxAaUm',
      revision: 1,
      step: 0,
      type: 'grant',
      owner: '28b3a91d-c7e3-44c6-b8ea-5b72650694bd',
      proposer: null,
      parent: null,
      budget: '',
      basicInformation: {
        projectName: 'Untitled',
        location: null,
        timeframe: '0.000',
        timeframeUnit: null,
        thumbnailPhoto: null
      },
      users: [],
      details: {
        mission: null,
        problemAddressed: null,
        currency: null,
        currencyType: null,
        additionalCurrencyInformation: null,
        legalAgreementFile: null,
        projectProposalFile: null,
        status: {
          milestones: {
            completed: 0,
            incompleted: 0
          },
          activities: {
            completed: 0,
            incompleted: 0
          },
          budget: '0',
          funding: '0',
          spending: '0',
          payback: '0'
        }
      },
      editing: false,
      cloneId: null
    }
  }
};
