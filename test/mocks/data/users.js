export const mockedUsers = {
  admin: {
    firstName: 'Administrator',
    lastName: '',
    email: 'federico.catala@globant.com',
    id: 'be67e301-c330-4800-8085-6c822436dc2f',
    isAdmin: true,
    forcePasswordChange: false,
    first: false,
    pin: true,
    seenModal: false
  },
  beneficiary: {
    firstName: 'Firstname',
    lastName: 'Beneficiary',
    email: 'federico.catala+20@globant.com',
    id: '37495329-004d-422b-8ce7-0e8155bb0183',
    isAdmin: false,
    forcePasswordChange: false,
    first: false,
    pin: true,
    seenModal: false
  },
  investor: {
    firstName: 'Firstname',
    lastName: 'Investor',
    email: 'federico.catala+21@globant.com',
    id: 'b067444b-1cae-4059-9d1c-576c24166f3d',
    isAdmin: false,
    forcePasswordChange: false,
    first: false,
    pin: true,
    seenModal: false
  },
  auditor1: {
    firstName: 'Firstname',
    lastName: 'Auditor 1',
    email: 'federico.catala+22@globant.com',
    id: '4d3aa496-943b-474f-a936-a037ab325b75',
    isAdmin: false,
    forcePasswordChange: false,
    first: false,
    pin: true,
    seenModal: false
  }
};
