/* https://localhost:3001/projects/0877a2b5-8f8e-45f8-a531-81baba3dd64b/evidences */

const evidences = {
  evidences: [
    {
      date: '2023-02-22T20:01:16.009Z',
      role: '',
      userName: 'Firstname Beneficiary',
      activityType: 'spending',
      amount: '25',
      destinationAccount: '0xceddc4a2a24f04a59194e7244f0475d78c10b332'
    }
  ]
};

/* https://localhost:3001/activities/172/evidences */

const evidences2 = {
  milestone: {
    id: 65,
    title: 'Milestone 1'
  },
  activity: {
    id: 172,
    title: 'Activity 1',
    description: 'adssad',
    acceptanceCriteria: 'asddsa',
    status: 'to-review',
    auditor: '4d3aa496-943b-474f-a936-a037ab325b75',
    budget: '100',
    current: '25',
    step: 0,
    activityHash: 'QmbDQeJzjZUfAojFgHa4PWcPrX5fGXQWrKb1V8Tux7T6mA',
    reason: null,
    createdAt: '2023-02-22T03:00:00.000Z',
    deleted: false,
    toSign: '0xcb41fe8f1bb547d075581b1e666e1e02779bbec6ab89d6bd5fde55244e866040',
    type: 'funding',
    claimCounter: 1,
    milestone: 65,
    proposer: 'b067444b-1cae-4059-9d1c-576c24166f3d',
    parent: null
  },
  evidences: [
    {
      id: 145,
      title: 'dasdas',
      description: 'adsdsa',
      type: 'transfer',
      amount: '25',
      destinationAccount: '0xac4d2ac230876f8bab54144cee315bc22250661f',
      txHash: '',
      status: 'approved',
      reason: '',
      files: [],
      createdAt: '2023-02-22T19:22:48.642Z'
    }
  ]
};
