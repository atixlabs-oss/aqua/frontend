/* https://localhost:3001/evidences/145 */

const evidence = {
  user: {
    id: 'b067444b-1cae-4059-9d1c-576c24166f3d',
    firstName: 'Firstname',
    lastName: 'Investor',
    address: '0x56129104491774918F140F023b8BE131fAAffEAD'
  },
  files: [],
  id: 145,
  title: 'dasdas',
  description: 'adsdsa',
  type: 'transfer',
  transferTxHash: '0x90778c3dd9b5f5edd724b171ef256453291d733b96d30a95c5b8697dd5ed8eb4',
  proof: null,
  approved: null,
  txHash: null,
  status: 'approved',
  reason: null,
  createdAt: '2023-02-22T19:22:48.642Z',
  amount: '25',
  destinationAccount: '0xac4d2ac230876f8bab54144cee315bc22250661f',
  roleDescription: null,
  activity: {
    id: 172,
    title: 'Activity 1',
    auditor: {
      id: '4d3aa496-943b-474f-a936-a037ab325b75',
      firstName: 'Firstname',
      lastName: 'Auditor 1',
      address: '0xDc6a8e732e371B1BD857e04353Dc0FCDa0029C53'
    },
    type: 'funding'
  },
  auditor: {
    id: '4d3aa496-943b-474f-a936-a037ab325b75',
    firstName: 'Firstname',
    lastName: 'Auditor 1',
    address: '0xDc6a8e732e371B1BD857e04353Dc0FCDa0029C53'
  },
  parent: null,
  currency: 'USDT',
  milestone: {
    id: 65,
    title: 'Milestone 1'
  }
};
