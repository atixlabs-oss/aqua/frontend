/* https://localhost:3001/projects/35da99a1-dba8-4695-8ce6-550096f9020b/transactions?type=sent */

const transactions = {
  transactions: [
    {
      txHash: '0xb5a94a017aa9957d6768f3594b78146331da41f2184a339a4ae7367c13a87825',
      value: '50',
      tokenSymbol: 'USDT',
      from: '0xac4d2ac230876f8bab54144cee315bc22250661f',
      to: '0xceddc4a2a24f04a59194e7244f0475d78c10b332',
      timestamp: 'Feb-17-2023 12:39:12 PM+UTC'
    },
    {
      txHash: '0xa9e8848d39ebcdaa4ff837e4f100b50ab3f8b0f646dd3b97638900934f45eb8f',
      value: '25',
      tokenSymbol: 'USDT',
      from: '0xac4d2ac230876f8bab54144cee315bc22250661f',
      to: '0xceddc4a2a24f04a59194e7244f0475d78c10b332',
      timestamp: 'Feb-17-2023 12:39:12 PM+UTC'
    },
    {
      txHash: '0x5e085d8350faf0ca031c96fb558828cce8b772c651f3152dcd39f64012a8b5fe',
      value: '25',
      tokenSymbol: 'USDT',
      from: '0xac4d2ac230876f8bab54144cee315bc22250661f',
      to: '0xceddc4a2a24f04a59194e7244f0475d78c10b332',
      timestamp: 'Feb-17-2023 12:38:48 PM+UTC'
    },
    {
      txHash: '0x90778c3dd9b5f5edd724b171ef256453291d733b96d30a95c5b8697dd5ed8eb4',
      value: '25',
      tokenSymbol: 'USDT',
      from: '0xac4d2ac230876f8bab54144cee315bc22250661f',
      to: '0xac4d2ac230876f8bab54144cee315bc22250661f',
      timestamp: 'Feb-16-2023 09:01:00 PM+UTC'
    },
    {
      txHash: '0x620b0b9b5d5f5c66ca6a3ee2d3059e123f3a3e5d82984d154633291e893a994b',
      value: '25',
      tokenSymbol: 'USDT',
      from: '0xac4d2ac230876f8bab54144cee315bc22250661f',
      to: '0xac4d2ac230876f8bab54144cee315bc22250661f',
      timestamp: 'Feb-16-2023 08:59:24 PM+UTC'
    }
  ]
};
