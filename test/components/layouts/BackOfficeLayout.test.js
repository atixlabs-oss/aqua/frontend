/**
 * AGUA Copyright (C) 2023  Atix Labs
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import React from 'react';
import { render } from '@testing-library/react';
import '@testing-library/jest-dom/extend-expect';
import { MockedAppProviders } from 'test/mocks/MockedAppProviders';
import BackOfficeLayout from 'components/Layouts/BackOfficeLayout/BackOfficeLayout';

describe('Components | Layouts | BackOfficeLayout', () => {
  let component;

  beforeAll(() => {
    component = (
      <MockedAppProviders>
        <BackOfficeLayout>
          <div>Children</div>
        </BackOfficeLayout>
      </MockedAppProviders>
    );
  });

  it('There should exist the title of the project', async () => {
    const content = render(component);
    const navbar = content.getByRole('navigation');

    expect(navbar).toBeInTheDocument();
  });

  it('There should be a div with the main content', async () => {
    const content = render(component);
    const mainContent = content.getByRole('main');

    expect(mainContent).toBeInTheDocument();
  });

  it('The children should be rendered', async () => {
    const content = render(component);
    const children = content.getByText(/Children/i, { selector: 'div' });

    expect(children).toBeInTheDocument();
  });
});
