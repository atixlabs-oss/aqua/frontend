/**
 * AGUA Copyright (C) 2023  Atix Labs
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import React from 'react';
import { render } from '@testing-library/react';
import '@testing-library/jest-dom/extend-expect';
import { MockedAppProviders } from 'test/mocks/MockedAppProviders';
import { LandingDraftLayout } from 'components/Layouts/LandingDraftLayout/LandingDraftLayout';

describe('Components | Layouts | LandingDraftLayout', () => {
  let component;

  beforeAll(() => {
    component = (
      <MockedAppProviders>
        <LandingDraftLayout header={<div>Header</div>} />
      </MockedAppProviders>
    );
  });

  it('There should exist the title of the project', async () => {
    const content = render(component);
    const navbar = content.getByRole('navigation');

    expect(navbar).toBeInTheDocument();
  });

  it('There should be a div with heading content', async () => {
    const content = render(component);
    const mainContent = content.getByRole('heading');

    expect(mainContent).toBeInTheDocument();
  });

  it('The header component should be rendered', async () => {
    const content = render(component);
    const children = content.getByText(/Header/i, { selector: 'div' });

    expect(children).toBeInTheDocument();
  });
});
