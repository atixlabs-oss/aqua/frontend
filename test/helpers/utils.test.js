const { ERROR_TYPES, ERROR_MESSAGES } = require('constants/constants');
const {
  getErrorTypes,
  getErrorMessagesFields,
  getErrorMessagesField,
  checkValidEmail,
  getExtensionFromUrl,
  decimalCount,
  capitalizeFirstLetter,
  getDateAndTime,
  checkHasActivityPending,
  checkIsAuditorOfAnActivity,
  sortActivitiesById
} = require('helpers/utils');
const { ACTIVITY_STATUS_ENUM } = require('model/activityStatus');
const { mockedProject } = require('test/mocks/data/project');
const { mockedUsers } = require('test/mocks/data/users');

describe('utils', () => {
  it('Get error types should return an array of unique errors', () => {
    const errors = getErrorTypes([
      [ERROR_TYPES.EMPTY],
      [ERROR_TYPES.EMPTY_FILE],
      [ERROR_TYPES.EMPTY]
    ]);
    expect(errors).toEqual([ERROR_TYPES.EMPTY, ERROR_TYPES.EMPTY_FILE]);
  });

  it('Get error messages fields from errors to show', () => {
    let errors = getErrorMessagesFields([[ERROR_TYPES.EMPTY]], [ERROR_TYPES.IMAGE_INVALID]);
    expect(errors).toEqual([]);

    errors = getErrorMessagesFields([[ERROR_TYPES.EMPTY]], [ERROR_TYPES.EMPTY]);
    expect(errors).toEqual([ERROR_MESSAGES.EMPTY]);

    errors = getErrorMessagesFields(
      [[ERROR_TYPES.EMPTY], [ERROR_TYPES.EMPTY_FILE], [ERROR_TYPES.EMPTY]],
      [ERROR_TYPES.EMPTY, ERROR_TYPES.EMPTY_FILE]
    );
    expect(errors).toEqual([ERROR_MESSAGES.EMPTY, ERROR_MESSAGES.EMPTY_FILE]);
  });

  it('Get error messages field', () => {
    let errors = getErrorMessagesField([ERROR_TYPES.EMPTY], [ERROR_TYPES.EMPTY]);
    expect(errors).toEqual([ERROR_MESSAGES.EMPTY]);

    errors = getErrorMessagesField(
      [ERROR_TYPES.EMPTY, ERROR_TYPES.EMPTY, ERROR_TYPES.EMPTY_FILE],
      [ERROR_TYPES.EMPTY_FILE]
    );
    expect(errors).toEqual([ERROR_MESSAGES.EMPTY_FILE]);

    errors = getErrorMessagesField(
      [ERROR_TYPES.EMPTY, ERROR_TYPES.EMPTY, ERROR_TYPES.EMPTY_FILE],
      []
    );
    expect(errors).toEqual([]);
  });

  it('Check valid email', () => {
    let isValid = checkValidEmail('test@gmail.com');
    expect(isValid).toBeTruthy();

    isValid = checkValidEmail('testgmail.com');
    expect(isValid).toBeFalsy();

    isValid = checkValidEmail('test@gmailcom');
    expect(isValid).toBeFalsy();

    isValid = checkValidEmail('test+1@gmail.com');
    expect(isValid).toBeTruthy();

    isValid = checkValidEmail('test.1@gmail.com');
    expect(isValid).toBeTruthy();
  });

  it('get extension from url', () => {
    let extension = getExtensionFromUrl('thisisanurl.com/file.jpg');
    expect(extension).toEqual('jpg');

    extension = getExtensionFromUrl('https://subdomain.domain.net/path1/path2/file.pdf');
    expect(extension).toEqual('pdf');
  });

  it('Decimal count', () => {
    let decimalsQuantity = decimalCount(102);
    expect(decimalsQuantity).toEqual(0);

    decimalsQuantity = decimalCount(102.45);
    expect(decimalsQuantity).toEqual(2);

    decimalsQuantity = decimalCount(0.234);
    expect(decimalsQuantity).toEqual(3);
  });

  it('Capitalize the first letter', () => {
    let textProcessed = capitalizeFirstLetter('text');
    expect(textProcessed).toEqual('Text');

    textProcessed = capitalizeFirstLetter('laboris do sit deserunt sunt nostrud officia commodo.');
    expect(textProcessed).toEqual('Laboris do sit deserunt sunt nostrud officia commodo.');
  });

  it('get date and time formatted', () => {
    let dateAndTimeFormatted = getDateAndTime('10/02/2022');
    expect(dateAndTimeFormatted).toEqual('November, 2nd 2022 - 24:00');

    dateAndTimeFormatted = getDateAndTime('10/02/2022 13:00');
    expect(dateAndTimeFormatted).toEqual('November, 2nd 2022 - 13:00');

    dateAndTimeFormatted = getDateAndTime('10/02/2022 13:00', 'minimal');
    expect(dateAndTimeFormatted).toEqual('02/10/2022 - 13:00');
  });

  it('check has activity pending', () => {
    let hasActivityPending = checkHasActivityPending({
      milestones: [{ activities: [{ name: 'test', status: ACTIVITY_STATUS_ENUM.APPROVED }] }]
    });
    expect(hasActivityPending).toBeFalsy();

    hasActivityPending = checkHasActivityPending({
      milestones: [{ activities: [{ name: 'test', status: ACTIVITY_STATUS_ENUM.IN_PROGRESS }] }]
    });
    expect(hasActivityPending).toBeFalsy();

    hasActivityPending = checkHasActivityPending({
      milestones: [{ activities: [{ name: 'test', status: ACTIVITY_STATUS_ENUM.NEW }] }]
    });
    expect(hasActivityPending).toBeFalsy();

    hasActivityPending = checkHasActivityPending({
      milestones: [{ activities: [{ name: 'test', status: ACTIVITY_STATUS_ENUM.REJECTED }] }]
    });
    expect(hasActivityPending).toBeFalsy();

    hasActivityPending = checkHasActivityPending({
      milestones: [{ activities: [{ name: 'test', status: ACTIVITY_STATUS_ENUM.TO_REVIEW }] }]
    });
    expect(hasActivityPending).toBeFalsy();

    hasActivityPending = checkHasActivityPending({
      milestones: [
        { activities: [{ name: 'test', status: ACTIVITY_STATUS_ENUM.PENDING_TO_APPROVE }] }
      ]
    });
    expect(hasActivityPending).toBeTruthy();

    hasActivityPending = checkHasActivityPending({
      milestones: [
        { activities: [{ name: 'test', status: ACTIVITY_STATUS_ENUM.PENDING_TO_REJECT }] }
      ]
    });
    expect(hasActivityPending).toBeTruthy();

    hasActivityPending = checkHasActivityPending({
      milestones: [
        { activities: [{ name: 'test', status: ACTIVITY_STATUS_ENUM.PENDING_TO_REVIEW }] }
      ]
    });
    expect(hasActivityPending).toBeTruthy();
  });

  it('Check is auditor of an activity', () => {
    let isAuditor = checkIsAuditorOfAnActivity({
      milestones: mockedProject.in_progress.milestones,
      auditorId: mockedUsers.auditor1.id
    });
    expect(isAuditor).toBeTruthy();

    isAuditor = checkIsAuditorOfAnActivity({
      milestones: mockedProject.in_progress.milestones,
      auditorId: 'random-id'
    });
    expect(isAuditor).toBeFalsy();
  });

  it('Sort activities by id', () => {
    let sortedActivities = sortActivitiesById({
      activities: [
        { id: 6, name: 'Activity 6' },
        { id: 10, name: 'Activity 10' },
        { id: 1, name: 'Activity 1' },
        { id: 3, name: 'Activity 3' }
      ]
    });
    expect(sortedActivities).toEqual([
      { id: 1, name: 'Activity 1' },
      { id: 3, name: 'Activity 3' },
      { id: 6, name: 'Activity 6' },
      { id: 10, name: 'Activity 10' }
    ]);

    sortedActivities = sortActivitiesById({
      activities: []
    });
    expect(sortedActivities).toEqual([]);
  });
});
