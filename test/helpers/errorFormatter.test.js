const { default: formatError } = require('helpers/errorFormatter');

const mockedErrorResponse = {
  response: {
    data: 'Data'
  }
};

const mockedErrorMessage = {
  message: 'Message'
};

describe('Error Formatter', () => {
  it('format error when contains response', () => {
    const errorFormatted = formatError(mockedErrorResponse);
    expect(errorFormatted).toMatch(/data/i);
  });

  it('format error when it does not contains response', () => {
    const errorFormatted = formatError(mockedErrorMessage);
    expect(errorFormatted).toMatch(/Message/i);
  });
});
