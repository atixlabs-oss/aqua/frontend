import { CURRENCIES } from 'constants/constants';
import common from 'locales/en/common.json';
import { mockedProject } from 'test/mocks/data/project';
const {
  getInitials,
  formatTimeframeValue,
  formatCurrencyAtTheBeginning,
  formatCurrency,
  removeDecimals,
  formatLeadWithZero,
  addMiddleEllipsis
} = require('helpers/formatter');
const { mockedUsers } = require('test/mocks/data/users');

const mockedCommon = text => {
  const textSplitted = text.split('.');
  const _text = textSplitted[1];
  return common['general'][_text];
};

common['general']['months'];

describe('Formatters', () => {
  it('getInitials', () => {
    const initials = getInitials(
      `${mockedUsers.beneficiary.firstName} ${mockedUsers.beneficiary.lastName}`
    );
    expect(initials).toEqual('FB');
  });
  it('formatTimeframeValue', () => {
    const formattedTimeframeValue = formatTimeframeValue({
      t: mockedCommon,
      timeframe: mockedProject.in_progress.basicInformation.timeframe,
      timeframeUnit: mockedProject.in_progress.basicInformation.timeframeUnit
    });

    expect(formattedTimeframeValue).toEqual('12.0 months');
  });
  it('formatCurrencyAtTheBeginning', () => {
    let currencyWithValue = formatCurrencyAtTheBeginning(CURRENCIES.fiat[0].label, '15.42333');
    expect(currencyWithValue).toEqual('USD 15.42');

    currencyWithValue = formatCurrencyAtTheBeginning(
      CURRENCIES.crypto[0].label,
      '15.42333123123423'
    );
    expect(currencyWithValue).toEqual('RBTC 15.42333123');

    currencyWithValue = formatCurrencyAtTheBeginning(CURRENCIES.fiat[0]);
    expect(currencyWithValue).toEqual('0.00');
  });
  it('formatCurrency', () => {
    let currencyWithValue = formatCurrency(CURRENCIES.fiat[0].label, '15.42333', true);
    expect(currencyWithValue).toEqual('15.42 USD');

    currencyWithValue = formatCurrency(CURRENCIES.crypto[0].label, '15.42333123123423', true);
    expect(currencyWithValue).toEqual('15.42333123 RBTC');

    currencyWithValue = formatCurrency(CURRENCIES.fiat[0]);
    expect(currencyWithValue).toEqual('0.00');
  });
  it('removeDecimals', () => {
    expect(removeDecimals(324.34234)).toEqual('324');
  });
  it('formatLeadWithZero', () => {
    expect(formatLeadWithZero(1)).toEqual('01');
  });
  it('addMiddleEllipsis', () => {
    expect(
      addMiddleEllipsis(
        'Occaecat voluptate labore laboris duis do commodo qui ullamco minim sint deserunt pariatur.'
      )
    ).toEqual('Occae...iatur.');
  });
});
