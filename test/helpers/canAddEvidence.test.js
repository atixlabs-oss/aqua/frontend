import { canAddEvidences as checkCanAddEvidences } from 'helpers/canAddEvidence';
import { ACTIVITY_TYPES_ENUM } from 'model/activityTypes';
import { mockedProject } from 'test/mocks/data/project';
import { mockedUsers } from 'test/mocks/data/users';

describe('CanAddEvidence', () => {
  it('canAddEvidences for funding', () => {
    let canAddEvidences = checkCanAddEvidences({
      project: mockedProject.in_progress,
      activityType: ACTIVITY_TYPES_ENUM.FUNDING,
      user: mockedUsers.admin
    });
    expect(canAddEvidences).toBeFalsy();

    canAddEvidences = checkCanAddEvidences({
      project: mockedProject.in_progress,
      activityType: ACTIVITY_TYPES_ENUM.FUNDING,
      user: mockedUsers.beneficiary
    });
    expect(canAddEvidences).toBeFalsy();

    canAddEvidences = checkCanAddEvidences({
      project: mockedProject.in_progress,
      activityType: ACTIVITY_TYPES_ENUM.FUNDING,
      user: mockedUsers.auditor1
    });
    expect(canAddEvidences).toBeFalsy();

    canAddEvidences = checkCanAddEvidences({
      project: mockedProject.in_progress,
      activityType: ACTIVITY_TYPES_ENUM.FUNDING,
      user: mockedUsers.investor
    });
    expect(canAddEvidences).toBeTruthy();
  });

  it('canAddEvidences for spending', () => {
    let canAddEvidences = checkCanAddEvidences({
      project: mockedProject.in_progress,
      activityType: ACTIVITY_TYPES_ENUM.SPENDING,
      user: mockedUsers.admin
    });
    expect(canAddEvidences).toBeFalsy();

    canAddEvidences = checkCanAddEvidences({
      project: mockedProject.in_progress,
      activityType: ACTIVITY_TYPES_ENUM.SPENDING,
      user: mockedUsers.beneficiary
    });
    expect(canAddEvidences).toBeTruthy();

    canAddEvidences = checkCanAddEvidences({
      project: mockedProject.in_progress,
      activityType: ACTIVITY_TYPES_ENUM.SPENDING,
      user: mockedUsers.investor
    });
    expect(canAddEvidences).toBeFalsy();

    canAddEvidences = checkCanAddEvidences({
      project: mockedProject.in_progress,
      activityType: ACTIVITY_TYPES_ENUM.SPENDING,
      user: mockedUsers.auditor1
    });
    expect(canAddEvidences).toBeFalsy();
  });

  it('canAddEvidences for payback', () => {
    let canAddEvidences = checkCanAddEvidences({
      project: mockedProject.in_progress,
      activityType: ACTIVITY_TYPES_ENUM.PAYBACK,
      user: mockedUsers.admin
    });
    expect(canAddEvidences).toBeFalsy();

    canAddEvidences = checkCanAddEvidences({
      project: mockedProject.in_progress,
      activityType: ACTIVITY_TYPES_ENUM.PAYBACK,
      user: mockedUsers.beneficiary
    });
    expect(canAddEvidences).toBeTruthy();

    canAddEvidences = checkCanAddEvidences({
      project: mockedProject.in_progress,
      activityType: ACTIVITY_TYPES_ENUM.PAYBACK,
      user: mockedUsers.investor
    });
    expect(canAddEvidences).toBeFalsy();

    canAddEvidences = checkCanAddEvidences({
      project: mockedProject.in_progress,
      activityType: ACTIVITY_TYPES_ENUM.PAYBACK,
      user: mockedUsers.auditor1
    });
    expect(canAddEvidences).toBeFalsy();
  });

  it('canAddEvidences if user not passed', () => {
    let canAddEvidences = checkCanAddEvidences({
      project: mockedProject.in_progress,
      activityType: ACTIVITY_TYPES_ENUM.FUNDING
    });
    expect(canAddEvidences).toBeFalsy();

    canAddEvidences = checkCanAddEvidences({
      project: mockedProject.in_progress,
      activityType: ACTIVITY_TYPES_ENUM.PAYBACK
    });
    expect(canAddEvidences).toBeFalsy();

    canAddEvidences = checkCanAddEvidences({
      project: mockedProject.in_progress,
      activityType: ACTIVITY_TYPES_ENUM.SPENDING
    });
    expect(canAddEvidences).toBeFalsy();
  });
});
