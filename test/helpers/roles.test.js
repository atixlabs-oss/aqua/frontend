const {
  checkIsActivityAuditor,
  checkIsBeneficiaryByProject,
  checkIsInvestorByProject,
  checkIsAuditorByProject,
  checkIsBeneficiaryOrInvestorByProject,
  checkRoleByProject
} = require('helpers/roles');
const { mockedEvidencesOfActivity } = require('test/mocks/data/evidences');
const { mockedProject } = require('test/mocks/data/project');
const { mockedUsers } = require('test/mocks/data/users');

describe('Test helpers | utils', () => {
  it('checkIsActivityAuditor', () => {
    const isActivityAuditor = checkIsActivityAuditor({
      user: mockedUsers.auditor1,
      activity: mockedEvidencesOfActivity.in_progress.activity
    });
    expect(isActivityAuditor).toBeTruthy();
  });

  it('checkIsBeneficiaryByProject', () => {
    let isActivityAuditor = checkIsBeneficiaryByProject({
      user: mockedUsers.beneficiary,
      project: mockedProject.in_progress
    });
    expect(isActivityAuditor).toBeTruthy();

    isActivityAuditor = checkIsBeneficiaryByProject({
      user: mockedUsers.investor,
      project: mockedProject.in_progress
    });
    expect(isActivityAuditor).toBeFalsy();

    isActivityAuditor = checkIsBeneficiaryByProject({
      user: mockedUsers.auditor1,
      project: mockedProject.in_progress
    });
    expect(isActivityAuditor).toBeFalsy();
  });

  it('checkIsInvestorByProject', () => {
    let isInvestor = checkIsInvestorByProject({
      user: mockedUsers.beneficiary,
      project: mockedProject.in_progress
    });
    expect(isInvestor).toBeFalsy();

    isInvestor = checkIsInvestorByProject({
      user: mockedUsers.investor,
      project: mockedProject.in_progress
    });
    expect(isInvestor).toBeTruthy();

    isInvestor = checkIsInvestorByProject({
      user: mockedUsers.auditor1,
      project: mockedProject.in_progress
    });
    expect(isInvestor).toBeFalsy();
  });

  it('checkIsAuditorByProject', () => {
    let isAuditor = checkIsAuditorByProject({
      user: mockedUsers.beneficiary,
      project: mockedProject.in_progress
    });
    expect(isAuditor).toBeFalsy();

    isAuditor = checkIsAuditorByProject({
      user: mockedUsers.investor,
      project: mockedProject.in_progress
    });
    expect(isAuditor).toBeFalsy();

    isAuditor = checkIsAuditorByProject({
      user: mockedUsers.auditor1,
      project: mockedProject.in_progress
    });
    expect(isAuditor).toBeTruthy();
  });

  it('checkIsBeneficiaryOrInvestorByProject', () => {
    let isBeneficiaryOrInvestor = checkIsBeneficiaryOrInvestorByProject({
      project: mockedProject.in_progress,
      user: mockedUsers.beneficiary
    });
    expect(isBeneficiaryOrInvestor).toBeTruthy();

    isBeneficiaryOrInvestor = checkIsBeneficiaryOrInvestorByProject({
      project: mockedProject.in_progress,
      user: mockedUsers.investor
    });
    expect(isBeneficiaryOrInvestor).toBeTruthy();

    isBeneficiaryOrInvestor = checkIsBeneficiaryOrInvestorByProject({
      project: mockedProject.in_progress,
      user: mockedUsers.auditor1
    });
    expect(isBeneficiaryOrInvestor).toBeFalsy();
  });

  it('checkRoleByProject', () => {
    let role = checkRoleByProject({
      user: mockedUsers.beneficiary,
      project: mockedProject.in_progress
    });
    expect(role?.name).toMatch(/Beneficiary/i);

    role = checkRoleByProject({
      user: mockedUsers.investor,
      project: mockedProject.in_progress
    });
    expect(role?.name).toMatch(/Investor/i);

    role = checkRoleByProject({
      user: mockedUsers.auditor1,
      project: mockedProject.in_progress
    });
    expect(role?.name).toMatch(/Auditor/i);

    role = checkRoleByProject({
      user: mockedUsers.admin,
      project: mockedProject.in_progress
    });
    expect(role?.name).toMatch(/Administrator/i);
  });
});
