const { stringToHexColor } = require('helpers/stringToHexColor');

describe('stringToHexColor', () => {
  it('string to hex color', () => {
    const hexColor = stringToHexColor('First name'); // ?
    expect(hexColor).toEqual('#1ba2c1');
  });
});
