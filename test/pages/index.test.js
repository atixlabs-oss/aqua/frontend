/**
 * AGUA Copyright (C) 2023  Atix Labs
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import React from 'react';
import { render, screen } from '@testing-library/react';
import '@testing-library/jest-dom/extend-expect';
import Index from 'pages';
import { MockedAppProviders } from 'test/mocks/MockedAppProviders';

describe('Homepage', () => {
  let component;

  beforeAll(() => {
    component = (
      <MockedAppProviders>
        <Index project={{}} onError={() => {}} Footer={() => <div></div>} />
      </MockedAppProviders>
    );
  });

  it('There should be a login button when there is no user logged in.', () => {
    render(component);
    const loginButtonContainer = screen.getByTestId('navbar-login-button');
    expect(loginButtonContainer).toBeInTheDocument();
  });
  it('When user click on the login button, a modal should appear.', () => {
    render(component);
    const loginButton = screen.getByTestId('navbar-login-button').querySelector('button');
    loginButton.click();
    const loginForm = screen.getByTestId('login-form');
    expect(loginForm).toBeInTheDocument();
  });

  it('When user on forgot your password?, the recovery password modal shoud appear', () => {
    render(component);
    const loginButton = screen.getByTestId('navbar-login-button').querySelector('button');
    loginButton.click();
    const recoveryForm = screen.getByTestId('recovery-password-modal');
    expect(recoveryForm).toBeInTheDocument();
  });

  it('There should be a logout button where there is an user logged in', () => {
    render(
      <MockedAppProviders withUser>
        <Index project={{}} onError={() => {}} Footer={() => <div></div>} />
      </MockedAppProviders>
    );
    const logoutButton = screen.getByText('Logout');
    expect(logoutButton).toBeInTheDocument();
  });
});
