/**
 * AGUA Copyright (C) 2023  Atix Labs
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import React from 'react';
import { render, waitFor } from '@testing-library/react';
import '@testing-library/jest-dom/extend-expect';
import { MockedAppProviders } from 'test/mocks/MockedAppProviders';
import { mockedProject } from 'test/mocks/data/project';

import ProjectEdit from 'pages/back-office/projects/edit/[projectId]';
import { mockedUsers } from 'test/mocks/data/users';

describe('Edit Project Page', () => {
  let component;

  beforeAll(() => {
    component = (
      <MockedAppProviders
        router={{ query: { projectId: mockedProject.draft.stepsCompleted.id } }}
        withUser
      >
        <ProjectEdit />
      </MockedAppProviders>
    );
  });

  it('There should exist the title of the project', async () => {
    const content = render(component);

    await waitFor(() => {
      const navbar = content.getByRole('navigation');
      expect(navbar).toBeInTheDocument();
    });
  });

  it('There should exist the correct buttons in the footer', async () => {
    const content = render(component);

    await waitFor(() => {
      const saveAndContinueLaterButton = content.getByRole('button', {
        name: /save & continue later/i
      });
      expect(saveAndContinueLaterButton).toBeInTheDocument();
      expect(saveAndContinueLaterButton).toBeEnabled();

      const deleteProjectButton = content.getByRole('button', { name: /delete project/i });
      expect(deleteProjectButton).toBeInTheDocument();
      expect(deleteProjectButton).toBeEnabled();

      const publishProjectButton = content.getByRole('button', { name: /publish project/i });
      expect(publishProjectButton).toBeInTheDocument();
      expect(publishProjectButton).toBeEnabled();
    });
  });

  it('Exist the four sections when editing', async () => {
    const content = render(component);

    await waitFor(() => {
      const basicInformationSection = content.getByText(/Basic Information/i, { selector: 'h3' });
      expect(basicInformationSection).toBeInTheDocument();
      const basicInformationNote = content.getByText(
        /Here you can complete the basic information of the project/i
      );
      expect(basicInformationNote).toBeInTheDocument();

      const projectDetailsSection = content.getByText(/Project Detail/i, { selector: 'h3' });
      expect(projectDetailsSection).toBeInTheDocument();
      const projectDetailsNote = content.getByText(/Here you can complete the project details/i);
      expect(projectDetailsNote).toBeInTheDocument();

      const projectUsersSection = content.getByText(/Project Users/i, { selector: 'h3' });
      expect(projectUsersSection).toBeInTheDocument();
      const projectUsersNote = content.getByText(
        /Here you can assign or create users to different roles/i
      );
      expect(projectUsersNote).toBeInTheDocument();

      const projectMilestonesSection = content.getByText(/Project Milestones/i, { selector: 'h3' });
      expect(projectMilestonesSection).toBeInTheDocument();
      const projectMilestonesNote = content.getByText(
        /Here you can upload the required milestones for your project/i
      );
      expect(projectMilestonesNote).toBeInTheDocument();
    });
  });

  it('Should has a preview project button enabled - draft & completed', async () => {
    const content = render(component);

    await waitFor(() => {
      const previewProjectButton = content.getByRole('button', { name: /See preview project/i });
      expect(previewProjectButton).toBeInTheDocument();
      expect(previewProjectButton).toBeEnabled();
    });
  });

  it('Should has a preview project button enabled - published & in progress', async () => {
    const content = render(
      <MockedAppProviders router={{ query: { projectId: mockedProject.in_progress.id } }} withUser>
        <ProjectEdit />
      </MockedAppProviders>
    );

    await waitFor(() => {
      const previewProjectButton = content.getByRole('button', { name: /See preview project/i });
      expect(previewProjectButton).toBeInTheDocument();
      expect(previewProjectButton).toBeEnabled();
    });
  });

  it('Edit buttons should be enabled if basic info was filled - draft & all completed', async () => {
    const content = render(component);

    await waitFor(() => {
      const editButtons = content.getAllByRole('button', {
        name: /edit/i
      });

      const basicInfoEditButton = editButtons[0];
      expect(basicInfoEditButton).toBeEnabled();

      const projectDetailsEditButton = editButtons[1];
      expect(projectDetailsEditButton).toBeEnabled();

      const assignUsersEditButton = editButtons[2];
      expect(assignUsersEditButton).toBeEnabled();

      const milestonesEditButton = editButtons[3];
      expect(milestonesEditButton).toBeEnabled();
    });
  });

  /* it('Only basic info edit button should be enabled if nothing was completed - draft & nothing completed', async () => {
    const content = render(
      <MockedAppProviders
        router={{ query: { projectId: mockedProject.draft.stepsUncompleted.id } }}
        user={mockedUsers.admin}
      >
        <ProjectEdit />
      </MockedAppProviders>
    );

    await waitFor(() => {
      const editButtons = content.getAllByRole('button', {
        name: /edit/i
      });

      content.container.outerHTML; //?

      const basicInfoEditButton = editButtons[0];
      expect(basicInfoEditButton).toBeEnabled();

      const projectDetailsEditButton = editButtons[1];
      expect(projectDetailsEditButton).toBeDisabled();

      const assignUsersEditButton = editButtons[2];
      expect(assignUsersEditButton).toBeDisabled();

      const milestonesEditButton = editButtons[3];
      expect(milestonesEditButton).toBeDisabled();
    });
  }); */

  it('all edit buttons except milestones should be enabled if only basic info is completed - draft & basic info completed', async () => {
    const content = render(
      <MockedAppProviders
        router={{ query: { projectId: mockedProject.draft.withBasicCompleted.id } }}
        user={mockedUsers.admin}
      >
        <ProjectEdit />
      </MockedAppProviders>
    );

    await waitFor(() => {
      const editButtons = content.getAllByRole('button', {
        name: /edit/i
      });

      const basicInfoEditButton = editButtons[0];
      expect(basicInfoEditButton).toBeEnabled();

      const projectDetailsEditButton = editButtons[1];
      expect(projectDetailsEditButton).toBeEnabled();

      const assignUsersEditButton = editButtons[2];
      expect(assignUsersEditButton).toBeEnabled();

      const milestonesEditButton = editButtons[3];
      expect(milestonesEditButton).toBeDisabled();
    });
  });

  it('Edit buttons should be enabled if basic info was filled - in progress', async () => {
    const content = render(
      <MockedAppProviders
        router={{ query: { projectId: mockedProject.in_progress.id } }}
        user={mockedUsers.admin}
      >
        <ProjectEdit />
      </MockedAppProviders>
    );
    await waitFor(() => {
      const editButtons = content.getAllByRole('button', {
        name: /edit/i
      });

      const basicInfoEditButton = editButtons[0];
      expect(basicInfoEditButton).toBeDisabled();

      const projectDetailsEditButton = editButtons[1];
      expect(projectDetailsEditButton).toBeDisabled();

      const assignUsersEditButton = editButtons[2];
      expect(assignUsersEditButton).toBeDisabled();

      const milestonesEditButton = editButtons[3];
      expect(milestonesEditButton).toBeDisabled();
    });
  });

  /* it('Edit Basic Information', async () => {
    const content = render(component);

    await waitFor(() => {
      const editButtons = content.getAllByRole('button', {
        name: /edit/i
      });

      const basicInfoEditButton = editButtons[0];
      basicInfoEditButton.click();
    });

    const projectNameTitle = content.getByText(
      mockedProject.draft.stepsCompleted.basicInformation.projectName,
      { selector: 'h4' }
    );
    expect(projectNameTitle).toBeInTheDocument();

    const projectNameField = content.getByLabelText(/Project Name/i);
    expect(projectNameField).toHaveValue(
      mockedProject.draft.stepsCompleted.basicInformation.projectName
    );

    fireEvent.change(projectNameField, { target: { value: 'Project name test' } });
    expect(projectNameField).toHaveValue('Project name test');

    const countryOfImpactSelector = content.getByText(/Islamic Republic of'/i);

    expect(countryOfImpactSelector).toBeInTheDocument();

  }); */
});
