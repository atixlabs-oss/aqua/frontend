/**
 * AGUA Copyright (C) 2023  Atix Labs
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import React from 'react';
import { render, act, waitFor } from '@testing-library/react';
import '@testing-library/jest-dom/extend-expect';
import Index from 'pages/[projectId]/index';
import { MockedAppProviders } from 'test/mocks/MockedAppProviders';
import { setupServer } from 'msw/node';
import { rest } from 'msw';
import { mockedProject } from 'test/mocks/data/project';
import { mockedEvidences } from 'test/mocks/data/evidences';
import { mockedChangelog } from 'test/mocks/data/changelog';

const server = setupServer(
  rest.get(
    `https://api.coa2.testing.atixlabs.xyz/projects/${mockedProject.in_progress.id}`,
    (req, res, ctx) => res(ctx.json(mockedProject.in_progress))
  ),
  rest.get(
    `https://api.coa2.testing.atixlabs.xyz/projects/${mockedProject.in_progress.id}/evidences`,
    (req, res, ctx) => res(ctx.json(mockedEvidences.in_progress))
  ),
  rest.get(
    `https://api.coa2.testing.atixlabs.xyz/projects/${mockedProject.in_progress.parent ||
      mockedProject.in_progress.id}/changelog`,
    (req, res, ctx) => res(ctx.json(mockedChangelog.in_progress))
  )
);

beforeAll(() => server.listen());
afterEach(() => server.resetHandlers());
afterAll(() => server.close());

describe('Project Page', () => {
  let component;

  beforeAll(() => {
    component = (
      <MockedAppProviders router={{ query: { projectId: mockedProject.in_progress.id } }}>
        <Index />
      </MockedAppProviders>
    );
  });

  it('There should exist the title of the project', async () => {
    const content = render(component);

    await waitFor(() => {
      const title = content.container.querySelector('h1').textContent;
      expect(title).toEqual(mockedProject.in_progress.basicInformation.projectName);
    });
  });

  it('There should exist a banner with project details', async () => {
    const content = render(component);

    await waitFor(() => {
      const country = content.getByTestId('projectHeroDetails-country');
      const countryValue = country.querySelector('h3').textContent;
      expect(countryValue).toEqual(mockedProject.in_progress.basicInformation.location);

      const timeframe = content.getByTestId('projectHeroDetails-timeframe');
      const timeframeValue = timeframe.querySelector('h3').textContent;
      expect(timeframeValue).toEqual('12.0 months');

      const budget = content.getByTestId('projectHeroDetails-budget');
      const budgetValue = budget.querySelector('h3').textContent;
      expect(budgetValue).toEqual('USDT 200.00000000');

      const beneficiary = content.getByTestId('projectHeroDetails-beneficiary');
      const beneficiaryValue = beneficiary.querySelector('h3').textContent;
      expect(beneficiaryValue).toEqual('Firstname Beneficiary');

      const projectVersion = content.getByTestId('projectHeroDetails-projectVersion');
      const projectVersionValue = projectVersion.querySelector('h3').textContent;
      expect(projectVersionValue).toEqual('1');
    });
  });

  it('Should contain the project type and account information', async () => {
    const content = render(component);
    await waitFor(() => {
      const projectType = content.getByText(new RegExp(mockedProject.in_progress.type, 'i'));
      expect(projectType).toBeInTheDocument();

      const accountInformation = content.getByText(
        mockedProject.in_progress.details.additionalCurrencyInformation
      );
      expect(accountInformation).toBeInTheDocument();
    });
  });

  it('There should exist a card for each project member with their names and roles', async () => {
    const content = render(component);
    await waitFor(() => {
      const projectMemberCard = content.getAllByTestId('project-member-card');

      const firstMemberCard = projectMemberCard[0];
      const firstMemberName = firstMemberCard.querySelector('h5').textContent;
      const firstMemberRole = firstMemberCard.querySelector('h6').textContent;
      const firstMockedMember = mockedProject.in_progress.users[0];
      const firstMockedMemberUser = firstMockedMember.users[0];
      expect(firstMemberName).toEqual(
        `${firstMockedMemberUser.firstName} ${firstMockedMemberUser.lastName}`
      );
      expect(firstMemberRole.toLowerCase()).toEqual(firstMockedMember.roleDescription);

      const secondMemberCard = projectMemberCard[1];
      const secondMemberName = secondMemberCard.querySelector('h5').textContent;
      const secondMemberRole = secondMemberCard.querySelector('h6').textContent;
      const secondMockedMember = mockedProject.in_progress.users[1];
      const secondMockedMemberUser = secondMockedMember.users[0];
      expect(secondMemberName).toEqual(
        `${secondMockedMemberUser.firstName} ${secondMockedMemberUser.lastName}`
      );
      expect(secondMemberRole.toLowerCase()).toEqual(secondMockedMember.roleDescription);

      const thirdMemberCard = projectMemberCard[2];
      const thirdMemberName = thirdMemberCard.querySelector('h5').textContent;
      const thirdMemberRole = thirdMemberCard.querySelector('h6').textContent;
      const thirdMockedMember = mockedProject.in_progress.users[2];
      const thirdMockedMemberUser = thirdMockedMember.users[0];
      expect(thirdMemberName).toEqual(
        `${thirdMockedMemberUser.firstName} ${thirdMockedMemberUser.lastName}`
      );
      expect(thirdMemberRole.toLowerCase()).toEqual(thirdMockedMember.roleDescription);
    });
  });

  it('There should be a card with the project scope and statement', async () => {
    const content = render(component);
    await waitFor(() => {
      const projectProgressCardsContainer = content.getByTestId('project-progress-cards');
      const projectProgressCards = projectProgressCardsContainer.childNodes;
      const projectScopeCard = projectProgressCards[0];
      const projectStatementCard = projectProgressCards[1];
      const projectScopeCardElements = projectScopeCard.childNodes;
      const projectStatementCardElements = projectStatementCard.childNodes;
      const milestonesProgress = projectScopeCardElements[1].textContent;
      const activitiesProgress = projectScopeCardElements[2].textContent;
      expect(milestonesProgress).toContain('Milestones Progress');
      expect(milestonesProgress).toContain('0.00%');
      expect(activitiesProgress).toContain('Activities Progress');
      expect(activitiesProgress).toContain('25.00%');

      const projectStatementBars = projectStatementCardElements[1];
      const projectStatementBarsChildren = projectStatementBars.childNodes;
      const budgetLabel = projectStatementBarsChildren[0].textContent;
      const budgetValue = projectStatementBarsChildren[2].textContent;
      expect(budgetLabel).toEqual('Budget');
      expect(budgetValue).toEqual('USDT 200.00000000');

      const fundingLabel = projectStatementBarsChildren[3].textContent;
      const fundingValue = projectStatementBarsChildren[5].textContent;
      expect(fundingLabel).toEqual('Funding');
      expect(fundingValue).toEqual('USDT 0.00000000');

      const spendingLabel = projectStatementBarsChildren[6].textContent;
      const spendingValue = projectStatementBarsChildren[8].textContent;
      expect(spendingLabel).toEqual('Spending');
      expect(spendingValue).toEqual('USDT 25.00000000');
    });
  });

  it('There should be a section with all the milestones', async () => {
    const content = render(component);
    await waitFor(() => {
      const milestones = content.getAllByTestId('milestone-item');
      expect(milestones.length).toEqual(mockedProject.in_progress.milestones.length);
    });
  });

  it('There should section with the cashflow', async () => {
    const content = render(component);
    await waitFor(() => {
      const cashflowTitle = content.getByText(/Cash flow/i, { selector: 'h2' });

      const cashflowTable = content.getAllByRole('table')[0];

      expect(cashflowTable).toHaveTextContent(/Date/i);
      expect(cashflowTable).toHaveTextContent(/Role/i);
      expect(cashflowTable).toHaveTextContent(/Username/i);
      expect(cashflowTable).toHaveTextContent(/Activity Type/i);
      expect(cashflowTable).toHaveTextContent(/Current Amount/i);
      expect(cashflowTable).toHaveTextContent(/Destination Account/i);

      expect(cashflowTitle).toBeInTheDocument();
    });
  });

  it('There should be a section with the blockchain changelog', async () => {
    const content = render(component);
    await waitFor(() => {
      const cashflowTable = content.getByText(/blockchain changelog/i, { selector: 'h2' });
      expect(cashflowTable).toBeInTheDocument();
    });
  });
});
