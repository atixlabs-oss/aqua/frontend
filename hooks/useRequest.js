/**
 * AGUA Copyright (C) 2023  Atix Labs
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import { useState, useEffect } from 'react';
import api, { getBaseURL } from '../api/api';

export default function useRequest(method, url, body, config) {
  const [data, setData] = useState(undefined);
  const [errors, setErrors] = useState(undefined);
  const [loading, setLoading] = useState(false);

  useEffect(() => {
    const doFetch = async () => {
      setErrors(undefined);
      setLoading(true);

      try {
        const result = await api.request({
          method,
          url: getBaseURL() + url,
          data: body,
          ...config // TODO : can this include the base url?
        });
        setData(result.data);
      } catch (error) {
        setData(undefined);
        setErrors(error);
      } finally {
        setLoading(false);
      }
    };

    doFetch();
  }, [method, url, body, config]);

  return [{ data, errors, loading }, () => {}];
}

export function useGet(url) {
  return useRequest('get', url);
}

export function usePost(url, data, config) {
  return useRequest('post', url, data, config);
}
