#FROM docker.atixlabs.com/node:12.16.0-alpine
FROM node:14.18.1

# Setting working directory. All the path will be relative to WORKDIR
WORKDIR /usr/src/app

# Installing dependencies
COPY package*.json ./
# RUN apk add git
RUN npm ci --force

# Copying source files
COPY . .

# Building app
RUN ls -la
RUN npx browserslist@latest --update-db

EXPOSE 3000

CMD npm run build-decision && npm start
