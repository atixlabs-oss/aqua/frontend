/**
 * AGUA Copyright (C) 2023  Atix Labs
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import i18n from 'i18n';

const { t } = i18n;

export const PROJECT_FORM_NAMES = {
  THUMBNAILS: 'thumbnails',
  DETAILS: 'details',
  PROPOSAL: 'proposal',
  MILESTONES: 'milestones',
  MAIN: 'main'
};

export const CURRENCIES = {
  fiat: [
    {
      value: 'USD',
      label: 'USD'
    },
    {
      value: 'EUR',
      label: 'EUR'
    },
    {
      value: 'CHF',
      label: 'CHF'
    },
    {
      value: 'GBP',
      label: 'GBP'
    }
  ],
  crypto: [
    {
      value: 'RBTC',
      label: 'RBTC'
    },
    {
      value: 'ETH',
      label: 'ETH'
    },
    {
      value: 'USDT',
      label: 'USDT'
    },
    {
      value: 'ETC',
      label: 'ETC'
    },
    {
      value: 'USDC',
      label: 'USDC'
    }
  ]
};

export const TIMEFRAME_UNITS = [
  {
    label: t('general.Weeks'),
    value: 'weeks'
  },
  {
    label: t('general.Months'),
    value: 'months'
  },
  {
    label: t('general.Years'),
    value: 'years'
  }
];

const isProduction = process.env.NEXT_PUBLIC_NODE_ENV === 'production';

export const CRYPTO_CURRENCY_PATH_SCANNER = {
  RBTC: txHash =>
    isProduction
      ? `https://blockscout.com/rsk/mainnet/tx/${txHash}`
      : `https://explorer.testnet.rsk.co/tx/${txHash}`,
  ETH: txHash =>
    isProduction
      ? `https://blockscout.com/eth/mainnet/tx/${txHash}`
      : `https://eth-sepolia.blockscout.com/tx/${txHash}`,
  USDT: txHash =>
    isProduction
      ? `https://etherscan.io/tx/${txHash}`
      : `https://sepolia.etherscan.io/tx/${txHash}`,
  ETC: txHash =>
    isProduction
      ? `https://blockscout.com/etc/mainnet/tx/${txHash}`
      : `https://blockscout.com/etc/mordor/tx/${txHash}`,
  USDC: txHash =>
    isProduction ? `https://etherscan.io/tx/${txHash}` : `https://sepolia.etherscan.io/tx/${txHash}`
};

export const ERROR_TYPES = {
  EMPTY: 'EMPTY',
  ALPHANUMERIC: 'ALPHANUMERIC',
  IMAGE_INVALID: 'IMAGE_INVALID',
  FILE: 'FILE',
  MORE_THAN_1_DECIMAL: 'MORE_THAN_1_DECIMAL',
  NO_ZERO: 'NO_ZERO',
  EMPTY_FILE: 'EMPTY_FILE',
  INVALID_FILE: 'INVALID_FILE',
  INVALID_WALLET_ADDRESS: 'INVALID_WALLET_ADDRESS',
  LESS_THAN_ZERO: 'LESS_THAN_ZERO'
};

export const ERROR_MESSAGES = {
  EMPTY: t('general.validationErrors.Incomplete required fields'),
  ALPHANUMERIC: t('general.validationErrors.Please input an alphanumeric value for this field'),
  IMAGE_INVALID: t(
    'general.validationErrors.The uploaded file does not meet the requirements Check them and try again'
  ),
  FILE: t('general.validationErrors.The file is invalid Review the recommendations and try again'),
  MORE_THAN_1_DECIMAL: t('general.validationErrors.This value cant have more than 1 decimals'),
  NO_ZERO: t('general.validationErrors.This value cant be zero'),
  EMPTY_FILE: t(
    'general.validationErrors.You must upload the required file before continuing_withAsterisk'
  ),
  INVALID_FILE: t(
    'general.validationErrors.The file is invalid Review the recommendations and try again_withAsterisk'
  ),
  INVALID_EMAIL: t('general.validationErrors.The current value is not a valid email'),
  NUMBER: t('general.validationErrors.Input a number value for this field'),
  INVALID_WALLET_ADDRESS: t('general.validationErrors.Input a valid wallet address'),
  LESS_THAN_ZERO: t('general.validationErrors.This value cant be less than zero')
};

export const KB_FACTOR_CONVERTER = 1000;
export const MB_FACTOR_CONVERTER = 1000000;

export const ACCESS_TOKEN_KEY = 'accessToken';
export const USER_KEY = 'user';
export const INACTIVE_USER_KEY = 'isInactiveUser';

export const EDITOR_VARIANT = {
  EDITING_CLONE: 'EDITING_CLONE',
  FIRST_EDITING: 'FIRST_EDITING'
};

export const COOKIES_ACCEPTED_KEY = 'cookies-accepted';
