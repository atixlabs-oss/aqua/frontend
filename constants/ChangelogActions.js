/**
 * AGUA Copyright (C) 2023  Atix Labs
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import { ROLES_IDS_NAMES } from 'components/organisms/AssignProjectUsers/constants';
import { formatLeadWithZero } from 'helpers/formatter';
import React from 'react';

import Link from 'next/link';

export const CHANGELOG_ACTIONS_ENUM = {
  CREATE_PROJECT: 'create_project',
  PUBLISH_PROJECT: 'publish_project',
  SEND_PROJECT_TO_REVIEW: 'send_project_to_review',
  EDIT_PROJECT_BASIC_INFO: 'edit_project_basic_information',
  EDIT_PROJECT_DETAILS: 'edit_project_details',
  ADD_USER_PROJECT: 'add_user_project',
  REMOVE_USER_PROJECT: 'remove_user_project',
  ADD_MILESTONE: 'add_milestone',
  REMOVE_MILESTONE: 'remove_milestone',
  ADD_ACTIVITY: 'add_activity',
  REMOVE_ACTIVITY: 'remove_activity',
  ADD_EVIDENCE: 'add_evidence',
  REJECT_EVIDENCE: 'reject_evidence',
  APPROVE_EVIDENCE: 'approve_evidence',
  REJECT_ACTIVITY: 'reject_activity',
  APPROVE_ACTIVITY: 'approve_activity',
  ACTIVITY_TO_REVIEW: 'activity_to_review',
  CANCEL_REVIEW: 'cancel_review',
  APPROVE_REVIEW: 'approve_review',
  PROJECT_CLONE: 'project_clone',
  UPDATE_MILESTONE: 'update_milestone',
  UPDATE_ACTIVITY: 'update_activity',
  DELETE_EVIDENCE: 'delete_evidence',
  START_REQUEST_TO_CANCEL_PROJECT: 'send_cancel_to_review'
};

const roleTranslation = (t, role) => {
  if (!t) return role;
  if (role === ROLES_IDS_NAMES[1]) return t('roles.beneficiary');
  if (role === ROLES_IDS_NAMES[2]) return t('roles.investor');
  if (role === ROLES_IDS_NAMES[3]) return t('roles.auditor');
  return role;
};

const changelogActions = (changelog, t) => {
  const _role = changelog?.user?.isAdmin ? 'Admin' : changelog?.user?.role;
  const role = roleTranslation(t, _role);
  const user = changelog?.user;
  const userName = `${user?.firstName} ${user?.lastName}`;
  const auditor = changelog?.activity?.auditor;
  const auditorName = `${auditor?.firstName} ${auditor?.lastName}`;
  const projectName = changelog?.project?.projectName;
  const revision = formatLeadWithZero(changelog?.revision);
  const milestoneTitle = changelog?.milestone?.title;
  const activityTitle = changelog?.activity?.title;
  const evidenceTitle = changelog?.evidence?.title;
  const extraData = changelog?.extraData;
  const projectId = changelog?.project?.id;
  const activityId = changelog?.activity?.id;
  const evidenceId = changelog?.evidence?.id;
  const reasonComment = changelog?.extraData?.reason
    ? ` ${t('changelogAction.andCommented')} ${changelog?.extraData?.reason}`
    : '';
  const isCancelledReview = changelog?.extraData?.isCancelRequested;

  const BASIC_INFORMATION_ACTIONS = {
    location: t('changelogAction.location'),
    thumbnailPhotoFile: t('changelogAction.thumbnailPhotoFile'),
    timeframe: t('changelogAction.timeframe'),
    timeframeUnit: t('changelogAction.timeframeUnit'),
    projectName: t('changelogAction.projectName'),
    dataComplete: t('changelogAction.dataComplete')
  };

  const PROJECT_DETAILS_ACTIONS = {
    legalAgreementFile: t('changelogAction.legalAgreementFile'),
    projectProposalFile: t('changelogAction.projectProposalFile'),
    currency: t('changelogAction.currency'),
    problemAddressed: t('changelogAction.problemAddressed'),
    currencyType: t('changelogAction.currencyType'),
    additionalCurrencyInformation: t('changelogAction.additionalCurrencyInformation'),
    mission: t('changelogAction.mission')
  };

  return {
    [CHANGELOG_ACTIONS_ENUM.CREATE_PROJECT]: {
      title: () => (
        <>
          <span className="coaChangelogItem__title --bold">{userName}</span>{' '}
          {t('changelogAction.createdAProject')}
        </>
      ),
      titleText: `${userName} ${t('changelogAction.createdAProject')}`,
      description: () => (
        <>
          <span className="coaChangelogItem__title --highlighted">{userName}</span> - {role} -{' '}
          {t('changelogAction.createdTheProject')}{' '}
          <span className="coaChangelogItem__title --highlighted">{projectName}</span>
        </>
      ),
      descriptionText: `${userName} - ${role} - ${t(
        'changelogAction.createdTheProject'
      )} ${projectName}`
    },
    [CHANGELOG_ACTIONS_ENUM.PUBLISH_PROJECT]: {
      title: () => (
        <>
          <span className="coaChangelogItem__title --bold">{userName}</span>
          {revision !== 1
            ? ` ${t('changelogAction.publishedNewVersion')}`
            : ` ${t('changelogAction.publishedTheProject')}`}
        </>
      ),
      titleText: `${userName} ${
        revision !== 1
          ? ` ${t('changelogAction.publishedNewVersion')}`
          : ` ${t('changelogAction.publishedTheProject')}`
      }
      }`,
      description: () => (
        <>
          <span className="coaChangelogItem__title --highlighted">{userName}</span> - {role} -{' '}
          {t('changelogAction.published')}{' '}
          <span className="coaChangelogItem__title --highlighted">{projectName}</span>{' '}
          {t('changelogAction.projectRevision')}{' '}
          <span className="coaChangelogItem__title --highlighted">REV-{revision}</span>
        </>
      ),
      descriptionText: `${userName} - ${role} - ${t(
        'changelogAction.published'
      )} ${projectName} ${t('changelogAction.projectRevision')} REV-${revision}`
    },
    [CHANGELOG_ACTIONS_ENUM.SEND_PROJECT_TO_REVIEW]: {
      title: () => (
        <>
          <span className="coaChangelogItem__title --bold">{userName}</span>{' '}
          {isCancelledReview
            ? t('changelogAction.sent the project cancellation to review')
            : t('changelogAction.sentTheProjectToReview')}
        </>
      ),
      titleText: `${userName} ${
        isCancelledReview
          ? t('changelogAction.sent the project cancellation to review')
          : t('changelogAction.sentTheProjectToReview')
      }}`,
      description: () => (
        <>
          <span className="coaChangelogItem__title --highlighted">{userName}</span> - {role} -{' '}
          {isCancelledReview
            ? t('changelogAction.sent the cancellation of the project')
            : t('changelogAction.sentTheProject')}{' '}
          <span className="coaChangelogItem__title --highlighted">{projectName}</span>{' '}
          {t('changelogAction.toReview')}
        </>
      ),
      descriptionText: `${userName} - ${role} - ${
        isCancelledReview
          ? t('changelogAction.sent the cancellation of the project')
          : t('changelogAction.sentTheProject')
      } ${projectName} ${t('changelogAction.toReview')}`
    },
    [CHANGELOG_ACTIONS_ENUM.EDIT_PROJECT_BASIC_INFO]: {
      title: () => (
        <>
          <span className="coaChangelogItem__title --bold">{userName}</span>{' '}
          {t('changelogAction.editedTheProjectBasicInfo')}
        </>
      ),
      titleText: `${userName} ${t('changelogAction.editedTheProjectBasicInfo')}`,
      description: () => (
        <>
          <span className="coaChangelogItem__title --highlighted">{userName}</span> - {role} -{' '}
          {BASIC_INFORMATION_ACTIONS[(extraData?.fieldName)]}
        </>
      ),
      descriptionText: `${userName} - ${role} - ${
        BASIC_INFORMATION_ACTIONS[(extraData?.fieldName)]
      }`
    },
    [CHANGELOG_ACTIONS_ENUM.EDIT_PROJECT_DETAILS]: {
      title: () => (
        <>
          <span className="coaChangelogItem__title --bold">{userName}</span>{' '}
          {t('changelogAction.editedTheProjectDetails')}
        </>
      ),
      titleText: `${userName} ${t('changelogAction.editedTheProjectDetails')}`,
      description: () => (
        <>
          <span className="coaChangelogItem__title --highlighted">{userName}</span> - {role} -{' '}
          {PROJECT_DETAILS_ACTIONS[(extraData?.fieldName)]}
        </>
      ),
      descriptionText: `${userName} - ${role} - ${PROJECT_DETAILS_ACTIONS[(extraData?.fieldName)]}`
    },
    [CHANGELOG_ACTIONS_ENUM.ADD_USER_PROJECT]: {
      title: () => (
        <>
          <span className="coaChangelogItem__title --bold">{userName}</span>{' '}
          {t('changelogAction.addedAUserToTheProject')}
        </>
      ),
      titleText: `${userName} ${t('changelogAction.addedAUserToTheProject')}`,
      description: () => (
        <>
          <span className="coaChangelogItem__title --highlighted">{userName}</span> - {role} -{' '}
          {t('changelogAction.addedTo')}{' '}
          <span className="coaChangelogItem__title --highlighted">
            {extraData?.user?.firstName} {extraData?.user?.lastName}
          </span>{' '}
          {t('changelogAction.as')}{' '}
          <span className="coaChangelogItem__title --highlighted">
            {ROLES_IDS_NAMES[(extraData?.role?.id)]}
          </span>{' '}
          {t('changelogAction.ofTheProject')}
        </>
      ),
      descriptionText: `${userName} - ${role} - ${t('changelogAction.addedTo')} ${
        extraData?.user?.firstName
      } ${extraData?.user?.lastName} ${t('changelogAction.as')} ${extraData?.role?.description} ${t(
        'changelogAction.ofTheProject'
      )}`
    },
    [CHANGELOG_ACTIONS_ENUM.REMOVE_USER_PROJECT]: {
      title: () => (
        <>
          <span className="coaChangelogItem__title --bold">{userName}</span>{' '}
          {t('changelogAction.removedAnUserOfTheProject')}
        </>
      ),
      titleText: `${userName} ${t('changelogAction.removedAnUserOfTheProject')}`,
      description: () => (
        <>
          <span className="coaChangelogItem__title --highlighted">{userName}</span> - {role} -{' '}
          {t('changelogAction.removed')}{' '}
          <span className="coaChangelogItem__title --highlighted">
            {extraData?.userFirstName} {extraData?.userLastName}
          </span>{' '}
          {t('changelogAction.as')}{' '}
          <span className="coaChangelogItem__title --highlighted">
            {t(`roles.${extraData?.roleDescription}`)}
          </span>{' '}
          {t('changelogAction.ofTheProject')}
        </>
      ),
      descriptionText: `${userName} - ${role} - ${t('changelogAction.removed')} ${
        extraData?.user?.firstName
      } ${extraData?.user?.lastName} ${t('changelogAction.as')} ${extraData?.role?.description} ${t(
        'changelogAction.ofTheProject'
      )}`
    },
    [CHANGELOG_ACTIONS_ENUM.ADD_MILESTONE]: {
      title: () => (
        <>
          <span className="coaChangelogItem__title --bold">{userName}</span>{' '}
          {t('changelogAction.createdAMilestone')}
        </>
      ),
      titleText: `${userName} ${t('changelogAction.createdAMilestone')}`,
      description: () => (
        <>
          <span className="coaChangelogItem__title --highlighted">{userName}</span> - {role} -{' '}
          {t('changelogAction.createdTheMilestone')}{' '}
          <span className="coaChangelogItem__title --highlighted">{milestoneTitle}</span>
        </>
      ),
      descriptionText: `${userName} - ${role} - ${t(
        'changelogAction.createdTheMilestone'
      )} ${milestoneTitle}`
    },
    [CHANGELOG_ACTIONS_ENUM.REMOVE_MILESTONE]: {
      title: () => (
        <>
          <span className="coaChangelogItem__title --bold">{userName}</span>{' '}
          {t('changelogAction.removedAMilestone')}
        </>
      ),
      titleText: `${userName} ${t('changelogAction.removedAMilestone')}`,
      description: () => (
        <>
          <span className="coaChangelogItem__title --highlighted">{userName}</span> - {role} -{' '}
          {t('changelogAction.removedTheMilestone')}{' '}
          <span className="coaChangelogItem__title --highlighted">{milestoneTitle}</span>
        </>
      ),
      descriptionText: `${userName} - ${role} - ${t(
        'changelogAction.removedTheMilestone'
      )} ${milestoneTitle}`
    },
    [CHANGELOG_ACTIONS_ENUM.ADD_ACTIVITY]: {
      title: () => (
        <>
          <span className="coaChangelogItem__title --bold">{userName}</span>{' '}
          {t('changelogAction.addedAnActivity')}
        </>
      ),
      titleText: `${userName} ${t('changelogAction.addedAnActivity')}`,
      description: () => (
        <>
          <span className="coaChangelogItem__title --highlighted">{userName}</span> - {role} -{' '}
          {t('changelogAction.createdTheActivity')}{' '}
          <Link
            href={`/${projectId}/activity/${activityId}/evidences`}
            className="coaChangelogItem__title --highlighted"
          >
            {activityTitle}
          </Link>{' '}
          {t('changelogAction.inTheMilestone')}{' '}
          <span className="coaChangelogItem__title --highlighted">{milestoneTitle}</span>
        </>
      ),
      descriptionText: `${userName} - ${role} - ${t(
        'changelogAction.createdTheActivity'
      )} ${activityTitle} ${t('changelogAction.inTheMilestone')} ${milestoneTitle}`
    },
    [CHANGELOG_ACTIONS_ENUM.REMOVE_ACTIVITY]: {
      title: () => (
        <>
          <span className="coaChangelogItem__title --bold">{userName}</span>{' '}
          {t('changelogAction.removedAnActivity')}
        </>
      ),
      titleText: `${userName} ${t('changelogAction.removedAnActivity')}`,
      description: () => (
        <>
          <span className="coaChangelogItem__title --highlighted">{userName}</span> - {role} -{' '}
          {t('changelogAction.removedTheActivity')}{' '}
          <span className="coaChangelogItem__title --highlighted">{activityTitle}</span> of the{' '}
          <span className="coaChangelogItem__title --highlighted">{milestoneTitle}</span> Milestone
        </>
      ),
      descriptionText: `${userName} - ${role} - ${t(
        'changelogAction.removedTheActivity'
      )} ${activityTitle} ${t('changelogAction.inTheMilestone')} ${milestoneTitle}`
    },
    [CHANGELOG_ACTIONS_ENUM.ADD_EVIDENCE]: {
      title: () => (
        <>
          <span className="coaChangelogItem__title --bold">{userName}</span> {t('general.uploaded')}{' '}
          {t('changelogAction.aNewEvidence')}
        </>
      ),
      titleText: `${userName} ${t('general.uploaded')} ${t('changelogAction.aNewEvidence')}`,
      description: () => (
        <>
          <span className="coaChangelogItem__title --highlighted">{userName}</span> - {role} -{' '}
          {t('general.uploaded')} {t('changelogAction.theEvidence')}{' '}
          <Link
            className="coaChangelogItem__title --highlighted"
            href={`/${projectId}/activity/${activityId}/evidences/${evidenceId}`}
          >
            {evidenceTitle}
          </Link>{' '}
          {t('general.to')} {t('changelogAction.theActivity')}{' '}
          <Link
            className="coaChangelogItem__title --highlighted"
            href={`/${projectId}/activity/${activityId}/evidences`}
          >
            {activityTitle}
          </Link>{' '}
          {t('general.ofThe')} {t('general.milestone')}{' '}
          <span className="coaChangelogItem__title --highlighted">{milestoneTitle}</span>
        </>
      ),
      descriptionText: `${userName} - ${role} - ${t('general.uploaded')} ${t(
        'changelogAction.theEvidence'
      )} ${evidenceTitle} ${t('general.to')} ${t(
        'changelogAction.theActivity'
      )} ${activityTitle} ${t('general.ofThe')} ${t('general.milestone')} ${milestoneTitle}`
    },
    [CHANGELOG_ACTIONS_ENUM.REJECT_EVIDENCE]: {
      title: () => (
        <>
          <span className="coaChangelogItem__title --bold">{userName}</span> {t('general.rejected')}{' '}
          {t('changelogAction.anEvidence')}
        </>
      ),
      titleText: `${userName} ${t('general.rejected')} ${t('changelogAction.anEvidence')}`,
      description: () => (
        <>
          <span className="coaChangelogItem__title --highlighted">{userName}</span> - {role} -{' '}
          {t('general.rejected')} {t('changelogAction.theEvidence')}{' '}
          <Link
            className="coaChangelogItem__title --highlighted"
            href={`/${projectId}/activity/${activityId}/evidences/${evidenceId}`}
          >
            {evidenceTitle}
          </Link>{' '}
          {t('general.of')} {t('changelogAction.theActivity')}{' '}
          <Link
            className="coaChangelogItem__title --highlighted"
            href={`/${projectId}/activity/${activityId}/evidences`}
          >
            {activityTitle}
          </Link>
          {reasonComment}
        </>
      ),
      descriptionText: `${userName} - ${role} - ${t('general.rejected')} ${t(
        'changelogAction.theEvidence'
      )} ${evidenceTitle} ${t('general.of')} ${t(
        'changelogAction.theActivity'
      )} ${activityTitle} ${reasonComment}`
    },
    [CHANGELOG_ACTIONS_ENUM.APPROVE_EVIDENCE]: {
      title: () => (
        <>
          <span className="coaChangelogItem__title --bold">{userName}</span> {t('general.approved')}{' '}
          {t('changelogAction.anEvidence')}
        </>
      ),
      titleText: `${userName} ${t('general.approved')} ${t('changelogAction.anEvidence')}`,
      description: () => (
        <>
          <span className="coaChangelogItem__title --highlighted">{userName}</span> - {role} -{' '}
          {t('general.approved')} {t('changelogAction.theEvidence')}{' '}
          <Link
            className="coaChangelogItem__title --highlighted"
            href={`/${projectId}/activity/${activityId}/evidences/${evidenceId}`}
          >
            {evidenceTitle}
          </Link>{' '}
          {t('general.of')} {t('changelogAction.theActivity')}{' '}
          <Link
            className="coaChangelogItem__title --highlighted"
            href={`/${projectId}/activity/${activityId}/evidences`}
          >
            {activityTitle}
          </Link>
        </>
      ),
      descriptionText: `${userName} - ${role} - ${t('general.approved')} ${t(
        'changelogAction.theEvidence'
      )} ${evidenceTitle} ${t('general.of')} ${t('changelogAction.theActivity')} ${activityTitle}`
    },
    [CHANGELOG_ACTIONS_ENUM.REJECT_ACTIVITY]: {
      title: () => (
        <>
          <span className="coaChangelogItem__title --bold">{userName}</span> {t('general.rejected')}{' '}
          {t('changelogAction.anActivity')}
        </>
      ),
      titleText: `${userName} ${t('general.rejected')} ${t('changelogAction.anActivity')}`,
      description: () => (
        <>
          <span className="coaChangelogItem__title --highlighted">{userName}</span> - {role} -{' '}
          {t('general.rejected')} {t('changelogAction.theActivity')}{' '}
          <Link
            className="coaChangelogItem__title --highlighted"
            href={`/${projectId}/activity/${activityId}/evidences`}
          >
            {activityTitle}
          </Link>{' '}
          {t('general.ofThe')} {t('general.milestone')}{' '}
          <span className="coaChangelogItem__title --highlighted">{milestoneTitle}</span>
        </>
      ),
      descriptionText: `${userName} - ${role} - ${t('general.rejected')} ${t(
        'changelogAction.theActivity'
      )} ${activityTitle} ${t('general.ofThe')} ${t('general.milestone')} ${milestoneTitle}`
    },
    [CHANGELOG_ACTIONS_ENUM.APPROVE_ACTIVITY]: {
      title: () => (
        <>
          <span className="coaChangelogItem__title --bold">{userName}</span> {t('general.approved')}{' '}
          {t('changelogAction.anActivity')}
        </>
      ),
      titleText: `${userName} ${t('general.approved')} ${t('changelogAction.anActivity')}`,
      description: () => (
        <>
          <span className="coaChangelogItem__title --highlighted">{userName}</span> - {role} -{' '}
          {t('general.approved')} {t('changelogAction.theActivity')}{' '}
          <Link
            className="coaChangelogItem__title --highlighted"
            href={`/${projectId}/activity/${activityId}/evidences`}
          >
            {activityTitle}
          </Link>{' '}
          {t('general.ofThe')} {t('general.milestone')}{' '}
          <span className="coaChangelogItem__title --highlighted">{milestoneTitle}</span>
        </>
      ),
      descriptionText: `${userName} - ${role} - ${t('general.approved')} ${t(
        'changelogAction.theActivity'
      )} ${activityTitle} ${t('general.ofThe')} ${t('general.milestone')} ${milestoneTitle}`
    },
    [CHANGELOG_ACTIONS_ENUM.ACTIVITY_TO_REVIEW]: {
      title: () => (
        <>
          <span className="coaChangelogItem__title --bold">{userName}</span> {t('general.sent')}{' '}
          {t('changelogAction.anActivity')} {t('changelogAction.toReview')}
        </>
      ),
      titleText: `${userName} ${t('general.sent')} ${t('changelogAction.anActivity')} ${t(
        'changelogAction.toReview'
      )}`,
      description: () => (
        <>
          <span className="coaChangelogItem__title --highlighted">{userName}</span> - {role} -{' '}
          {t('general.sent')} {t('changelogAction.theActivity')}{' '}
          <Link
            className="coaChangelogItem__title --highlighted"
            href={`/${projectId}/activity/${activityId}/evidences`}
          >
            {activityTitle}
          </Link>{' '}
          {t('general.ofThe')} {t('general.milestone')}{' '}
          <span className="coaChangelogItem__title --highlighted">{milestoneTitle}</span>{' '}
          {t('changelogAction.toBeReviewedBy')}{' '}
          <span className="coaChangelogItem__title --highlighted">{auditorName}</span> auditor
        </>
      ),
      descriptionText: `${userName} - ${role} - ${t('general.sent')} ${t(
        'changelogAction.theActivity'
      )} ${activityTitle} ${t('general.ofThe')} ${t('general.milestone')} ${milestoneTitle} ${t(
        'changelogAction.toBeReviewedBy'
      )} ${auditorName} auditor`
    },
    [CHANGELOG_ACTIONS_ENUM.CANCEL_REVIEW]: {
      title: () => (
        <>
          <span className="coaChangelogItem__title --bold">{userName}</span>{' '}
          {isCancelledReview
            ? t('changelogAction.rejected the project cancellation')
            : `${t('general.canceled')} ${t('changelogAction.aRevision')}`}
        </>
      ),
      titleText: `${userName} ${
        isCancelledReview
          ? t('changelogAction.rejected the project cancellation')
          : `${t('general.canceled')} ${t('changelogAction.aRevision')}`
      }`,
      description: () => (
        <>
          <span className="coaChangelogItem__title --highlighted">{userName}</span> - {role} -{' '}
          {isCancelledReview
            ? t(
                'changelogAction.rejected the project cancellation and was created the new revision'
              )
            : `${t('general.canceled')} ${t('changelogAction.theRevision')}`}{' '}
          <span className="coaChangelogItem__title --highlighted">REV-{revision}</span>
          {reasonComment}
        </>
      ),
      descriptionText: `${userName} - ${role} - ${
        isCancelledReview
          ? t('changelogAction.rejected the project cancellation and was created the new revision')
          : `${t('general.canceled')} ${t('changelogAction.theRevision')}`
      } REV-${revision}${reasonComment}`
    },
    [CHANGELOG_ACTIONS_ENUM.APPROVE_REVIEW]: {
      title: () => (
        <>
          <span className="coaChangelogItem__title --bold">{userName}</span> {t('general.approved')}{' '}
          {isCancelledReview
            ? t('changelogAction.the project cancellation')
            : t('changelogAction.aRevision')}
        </>
      ),
      titleText: `${userName} ${t('general.approved')} ${
        isCancelledReview
          ? t('changelogAction.the project cancellation')
          : t('changelogAction.aRevision')
      }`,
      description: () => (
        <>
          <span className="coaChangelogItem__title --highlighted">{userName}</span> - {role} -{' '}
          {t('general.approved')}{' '}
          {isCancelledReview
            ? t('changelogAction.the project cancellation and was created the new revision')
            : t('changelogAction.theRevision')}{' '}
          <span className="coaChangelogItem__title --highlighted">REV-{revision}</span>
        </>
      ),
      descriptionText: `${userName} - ${role} - ${t('general.approved')} ${
        isCancelledReview
          ? t('changelogAction.the project cancellation and was created the new revision')
          : t('changelogAction.theRevision')
      }  REV-${revision}`
    },
    [CHANGELOG_ACTIONS_ENUM.PROJECT_CLONE]: {
      title: () => (
        <>
          <span className="coaChangelogItem__title --bold">{userName}</span> {t('general.created')}{' '}
          {t('changelogAction.aNewRevision')}
        </>
      ),
      titleText: `${userName} ${t('general.created')} ${t('changelogAction.aNewRevision')}`,
      description: () => (
        <>
          <span className="coaChangelogItem__title --highlighted">{userName}</span> - {role} -{' '}
          {t('general.created')} {t('changelogAction.theRevision')}{' '}
          <span className="coaChangelogItem__title --highlighted">REV-{revision}</span>
        </>
      ),
      descriptionText: `${userName} - ${role} - ${t('general.created')} ${t(
        'changelogAction.theRevision'
      )} REV-${revision}`
    },
    [CHANGELOG_ACTIONS_ENUM.UPDATE_ACTIVITY]: {
      title: () => (
        <>
          <span className="coaChangelogItem__title --bold">{userName}</span> edited an activity
        </>
      ),
      titleText: `${userName} edited an activity`,
      description: () => (
        <>
          <span className="coaChangelogItem__title --highlighted">{userName}</span> - {role} -{' '}
          edited the activity{' '}
          <span className="coaChangelogItem__title --highlighted">{activityTitle}</span>
        </>
      ),
      descriptionText: `${userName} - ${role} - edited the activity ${activityTitle}`
    },
    [CHANGELOG_ACTIONS_ENUM.UPDATE_MILESTONE]: {
      title: () => (
        <>
          <span className="coaChangelogItem__title --bold">{userName}</span> edited a milestone
        </>
      ),
      titleText: `${userName} edited a milestone`,
      description: () => (
        <>
          <span className="coaChangelogItem__title --highlighted">{userName}</span> - {role} -{' '}
          edited the milestone{' '}
          <span className="coaChangelogItem__title --highlighted">{milestoneTitle}</span>
        </>
      ),
      descriptionText: `${userName} - ${role} - edited the milestone ${milestoneTitle}`
    },
    [CHANGELOG_ACTIONS_ENUM.DELETE_EVIDENCE]: {
      title: () => (
        <>
          <span className="coaChangelogItem__title --bold">{userName}</span>{' '}
          {t('changelogAction.deletedAnEvidence')}
        </>
      ),
      titleText: `${userName} ${t('changelogAction.deletedAnEvidence')}`,
      description: () => (
        <>
          <span className="coaChangelogItem__title --highlighted">{userName}</span> - {role} -{' '}
          {t('changelogAction.deletedTheEvidence')}{' '}
          <Link
            className="coaChangelogItem__title --highlighted"
            href={`/${projectId}/activity/${activityId}/evidences/${evidenceId}`}
          >
            {evidenceTitle}
          </Link>{' '}
          {t('changelogAction.of')} {t('changelogAction.theActivity')}{' '}
          <Link
            className="coaChangelogItem__title --highlighted"
            href={`/${projectId}/activity/${activityId}/evidences`}
          >
            {activityTitle}
          </Link>{' '}
          {t('changelogAction.thatBelongsTo')} {t('changelogAction.theMilestone')}{' '}
          <span className="coaChangelogItem__title --highlighted">{milestoneTitle}</span>
        </>
      ),
      descriptionText: `${userName} - ${role} - ${t(
        'changelogAction.deletedTheEvidence'
      )} ${evidenceTitle} ${t('changelogAction.of')} ${t(
        'changelogAction.theActivity'
      )} ${activityTitle} ${t('changelogAction.thatBelongsTo')} ${t(
        'changelogAction.theMilestone'
      )} ${milestoneTitle}`
    },
    [CHANGELOG_ACTIONS_ENUM.START_REQUEST_TO_CANCEL_PROJECT]: {
      title: () => (
        <>
          <span className="coaChangelogItem__title --bold">{userName}</span>{' '}
          {t('changelogAction.hasStartedTheRequestTo')}{' '}
          <span className="coaChangelogItem__title --bold">{t('general.actions.cancel')}</span>{' '}
          {t('changelogAction.theProject')}
        </>
      ),
      titleText: `${userName} ${t('changelogAction.hasStartedTheRequestToCancelTheProject')}`,
      description: () => (
        <>
          <span className="coaChangelogItem__title --highlighted">{userName}</span> - {role} -{' '}
          {t('changelogAction.hasStartedTheRequestToCancelTheProject')}{' '}
          <span className="coaChangelogItem__title --highlighted">{projectName}</span>
        </>
      ),
      descriptionText: `${userName} - ${role} - ${t(
        'changelogAction.hasStartedTheRequestToCancelTheProject'
      )} ${projectName}`
    }
  };
};

export default changelogActions;
