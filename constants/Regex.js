/**
 * AGUA Copyright (C) 2023  Atix Labs
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

export const onlyAlphanumerics = /^[ A-Za-z0-9\r\n]*$/;
export const VALID_EMAIL_REGEX = /^([a-zA-Z0-9_\-.+!#$%&'*\/=?^`{|}~]+)@([a-zA-Z0-9_\-.]+)\.([a-zA-Z]{2,5})$/;
export const ONLY_NUMBERS = /^[1-9]\d*(\.\d+)?$/;

export const ETH_WALLET_ADDRESS = /^0x[a-fA-F0-9]{40}$/;
export const USDT_WALLET_ADDRESS = /^(0x)?[0-9a-fA-F]{40}$/;
export const ETC_WALLET_ADDRESS = /^0x[a-fA-F0-9]{40}$/;
export const RBTC_WALLET_ADDRESS = /^(1|3)[a-zA-Z0-9]{25,34}$/;
