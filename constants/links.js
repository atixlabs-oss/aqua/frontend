/**
 * AGUA Copyright (C) 2023  Atix Labs
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

const BLOCKCHAIN_ENUM = {
  ETHEREUM: 'ETHEREUM',
  RSK: 'RSK'
};

const NETWORK_ENUM = {
  MAINNET: 'MAINNET',
  TESTNET: 'TESTNET'
};

export const links = {
  address: _address => {
    if (
      process.env.NEXT_PUBLIC_BLOCKCHAIN === BLOCKCHAIN_ENUM.ETHEREUM &&
      process.env.NEXT_PUBLIC_NETWORK === NETWORK_ENUM.MAINNET
    ) {
      return `https://etherscan.io/address/${_address}`;
    }
    if (
      process.env.NEXT_PUBLIC_BLOCKCHAIN === BLOCKCHAIN_ENUM.ETHEREUM &&
      process.env.NEXT_PUBLIC_NETWORK === NETWORK_ENUM.TESTNET
    ) {
      return `https://sepolia.etherscan.io/address/${_address}`;
    }
    if (
      process.env.NEXT_PUBLIC_BLOCKCHAIN === BLOCKCHAIN_ENUM.RSK &&
      process.env.NEXT_PUBLIC_NETWORK === NETWORK_ENUM.MAINNET
    ) {
      return `https://explorer.rsk.co/address/${_address}`;
    }
    if (
      process.env.NEXT_PUBLIC_BLOCKCHAIN === BLOCKCHAIN_ENUM.RSK &&
      process.env.NEXT_PUBLIC_NETWORK === NETWORK_ENUM.TESTNET
    ) {
      return `https://explorer.testnet.rsk.co/address/${_address}`;
    }
    throw new Error('Network not supported');
  },
  transaction: () => {
    if (
      process.env.NEXT_PUBLIC_BLOCKCHAIN === BLOCKCHAIN_ENUM.ETHEREUM &&
      process.env.NEXT_PUBLIC_NETWORK === NETWORK_ENUM.MAINNET
    ) {
      return 'https://etherscan.io';
    }
    if (
      process.env.NEXT_PUBLIC_BLOCKCHAIN === BLOCKCHAIN_ENUM.ETHEREUM &&
      process.env.NEXT_PUBLIC_NETWORK === NETWORK_ENUM.TESTNET
    ) {
      return 'https://sepolia.etherscan.io';
    }
    if (
      process.env.NEXT_PUBLIC_BLOCKCHAIN === BLOCKCHAIN_ENUM.RSK &&
      process.env.NEXT_PUBLIC_NETWORK === NETWORK_ENUM.MAINNET
    ) {
      return 'https://explorer.rsk.co';
    }
    if (
      process.env.NEXT_PUBLIC_BLOCKCHAIN === BLOCKCHAIN_ENUM.RSK &&
      process.env.NEXT_PUBLIC_NETWORK === NETWORK_ENUM.TESTNET
    ) {
      return 'https://explorer.testnet.rsk.co';
    }
    throw new Error('Network not supported');
  },
  providers: () => {
    if (
      process.env.NEXT_PUBLIC_BLOCKCHAIN === BLOCKCHAIN_ENUM.ETHEREUM &&
      process.env.NEXT_PUBLIC_NETWORK === NETWORK_ENUM.MAINNET
    ) {
      return ['https://gateway.tenderly.co/public/mainnet'];
    }
    if (
      process.env.NEXT_PUBLIC_BLOCKCHAIN === BLOCKCHAIN_ENUM.ETHEREUM &&
      process.env.NEXT_PUBLIC_NETWORK === NETWORK_ENUM.TESTNET
    ) {
      const privateNodes = [
        process.env.NEXT_PUBLIC_ALCHEMY_PRIVATE_NODE_1,
        process.env.NEXT_PUBLIC_ALCHEMY_PRIVATE_NODE_2
      ];

      const publicNodes = [
        'https://eth-sepolia.g.alchemy.com/v2/bBFtb8a7JXrqvidrg605o56lVeduwHti',
        'https://gateway.tenderly.co/public/sepolia',
        'https://ethereum-sepolia.blockpi.network/v1/rpc/public',
        'https://sepolia.gateway.tenderly.co',
        'https://api.zan.top/node/v1/eth/sepolia/public',
        'https://eth-sepolia-public.unifra.io',
        'https://eth-sepolia.g.alchemy.com/v2/demo',
        'https://eth-sepolia.public.blastapi.io'
      ];

      return process.env.NEXT_PUBLIC_USE_PRIVATE_NODES ? privateNodes : publicNodes;
    }
    if (
      process.env.NEXT_PUBLIC_BLOCKCHAIN === BLOCKCHAIN_ENUM.RSK &&
      process.env.NEXT_PUBLIC_NETWORK === NETWORK_ENUM.MAINNET
    ) {
      return ['https://public-node.testnet.rsk.co'];
    }
    if (
      process.env.NEXT_PUBLIC_BLOCKCHAIN === BLOCKCHAIN_ENUM.RSK &&
      process.env.NEXT_PUBLIC_NETWORK === NETWORK_ENUM.TESTNET
    ) {
      return ['https://public-node.rsk.co'];
    }
    throw new Error('Network not supported');
  }
};
